@extends('layouts.home')
@push('styles')
  <link rel="stylesheet" href="{{asset('new_assets/vendor/select2/dist/css/select2.css')}}">
@endpush
@push('script')
  <script src="{{asset('new_assets/vendor/select2/dist/js/select2.js')}}"></script>
@endpush
@section('content')

<div class="main-content-inner">

  	<div class="page-content">

      <div class="col-md-12">
        <div class="col-sm-6">
          <div class="card bg-white text-center">
            <h4 class="card-header card-title">Grafik Pembelian Obat</h4>
              <select data-placeholder="Your Favorite Football Team" class=" select2" style="width: 200px;">
                <option>PT APOTEK ID</option>
                <option>PT ASIA PACIFIC</option>
              </select>
              <div class="card-block">
                <div style="height: 250px;" height="250" id="chart-pembelian"></div>
              </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card bg-white text-center">
            <h4 class="card-header card-title">Grafik Penjualan Obat</h4>
              <div class="card-block">
                <div style="height: 277px;" height="277" id="chart-penjualan"></div>
              </div>
          </div>
        </div>
      </div>

  		<div class="col-md-12">
        <div class="col-sm-6">
          <div class="card bg-white text-center">
            <h4 class="card-header card-title">Grafik Pembelian Obat</h4>
              <select data-placeholder="Your Favorite Football Team" class=" select2" style="width: 200px;">
                <option>Januari</option>
                <option>Februari</option>
              </select>
              <div class="card-block">
                <div style="height: 250px;" height="250" id="chart-obat"></div>
              </div>
          </div>
        </div>
  		</div>

	</div>
</div>
@stop
@section('js')
<script src="{{asset('assets/js/highcharts.js')}}"></script>
<script src="{{asset('assets/js/series-label.js')}}"></script>
<script src="{{asset('assets/js/exporting.js')}}"></script>

<script type="text/javascript">
$(document).ready( function () {

$('.select2').select2();

	function makeChartPembelian(values){
		Highcharts.chart('chart-pembelian', {

		    title: {
		        text: ''
		    },
		    exporting: {
		    	enabled: false
		    },
		    credits: {
		      	enabled: false
		  	},
		  	yAxis: {
		        title: {
		            text: 'Rupiah'
		        }
		    },
		    xAxis: {
        		categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        	},
        	tooltip: {
			    formatter: function() {
			    	var number = this.y;
			        return this.x + ' : ' + 'Rp ' + number.toLocaleString('id-ID');
			    }
			},
		    series: [{
		        name: 'Total Pembelian',
		        data: values
		    }],

		});
	}

	function makeChartPenjualan(values){
		Highcharts.chart('chart-penjualan', {

		    title: {
		        text: ''
		    },
		    exporting: {
		    	enabled: false
		    },
		    credits: {
		      	enabled: false
		  	},
		  	yAxis: {
		        title: {
		            text: 'Rupiah'
		        }
		    },
		    xAxis: {
        		categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        	},
        	tooltip: {
			    formatter: function() {
			    	var number = this.y;
			        return this.x + ' : ' + 'Rp ' + number.toLocaleString('id-ID');
			    }
			},
		    series: [{
		        name: 'Total',
		        data: values
		    }],

		});
	}

	function makeChartObatTerlaris(obat, terjual){
		Highcharts.chart('chart-obat', {

		    title: {
		        text: ''
		    },
		    exporting: {
		    	enabled: false
		    },
		    credits: {
		      	enabled: false
		  	},
		  	yAxis: {
		        title: {
		            text: 'Total Terjual'
		        }
		    },
		    xAxis: {
        		categories: obat
        	},
        	tooltip: {
			    formatter: function() {
			    	var number = this.y;
			        return this.x + ' : ' + number.toLocaleString('id-ID');
			    }
			},
		    series: [{
		        name: 'Obat',
		        data: terjual
		    }],

		});
	}

	function getDataPembelian(){
		var currentTime = new Date();
		var year = currentTime.getFullYear();

		$.ajax({
	        type : 'get',
	        url : "{{url('get-data-pembelian')}}",
		    success: function (data) {
		    	//console.log(data);
		    	var values = [];
		    	for (i = 0; i < 12; i++) {

				    $.each(data, function(index, value){
				    	if(i+1 == value.bulan){
				    		values.push(value.total_pembelian);
				    	}else{
				    		values.push(0);
				    	}
		    		});

				}
				//console.log(values[0]);
				makeChartPembelian(values);

		    }
	    });
	}

	function getDataPenjualan(){
		var currentTime = new Date();
		var year = currentTime.getFullYear();

		$.ajax({
	        type : 'get',
	        url : "{{url('get-data-penjualan')}}",
		    success: function (data) {
		    	//console.log(data);
		    	var values = [];
		    	for (i = 0; i < 12; i++) {

				    $.each(data, function(index, value){
				    	if(i+1 == value.bulan){
				    		values.push(value.total_penjualan);
				    	}else{
				    		values.push(0);
				    	}
		    		});

				}
				makeChartPenjualan(values);

		    }
	    });
	}

	function getDataObatTerlaris(){
		var currentTime = new Date();
		var year = currentTime.getFullYear();

		$.ajax({
	        type : 'get',
	        url : "{{url('get-data-obat-terlaris')}}",
		    success: function (data) {
		    	//console.log(data);
		    	var obat = [];
		    	var terjual = [];
			    $.each(data, function(index, value){
			    		obat.push(value.nama_obat);
			    		terjual.push(parseInt(value.jumlah_obat));
	    		});
				makeChartObatTerlaris(obat, terjual);

		    }
	    });
	}

	getDataPembelian();
	getDataPenjualan();
	getDataObatTerlaris();

});
</script>
@stop
