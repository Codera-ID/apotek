@extends('layouts.app')

@section('content')
<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<i class="ace-icon fa fa-leaf green"></i>
									<span class="red">Apotek</span>
									<span class="white" id="id-text2">Application</span>
								</h1>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-coffee green"></i>
												Please Enter Your Information
											</h4>

											<div class="space-6"></div>
                                            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                                {{ csrf_field() }}
												<fieldset>
                                                    <label class="block clearfix">
                                                        <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                                        
                                                            <span class="block input-icon input-icon-right">
                                                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" />
                                                                <i class="ace-icon fa fa-envelope"></i>
                                                            </span>

                                                            @if ($errors->has('email'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('email') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </label>

													<label class="block clearfix">
                                                        <div class="{{ $errors->has('password') ? ' has-error' : '' }}">    
                                                            <span class="block input-icon input-icon-right">
                                                                <input type="password" class="form-control"  name="password" placeholder="Password" />
                                                                <i class="ace-icon fa fa-lock"></i>
                                                            </span>

                                                            @if ($errors->has('password'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('password') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
													</label>

													<div class="space"></div>

													<div class="clearfix">
														<label class="inline">
															<input type="checkbox" class="ace" name="remember" {{ old('remember') ? 'checked' : '' }} />
															<span class="lbl"> Remember Me</span>
														</label>

														<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Login</span>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>
										</div><!-- /.widget-main -->

										<div class="toolbar clearfix">
											<div>
												<a href="{{ route('register') }}" data-target="#signup-box" class="user-signup-link">
													Register
													<i class="ace-icon fa fa-arrow-right"></i>
												</a>
											</div>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->
    </body>
@endsection
