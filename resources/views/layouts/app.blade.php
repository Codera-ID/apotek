<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Login - Admin Apotek</title>

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" />
		<link rel="stylesheet" href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" />
		@yield('css')
		<!-- ace styles -->
		<link rel="stylesheet" href="{{asset('css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />
		<link rel="stylesheet" href="{{asset('css/style.css')}}" class="ace-main-stylesheet" />

	</head>
</head>
<body>
    <div id="app">
        @yield('content')
    </div>

    <script src="{{asset('vendors/jquery/dist/jquery-2.1.4.min.js')}}"></script>
    <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/ace-elements.min.js')}}"></script>
    <script src="{{asset('js/ace.min.js')}}"></script>

    @yield('js')

</body>
<footer>
	@yield('footer')
</footer>
</html>
