<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Dashboard - Admin Apotek</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="{{asset('new_assets/styles/webfont.css')}}">
  <link rel="stylesheet" href="{{asset('new_assets/styles/climacons-font.css')}}">
  <link rel="stylesheet" href="{{asset('new_assets/vendor/bootstrap/dist/css/bootstrap.css')}}">
  <link rel="stylesheet" href="{{asset('new_assets/styles/font-awesome.css')}}">
	@yield('css')
  <link rel="stylesheet" href="{{asset('new_assets/styles/card.css')}}">
  <link rel="stylesheet" href="{{asset('new_assets/styles/sli.css')}}">
  <link rel="stylesheet" href="{{asset('new_assets/styles/animate.css')}}">
  <link rel="stylesheet" href="{{asset('new_assets/styles/app.css')}}">
  <link rel="stylesheet" href="{{asset('new_assets/styles/app.skins.css')}}">
	@stack('styles')

  <!-- endbuild -->
</head>

<body class="page-loading">
  <!-- page loading spinner -->
  <div class="pageload">
    <div class="pageload-inner">
      <div class="sk-rotating-plane"></div>
    </div>
  </div>
  <!-- /page loading spinner -->
  <div class="app layout-fixed-header">
    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">
      <div class="brand">
        <!-- /toggle small sidebar menu -->
        <!-- toggle offscreen menu -->
        <div class="toggle-offscreen">
          <a href="javascript:;" class="visible-xs hamburger-icon" data-toggle="offscreen" data-move="ltr">
            <span></span>
            <span></span>
            <span></span>
          </a>
        </div>
        <!-- /toggle offscreen menu -->
        <!-- logo -->
        <a class="brand-logo">
          <span>APOTEK</span>
        </a>
        <a href="#" class="small-menu-visible brand-logo">A</a>
        <!-- /logo -->
      </div>

      <!-- main navigation -->
      <nav role="navigation">
        <ul class="nav">
          <!-- dashboard -->
          <li class="{!! Request::is('home') ? 'open' : '' !!}">
            <a href="{{url('home')}}">
              <i class="fa fa-home"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <!-- /dashboard -->

          <li class="{!! Request::is('dokter') || Request::is('dokter/*') ? 'open menu-accordion' : '' !!}">
            <a href="javascript:;">
              <i class="fa fa-plus"></i>
              <span>Master Klinik</span>
            </a>
            <ul class="sub-menu">
							<li class="{!! Request::is('dokter') || Request::is('dokter/*') ? 'open' : '' !!}">
								<a href="{{url('dokter')}}">
									<i class="menu-icon fa fa-caret-right"></i> Data Dokter</a>
								<b class="arrow"></b>
							</li>
            </ul>
          </li>

					<li class="{!!  Request::is('kat-pasien') || Request::is('kat-pasien/*') || Request::is('pabrik') || Request::is('pabrik/*') || Request::is('supplier') || Request::is('supplier/*') || Request::is('gudang') ||  Request::is('gudang/*') || Request::is('golongan-obat') ||  Request::is('golongan-obat/*') || Request::is('kategori-obat') || Request::is('kategori-obat/*') || Request::is('lokasi-obat') || Request::is('lokasi-obat/*') || Request::is('satuan-obat') ||  Request::is('satuan-obat/*') || Request::is('obat') || Request::is('obat/*') || Request::is('racikan') || Request::is('racikan/*') || Request::is('apoteker') || Request::is('apoteker/*') || Request::is('pasien') || Request::is('pasien/*') ? 'open menu-accordion' : '' !!}">
						<a href="javascript:;">
							<i class="fa fa-flask"></i>
							<span>Master Apotek</span>
						</a>
						<ul class="sub-menu">
							<li class="{!! Request::is('pabrik') || Request::is('pabrik/*') ? 'open' : '' !!}">
							      <a href="{{url('pabrik')}}"><i class="menu-icon fa fa-caret-right"></i> Data Pabrik</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('supplier') || Request::is('supplier/*') ? 'open' : '' !!}">
							      <a href="{{url('supplier')}}"><i class="menu-icon fa fa-caret-right"></i> Data Supplier</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('gudang') || Request::is('gudang/*') ? 'open' : '' !!}">
							      <a href="{{url('gudang')}}"><i class="menu-icon fa fa-caret-right"></i> Data Gudang</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('golongan-obat') || Request::is('golongan-obat/*') ? 'open' : '' !!}">
							      <a href="{{url('golongan-obat')}}"><i class="menu-icon fa fa-caret-right"></i> Data Golongan Obat</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('kategori-obat') || Request::is('kategori-obat/*') ? 'open' : '' !!}">
							      <a href="{{url('kategori-obat')}}"><i class="menu-icon fa fa-caret-right"></i> Data Kategori Obat</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('lokasi-obat') || Request::is('lokasi-obat/*') ? 'open' : '' !!}">
							      <a href="{{url('lokasi-obat')}}"><i class="menu-icon fa fa-caret-right"></i> Data Lokasi Obat</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('satuan-obat') || Request::is('satuan-obat/*') ? 'open' : '' !!}">
							      <a href="{{url('satuan-obat')}}"><i class="menu-icon fa fa-caret-right"></i> Data Satuan Obat</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('obat') || Request::is('obat/*') ? 'open' : '' !!}">
							      <a href="{{url('obat')}}"><i class="menu-icon fa fa-caret-right"></i> Data Obat</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('racikan') || Request::is('racikan/*') ? 'open' : '' !!}">
							      <a href="{{url('racikan')}}"><i class="menu-icon fa fa-caret-right"></i> Data Obat Racikan</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('apoteker') || Request::is('apoteker/*') ? 'open' : '' !!}">
							      <a href="{{url('apoteker')}}"><i class="menu-icon fa fa-caret-right"></i> Data Apoteker</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('pasien') || Request::is('pasien/*') ? 'open' : '' !!}">
							      <a href="{{url('pasien')}}"><i class="menu-icon fa fa-caret-right"></i> Data Pasien</a>
							      <b class="arrow"></b>
							    </li>
						</ul>
					</li>

					<li class="{!! Request::is('retur-penjualan/*') || Request::is('retur-penjualan') || Request::is('penjualan-obat') || Request::is('penjualan-obat/*') || Request::is('retur-pembelian') || Request::is('retur-pembelian/*') || Request::is('po') || Request::is('po/*') || Request::is('po/data-hapus') || Request::is('po/data-hapus/*') || Request::is('pembelian-obat') || Request::is('pembelian-obat/*') || Request::is('mutasi-obat') || Request::is('mutasi-obat/*') || Request::is('mutasi-konsinyasi-obat') || Request::is('mutasi-konsinyasi-obat/*') ? 'open menu-accordion' : '' !!}">
						<a href="javascript:;">
							<i class="fa fa-recycle"></i>
							<span>Transaksi Apotek</span>
						</a>
						<ul class="sub-menu">
							<li class="{!! Request::is('po') || Request::is('po/*') && !Request::is('po/data-hapus') ? 'open' : '' !!}">
							      <a href="{{url('po')}}"><i class="menu-icon fa fa-caret-right"></i> Purchase Order</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('po/data-hapus') ? 'open' : '' !!}">
							      <a href="{{url('po/data-hapus')}}"><i class="menu-icon fa fa-caret-right"></i> Hapus Purchase Order</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('pembelian-obat') || Request::is('pembelian-obat/*') && !Request::is('pembelian-obat/data-hapus') ? 'open' : '' !!}">
							      <a href="{{url('pembelian-obat')}}"><i class="menu-icon fa fa-caret-right"></i> Pembelian Obat</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('pembelian-obat/data-hapus') ? 'open' : '' !!}">
							      <a href="{{url('pembelian-obat/data-hapus')}}"><i class="menu-icon fa fa-caret-right"></i> Hapus Pembelian Obat</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('retur-pembelian') ? 'open' : '' !!}">
							      <a href="{{url('retur-pembelian')}}"><i class="menu-icon fa fa-caret-right"></i> Retur Pembelian Obat</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('retur-pembelian/data-hapus') ? 'open' : '' !!}">
							      <a href="{{url('retur-pembelian/data-hapus')}}"><i class="menu-icon fa fa-caret-right"></i> Hapus Retur Pembelian Obat</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('penjualan-obat') ? 'open' : '' !!}">
							      <a href="{{url('penjualan-obat')}}"><i class="menu-icon fa fa-caret-right"></i> Penjualan Obat</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('penjualan-obat/data-hapus') ? 'open' : '' !!}">
							      <a href="{{url('penjualan-obat/data-hapus')}}"><i class="menu-icon fa fa-caret-right"></i> Hapus Penjualan Obat</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('retur-penjualan/tambah') ? 'open' : '' !!}">
							      <a href="{{url('retur-penjualan/tambah')}}"><i class="menu-icon fa fa-caret-right"></i> Retur Penjualan Obat</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('retur-penjualan/data-hapus') ? 'open' : '' !!}">
							      <a href="{{url('retur-penjualan/data-hapus')}}"><i class="menu-icon fa fa-caret-right"></i> Hapus Retur Penjualan Obat</a>
							      <b class="arrow"></b>
							    </li>
							    <li class="{!! Request::is('mutasi-obat') || Request::is('mutasi-obat/*') ? 'open' : '' !!}">
							      <a href="{{url('mutasi-obat')}}"><i class="menu-icon fa fa-caret-right"></i> Mutasi Obat</a>
							      <b class="arrow"></b>
							    </li>
							   <li class="{!! Request::is('mutasi-konsinyasi-obat') || Request::is('mutasi-konsinyasi-obat/*') ? 'open' : '' !!}">
							      <a href="{{url('mutasi-konsinyasi-obat')}}"><i class="menu-icon fa fa-caret-right"></i> Mutasi Konsinyasi Obat</a>
							      <b class="arrow"></b>
							    </li>
						</ul>
					</li>

					<li class="{!! Request::is('salinan-resep') || Request::is('salinan-resep/*') || Request::is('pembayaran-konsinyasi-obat') || Request::is('pembayaran-konsinyasi-obat/*') || Request::is('ubah-harga-obat') || Request::is('ubah-harga-obat/*') || Request::is('salinan-resep') || Request::is('salinan-resep/*') || Request::is('barcode') || Request::is('barcode/*') || Request::is('import-excel') || Request::is('import-excel/*') || Request::is('stok-opname') || Request::is('stok-opname/*') || Request::is('pembayaran-piutang-obat') || Request::is('pembayaran-piutang-obat/*') || Request::is('pembayaran-hutang') || Request::is('pembayaran-hutang/*') || Request::is('pembayaran-konsinya-obat') || Request::is('pembayaran-konsinya-obat/*') ? 'open menu-accordion' : '' !!}">
						<a href="javascript:;">
							<i class="fa fa-star-o"></i>
							<span>Utilitas Apotek</span>
						</a>
						<ul class="sub-menu">
							<li class="{!! Request::is('barcode') || Request::is('barcode/*') ? 'open' : '' !!}">
										<a href="{{url('barcode')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Cetak Barcode
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('import-excel') || Request::is('import-excel/*') ? 'open' : '' !!}">
										<a href="{{url('import-excel')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Import Data Dari Excel
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('ubah-harga-obat') || Request::is('ubah-harga-obat/*') ? 'open' : '' !!}">
										<a href="{{url('ubah-harga-obat')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Ubah Harga Obat
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('stok-opname') || Request::is('stok-opname/*') ? 'open' : '' !!}">
										<a href="{{url('stok-opname')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Stok Opname
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('pembayaran-hutang') || Request::is('pembayaran-hutang/*') ? 'open' : '' !!}">
										<a href="{{url('pembayaran-hutang')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Pembayaran Hutang
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('pembayaran-piutang-obat') || Request::is('pembayaran-piutang-obat/*') ? 'open' : '' !!}">
										<a href="{{url('pembayaran-piutang-obat')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Pembayaran Piutang Obat
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('pembayaran-konsinyasi-obat') || Request::is('pembayaran-konsinyasi-obat/*') ? 'open' : '' !!}">
										<a href="{{url('pembayaran-konsinyasi-obat')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Pembayaran Konsinyasi Obat
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('salinan-resep') || Request::is('salinan-resep/*') ? 'open' : '' !!}">
										<a href="{{url('salinan-resep')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Salinan Resep
										</a>

										<b class="arrow"></b>
									</li>
						</ul>
					</li>

					<li class="{!! Request::is('laporan-data-stok-harga') || Request::is('laporan-data-stok-harga/*')  || Request::is('laporan-data-perubahan-harga') || Request::is('laporan-data-perubahan-harga/*') ||  Request::is('laporan-data-obat-habis') || Request::is('laporan-data-obat-habis/*') || Request::is('laporan-data-obat-expired') || Request::is('laporan-data-obat-expired/*') || Request::is('laporan-data-pasien') || Request::is('laporan-data-pasien/*') || Request::is('laporan-data-apoteker') || Request::is('laporan-data-apoteker/*') || Request::is('laporan-data-gudang') || Request::is('laporan-data-gudang/*') || Request::is('laporan-data-supplier') || Request::is('laporan-data-supplier/*') || Request::is('laporan-data-pabrik') || Request::is('laporan-data-pabrik/*') || Request::is('laporan-data-mutasi-obat') || Request::is('laporan-data-mutasi-obat/*')?   'open menu-accordion' : '' !!}">
						<a href="javascript:;">
							<i class="fa fa-print"></i>
							<span>Laporan Master</span>
						</a>
						<ul class="sub-menu">
							<li class="{!! Request::is('laporan-data-pabrik') || Request::is('laporan-data-pabrik/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-pabrik')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Pabrik
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-supplier') || Request::is('laporan-data-supplier/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-supplier')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Supplier
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-gudang') || Request::is('laporan-data-gudang/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-gudang')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Gudang
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-apoteker') || Request::is('laporan-data-apoteker/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-apoteker')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Apoteker
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-pasien') || Request::is('laporan-data-pasien/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-pasien')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Pasien
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-stok-obat') || Request::is('laporan-data-stok-obat/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-stok-obat')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Stok Obat
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-mutasi-obat') || Request::is('laporan-data-mutasi-obat/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-mutasi-obat')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Mutasi Obat
										</a>

										<b class="arrow"></b>
									</li>
									<li class="{!! Request::is('laporan-data-perubahan-harga') || Request::is('laporan-data-perubahan-harga/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-perubahan-harga')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Perubahan Harga Obat
										</a>

										<b class="arrow"></b>
									</li>
									<li class="{!! Request::is('laporan-data-stok-harga') || Request::is('laporan-data-stok-harga/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-stok-harga')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Stok Dan Data Harga Obat
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-obat-expired') || Request::is('laporan-data-obat-expired/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-obat-expired')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Obat Expired
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-obat-habis') || Request::is('laporan-data-obat-habis/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-obat-habis')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Obat Habis
										</a>

										<b class="arrow"></b>
									</li>
						</ul>
					</li>

					<li class="{!! Request::is('laporan-data-purchase-order') || Request::is('laporan-data-purchase-order/*') || Request::is('laporan-data-pembelian-obat') || Request::is('laporan-data-pembelian-obat/*') || Request::is('laporan-data-retur-pembelian') || Request::is('laporan-data-retur-pembelian/*') || Request::is('laporan-data-penjualan-obat') || Request::is('laporan-data-penjualan-obat/*') || Request::is('laporan-data-retur-penjualan') || Request::is('laporan-data-retur-penjualan/*') || Request::is('laporan-data-statistik-penjualan') || Request::is('laporan-data-statistik-penjualan/*') || Request::is('laporan-data-obat-konsinyasi') || Request::is('laporan-data-obat-konsinyasi/*') ? 'open menu-accordion' : '' !!}">
						<a href="javascript:;">
							<i class="fa fa-pie-chart"></i>
							<span>Laporan Transaksi</span>
						</a>
						<ul class="sub-menu">
							<li class="{!! Request::is('laporan-data-purchase-order') || Request::is('laporan-data-purchase-order/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-purchase-order')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Purchase Order (PO)
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-pembelian-obat') || Request::is('laporan-data-pembelian-obat/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-pembelian-obat')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Pembelian Obat
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-retur-pembelian') || Request::is('laporan-data-retur-pembelian/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-retur-pembelian')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Retur Pembelian Obat
										</a>

										<b class="arrow"></b>
									</li>

									<li class="">
										<a href="wysiwyg.html">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Pembayaran Hutang
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-obat-konsinyasi') || Request::is('laporan-data-obat-konsinyasi/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-obat-konsinyasi')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Obat Konsinyasi
										</a>

										<b class="arrow"></b>
									</li>

									<li class="">
										<a href="dropzone.html">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Pembayaran Konsinyasi
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-penjualan-obat') || Request::is('laporan-data-penjualan-obat/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-penjualan-obat')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Penjualan Obat
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-retur-penjualan') || Request::is('laporan-data-retur-penjualan/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-retur-penjualan')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Retur Penjualan Obat
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-data-statistik-penjualan') || Request::is('laporan-data-statistik-penjualan/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-data-statistik-penjualan')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Statistik Penjualan Obat
										</a>

										<b class="arrow"></b>
									</li>

									<li class="">
										<a href="dropzone.html">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Pembayaran Piutang Obat
										</a>

										<b class="arrow"></b>
									</li>

									<li class="">
										<a href="dropzone.html">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Stok Opname
										</a>

										<b class="arrow"></b>
									</li>
						</ul>
					</li>

					<li class="{!! Request::is('laporan-detail-akun') || Request::is('laporan-detail-akun/*') || Request::is('laporan-tipe-akun') || Request::is('laporan-tipe-akun/*') || Request::is('laporan-neraca') || Request::is('laporan-neraca/*') || Request::is('laporan-keuangan') || Request::is('laporan-keuangan/*') || Request::is('laporan-pajak') || Request::is('laporan-pajak/*') || Request::is('laporan-penambahan-modal') || Request::is('laporan-penambahan-modal/*') || Request::is('laporan-laba') || Request::is('laporan-laba/*') || Request::is('laporan-buku-besar') || Request::is('laporan-buku-besar/*') || Request::is('laporan-jurnal-keuangan') || Request::is('laporan-jurnal-keuangan/*') || Request::is('jurnal-keuangan') || Request::is('jurnal-keuangan/*') || Request::is('jurnal-singkat') || Request::is('jurnal-singkat/*') || Request::is('akun') || Request::is('akun/*')  ? 'open menu-accordion' : '' !!}">
						<a href="javascript:;">
							<i class="fa fa-university"></i>
							<span>Keuangan</span>
						</a>
						<ul class="sub-menu">
							<li class="{!! Request::is('akun') || Request::is('akun/*')  ? 'open' : '' !!}">
										<a href="{{url('akun')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Akun Keuangan
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-keuangan') || Request::is('laporan-keuangan/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-keuangan')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Laporan Keuangan
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('jurnal-singkat') || Request::is('jurnal-singkat/*')  ? 'open' : '' !!}">
										<a href="{{url('jurnal-singkat')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Jurnal Singkat
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('jurnal-keuangan') || Request::is('jurnal-keuangan/*')  ? 'open' : '' !!}">
										<a href="{{url('jurnal-keuangan')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Data Jurnal Keuangan
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-jurnal-keuangan') || Request::is('laporan-jurnal-keuangan/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-jurnal-keuangan')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Laporan Jurnal Keuangan
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-buku-besar') || Request::is('laporan-buku-besar/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-buku-besar')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Laporan Buku Besar
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-neraca') || Request::is('laporan-neraca/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-neraca')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Laporan Neraca Umum
										</a>

										<b class="arrow"></b>
									</li>

									<!-- <li class="{!! Request::is('laporan-tipe-akun') || Request::is('laporan-tipe-akun/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-tipe-akun')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Laporan Tipe Akun
										</a>

										<b class="arrow"></b>
									</li> -->

<!--
									<li class="{!! Request::is('laporan-detail-akun') || Request::is('laporan-detail-akun/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-detail-akun')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Laporan Detail Akun
										</a>

										<b class="arrow"></b>
									</li> -->

									<!-- <li class="{!! Request::is('laporan-laba') || Request::is('laporan-laba/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-laba')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Laporan Laba (Rugi)
										</a>

										<b class="arrow"></b>
									</li> -->

									<li class="{!! Request::is('laporan-penambahan-modal') || Request::is('laporan-penambahan-modal/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-penambahan-modal')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Laporan Penambahan Modal
										</a>

										<b class="arrow"></b>
									</li>

									<li class="{!! Request::is('laporan-pajak') || Request::is('laporan-pajak/*')  ? 'open' : '' !!}">
										<a href="{{url('laporan-pajak')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											Laporan Pajak
										</a>

										<b class="arrow"></b>
									</li>
						</ul>
					</li>

					<li class="{!! Request::is('data-group-user') || Request::is('data-group-user/*') || Request::is('data-jadwal-shift') || Request::is('data-jadwal-shift/*') || Request::is('pergantian-shift') || Request::is('pergantian-shift/*')  || Request::is('data-user') || Request::is('data-user/*') || Request::is('identitas') || Request::is('identitas/*') || Request::is('pengaturan-aplikasi') || Request::is('pengaturan-aplikasi/*' ) ? 'open menu-accordion' : '' !!}">
						<a href="javascript:;">
							<i class="fa fa-gear"></i>
							<span>Sistem</span>
						</a>
						<ul class="sub-menu">
							<li class="{!! Request::is('identitas') || Request::is('identitas/*') ? 'open' : '' !!}">
											<a href="{{url('identitas')}}">
												<i class="menu-icon fa fa-caret-right"></i>
												Identitas
											</a>

											<b class="arrow"></b>
										</li>

										<li class="{!! Request::is('pengaturan-aplikasi') || Request::is('pengaturan-aplikasi/*') ? 'open' : '' !!}">
											<a href="{{url('pengaturan-aplikasi')}}">
												<i class="menu-icon fa fa-caret-right"></i>
												Pengaturan Aplikasi
											</a>

											<b class="arrow"></b>
										</li>

										<li class="{!! Request::is('data-group-user') || Request::is('data-group-user/*') ? 'open' : '' !!}">
											<a href="{{url('data-group-user')}}">
												<i class="menu-icon fa fa-caret-right"></i>
												Data Group User
											</a>

											<b class="arrow"></b>
										</li>

										<li class="{!! Request::is('data-user') || Request::is('data-user/*') ? 'open' : '' !!}">
											<a href="{{url('data-user')}}">
												<i class="menu-icon fa fa-caret-right"></i>
												Data User
											</a>

											<b class="arrow"></b>
										</li>

										<li class="{!! Request::is('data-jadwal-shift') || Request::is('data-jadwal-shift/*') ? 'open' : '' !!}">
											<a href="{{url('data-jadwal-shift')}}">
												<i class="menu-icon fa fa-caret-right"></i>
												Laporan Ganti Shift
											</a>

											<b class="arrow"></b>
										</li>

										<li class="{!! Request::is('data-jadwal-shift') || Request::is('data-jadwal-shift/*') ? 'open' : '' !!}">
											<a href="{{url('data-jadwal-shift')}}">
												<i class="menu-icon fa fa-caret-right"></i>
												Data Jadwal Shift
											</a>

											<b class="arrow"></b>
										</li>

										<li class="{!! Request::is('pergantian-shift') || Request::is('pergantian-shift/*') ? 'open' : '' !!}">
											<a href="{{url('pergantian-shift')}}">
												<i class="menu-icon fa fa-caret-right"></i>
												Pergantian Shift
											</a>

											<b class="arrow"></b>
										</li>
						</ul>
					</li>

        </ul>
      </nav>
      <!-- /main navigation -->
    </div>
    <!-- /sidebar panel -->
    <!-- content panel -->
    <div class="main-panel">
      <!-- top header -->
      <div class="header navbar">
        <div class="brand visible-xs">
          <!-- toggle offscreen menu -->
          <div class="toggle-offscreen">
            <a href="javascript:;" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
              <span></span>
              <span></span>
              <span></span>
            </a>
          </div>
          <!-- /toggle offscreen menu -->
          <!-- logo -->
          <a class="brand-logo">
            <span>REACTOR</span>
          </a>
          <!-- /logo -->
        </div>
        <ul class="nav navbar-nav hidden-xs">
          <li>
            <a href="javascript:;" class="small-sidebar-toggle ripple" data-toggle="layout-small-menu">
              <i class="icon-toggle-sidebar"></i>
            </a>
          </li>
          <li class="searchbox">
            <a href="javascript:;" data-toggle="search">
              <i class="search-close-icon icon-close hide"></i>
              <i class="search-open-icon icon-magnifier"></i>
            </a>
          </li>
          <li class="navbar-form search-form hide">
            <input type="search" class="form-control search-input" placeholder="Start typing...">
            <div class="search-predict hide">
              <a href="#">Searching for 'purple rain'</a>
              <div class="heading">
                <span class="title">People</span>
              </div>
              <ul class="predictive-list">
                <li>
                  <a class="avatar" href="#">
                    <img src="images/face1.jpg" class="img-circle" alt="">
                    <span>Tammy Carpenter</span>
                  </a>
                </li>
                <li>
                  <a class="avatar" href="#">
                    <img src="images/face2.jpg" class="img-circle" alt="">
                    <span>Catherine Moreno</span>
                  </a>
                </li>
                <li>
                  <a class="avatar" href="#">
                    <img src="images/face3.jpg" class="img-circle" alt="">
                    <span>Diana Robertson</span>
                  </a>
                </li>
                <li>
                  <a class="avatar" href="#">
                    <img src="images/face4.jpg" class="img-circle" alt="">
                    <span>Emma Sullivan</span>
                  </a>
                </li>
              </ul>
              <div class="heading">
                <span class="title">Page posts</span>
              </div>
              <ul class="predictive-list">
                <li>
                  <a class="avatar" href="#">
                    <img src="images/unsplash/img2.jpeg" class="img-rounded" alt="">
                    <span>The latest news for cloud-based developers </span>
                  </a>
                </li>
                <li>
                  <a class="avatar" href="#">
                    <img src="images/unsplash/img2.jpeg" class="img-rounded" alt="">
                    <span>Trending Goods of the Week</span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
        </ul>
        <ul class="nav navbar-nav navbar-right hidden-xs">
					<li>
            <a href="javascript:;" class="ripple" data-toggle="dropdown">
              <i class="icon-bell"></i>
            </a>
            <ul class="dropdown-menu notifications">
              <li class="notifications-header">
                <p class="text-muted small">You have 3 new messages</p>
              </li>
              <li>
                <ul class="notifications-list">
                  <li>
                    <a href="javascript:;">
                      <div class="notification-icon">
                        <div class="circle-icon bg-success text-white">
                          <i class="icon-bulb"></i>
                        </div>
                      </div>
                      <span class="notification-message"><b>Sean</b> launched a new application</span>
                      <span class="time">2s</span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
					<li>
            <a href="javascript:;" class="ripple" data-toggle="dropdown">
              <i class="icon-bell"></i>
            </a>
            <ul class="dropdown-menu notifications">
              <li class="notifications-header">
                <p class="text-muted small">You have 3 new messages</p>
              </li>
              <li>
                <ul class="notifications-list">
                  <li>
                    <a href="javascript:;">
                      <div class="notification-icon">
                        <div class="circle-icon bg-success text-white">
                          <i class="icon-bulb"></i>
                        </div>
                      </div>
                      <span class="notification-message"><b>Sean</b> launched a new application</span>
                      <span class="time">2s</span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
					<li>
            <a href="javascript:;" class="ripple" data-toggle="dropdown">
              <i class="icon-bell"></i>
            </a>
            <ul class="dropdown-menu notifications">
              <li class="notifications-header">
                <p class="text-muted small">You have 3 new messages</p>
              </li>
              <li>
                <ul class="notifications-list">
                  <li>
                    <a href="javascript:;">
                      <div class="notification-icon">
                        <div class="circle-icon bg-success text-white">
                          <i class="icon-bulb"></i>
                        </div>
                      </div>
                      <span class="notification-message"><b>Sean</b> launched a new application</span>
                      <span class="time">2s</span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li>
            <a href="javascript:;" class="ripple" data-toggle="dropdown">
              <i class="icon-bell"></i>
            </a>
            <ul class="dropdown-menu notifications">
              <li class="notifications-header">
                <p class="text-muted small">You have 3 new messages</p>
              </li>
              <li>
                <ul class="notifications-list">
                  <li>
                    <a href="javascript:;">
                      <div class="notification-icon">
                        <div class="circle-icon bg-success text-white">
                          <i class="icon-bulb"></i>
                        </div>
                      </div>
                      <span class="notification-message"><b>Sean</b> launched a new application</span>
                      <span class="time">2s</span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li>
            <a href="javascript:;" class="ripple" data-toggle="dropdown">
              <img src="{{asset('images/user.jpg')}}" class="header-avatar img-circle" alt="user" title="{{ Auth::user()->name }}">
              <span>{{ Auth::user()->name }}</span>
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <a href="javascript:;">Settings</a>
              </li>
              <li>
                <a href="javascript:;">Upgrade</a>
              </li>
              <li>
                <a href="javascript:;">
                  <span class="label bg-danger pull-right">34</span>
                  <span>Notifications</span>
                </a>
              </li>
              <li role="separator" class="divider"></li>
              <li>
                <a href="javascript:;">Help</a>
              </li>
              <li>
                <a href="extras-signin.html">Logout</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- /top header -->
      <!-- main area -->
      <div class="main-content">
				@yield('content')
			</div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->
    <!-- bottom footer -->
    <footer class="content-footer">
      <nav class="footer-right">
        <ul class="nav">
          <li>
            <a href="javascript:;">Feedback</a>
          </li>
          <li>
            <a href="javascript:;" class="scroll-up">
              <i class="fa fa-angle-up"></i>
            </a>
          </li>
        </ul>
      </nav>
      <nav class="footer-left hidden-xs">
        <ul class="nav">
          <li>
            <a href="javascript:;"><span>About</span> Reactor</a>
          </li>
          <li>
            <a href="javascript:;">Privacy</a>
          </li>
          <li>
            <a href="javascript:;">Terms</a>
          </li>
          <li>
            <a href="javascript:;">Help</a>
          </li>
        </ul>
      </nav>
    </footer>
    <!-- /bottom footer -->

  </div>
  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="{{asset('new_assets/scripts/helpers/modernizr.js')}}"></script>
  <script src="{{asset('new_assets/vendor/jquery/dist/jquery.js')}}"></script>
  <script src="{{asset('new_assets/vendor/bootstrap/dist/js/bootstrap.js')}}"></script>
  <script src="{{asset('new_assets/vendor/fastclick/lib/fastclick.js')}}"></script>
  <script src="{{asset('new_assets/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
  <script src="{{asset('new_assets/scripts/helpers/smartresize.js')}}"></script>
  <script src="{{asset('new_assets/scripts/constants.js')}}"></script>
  <script src="{{asset('new_assets/scripts/main.js')}}"></script>
	@stack('script')
	@yield('js')
  <!-- endbuild -->
</body>

</html>
