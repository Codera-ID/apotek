@extends('layouts.app')
@section('css')
<style>
.head hr {
    border: none;
    height: 2px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}

.table, table td, .table th , .table>thead>tr{ 
    background-color: #fff !important; 
    background: #fff !important;
}

.table-name {
    border-bottom:0px !important;
}
.table-name th, .table-name td {
    border: 1px !important;
}

body{
  background-color: #fff !important; 
  background: #fff !important;
}

hr {
    border: none;
    height: 1px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}
</style>
@stop
@section('content')
<div class="main-content-inner">
  <div class="page-content">
    <div>
        <table>
          <tr>
            <td style="padding-right:20px">
            <img style="width:80px;" data-src="{{('images/'.$identitas->logo )}}" alt="..." src="{{('images/'.$identitas->logo )}}">
            </td>
            <td>
            <h3><b>{{$identitas->nama}}</b></h3>
                <h5>No. Izin Praktek : {{$identitas->no_registrasi}}</h5>
                <h5>{{$identitas->alamat}}</h5>
                <h5>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</h5>   
            </td>
          </tr>
        </table>
    </div><!-- /.page-header -->
    <div class="head"><hr></div>
    <br>
    <h2 class="center"><b>LAPORAN REKAP PEMBELIAN OBAT</b></h2>
    <h2 class="center"><b>PERIODE TANGGAL {{date_format(new DateTime($start_date),'d F Y')}} s/d  {{date_format(new DateTime($end_date),'d F Y')}}</b></h2>
    <hr>
    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <table class="table table-bordered">
          <thead>
            <tr>
              <th class="center">No</th>
              <th>Tanggal</th>
              <th>No. Faktur</th>
              <th>No. PO</th>
              <th colspan="2">Nama Supplier</th>
              <th>Gudang</th>
              <th>Jenis</th>
              <th>Jatuh Tempo</th>
            </tr>
          </thead>

          <tbody>
            <?php $i=1; $grand_total =0; $total_konsinyasi=0; $total_tunai=0; $total_hutang = 0; ?>
            @foreach($data as $key)
            <tr>
              <td class="center">{{$i}}</td>
              <td>{{$key->tgl_faktur}}</td>
              <td>{{$key->no_faktur}}</td>
              <td>{{$key->no_po}}</td>
              <td colspan="2">{{$key->nama_supplier}}</td>
              <td>{{$key->nama_gudang}}</td>
              <td>{{$key->jenis_pembayaran}}</td>
              <td>{{$key->jatuh_tempo}}</td>
            </tr>
            <?php $i++; $total_transaksi = 0; $subtotal = 0; $diskon = 0; $pajak = 0; 
            $pembelian_detail = DB::table('pembelian_detail')->where('no_faktur',$key->no_faktur)
              ->join('obat', 'pembelian_detail.kd_obat', '=', 'obat.kd_obat')
              ->join('satuan', 'pembelian_detail.kd_satuan', '=', 'satuan.kd_satuan')->get();
            ?>
            <tr>
              <th>Kode Obat</th>
              <th>Nama Obat</th>
              <th>Satuan</th>
              <th>Tanggal Exp</th>
              <th>No Batch</th>
              <th>Harga</th>
              <th>Jumlah</th>
              <th>Diskon</th>
              <th>Total</th> 
            </tr>
            @foreach($pembelian_detail as $item)
              <tr>
                <td>{{$item->kd_obat}}</td>
                <td>{{$item->nama_obat}}</td>
                <td>{{$item->nama_sat}}</td>
                <td>{{date_format(new DateTime($item->tgl_exp),'d M Y')}}</td>
                <td>{{$item->no_batch}}</td>
                <td style="text-align:right">{{number_format($item->harga,2,",",".")}}</td>
                <td style="text-align:right">{{$item->jmlh}}</td>
                <td style="text-align:right">{{$item->diskon}} %</td>
                <td style="text-align:right">{{number_format(($item->harga*$item->jmlh)*(1-($item->diskon/100)),2,",",".")}}</td>
              </tr>
                <?php $subtotal += ($item->harga*$item->jmlh)*(1-($item->diskon/100));
                if($key->diskon > 0){
                  $diskon = $subtotal*($key->diskon/100);
                  if($key->pajak > 0){
                    $pajak = ($subtotal-$diskon)*($key->pajak/100);
                  }
                }
                $total_transaksi = $subtotal - $diskon + $pajak + $key->biaya;
                ?>
            @endforeach
            <tr>
              <td colspan="7"><b>Subtotal</b></td>
              <td colspan="2" style="text-align:right;"><b>{{number_format($subtotal,2,",",".")}}</b></td>
            </tr>
            <tr>
              <td colspan="7"><b>Diskon</b></td>
              <td colspan="2" style="text-align:right;"><b>{{number_format($diskon,2,",",".")}}</b></td>
            </tr>
            <tr>
              <td colspan="7"><b>Pajak</b></td>
              <td colspan="2" style="text-align:right;"><b>{{number_format($pajak,2,",",".")}}</b></td>
            </tr>
            <tr>
              <td colspan="7"><b>Biaya</b></td>
              <td colspan="2" style="text-align:right;"><b>{{number_format($key->biaya,2,",",".")}}</b></td>
            </tr>
            <tr>
              <td colspan="7"><b>Total Transaksi</b></td>
              <td colspan="2" style="text-align:right;"><b>{{number_format($total_transaksi,2,",",".")}}</b></td>
            </tr>
            <?php $grand_total += $total_transaksi?>
            @if ($key->jenis_pembayaran == 'TUNAI')
              {{$total_tunai += $total_transaksi}}
            @elseif ($key->jenis_pembayaran == 'KONSINYASI')
              {{$total_konsinyasi += $total_transaksi}}
            @elseif ($key->jenis_pembayaran == 'HUTANG')
              {{$total_hutang += $total_transaksi}}
            @endif 
            @endforeach
            <tr>
              <td colspan="7" style="text-align:right;"><b>Tunai</b></td>
              <td><b>:</b></td>
              <td style="text-align:right;"><b>{{number_format($total_tunai,2,",",".")}}</b></td>
            </tr>
            <tr>
              <td colspan="7" style="text-align:right;"><b>Hutang</b></td>
              <td><b>:</b></td>
              <td style="text-align:right;"><b>{{number_format($total_hutang,2,",",".")}}</b></td>
            </tr>
            <tr>
              <td colspan="7" style="text-align:right;"><b>Konsinyasi</b></td>
              <td><b>:</b></td>
              <td style="text-align:right;"><b>{{number_format($total_konsinyasi,2,",",".")}}</b></td>
            </tr>
            <tr>
              <td colspan="7" style="text-align:right;"><b>Grand Total Transaksi</b></td>
              <td><b>:</b></td>
              <td style="text-align:right;"><b>{{number_format($grand_total,2,",",".")}}</b></td>
            </tr>
          </tbody>
        </table>
        <table class="table table-name">
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%">Tempat, {{date('d F Y')}}</td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%">Penanggung Jawab</td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%"></td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%"></td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%"></td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%">{{ Auth::user()->name }}</td>
            </tr>
        </table>
        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('footer')
@stop