@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Laporan Transaksi Apotek</a>
      </li>
      <li class="active">Data Pembelian Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
    <div class="page-header">
      <h1>
        Laporan Data Pembelian Obat
      </h1>
      <br>
      <br>
      <div class="clearfix">
        <div class="col-xs-12">
          <form class="form-inline" action="{{url('laporan-data-pembelian-obat')}}" method="get" style="margin:10px 0px">
            <label class="control-label no-padding-right">Pilih Periode</label>
            <div class="input-daterange input-group">
                <input type="text" class="form-control" name="start" value="{{$start_date}}"/>
                <span class="input-group-addon">
                <i class="fa fa-exchange"></i>
                </span>

                <input type="text" class="form-control" name="end" value="{{$end_date}}"/>
            </div>
            <button type="submit" class="btn btn-sm btn-default"> <i class="fa fa-search"></i> Cari </button>              
          </form>
        </div>
        <div class="pull-right tableTools-container">
          <div class="pull-right tableTools-container">
            <div class="dt-buttons btn-overlap btn-group">
              <a href="{{url('export-pembelian-obat/'.$start_date.'/'.$end_date)}}" class="dt-button buttons-csv buttons-html5 btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-database bigger-110 orange"></i> <span class="">Excel</span></span></a>
              <a href="{{url('export-detail-pembelian-obat/'.$start_date.'/'.$end_date)}}" class="dt-button buttons-csv buttons-html5 btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-database bigger-110 orange"></i> <span class="">Detail</span></span></a>
              <a href="{{url('cetak-pembelian-obat/'.$start_date.'/'.$end_date)}}" class="dt-button buttons-print btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-print bigger-110 grey"></i> <span class="">Cetak</span></span></a>
              <a href="{{url('cetak-detail-pembelian-obat/'.$start_date.'/'.$end_date)}}" class="dt-button buttons-print btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-print bigger-110 grey"></i> <span class="">Detail</span></span></a>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.page-header -->

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
          <div class="col-xs-12">
          @if (count($data) == 0)
            Tidak ada data yang ditampilkan.
          @elseif (count($data) > 0)
            Menampilkan data dari 1 - {{count($data)}}
          @endif
            <div class="table-header">
              Data Pembelian Obat
            </div>

            <!-- div.table-responsive -->

            <!-- div.dataTables_borderWrap -->
            <div>
               <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <th class="center">#</th>
                      <th>Tanggal</th>
                      <th>No. Faktur</th>
                      <th>PO No.</th>
                      <th>Supplier</th>
                      <th>Jenis</th>
                      <th>Gudang</th>
                      <th>Diskon(%)</th>
                      <th>Pajak(%)</th>
                      <th>Grand Total</th>                
                    </tr>
                  </thead>

                  <tbody>
                    <?php $i=1; $total=0?>
                    @if (count($data) == 0)
                      <tr>
                        <td colspan="11">Data tidak ada.</td>
                      <tr>
                    @else
                      @foreach($data as $key)
                      <tr>
                        <td class="center">
                          <div class="action-buttons">
                            <a href="#" class="green bigger-140 show-details-btn" title="Show Details">
                              <i class="ace-icon fa fa-angle-double-down"></i>
                              <span class="sr-only">Details</span>
                            </a>
                          </div>
                        </td>
                        <td>{{$key->tgl_faktur}}</td>
                        <td>{{$key->no_faktur}}</td>
                        <td>{{$key->no_po}}</td>
                        <td>{{$key->nama_supplier}}</td>
                        <td>{{$key->jenis_pembayaran}}</td>
                        <td>{{$key->nama_gudang}}</td>
                        <td>{{$key->diskon}}</td>
                        <td>{{$key->pajak}}</td>
                        <td>{{$key->total}}</td>
                      </tr>
                      <tr class="detail-row">
                        <td colspan="11">
                          <div class="table-detail">
                            <div class="row">
                              <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                  <tr>
                                    <th class="center">No.</th>
                                    <th>Kode Obat</th>
                                    <th>Nama Obat</th>
                                    <th>Satuan</th>
                                    <th>Tanggal Exp</th>
                                    <th>No Batch</th>
                                    <th>Harga</th>
                                    <th>Jumlah</th>
                                    <th>Diskon</th>
                                    <th>Total</th>                  
                                  </tr>
                                </thead>
                                <tbody>
                                <?php $a=1;

                                $pembelian_detail = DB::table('pembelian_detail')->where('no_faktur',$key->no_faktur)
                                          ->join('obat', 'pembelian_detail.kd_obat', '=', 'obat.kd_obat')
                                          ->join('satuan', 'pembelian_detail.kd_satuan', '=', 'satuan.kd_satuan')->get();
                                 ?>
                                @foreach($pembelian_detail as $item)
                                <tr data-key="0">
                                  <td>{{$a}}</td>
                                  <td>{{$item->kd_obat}}</td>
                                  <td>{{$item->nama_obat}}</td>
                                  <td>{{$item->nama_sat}}</td>
                                  <td>{{$item->tgl_exp}}</td>
                                  <td>{{$item->no_batch}}</td>
                                  <td>{{$item->harga}}</td>
                                  <td>{{$item->jmlh}}</td>
                                  <td>{{$item->diskon}}</td>
                                  <td>{{($item->harga*$item->jmlh)*(1-($item->diskon/100))}}
                                  <?php $a++; ?>
                                </tr>
                                @endforeach
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </td>
                      </tr>

                      <?php $i++; $total += $key->total;?>
                      @endforeach
                    @endif

                  </tbody>
              </table>
              <div style="text-align:right; margin: 5px 0px"><b>Total Pembelian Obat: {{number_format($total,2,",",".")}}</b>
              </div>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('.show-details-btn').on('click', function(e) {
    e.preventDefault();
    $(this).closest('tr').next().toggleClass('open');
    $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
  });

  //or change it into a date range picker
  $('.input-daterange').datepicker({
              autoclose:true,
              format: 'yyyy-mm-dd'
          });
      
  //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
  $('input[name=date-range-picker]').daterangepicker({
    'applyClass' : 'btn-sm btn-success',
    'cancelClass' : 'btn-sm btn-default',
    locale: {
      applyLabel: 'Apply',
      cancelLabel: 'Cancel',
    }
  })
  .prev().on(ace.click_event, function(){
    $(this).next().focus();
  });

});

</script>
@stop
