<html>

    <tbody>
        <tr>
            <td>{{$identitas->nama}}</td>
        </tr>
        <tr>        
            <td>No. Izin Praktek : {{$identitas->no_registrasi}}</td>
        </tr>  
        <tr>                      
            <td>{{$identitas->alamat}}</td>
        </tr>        
        <tr>            
            <td>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</td>
        </tr>
        <tr>
            <td align="center">LAPORAN REKAP PEMBELIAN OBAT</td>
        </tr>
        <tr>
            <td align="center">PERIODE TANGGAL {{date_format(new DateTime($start_date),'d F Y')}} s/d {{date_format(new DateTime($end_date),'d F Y')}}</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th class="center">No</th>
            <th>Tanggal</th>
            <th>No. Faktur</th>
            <th>No. PO</th>
            <th>Supplier</th>
            <th>Jenis</th>
            <th>Gudang</th>
            <th>Diskon(%)</th>
            <th>Pajak(%)</th>
            <th>Total</th>  
        </tr>
    </thead>

    <tbody>
        <?php $i=1; $grand_total =0; $total_konsinyasi=0; $total_tunai=0; $total_hutang = 0; ?>
            @foreach($data as $key)
            <tr>
              <td class="center">{{$i}}</td>
              <td>{{$key->tgl_faktur}}</td>
              <td>{{$key->no_faktur}}</td>
              <td>{{$key->no_po}}</td>
              <td>{{$key->nama_supplier}}</td>
              <td>{{$key->jenis_pembayaran}}</td>
              <td>{{$key->nama_gudang}}</td>
              <td>{{$key->diskon}}</td>
              <td>{{$key->pajak}}</td>
              <td>{{number_format($key->total,2,",",".")}}</td>
            </tr>
            <?php $i++; $grand_total += $key->total;?>
            @if ($key->jenis_pembayaran == 'TUNAI')
              {{$total_tunai += $key->total}}
            @elseif ($key->jenis_pembayaran == 'KONSINYASI')
              {{$total_konsinyasi += $key->total}}
            @elseif ($key->jenis_pembayaran == 'HUTANG')
              {{$total_hutang += $key->total}}
            @endif 
            @endforeach
            <tr>
              <td colspan="7" style="text-align:right;"><b>Tunai</b></td>
              <td style="text-align:right;"><b>:</b></td>
              <td colspan="2" style="text-align:right;"><b>{{number_format($total_tunai,2,",",".")}}</b></td>
            </tr>
            <tr>
              <td colspan="7" style="text-align:right;"><b>Hutang</b></td>
              <td style="text-align:right;"><b>:</b></td>
              <td colspan="2" style="text-align:right;"><b>{{number_format($total_hutang,2,",",".")}}</b></td>
            </tr>
            <tr>
              <td colspan="7" style="text-align:right;"><b>Konsinyasi</b></td>
              <td style="text-align:right;"><b>:</b></td>
              <td colspan="2" style="text-align:right;"><b>{{number_format($total_konsinyasi,2,",",".")}}</b></td>
            </tr>
            <tr>
              <td colspan="7" style="text-align:right;"><b>Grand Total Transaksi</b></td>
              <td style="text-align:right;"><b>:</b></td>
              <td colspan="2" style="text-align:right;"><b>{{number_format($grand_total,2,",",".")}}</b></td>
            </tr>
    </tbody>

</html>