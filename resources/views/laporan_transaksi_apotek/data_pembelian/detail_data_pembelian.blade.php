@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Laporan Transaksi Apotek</a>
      </li>
      <li class="active">Data Pembelian Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					NO. FAKTUR {{$no_faktur}} 
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
          <div class="table-header">
            Data Obat
          </div>
          <div>
            <div class="table-responsive table_po">

            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Kode Obat</th>
                  <th>Nama Obat</th>
                  <th>Jumlah</th>
                  <th>Satuan</th>
                  <th>Harga</th>
                  <th>Diskon</th>
                  <th>Sub Total</th>
                </tr>
              </thead>

              <tbody>
                <?php $i=1; ?>
                @foreach($obat as $key)
                <tr>
                    <td class="center">{{$i}}</td>
                    <td>{{$key->kd_obat}}</td>   
                    <td>{{$key->nama_obat}}</td>                  
                    <td>{{$key->jmlh}}</td>                    
                    <td>{{$key->nama_sat}}</td>              
                    <td>{{$key->harga}}</td>                    
                    <td>{{$key->diskon}}</td>                    
                    <td>{{$key->jmlh*$key->harga*(1-($key->diskon/100))}}</td>                    
                </tr>
                <?php $i++; ?>
                @endforeach

              </tbody>
            </table>
          </div>
          </div>
          <div class="clearfix form-actions">
              <div class="pull-right">
                <a href="{{url('laporan-data-pembelian-obat')}}">
                <button class="btn btn-danger" type="button">
                  <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
                    Kembali
                </button>
              </a>
              </div>
             </div>
             <div id="coba2"></div>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
});
@stop
