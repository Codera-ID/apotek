@extends('layouts.home')

@section('css')
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Laporan Transaksi Apotek</a>
      </li>
      <li class="active">Data Purchase Order (PO)</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
    <div class="page-header">
      <h1>
        Laporan Data Purchase Order
      </h1>
        <br>
        <br>
        <div class="clearfix">
        <form class="form-horizontal" action="{{url('laporan-data-purchase-order')}}" method="get">
            <div class="pull-left col-xs-4">                
                <label for="form-field-select-1">Jenis Invoice</label>
                <select name="jenis_invoice" class="form-control" id="form-field-select-1">
                    <option value="cetak">Tanpa Cetak</option>
                    <option value="invoice">Invoice (A5)</option>          
                </select>
                <br>

                <label class="control-label no-padding-right">Pilih Periode</label>
                <div class="input-daterange input-group">
                    <input type="text" class="form-control" name="start" value="{{ $start_date }}"/>
                    <span class="input-group-addon">
                    <i class="fa fa-exchange"></i>
                    </span>

                    <input type="text" class="form-control" name="end" value="{{ $end_date }}"/>
                </div>
                <br>

                <span class="input-group-btn">
                    <button type="submit" class="btn btn-purple btn-sm">
                        <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                        Cari
                    </button>
                </span>
            </div>
        </form>
        </div>
      <div class="clearfix">
        <div class="pull-right tableTools-container">
            <div class="dt-buttons btn-overlap btn-group">
                <a href="{{url('export-purchase-order')}}" class="dt-button buttons-csv buttons-html5 btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-database bigger-110 orange"></i> <span class="">Export</span></span></a>
                <a href="{{url('export-detail-purchase-order')}}" class="dt-button buttons-csv buttons-html5 btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-database bigger-110 orange"></i> <span class="">Export Detail</span></span></a>
                <a href="{{url('cetak-purchase-order')}}" class="dt-button buttons-print btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-print bigger-110 grey"></i> <span class="">Print</span></span></a>
                <a href="{{url('cetak-detail-purchase-order')}}" class="dt-button buttons-print btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-print bigger-110 grey"></i> <span class="">Print Detail</span></span></a>
            </div>
        </div>
      </div>
    </div><!-- /.page-header -->

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
          <div class="col-xs-12">
            <div class="table-header">
              Data Purchase Order
            </div>

            <!-- div.table-responsive -->

            <!-- div.dataTables_borderWrap -->
            <div>
              <table id="dynamic-table" class="table table-striped table-bordered table-hover data">
                <thead>
                  <tr>
                    <th class="center">#</th>
                    <th>Tanggal</th>
                    <th>No. PO</th>
                    <th>Supllier</th>
                    <th>Jenis</th>
                    <th>Total</th>
                    <th>Keterangan</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $i=0; $total=0; ?>
                  @foreach($data as $key)
                  <tr>
                    <td class="center">
                      <a href="#" data-toggle="collapse" data-target="{{'#detail'.$i}}">
                        <span class="glyphicon glyphicon-collapse-down"></span>
                      </a>
                    </td>
                    <td>{{$key->tgl_po}}</td>
                    <td>{{$key->no_po}}</td>
                    <td>{{$key->nama_supplier}}</td>
                    <td>{{$key->jenis_obat}}</td>
                    <td>{{$key->total}}</td>
                    <td>{{$key->keterangan}}</td>
                    <td class="center">
                      <div class="hidden-sm hidden-xs action-buttons">
                        <a class="blue" href="{{url('cetak-purchase-order/faktur/'.$key->no_po)}}">
                            <i class="ace-icon fa fa-print bigger-130"></i>
                        </a>
                      </div>
                    </td>
                  </tr>
                  <tr id="{{'detail'.$i}}" class="collapse">
                      <td colspan="8">
                        <div class="skip-export kv-expanded-row w4" data-index="0" data-key="0" style="display: block;">
                          <div id="w8" class="grid-view">
                            <table class="table table-striped table-bordered">
                              <thead>
                              <tr>
                                <th style="width:15px;">No.</th><th style="width:140px;">Kode Obat</th>
                                <th style="width:180px;">Nama Obat</th><th style="width:80px; text-align:right;">Jumlah</th>
                                <th style="width:140px;">Satuan</th><th style="width:120px; text-align:right;">Harga</th>
                                <th style="width:80px; text-align:right;">Diskon</th>
                                <th style="width:120px; text-align:right;">Subtotal</th>
                              </tr>
                              </thead>
                              <tbody>
                              <?php $a=1;

                              $po_detail = DB::table('po_detail')->where('no_po',$key->no_po)
                                        ->join('obat', 'po_detail.kd_obat', '=', 'obat.kd_obat')
                                        ->join('satuan', 'po_detail.kd_satuan', '=', 'satuan.kd_satuan')->get();
                               ?>
                              @foreach($po_detail as $item)
                              <tr data-key="0">
                                <td>{{$a}}</td>
                                <td>{{$item->kd_obat}}</td>
                                <td>{{$item->nama_obat}}</td>
                                <td style="text-align:right">{{$item->jmlh}}</td>
                                <td>{{$item->kd_sat_obat}}</td>
                                <td style="text-align:right">{{$item->harga}}</td>
                                <td style="text-align:right">{{$item->diskon}} %</td>
                                <td style="text-align:right">{{$item->subtotal}}</td>
                                <?php $a++; ?>
                              </tr>
                              @endforeach
                            </tbody>
                            </table>
                          </div>
                        </div>
                      </td>
                  </tr>
                  <?php $i++; $total += $key->total; ?>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  var total = {{$total}};
  total = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  console.log(total);

  $('#dynamic-table').after('<div style="text-align:right; margin: 5px 0px"><b>Total Purchase Order: '+total+'</b>');

  //or change it into a date range picker
  $('.input-daterange').datepicker({
              autoclose:true,
              format: 'yyyy-mm-dd'
          });

  //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
  $('input[name=date-range-picker]').daterangepicker({
    'applyClass' : 'btn-sm btn-success',
    'cancelClass' : 'btn-sm btn-default',
    locale: {
      applyLabel: 'Apply',
      cancelLabel: 'Cancel',
    }
  })
  .prev().on(ace.click_event, function(){
    $(this).next().focus();
  });
});

  var myTable = $('#dynamic-table').DataTable({
        "aoColumns": [
            { "bSortable": false },
            null, null, null, null, null, null
        ],
        "aaSorting": [],
  });
</script>
@stop
