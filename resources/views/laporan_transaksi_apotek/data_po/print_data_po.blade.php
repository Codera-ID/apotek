@extends('layouts.app')
@section('css')
<style>
.head hr {
    border: none;
    height: 2px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}

.table, table td, .table th , .table>thead>tr{ 
    background-color: #fff !important; 
    background: #fff !important;
}

.table-name {
    border-bottom:0px !important;
}
.table-name th, .table-name td {
    border: 1px !important;
}

body{
  background-color: #fff !important; 
  background: #fff !important;
}

hr {
    border: none;
    height: 1px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}
</style>
@stop
@section('content')
<div class="main-content-inner">
  <div class="page-content">
    <div>
        <table>
          <tr>
            <td style="padding-right:20px">
            <img style="width:80px;" data-src="{{('images/'.$identitas->logo )}}" alt="..." src="{{('images/'.$identitas->logo )}}">
            </td>
            <td>
            <h3><b>{{$identitas->nama}}</b></h3>
                <h5>No. Izin Praktek : {{$identitas->no_registrasi}}</h5>
                <h5>{{$identitas->alamat}}</h5>
                <h5>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</h5>   
            </td>
          </tr>
        </table>
    </div><!-- /.page-header -->
    <div class="head"><hr></div>
    <br>
    <h2 class="center"><b>LAPORAN REKAP PURCHASE ORDER</b></h2>
    <hr>
    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <table class="table table-bordered">
          <thead>
            <tr>
              <th class="center">#</th>
              <th>Tanggal</th>
              <th>No.PO</th>
              <th>Supplier</th>
              <th>Jenis</th>
              <th>Total</th>
              <th>Keterangan</th> 
            </tr>
          </thead>

          <tbody>
            <?php $i=1; $total=0; ?>
            @foreach($data as $key)
            <tr>
              <td class="center">{{$i}}</td>
              <td>{{$key->tgl_po}}</td>
              <td>{{$key->no_po}}</td>
              <td>{{$key->nama_supplier}}</td>
              <td>{{$key->jenis_obat}}</td>
              <td>{{$key->total}}</td>
              <td>{{$key->keterangan}}</td>
            </tr>
            <?php $i++; $total += $key->total; ?>
            @endforeach
            <tr>
              <td colspan="7" style="text-align:right;">Total  : {{$total}}</td>
            </tr>
          </tbody>
        </table>
        <table class="table table-name">
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%">Tempat, Tanggal</td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%">Penanggung Jawab</td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%"></td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%"></td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%"></td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%">{{ Auth::user()->name }}</td>
            </tr>
        </table>
        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('footer')
@stop