<html>

    <tbody>
        <tr>
            <td>{{$identitas->nama}}</td>
        </tr>
        <tr>        
            <td>No. Izin Praktek : {{$identitas->no_registrasi}}</td>
        </tr>  
        <tr>                      
            <td>{{$identitas->alamat}}</td>
        </tr>        
        <tr>            
            <td>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</td>
        </tr>
        <tr>
            <td align="center">LAPORAN REKAP PURCHASE ORDER</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th class="center">#</th>
            <th>Tanggal</th>
            <th>No.PO</th>
            <th>Supplier</th>
            <th>Jenis</th>
            <th>Total</th>
            <th>Keterangan</th>   
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach($data as $key)
        <tr>
            <td class="center">{{$i}}</td>
            <td>{{$key->tgl_po}}</td>
            <td>{{$key->no_po}}</td>
            <td>{{$key->nama_supplier}}</td>
            <td>{{$key->jenis_obat}}</td>
            <td>{{$key->total}}</td>
            <td>{{$key->keterangan}}</td>
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>

</html>