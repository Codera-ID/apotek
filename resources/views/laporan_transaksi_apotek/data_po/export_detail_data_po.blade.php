<html>

    <tbody>
        <tr>
            <td>{{$identitas->nama}}</td>
        </tr>
        <tr>        
            <td>No. Izin Praktek : {{$identitas->no_registrasi}}</td>
        </tr>  
        <tr>                      
            <td>{{$identitas->alamat}}</td>
        </tr>        
        <tr>            
            <td>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</td>
        </tr>
        <tr>
            <td align="center">LAPORAN REKAP PURCHASE ORDER (PO)</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th class="center">#</th>
            <th>Tanggal</th>
            <th>No.PO</th>
            <th>Supplier</th>
            <th>Jenis</th>
            <th>Total</th>
            <th colspan="2">Keterangan</th>   
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach($data as $key)
        <tr>
            <td class="center">{{$i}}</td>
            <td>{{$key->tgl_po}}</td>
            <td>{{$key->no_po}}</td>
            <td>{{$key->nama_supplier}}</td>
            <td>{{$key->jenis_obat}}</td>
            <td>{{$key->total}}</td>
            <td>{{$key->keterangan}}</td>
        </tr>
        <?php
        $po_detail = DB::table('po_detail')
                    ->where('no_po',$key->no_po)
                    ->join('obat', 'po_detail.kd_obat', '=', 'obat.kd_obat')
                    ->join('satuan', 'po_detail.kd_satuan', '=', 'satuan.kd_satuan')
                    ->get();
        ?>
        <tr>
            <th></th>
            <th class="center"><b>Kode Obat</b></th>
            <th class="center"><b>Nama Obat</b></th>
            <th class="center"><b>Jumlah</b></th>
            <th class="center"><b>Satuan</b></th>
            <th class="center"><b>Harga</b></th>
            <th class="center"><b>Diskon (%)</b></th> 
            <th class="center"><b>Subtotal</b></th>
        </tr>
            @foreach($po_detail as $item)
               <tr>
                    <td></td>
                    <td>{{$item->kd_obat}}</td>
                    <td>{{$item->nama_obat}}</td>
                    <td style="text-align:right">{{$item->jmlh}}</td>
                    <td>{{$item->kd_sat_obat}}</td>
                    <td style="text-align:right">{{$item->harga}}</td>
                    <td style="text-align:right">{{$item->diskon}} %</td>
                    <td style="text-align:right">{{$item->subtotal}}</td>
                </tr>
            @endforeach
        <?php $i++; ?>
        @endforeach
    </tbody>

</html>