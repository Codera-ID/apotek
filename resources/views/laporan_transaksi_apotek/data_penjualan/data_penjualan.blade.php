@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Laporan Transaksi Apotek</a>
      </li>
      <li class="active">Data Penjualan Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
    <div class="page-header">
      <h1>
        Laporan Data Penjualan Obat
      </h1>
      <br>
      <br>
      <div class="clearfix">
          <form class="form-horizontal" action="{{url('laporan-data-purchase-order')}}" method="get">
              <div class="pull-left col-xs-4">                
                  <label class="control-label no-padding-right">Pilih Periode</label>
                  <div class="input-daterange input-group">
                      <input type="text" class="form-control" name="start" value="{{ $start_date }}"/>
                      <span class="input-group-addon">
                      <i class="fa fa-exchange"></i>
                      </span>
                      <input type="text" class="form-control" name="end" value="{{ $end_date }}"/>
                  </div>
                  <br>
                  <span class="input-group-btn">
                      <button type="submit" class="btn btn-purple btn-sm">
                          <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                          Cari
                      </button>
                  </span>
              </div>
          </form>
        </div>
        <div class="clearfix">
        <div class="pull-right tableTools-container"></div>
      </div>
    </div><!-- /.page-header -->

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
          <div class="col-xs-12">
            <div class="table-header">
              Data Penjualan Obat
            </div>

            <!-- div.table-responsive -->

            <!-- div.dataTables_borderWrap -->
            <div>
               <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="center">#</th>
                    <th>Tanggal</th>
                    <th>No. Faktur</th>
                    <th>Pasien</th>
                    <th>Dokter</th>
                    <th>Jenis</th>
                    <th>Gudang</th>
                    <th>Diskon(%)</th>
                    <th>Pajak(%)</th>
                    <th>Grand Total</th>
                    <th>Action</th>                  
                  </tr>
                </thead>

                <tbody>
                  <?php $i=1; ?>
                  @foreach($data as $key)
                  <tr>
                    <td class="center">{{$i}}</td>
                    <td>{{$key->tgl_faktur}}</td>
                    <td>{{$key->no_faktur}}</td>
                    <td>{{$key->nama_pasien}}</td>
                    <td>{{$key->nama_dokter}}</td>
                    <td>{{$key->jenis_pembayaran}}</td>
                    <td>{{$key->nama_gudang}}</td>
                    <td>{{$key->disc_harga_jual}}%</td>
                    <td>{{$key->pajak}}</td>
                    <td>{{$key->total}}</td>
                    <td class="center">
                        <div class="hidden-sm hidden-xs action-buttons">
                            <a class="blue" href="{{url('laporan-data-penjualan-obat/obat/'.$key->no_faktur)}}">
                                <i class="ace-icon fa fa-search-plus bigger-130"></i>
                            </a>
                        </div>
                    </td>
                  </tr>
                  <?php $i++; ?>
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  var myTable = $('#dynamic-table').DataTable({
        "aoColumns": [
            { "bSortable": false },
            null, null,null, null, null, null, null, null, null,
            { "bSortable": false }
        ],
        "aaSorting": [],
  });

      $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
        
        new $.fn.dataTable.Buttons( myTable, {
          buttons: [
            {
            "extend": "csv",
            "text": "<i class='fa fa-database bigger-110 orange'></i> <span class=''>Export</span>",
            "className": "btn btn-white btn-primary btn-bold"
            },
            {
            "extend": "excel",
            "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
            "className": "btn btn-white btn-primary btn-bold"
            },
            {
            "extend": "print",
            "text": "<i class='fa fa-print bigger-110 grey'></i> <span class=''>Print</span>",
            "className": "btn btn-white btn-primary btn-bold",
            autoPrint: false,
            message: 'This print was produced using the Print button for DataTables'
            }     
          ]
        } );
        myTable.buttons().container().appendTo( $('.tableTools-container') );

        //or change it into a date range picker
        $('.input-daterange').datepicker({
                    autoclose:true,
                    format: 'yyyy-mm-dd'
                });
      
      
        //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
        $('input[name=date-range-picker]').daterangepicker({
          'applyClass' : 'btn-sm btn-success',
          'cancelClass' : 'btn-sm btn-default',
          locale: {
            applyLabel: 'Apply',
            cancelLabel: 'Cancel',
          }
        })
        .prev().on(ace.click_event, function(){
          $(this).next().focus();
        });
});
</script>
@stop
