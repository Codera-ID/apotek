@extends('layouts.app')
@section('css')
<style>
.head hr {
    border: none;
    height: 2px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}

.table, table td, .table th , .table>thead>tr{ 
    background-color: #fff !important; 
    background: #fff !important;
}

.table-name {
    border-bottom:0px !important;
}
.table-name th, .table-name td {
    border: 1px !important;
}

body{
  background-color: #fff !important; 
  background: #fff !important;
}

hr {
    border: none;
    height: 1px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}
</style>
@stop
@section('content')
<div class="main-content-inner">
  <div class="page-content">
    <div>
        <table>
          <tr>
            <td style="padding-right:20px">
            <img style="width:80px;" data-src="{{('images/'.$identitas->logo )}}" alt="..." src="{{('images/'.$identitas->logo )}}">
            </td>
            <td>
            <h3><b>{{$identitas->nama}}</b></h3>
                <h5>No. Izin Praktek : {{$identitas->no_registrasi}}</h5>
                <h5>{{$identitas->alamat}}</h5>
                <h5>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</h5>   
            </td>
          </tr>
        </table>
    </div><!-- /.page-header -->
    <div class="head"><hr></div>
    <br>
    <h2 class="center"><b>LAPORAN REKAP RETUR PEMBELIAN OBAT</b></h2>
    <h2 class="center"><b>PERIODE TANGGAL {{date_format(new DateTime($start_date),'d F Y')}} s/d  {{date_format(new DateTime($end_date),'d F Y')}}</b></h2>
    <hr>
    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <table class="table table-bordered">
          <thead>
            <tr>
              <th class="center">No</th>
              <th>Tanggal Retur</th>
              <th>No. Faktur</th>
              <th>No. PO</th>
              <th colspan="2">Supplier</th>
              <th colspan="2">Jenis</th>
              <th colspan="2">Jatuh Tempo</th> 
            </tr>
          </thead>
          <tbody>
            <?php $i=1; $grand_total =0;?>
            @foreach($data as $key)
            <tr>
              <td class="center">{{$i}}</td>
              <td>{{$key->tanggal_retur}}</td>
              <td>{{$key->no_faktur}}</td>
              <td>{{$key->no_po}}</td>
              <td colspan="2">{{$key->nama_supplier}}</td>
              <td colspan="2">{{$key->jenis_pembayaran}}</td>
              <td colspan="2"></td>
            </tr>
            <tr>
              <th class="center">Kode Obat</th>
              <th>Nama Obat</th>
              <th>Satuan</th>
              <th>Tanggal Exp</th>
              <th>No Batch</th>
              <th>Harga</th>
              <th>Jumlah</th>
              <th>Diskon</th>
              <th>Potongan</th>
              <th>Total</th>            
            </tr>
            <?php $i++; $subtotal=0;
            $retur_detail = DB::table('retur_pembelian_obat')->where('no_faktur',$key->no_faktur)
              ->join('detail_retur_pembelian', 'detail_retur_pembelian.no_retur_pembelian', '=', 'retur_pembelian_obat.no_retur_pembelian')
              ->join('obat', 'detail_retur_pembelian.kd_obat', '=', 'obat.kd_obat')
              ->join('satuan', 'detail_retur_pembelian.kd_satuan', '=', 'satuan.kd_satuan')->get();
             ?>
              @foreach($retur_detail as $item)
                <tr>
                  <td>{{$item->kd_obat}}</td>
                  <td>{{$item->nama_obat}}</td>
                  <td>{{$item->nama_sat}}</td>
                  <td>{{$item->tgl_expired}}</td>
                  <td>{{$item->no_batch}}</td>
                  <td>{{number_format($item->harga_rata_rata,2,",",".")}}</td>
                  <td>{{number_format($item->jmlh,2,",",".")}}</td>
                  <td>{{number_format(0,2,",",".")}}</td>
                  <td>{{number_format($item->potongan,2,",",".")}}</td>
                  <td>{{number_format($item->total,2,",",".")}}
                </tr>
                <?php $subtotal += $item->total;?>
              @endforeach
              <tr>
                <td colspan="7" style="text-align:right;"><b>Subtotal</b></td>
                <td style="text-align:right;"><b>:</b></td>
                <td colspan="2" style="text-align:right;"><b>{{number_format($subtotal,2,",",".")}}</b></td>
              </tr>
              <?php $grand_total += $subtotal;?> 
            @endforeach
            <tr>
              <td colspan="10"></td>
            </tr>
            <tr>
              <td colspan="10" style="text-align:right;"><b>Total Retur : {{number_format($grand_total,2,",",".")}}</b></td>
            </tr>
          </tbody>
        </table>
        <table class="table table-name">
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%">Tempat, {{date('d F Y')}}</td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%">Penanggung Jawab</td>
            </tr>
            <tr>
              <td colspan="3"></td>
            </tr>
            <tr>
              <td colspan="3"></td>
            </tr>
            <tr>
              <td colspan="3"></td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%">{{ Auth::user()->name }}</td>
            </tr>
        </table>
        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('footer')
@stop