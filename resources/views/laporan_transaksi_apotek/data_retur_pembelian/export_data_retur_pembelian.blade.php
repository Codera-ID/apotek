<html>

    <tbody>
        <tr>
            <td>{{$identitas->nama}}</td>
        </tr>
        <tr>        
            <td>No. Izin Praktek : {{$identitas->no_registrasi}}</td>
        </tr>  
        <tr>                      
            <td>{{$identitas->alamat}}</td>
        </tr>        
        <tr>            
            <td>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</td>
        </tr>
        <tr>
            <td align="center">LAPORAN REKAP RETUR PEMBELIAN OBAT</td>
        </tr>
        <tr>
            <td align="center">PERIODE TANGGAL {{date_format(new DateTime($start_date),'d F Y')}} s/d {{date_format(new DateTime($end_date),'d F Y')}}</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th class="center">No</th>
            <th>Tanggal Retur</th>
            <th>No. Faktur</th>
            <th>No. PO</th>
            <th>Jenis</th>
            <th>Supplier</th>
            <th>Grand Total</th>   
        </tr>
    </thead>

    <tbody>
      <?php $i=1;?>
      @foreach($data as $key)
        <tr>
          <td class="center">{{$i}}</td>
          <td>{{$key->tanggal_retur}}</td>
          <td>{{$key->no_faktur}}</td>
          <td>{{$key->no_po}}</td>
          <td>{{$key->jenis_pembayaran}}</td>
          <td>{{$key->nama_supplier}}</td>
          <td>{{number_format($key->total,2,",",".")}}</td>
        </tr>
        <?php $i++;?>
      @endforeach
    </tbody>

</html>