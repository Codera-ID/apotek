<html>

    <tbody>
        <tr>
            <td>{{$identitas->nama}}</td>
        </tr>
        <tr>        
            <td>No. Izin Praktek : {{$identitas->no_registrasi}}</td>
        </tr>  
        <tr>                      
            <td>{{$identitas->alamat}}</td>
        </tr>        
        <tr>            
            <td>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</td>
        </tr>
        <tr>
            <td align="center">LAPORAN REKAP DETAIL RETUR PEMBELIAN OBAT</td>
        </tr>
        <tr>
            <td align="center">PERIODE TANGGAL {{date_format(new DateTime($start_date),'d F Y')}} s/d {{date_format(new DateTime($end_date),'d F Y')}}</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th class="center">No</th>
            <th>Tanggal Retur</th>
            <th>No. Faktur</th>
            <th>No. PO</th>
            <th>Jenis</th>
            <th>Supplier</th>
            <th>Grand Total</th>   
        </tr>
    </thead>

    <tbody>
      <?php $i=1;?>
      @foreach($data as $key)
        <tr>
          <td class="center">{{$i}}</td>
          <td>{{$key->tanggal_retur}}</td>
          <td>{{$key->no_faktur}}</td>
          <td>{{$key->no_po}}</td>
          <td>{{$key->jenis_pembayaran}}</td>
          <td>{{$key->nama_supplier}}</td>
          <td>{{number_format($key->total,2,",",".")}}</td>
        </tr>
        <?php $i++;
        $retur_detail = DB::table('retur_pembelian_obat')->where('no_faktur',$key->no_faktur)
          ->join('detail_retur_pembelian', 'detail_retur_pembelian.no_retur_pembelian', '=', 'retur_pembelian_obat.no_retur_pembelian')
          ->join('obat', 'detail_retur_pembelian.kd_obat', '=', 'obat.kd_obat')
          ->join('satuan', 'detail_retur_pembelian.kd_satuan', '=', 'satuan.kd_satuan')->get();
         ?>
         <tr>
          <th class="center"></th>
          <th>Nama Obat</th>
          <th>Satuan</th>
          <th>Jumlah</th>
          <th>Potongan</th>
          <th>Total</th>            
        </tr>
        @foreach($retur_detail as $item)
          <tr data-key="0">
            <td></td>
            <td>{{$item->nama_obat}}</td>
            <td>{{$item->nama_sat}}</td>
            <td>{{$item->jmlh}}</td>
            <td>{{$item->potongan}}</td>
            <td>{{number_format($item->total,2,",",".")}}</td>
            <td></td>
          </tr>
        @endforeach
      @endforeach
    </tbody>

</html>