@extends('layouts.app')
@section('css')
<style>
.head hr {
    border: none;
    height: 2px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}

.table, table td, .table th , .table>thead>tr{ 
    background-color: #fff !important; 
    background: #fff !important;
}

.table-name {
    border-bottom:0px !important;
}
.table-name th, .table-name td {
    border: 1px !important;
}

body{
  background-color: #fff !important; 
  background: #fff !important;
}

hr {
    border: none;
    height: 1px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}
</style>
@stop
@section('content')
<div class="main-content-inner">
  <div class="page-content">
    <div>
        <table>
          <tr>
            <td style="padding-right:20px">
            <img style="width:80px;" data-src="{{('images/'.$identitas->logo )}}" alt="..." src="{{('images/'.$identitas->logo )}}">
            </td>
            <td>
            <h3><b>{{$identitas->nama}}</b></h3>
                <h5>No. Izin Praktek : {{$identitas->no_registrasi}}</h5>
                <h5>{{$identitas->alamat}}</h5>
                <h5>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</h5>   
            </td>
          </tr>
        </table>
    </div><!-- /.page-header -->
    <div class="head"><hr></div>
    <br>
    <h2 class="center"><b>LAPORAN REKAP RETUR PEMBELIAN OBAT</b></h2>
    <h2 class="center"><b>PERIODE TANGGAL {{date_format(new DateTime($start_date),'d F Y')}} s/d  {{date_format(new DateTime($end_date),'d F Y')}}</b></h2>
    <hr>
    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <table class="table table-bordered">
          <thead>
            <tr>
              <th class="center">No</th>
              <th>Tanggal Retur</th>
              <th>No. Faktur</th>
              <th>No. PO</th>
              <th>Supplier</th>
              <th>Jenis</th>
              <th>Total</th> 
            </tr>
          </thead>
          <tbody>
            <?php $i=1; $grand_total =0;?>
            @foreach($data as $key)
            <tr>
              <td class="center">{{$i}}</td>
              <td>{{$key->tanggal_retur}}</td>
              <td>{{$key->no_faktur}}</td>
              <td>{{$key->no_po}}</td>
              <td>{{$key->nama_supplier}}</td>
              <td>{{$key->jenis_pembayaran}}</td>
              <td>{{number_format($key->total,2,",",".")}}</td>
            </tr>
            <?php $i++; $grand_total += $key->total;?> 
            @endforeach
            <tr>
              <td colspan="7"></td>
            </tr>
            <tr>
              <td colspan="6" style="text-align:right;"><b>Total Retur :</b></td>
              <td style="text-align:right;"><b>{{number_format($grand_total,2,",",".")}}</b></td>
            </tr>
          </tbody>
        </table>
        <table class="table table-name">
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%">Tempat, {{date('d F Y')}}</td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%">Penanggung Jawab</td>
            </tr>
            <tr>
              <td colspan="3"></td>
            </tr>
            <tr>
              <td colspan="3"></td>
            </tr>
            <tr>
              <td colspan="3"></td>
            </tr>
            <tr>
              <td width="33%"></td>
              <td width="33%"></td>
              <td class="center" width="33%">{{ Auth::user()->name }}</td>
            </tr>
        </table>
        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('footer')
@stop