@extends('layouts.home')
@push('styles')
<link rel="stylesheet" href="{{asset('new_assets/vendor/datatables/media/css/datatables.bootstrap.css')}}">
@endpush

@section('content')
  <div class="page-title">
    <div class="title">Master Klinik</div>
    <div class="sub-title"><i class="ace-icon fa fa-angle-double-right"></i> Data Dokter</div>
  </div>
  @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
  @endif

  <div class="col-md-12">
    <div class="card bg-white">
      <div class="card-header">
        <div class="pull-left">Data Dokter</div>
          <div class="pull-right all-button">
            <a class="btn btn-primary btn-sm" href="{{url('dokter/tambah')}}"><i class="fa fa-plus"></i> Tambah</a>
            <!-- <a class="btn btn-primary btn-sm" href="{{url('dokter/print')}}"><i class="glyphicon glyphicon-print"></i> Print</a> -->
            <!-- <a class="btn btn-primary btn-sm" href="{{url('dokter/export')}}"><i class="fa fa-upload"></i> Export</a> -->
          </div>
      </div>
      <div class="card-block">
        <table id="dynamic-table" class="table table-bordered table-condensed datatable m-b-0 table-striped">
          <thead>
            <tr>
              <th class="center">#</th>
              <th>Nama</th>
              <th>Spesialis</th>
              <th>Alamat</th>
              <th>
                <i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
                Tanggal Mulai Tugas
              </th>
              <th class="hidden-480">Status</th>
              <th class="hidden-480">Action</th>
            </tr>
          </thead>

          <tbody>
            <?php $i=1; ?>
            @foreach($dokter as $key)
            <tr>
              <td class="center">{{$i}}</td>
              <td>{{$key->nama_dokter}}</td>
              <td>{{$key->spesialis}}</td>
              <td>{{$key->alamat}}</td>
              <td>{{$key->tanggal_mulai_tugas}}</td>
              <td>{{$key->status}}</td>
              <td>
                <div class="hidden-sm hidden-xs action-buttons">
                  <a class="blue" href="{{url('dokter/detail/'.$key->id_dokter)}}">
                    <i class="ace-icon fa fa-search-plus bigger-130"></i>
                  </a>

                  <a class="green" href="{{url('dokter/ubah/'.$key->id_dokter)}}">
                    <i class="ace-icon fa fa-pencil bigger-130"></i>
                  </a>

                  <a class="red" href="{{url('dokter/hapus/'.$key->id_dokter)}}">
                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                  </a>
                </div>
              </td>
            </tr>
            <?php $i++; ?>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>

@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  var myTable = $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
  });

} );
</script>
@stop
