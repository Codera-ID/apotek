@extends('layouts.home')
@section('content')
<div class="page-title">
  <div class="title">Master Klinik</div>
  <div class="sub-title"><i class="ace-icon fa fa-angle-double-right"></i> Detail Dokter</div>
</div>

<div class="col-md-12">
  <div class="card bg-white">
    <div class="card-header">
      <div class="pull-left">Detail Dokter</div>
        <div class="pull-right all-button">
          <a class="btn btn-primary btn-sm" href="{{url('dokter/tambah')}}"><i class="fa fa-plus"></i> Tambah</a>
          <!-- <a class="btn btn-primary btn-sm" href="{{url('dokter/print')}}"><i class="glyphicon glyphicon-print"></i> Print</a> -->
          <!-- <a class="btn btn-primary btn-sm" href="{{url('dokter/export')}}"><i class="fa fa-upload"></i> Export</a> -->
        </div>
    </div>
    <div class="card-block">
      <table id="dynamic-table" class="table table-bordered table-condensed datatable m-b-0 table-striped">
        <tbody>
          <tr>
            <th>Nama</th>
            <td>{{$dokter->nama_dokter}}</td>
          </tr>
          <tr>
            <th>Spesialis</th>
            <td>{{$dokter->spesialis}}</td>
          </tr>
          <tr>
            <th>Alamat</th>
            <td>{{$dokter->alamat}}</td>
          </tr>
          <tr>
            <th>Kota</th>
            <td>{{$dokter->kota}}</td>
          </tr>
          <tr>
            <th>No. Telepon</th>
            <td>{{$dokter->no_tlpn}}</td>
          </tr>
          <tr>
            <th>Email</th>
            <td>{{$dokter->email}}</td>
          </tr>
          <tr>
            <th>Tanggal Mulai Tugas</th>
            <td>{{$dokter->tanggal_mulai_tugas}}</td>
          </tr>
          <tr>
            <th>Tanggal Registrasi</th>
            <td>{{$dokter->created_at}}</td>
          </tr>
          <tr>
            <th>Status</th>
            <td>{{$dokter->status}}</td>
          </tr>
        </tbody>
      </table>
</br>
</br>
      <div class="row">

        <div class="col-md-12">

          <a href="{{url('dokter/ubah/'.$dokter->id_dokter)}}">
          <button class="btn btn-warning btn-sm btn-icon loading-demo mr5 pull-left" type="button">
            <i class="ace-icon fa fa-check"></i>
              Ubah
          </button>
        </a>

          <a href="{{url('dokter')}}">
          <button class="btn btn-danger btn-sm btn-icon loading-demo mr5 pull-right" type="button">
            <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
              Kembali
          </button>
        </a>
        </div>
  </div>

    </div>

  </div>
</div>

@stop
@section('js')
@stop
