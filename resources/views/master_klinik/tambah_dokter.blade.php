@extends('layouts.home')
@push('styles')
<link rel="stylesheet" href="{{asset('new_assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')}}">
@endpush

@section('content')
<div class="page-title">
  <div class="title">Master Klinik</div>
  <div class="sub-title"><i class="ace-icon fa fa-angle-double-right"></i> Tambah Dokter</div>
</div>

<div class="col-md-12">
  <div class="card bg-white">
    <div class="card-header">
      Tambah Data Dokter
    </div>
    <div class="card-block">
      <div class="row m-a-0">
        <div class="col-lg-12">
          <!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('dokter/tambah')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="col-sm-6">

    						<div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Nama </label>
    								<div class="col-sm-8">
    									<input type="text" id="form-field-1" name="nama_dokter" class="form-control" required>
    								</div>
    						</div>

                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Spesialis </label>
    								<div class="col-sm-8">
    									<input type="text" id="form-field-1" name="spesialis_dokter" class="form-control" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Alamat </label>
    								<div class="col-sm-8">
    									<textarea name="alamat" class="form-control"></textarea>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Kota </label>
    								<div class="col-sm-8">
    									<input type="text" id="form-field-1" name="kota" class="form-control" required>
    								</div>
    						</div>

    						<div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1">No. Telepon </label>
    								<div class="col-sm-8">
    									<input type="text" id="form-field-1" name="no_tlpn"class="form-control" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Email </label>
    								<div class="col-sm-8">
    									<input type="email" id="form-field-1" name="email" class="form-control" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Tanggal Mulai Tugas </label>
    								<div class="col-sm-8">
                      <input type="text" id="form-field-1" name="tgl_mulai" class="form-control m-b" data-provide="datepicker" placeholder="">
    									<!--<input type="text" id="form-field-1" name="tgl_mulai" class="datepicker form-control" required>-->
    								</div>
    						</div>
              </div>
          </div>
						<div class="clearfix"></div>
							<div class="col-md-12">
                <button type="submit" class="btn btn-success btn-sm btn-icon loading-demo mr5">
                  <i class="fa fa-check mr5"></i>
                  <span>Submit</span>
                </button>

                <button type="reset" class="btn btn-default btn-sm btn-icon loading-demo mr5">
                  <i class="fa fa-undo mr5"></i>
                  <span>&nbsp;&nbsp;Reset&nbsp;</span>
                </button>

                <a href="{{url('dokter')}}">
                <button class="btn btn-danger btn-sm btn-icon loading-demo mr5 pull-right" type="button">
                  <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
                    Kembali
                </button>
              </a>
							</div>



			      </form>
						<!-- PAGE CONTENT ENDS -->
        </div>
      </div>
    </div>
  </div>
</div>

@stop
@section('js')
<script src="{{asset('new_assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript">
  $(document).ready( function () {

		$('.datepicker').datepicker({
				format: 'yyyy-mm-dd',
		});
  });
</script>
@stop
