@extends('layouts.home')
@section('css')
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.min.css')}}" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Keuangan</a>
      </li>
      <li class="active">Data Jurnal Keuangan</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Data Jurnal Keuangan</h1>
          <div class="pull-right">
            <a class="btn btn-primary btn-sm" href="{{url('jurnal-keuangan/tambah-jurnal-keuangan')}}"><i class="fa fa-plus"></i>  Tambah</a>
          </div>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <form class="form-inline" action="{{url('jurnal-keuangan')}}" method="get" style="margin:10px 0px">
              <label>Pilihan Periode</label>
              <div class="input-daterange">
                <div class="form-group">
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="start" class="form-control" id="rangepicker6"  value="{{ $start }}"/>
                  </div>
                </div>
                <div class="form-group" style="margin-left:10px">
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="end" class="form-control" id="rangepicker7" value="{{ $end }}"/>
                  </div>
                </div>
                <button type="submit" class="btn btn-sm btn-default"> <i class="fa fa-search"></i> Cari </button>              
              </div>
            </form>
            <div>
              <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Tanggal</th>
                    <th>No. Bukti</th>
                    <th>Keterangan</th>
                    <th>Nominal</th>
                    <th>Petugas</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>
                @foreach($data as $jurnals)
                  <tr>
                    <td>{{$jurnals->tanggal}}</td>
                    <td>{{$jurnals->nobukti}}</td>
                    <td>{{$jurnals->deskripsi}}</td>
                    <td>{{$jurnals->nominal}}</td>
                    <td>{{$jurnals->name}}</td>
                    <td>Aktif</td>
                    <td>
                        <a class="red" href="{{url('jurnal-keuangan/hapus/'.$jurnals->kd_jurnal)}}">
                          <i class="ace-icon fa fa-trash-o bigger-130"></i>
                        </a>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
});
} );
    //or change it into a date range picker
    $('.input-daterange').datepicker({
      autoclose:true,
      format: 'yyyy-mm-dd'
    });

    //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
    $('input[name=date-range-picker]').daterangepicker({
      'applyClass' : 'btn-sm btn-success',
      'cancelClass' : 'btn-sm btn-default',
      locale: {
      applyLabel: 'Apply',
      cancelLabel: 'Cancel',
      }
      })
      .prev().on(ace.click_event, function(){
      $(this).next().focus();
    });
</script>
@stop
