@extends('layouts.home')
@section('css')
<link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Keuangan</a>
      </li>
      <li class="active">Data Jurnal Keuangan</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
          <h1>
            Tambah Jurnal Keuangan

          </h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <form class="form-horizontal" action="{{url('jurnal-keuangan/tambah-jurnal-keuangan')}}" method="post">
            {{csrf_field()}}
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Tanggal</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px;" name="saldo_normal">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="tanggal" class="form-control" id="rangepicker6"/>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label no-padding-right">No. Bukti</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px">
                    <input type="text" name="no_bukti" value="" class="form-control">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Jurnal Singkat</label>
                <div class="col-sm-9">
                  <select class="chosen-select col-sm-7" id="shortjurnal" name="jurnal_id">
                    <option>Pilih Jurnal Singkat</option>
                    @foreach($jurnal as $jurnal)
                      <option value="{{$jurnal->id_jurnal}}">{{$jurnal->deskripsi}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Deskripsi</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px;" name="saldo_normal">
                    <textarea name="deskripsi" rows="5" cols="80" class="form-control"></textarea>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Akun Debit</label>
                <div class="col-sm-9">
                  <select class="chosen-select col-sm-7" id="akundebit" name="akun_debit">
                    <option>Pilih Akun Debit</option>

                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Akun Kredit</label>
                <div class="col-sm-9">
                  <select class="chosen-select col-sm-7" id="akunkredit" name="akun_kredit">
                    <option>Pilih Akun Kredit</option>

                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label no-padding-right">Nominal</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px">
                    <input type="text" name="nominal" value="" class="form-control">
                  </div>
                </div>
              </div>

              <input type="submit" name="" value="Tambah" class="btn btn-success">
              <input type="reset" name="" value="Reset" class="btn btn-default">
            </form>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
    $("#rangepicker6").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });

    $('#shortjurnal').change(function() {
      $.getJSON("{{url('jurnal-keuangan/get-short-jurnal?id=')}}"+$(this).val(), function(data) {
          var debit = $('#akundebit');
          var credit = $('#akunkredit');
          debit.empty();
          credit.empty();

          $.each(data.debit, function(index, element) {
            console.log(element);            
            debit.append("<option value='"+ element.kode_akun +"'>" + element.kode_akun + " / " + element.nama_akun + "</option>");
          });

          debit.attr("disabled",true);

          $.each(data.kredit, function(index, element) {
            console.log(element);            
            credit.append("<option value='"+ element.kode_akun +"'>" + element.kode_akun + " / " + element.nama_akun + "</option>");
          });

          credit.attr("disabled",true);          
      });
    });
</script>
@stop
