@extends('layouts.home')
@section('css')
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Keuangan</a>
      </li>
      <li class="active">Laporan Tipe Akun</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Laporan Tipe Akun</h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <div class="col-sm-12">
              <form class="form-inline pull-left" action="index.html" method="post" style="margin:10px 0px" >
                <label>Pilihan Periode</label>
                <div class="form-group">
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="trf" class="form-control" id="rangepicker6"/>
                  </div>
                </div>
                <div class="form-group" style="margin-left:10px">
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="trf" class="form-control" id="rangepicker7"/>
                  </div>
                </div>
                <button type="submit" name="button" class="btn btn-sm btn-default"> <i class="fa fa-search"></i> Cari </button>
              </form>
              <div class="pull-right">
                <a class="btn btn-warning btn-sm" href="{{url('akun/print')}}"><i class="glyphicon glyphicon-print"></i> Print</a>
                <a class="btn btn-success btn-sm" href="{{url('akun/print')}}"><i class="glyphicon glyphicon-print"></i> Excel</a>
              </div>
            </div>

            <div class="col-sm-12" style="padding:0px">
              <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                  
                </thead>

                <tbody>
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
});
} );
$("#rangepicker6").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
    $("#rangepicker7").daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        });
</script>
@stop
