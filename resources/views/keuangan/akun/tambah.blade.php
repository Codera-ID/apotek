@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Keuangan</a>
      </li>
      <li class="active">Data Akun Keungan</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
          <h1>
            Tambah Akun Keuangan

          </h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <form class="form-horizontal" action="{{url('akun/tambah-akun/post')}}" method="post">
              {{csrf_field()}}
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Kelompok Akun</label>
                <div class="col-sm-9">
                  <select class="chosen-select col-sm-7" id="kelompokakun" name="kelompok_akun">
                    <option>Pilih Kelompok Akun</option>
                    @foreach($kelompokakun as $kelompok)
                    <option value="{{$kelompok->kode_kelompok_akun}}">{{$kelompok->nama_kelompok_akun}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Pos Akun</label>
                <div class="col-sm-9">
                  <div class="col-sm-6" style="padding:0px" style="width:414px;">
                    <select class="form-control" id="posakun" name="posakun">
                      <option value=""></option>
                    </select>
                  </div>
                  <span class="input-group-btn">
                    <button type="button" id="tambah" name="button" class="btn btn-primary btn-sm" style="padding:4px 9px; margin-left:30px;"> <i class="fa fa-plus"></i></button>
                  </span>
                </div>
              </div>
              <div class="form-group" id="boom">
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Saldo Normal</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px;" name="saldo_normal">

                    <input type="text" id="saldo"class="form-control" name="saldo_normal"readonly/>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Kode Akun</label>
                <div class="col-sm-9">
                  <div class="col-sm-3" style="padding:0px;" name="saldo_normal">
                    <input type="text" id="idposakun" class="form-control" name="idposakun" readonly/>
                  </div>
                  
                  <div class="col-sm-4" name="saldo_normal">
                    <input type="text" id="kodeakun" class="form-control" name="kodeakun"/>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Nama Akun</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px;">

                    <input type="text" id="saldo"class="form-control" name="nama_akun"/>
                  </div>
                </div>
              </div>
              <input type="submit" name="" value="Tambah" class="btn btn-success">
            </form>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')

<script type="text/javascript">

$('#kelompokakun').change(function() {
  $.getJSON("{{url('/kelompok-akun?id=')}}"+$(this).val(), function(data) {

      var location = $('#posakun');
      location.empty();

      location.append("<option value='#'>Pilih Pos Akun</option>");      
      $.each(data, function(index, element) {
          location.append("<option value='"+ element.kode_pos_akun +"'>" + element.nama_pos_akun + "</option>");
      });
 });
 $.getJSON("{{url('/saldo?id=')}}"+$(this).val()+"kode=0", function(data) {
     var location = $('#saldo');
     location.empty();
     $.each(data, function(index, element) {
         location.val(data.saldo_normal);
     });
  });
});
$('#posakun').change(function() {
    var posakun = $(this).val();
    console.log(posakun);
    $("#idposakun").val(posakun);
    $.getJSON("{{url('/saldo?id=')}}"+$(this).val()+"&kode=1&posakun="+posakun, function(data) {
    
    });
});
$('#tambah').click(function() {
  var location = $('#boom');
  location.empty();
  $('#posakun').attr("disabled",true);
  location.append('<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Nama Pos Akun</label>');
  location.append('<div class="col-sm-9"><input type="text" name="posakun2" class=" col-sm-7" /></div>');
});
</script>
@stop
