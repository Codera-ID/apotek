@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Keuangan</a>
      </li>
      <li class="active">Data Akun Keuangan</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Data Akun Keuangan</h1>
          <div class="pull-right">
            <a class="btn btn-primary btn-sm" href="{{url('akun/tambah-akun')}}"><i class="fa fa-plus"></i>  Tambah</a>
          </div>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <div>
              <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="center">No.</th>
                    <th>Kelompok Akun</th>
                    <th>Pos Akun</th>
                    <th>Kode Akun</th>
                    <th>Nama Akun</th>
                    <th>Saldo Normal</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $i=1; ?>
                  @foreach($akun as $akuns)
                  <tr>
                    <td class="center">{{$i}}</td>
                    <td>{{$akuns->nama_kelompok_akun}}</td>
                    <td>{{$akuns->nama_pos_akun}}</td>
                    <td>{{$akuns->kode_akun}}</td>
                    <td>{{$akuns->nama_akun}}</td>
                    <td>{{$akuns->saldo_normal}}</td>
                    <td>{{$akuns->status}}</td>
                    <td>
                        <a class="green" href="{{url('akun/ubah-akun/'.$akuns->kode_akun)}}">
                          <i class="ace-icon fa fa-pencil bigger-130"></i>
                        </a>

                        <a class="red" href="{{url('akun/hapus-akun/'.$akuns->kode_akun)}}">
                          <i class="ace-icon fa fa-trash-o bigger-130"></i>
                        </a>
                    </td>
                  </tr>
                  <?php $i++; ?>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  var myTable = $('#dynamic-table').DataTable({
    "aoColumns": [
            { "bSortable": false },
            null, null,null, null, null, null,
            { "bSortable": false }
        ],
        "aaSorting": [],
  });

  $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
    
    new $.fn.dataTable.Buttons( myTable, {
      buttons: [
        {
        "extend": "csv",
        "text": "<i class='fa fa-database bigger-110 orange'></i> <span class=''>Export</span>",
        "className": "btn btn-white btn-primary btn-bold"
        },
        {
        "extend": "excel",
        "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
        "className": "btn btn-white btn-primary btn-bold"
        },
        {
        "extend": "print",
        "text": "<i class='fa fa-print bigger-110 grey'></i> <span class=''>Print</span>",
        "className": "btn btn-white btn-primary btn-bold",
        autoPrint: false,
        message: 'This print was produced using the Print button for DataTables'
        }		  
      ]
    } );
    myTable.buttons().container().appendTo( $('.tableTools-container') );
} );
</script>
@stop
