@extends('layouts.home')
@section('css')
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.min.css')}}" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Keuangan</a>
      </li>
      <li class="active">Laporan Buku Besar</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Laporan Buku Besar</h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <div class="col-sm-12">
              <form class="form-inline" action="{{url('laporan-buku-besar')}}" method="get" style="margin:10px 0px">
                <label>Pilihan Periode</label>
                <div class="input-daterange">
                  <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="start" class="form-control" id="rangepicker6"  value="{{ $start }}"/>
                    </div>
                  </div>
                  <div class="form-group" style="margin-left:10px">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="end" class="form-control" id="rangepicker7" value="{{ $end }}"/>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-sm btn-default"> <i class="fa fa-search"></i> Cari </button>              
                </div>
              </form>
              <div class="pull-right tableTools-container"></div>
            </div>
            <div class="col-sm-12" style="padding:0px">
              <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Kelompok Akun</th>
                    <th>Akun Pos</th>
                    <th>Kode</th>
                    <th>Nama Akun</th>
                    <th>Tanggal</th>
                    <th>No</th>
                    <th>Keterangan</th>
                    <th>Debit</th>
                    <th>Kredit</th>
                    <th>Status</th>
                  </tr>
                </thead>

                <tbody>
                <?php $i=1; ?>
                    @foreach($data as $key)
                    <tr>
                      <td class="center">{{$i}}</td>
                      <td>{{$key->kelompok_akun}}</td>
                      <td>{{$key->akun_pos}}</td>
                      <td>{{$key->kode_akun}}</td>
                      <td>{{$key->nama_akun}}</td>
                      <td>{{$key->tanggal}}</td>
                      <td>{{$key->nobukti}}</td>
                      <td>{{$key->deskripsi}}</td>                      
                      <td>{{$key->nominal_debit}}</td>
                      <td>{{$key->nominal_kredit}}</td>                      
                      <td>Aktif</td>
                    </tr>
                    <?php $i++; ?>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  var myTable = $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
  });

  $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
  new $.fn.dataTable.Buttons( myTable, {
    buttons: [
      {
      "extend": "csv",
      "text": "<i class='fa fa-database bigger-110 orange'></i> <span class=''>Export</span>",
      "className": "btn btn-white btn-primary btn-bold"
      },
      {
      "extend": "excel",
      "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
      "className": "btn btn-white btn-primary btn-bold"
      },
      {
      "extend": "print",
      "text": "<i class='fa fa-print bigger-110 grey'></i> <span class=''>Print</span>",
      "className": "btn btn-white btn-primary btn-bold",
      autoPrint: false,
      message: 'This print was produced using the Print button for DataTables'
      }		  
    ]
  } );
  myTable.buttons().container().appendTo( $('.tableTools-container') );
} );

//or change it into a date range picker
  $('.input-daterange').datepicker({
      autoclose:true,
      format: 'yyyy-mm-dd'
    });

    //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
    $('input[name=date-range-picker]').daterangepicker({
      'applyClass' : 'btn-sm btn-success',
      'cancelClass' : 'btn-sm btn-default',
      locale: {
      applyLabel: 'Apply',
      cancelLabel: 'Cancel',
      }
      })
      .prev().on(ace.click_event, function(){
      $(this).next().focus();
});
</script>
@stop
