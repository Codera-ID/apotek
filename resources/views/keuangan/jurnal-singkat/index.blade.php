@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Keuangan</a>
      </li>
      <li class="active">Data Jurnal Singkat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Data Jurnal Singkat</h1>
          <div class="pull-right">
            <a class="btn btn-primary btn-sm" href="{{url('jurnal-singkat/tambah-jurnal-singkat')}}"><i class="fa fa-plus"></i>  Tambah</a>
          </div>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <div>
              <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Deskripsi</th>
                    <th>Jenis</th>
                    <th>Akun Debit</th>
                    <th>Akun Kredit</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>
                  @foreach($jurnal as $jurnals)
                  <tr>
                    <td>{{$jurnals->deskripsi}}</td>
                    <td>{{$jurnals->jenis}}</td>
                    <td>{{$jurnals->kode_akun_debit}}</td>
                    <td>{{$jurnals->kode_akun_kredit}}</td>
                    <td>Aktif</td>
                    <td>
                        <a class="red" href="{{url('jurnal-singkat/hapus/'.$jurnals->id_jurnal)}}">
                          <i class="ace-icon fa fa-trash-o bigger-130"></i>
                        </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
});
} );
</script>
@stop
