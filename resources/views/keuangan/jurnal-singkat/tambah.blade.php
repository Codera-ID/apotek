@extends('layouts.home')
@section('css')
<link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />

@stop
@section('content')
<style media="screen">
.chosen-container>.chosen-single, [class*=chosen-container]>.chosen-single{
  background: #fff;
}
  .chosen-container-single .chosen-single{
    background: #fff;
  }
</style>
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Tables</a>
      </li>
      <li class="active">Simple &amp; Dynamic</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
          <h1>
            Tambah Jurnal Singkat
          </h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <form class="form-horizontal" action="{{url('jurnal-singkat/tambah-jurnal-singkat')}}" method="post">
              {{csrf_field()}}
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Deskripsi</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px;" name="saldo_normal">
                    <textarea name="deskripsi" rows="5" cols="80" class="form-control"></textarea>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Jenis</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px;">
                    <select class=" form-control" id="jenis" name="jenis">
                      <option value="Penerimaan Kas">Penerimaan Kas</option>
                      <option value="Pengeluaran Kas">Pengeluaran Kas</option>
                      <option value="Transaksi Lain">Transaksi Lain</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Akun Debit</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px;">
                    <select class="chosen-select form-control" name="akun_debit">
                      @foreach($debit as $akunks)
                      <option value="{{$akunks->kode_akun}}">{{$akunks->kode_akun}} / {{$akunks->nama_akun}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Akun Kredit</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px;">
                    <select class="chosen-select form-control" id="kelompokakun" name="akun_kredit">
                      @foreach($kredit as $akunks)
                      <option value="{{$akunks->kode_akun}}">{{$akunks->kode_akun}} / {{$akunks->nama_akun}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <input type="submit" name="" value="Tambah" class="btn btn-success">
              <input type="reset" name="" value="Reset" class="btn btn-default">
            </form>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>
<script type="text/javascript">
  $('.chosen-select').chosen();
</script>
@stop
