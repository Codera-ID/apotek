@extends('layouts.home')
@section('css')
<link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Tables</a>
      </li>
      <li class="active">Simple &amp; Dynamic</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
          <h1>
            Tambah Akun Keuangan

          </h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <form class="form-horizontal" action="index.html" method="post">
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Tanggal</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px;" name="saldo_normal">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="tanggal" class="form-control" id="rangepicker6"/>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label no-padding-right">No. Bukti</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px">
                    <input type="text" name="bukti" value="" class="form-control">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Jurnal Singkat</label>
                <div class="col-sm-9">
                  <select class="chosen-select col-sm-7" id="kelompokakun" name="kelompok_akun">
                    <option>Pilih Jurnal Singkat</option>

                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Deskripsi</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px;" name="saldo_normal">
                    <textarea name="deskripsi" rows="5" cols="80" class="form-control"></textarea>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Akun Debit</label>
                <div class="col-sm-9">
                  <select class="chosen-select col-sm-7" id="kelompokakun" name="kelompok_akun">
                    <option>Pilih Akun Debit</option>

                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Akun Kredit</label>
                <div class="col-sm-9">
                  <select class="chosen-select col-sm-7" id="kelompokakun" name="kelompok_akun">
                    <option>Pilih Akun Kredit</option>

                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label no-padding-right">Nominal</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px">
                    <input type="text" name="bukti" value="" class="form-control">
                  </div>
                </div>
              </div>

              <input type="submit" name="" value="Tambah" class="btn btn-success">
              <input type="reset" name="" value="Reset" class="btn btn-default">
            </form>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script type="text/javascript">
$('.chosen-select').chosen();
    $("#rangepicker6").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
</script>
@stop
