@extends('layouts.home')
@section('css')
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Keuangan</a>
      </li>
      <li class="active">Laporan Pajak</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Laporan Pajak</h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <div class="col-sm-12">
              <form class="form-inline pull-left" action="{{url('laporan-pajak')}}" method="get" style="margin:10px 0px" >
                <label>Pilihan Periode</label>
                <div class="form-group">
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="trf" class="form-control" id="rangepicker6"/>
                  </div>
                </div>
                <div class="form-group" style="margin-left:10px">
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="trf" class="form-control" id="rangepicker7"/>
                  </div>
                </div>
                <button type="submit" class="btn btn-sm btn-default"> <i class="fa fa-search"></i> Cari </button>
              </form>
              <div class="pull-right tableTools-container"></div>
            </div>

            <div class="col-sm-12" style="padding:0px">
              <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Tanggal</th>
                    <th>No. Faktur</th>
                    <th>Nama Pasien</th>
                    <th>Nama Dokter</th>
                    <th>Grand Total</th>
                    <th>Pajak (%)</th>
                    <th>Nominal Pajak</th>
                    <th>Petugas</th>
                  </tr>
                </thead>

                <tbody>
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  var myTable = $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
  });

  $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
  new $.fn.dataTable.Buttons( myTable, {
    buttons: [
      {
      "extend": "csv",
      "text": "<i class='fa fa-database bigger-110 orange'></i> <span class=''>Export</span>",
      "className": "btn btn-white btn-primary btn-bold"
      },
      {
      "extend": "excel",
      "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
      "className": "btn btn-white btn-primary btn-bold"
      },
      {
      "extend": "print",
      "text": "<i class='fa fa-print bigger-110 grey'></i> <span class=''>Print</span>",
      "className": "btn btn-white btn-primary btn-bold",
      autoPrint: false,
      message: 'This print was produced using the Print button for DataTables'
      }		  
    ]
  } );
  myTable.buttons().container().appendTo( $('.tableTools-container') ); 
});
$("#rangepicker6").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
    $("#rangepicker7").daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        });
</script>
@stop
