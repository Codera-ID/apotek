@extends('layouts.home')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/material-design-icons/css/material-design-iconic-font.min.css')}}"/>
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Sistem</a>
      </li>
      <li class="active">Data Jadwal Shift</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Data Jadwal Shift</h1>
          <div class="pull-right">
            <a class="btn btn-primary btn-sm" href="{{url('data-jadwal-shift/tambah')}}"><i class="fa fa-plus"></i>  Tambah</a>
          </div>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <div>
              <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Action</th>
                    <th>Nama</th>
                    <th>Jam Mulai</th>
                    <th>Jam Selesai</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  @foreach($shift as $shift)
                  <tr>
                    <td>{{$i++}}</td>
                    <td><a  title="edit"  href="{{url('data-jadwal-shift/edit/'.$shift->id_js)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>/
                       <a href="#md-footer-warning-{{$shift->id_js}}" title="hapus" data-toggle="modal"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                    <td>{{$shift->nama}}</td>
                    <td>{{$shift->mulai}}</td>
                    <td>{{$shift->selesai}}</td>
                  </tr>
                  <div id="md-footer-warning-{{$shift->id_js}}" tabindex="-1" role="dialog" class="modal fade">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                        </div>
                        <div class="modal-body">
                          <div class="text-center">
                            <div class="text-warning" style="color:#FFB752"><span class="modal-main-icon mdi mdi-alert-triangle" style="font-size: 80px;"></span></div>
                            <h3>Warning!</h3>
                            <p>Yakin Data Akan Di Hapus?</p>
                            <div class="xs-mt-50">

                              <form class="" action="{{url('/data-jadwal-shift/delete/'.$shift->id_js)}}" method="post">
                                {{csrf_field()}}
                                <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                                <button type="submit"  class="btn btn-space btn-primary">Proceed</button>
                              </form>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
});
} );
</script>
@stop
