@extends('layouts.home')
@section('css')
<link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-timepicker.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Sistem</a>
      </li>
      <li class="active">Ubah Jadwal Shift</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
          <h1>
            Ubah Jadwal Shift

          </h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <form class="form-horizontal" action="{{url('data-jadwal-shift/edit/'.$shift->id_js)}}" method="post">
              {{csrf_field()}}
              <div class="form-group">
                <label for="" class="col-sm-3 control-label no-padding-right">Nama</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px">
                    <input type="text" name="nama" value="{{$shift->nama}}" class="form-control" >
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label no-padding-right">Mulai</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px">
                    <div class="input-group bootstrap-timepicker">
                      <input id="timepicker1" type="text" class="form-control" name="mulai" value="{{$shift->mulai}}" />
                      <span class="input-group-addon">
                        <i class="fa fa-clock-o bigger-110"></i>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label no-padding-right">Selesai</label>
                <div class="col-sm-9">
                  <div class="col-sm-7" style="padding:0px">
                    <div class="input-group bootstrap-timepicker">
                      <input id="timepicker2" type="text" class="form-control" name="selesai" value="{{$shift->selesai}}"/>
                      <span class="input-group-addon">
                        <i class="fa fa-clock-o bigger-110"></i>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <input type="submit" name="" value="Tambah" class="btn btn-success">
              <input type="reset" name="" value="Reset" class="btn btn-default">
            </form>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')

<script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-timepicker.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>

<script type="text/javascript">
$('.chosen-select').chosen();
    $("#rangepicker6").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
    $('#timepicker1').timepicker({
      minuteStep: 1,
      showSeconds: true,
      showMeridian: false,
      disableFocus: true,
      icons: {
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down'
      }
    }).on('focus', function() {
      $('#timepicker1').timepicker('showWidget');
    }).next().on(ace.click_event, function(){
      $(this).prev().focus();
    });
    $('#timepicker2').timepicker({
      minuteStep: 1,
      showSeconds: true,
      showMeridian: false,
      disableFocus: true,
      icons: {
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down'
      }
    }).on('focus', function() {
      $('#timepicker2').timepicker('showWidget');
    }).next().on(ace.click_event, function(){
      $(this).prev().focus();
    });
</script>
@stop
