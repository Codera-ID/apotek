@extends('layouts.home')
@section('css')
<link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
<link href="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.css')}}" rel="stylesheet" />
@stop
@section('content')
<style media="screen">
  label{
    font-weight: bold;
  }
</style>
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Sistem</a>
      </li>
      <li class="active">Identitas</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
          <h1>
            Data Identitas Apotek

          </h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <form class="form-horizontal" action="{{url('identitas/'.$ident->id)}}" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
              
              <div class="col-sm-12">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Nama</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <input type="text" name="nama" value="{{$ident->nama}}" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">No. Registrasi</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <input type="text" name="registrasi" value="{{$ident->no_registrasi}}" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Pemilik</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <input type="text" name="pemilik" value="{{$ident->pemilik}}" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Penanggung Jawab</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <input type="text" name="pj" value="{{$ident->penanggung_jawab}}" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Penanggung Jawab Laboratorium</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <input type="text" name="pj_lab" value="{{$ident->penanggung_jawab_lab}}" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 no-padding-right" for="form-field-1">Alamat</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px;" name="saldo_normal">
                      <textarea name="alamat" rows="5" cols="80" class="form-control">{{$ident->alamat}}</textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Kota</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <input type="text" name="kota" value="{{$ident->kota}}" class="form-control">
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="col-sm-2 no-padding-right">Telepon</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <input type="text" name="telp" value="{{$ident->telepon}}" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 no-padding-right">Email</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <input type="text" name="email" value="{{$ident->email }}" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Website</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <input type="text" name="website" value="{{$ident->website}}" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Logo</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                              <img data-src="{{('images/'.$ident->logo )}}" alt="..." src="{{('images/'.$ident->logo )}}"></div>
                          <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                          <div>
                              <span class="btn btn-default btn-file">
                                  <span class="fileinput-new">Select image</span>
                                  <span class="fileinput-exists">Change</span>
                                  <input type="file" name="img"></span>
                              <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
              <input type="submit" name="" value="Ubah" class="btn btn-success">
              <input type="reset" name="" value="Reset" class="btn btn-default">
            </form>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script src="{{asset('vendors/holder/holder.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.js')}}"></script>
<script type="text/javascript">
$('.chosen-select').chosen();
    $("#rangepicker6").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
</script>
@stop
