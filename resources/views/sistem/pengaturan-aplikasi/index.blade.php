@extends('layouts.home')
@section('css')
<link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Sistem</a>
      </li>
      <li class="active">Pengaturan Aplikasi</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
          <h1>
            Pengaturan Aplikasi

          </h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-sm-12 widget-container-col">
            <div class="widget-box" id="widget-box-10">
              <div class="widget-header widget-header-small">
                <div class="widget-toolbar no-border">
                  <ul class="nav nav-tabs" id="myTab">
                    <li class="active">
                      <a data-toggle="tab" href="#cetak">Cetak Faktur</a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#notifikasi">Notifikasi</a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#informasi">Informasi</a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#database">Database</a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#tuslah">Tuslah dan Embalase</a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#harga">Harga Jual PPN</a>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="widget-body">
                <div class="widget-main padding-6">
                  <div class="tab-content">
                    <div id="cetak" class="tab-pane in active">
                      <form id="pengaturan-cetak" class="form-horizontal" role="form" action="{{url('pengaturan-aplikasi/simpan-cetak-faktur')}}">
                        <div class="row">
                          <div class="form-group col-sm-4">
                            <label class="col-sm-5 control-label" for="form-field-1">Purchase Order</label>

                            <div class="col-sm-7">
                              <select class="form-control" id="po-cetak" name="purchase_order">
                              @if ($pengaturan->purchase_order == 0)
                                <option value="0" selected>INVOICE (A5)</option>
                                <option value="1">Tanpa Cetak</option>
                              @elseif ($pengaturan->
                              purchase_order == 1)
                              <option value="0">INVOICE (A5)</option>
                                <option value="1" selected>Tanpa Cetak</option>
                              @endif
                              </select>
                            </div>
                          </div>

                          <div class="form-group col-sm-3">
                            <label class="col-sm-5 control-label no-padding-right" for="form-field-1-1">Margin Kiri</label>

                            <div class="col-sm-7">
                              <input type="text" id="po-kiri" placeholder="Ukuran CM" name="mki_po" value="{{$pengaturan->mki_po}}"class="form-control" />
                            </div>
                          </div>

                          <div class="form-group col-sm-3">
                            <label class="col-sm-5 control-label no-padding-right" for="form-field-1-1">Margin Kanan</label>

                            <div class="col-sm-7">
                              <input type="text" id="po-kanan" placeholder="Ukuran CM" name="mka_po" value="{{$pengaturan->mka_po}}" class="form-control" />
                            </div>
                          </div>
                          <div class="form-group col-sm-2">
                            <span class="">
                                <label class="middle">
                                  @if ($pengaturan->catatan_po != 0)
                                  <input class="ace" type="checkbox" id="po-tampilkan-catatan" name="catatan_po" value ="0" checked/>
                                  <span class="lbl"> Tampilkan Catatan!</span>
                                  @else
                                  <input class="ace" type="checkbox" id="po-tampilkan-catatan" value="1" name="catatan_po"/>
                                  <span class="lbl"> Tampilkan Catatan!</span>
                                  @endif
                                </label>
                              </span>
                          </div> 
                        </div>
                        <div class="row">
                          <div class="form-group col-sm-4">
                            <label class="col-sm-5 control-label" for="form-field-1">Retur Pembelian Obat</label>

                            <div class="col-sm-7">
                               <select class="form-control" name="retur_pembelian_obat">
                              @if ($pengaturan->retur_pembelian_obat == 0)
                                <option value="0" selected>INVOICE (A5)</option>
                                <option value="1">Tanpa Cetak</option>
                              @elseif ($pengaturan->
                              retur_pembelian_obat == 1)
                              <option value="0">INVOICE (A5)</option>
                                <option value="1" selected>Tanpa Cetak</option>
                              @endif
                              </select>
                            </div>
                          </div>

                          <div class="form-group col-sm-3">
                            <label class="col-sm-5 control-label no-padding-right" for="form-field-1-1">Margin Kiri</label>

                            <div class="col-sm-7">
                              <input type="text" id="retur-pembelian-kiri" value="{{$pengaturan->mki_retur_pembelian}}" placeholder="Ukuran CM" class="form-control" />
                            </div>
                          </div>

                          <div class="form-group col-sm-3">
                            <label class="col-sm-5 control-label no-padding-right" for="form-field-1-1">Margin Kanan</label>

                            <div class="col-sm-7">
                              <input type="text" id="retur-pembelian-kanan" value="{{$pengaturan->mka_retur_pembelian}}" placeholder="Ukuran CM" class="form-control" />
                            </div>
                          </div>
                          <div class="form-group col-sm-2">
                            <span class="">
                                <label class="middle">
                                  @if ($pengaturan->catatan_retur_pembelian != 0)
                                  <input class="ace" type="checkbox" id="po-tampilkan-catatan" name="catatan_retur_pembelian" value="0" checked/>
                                  <span class="lbl"> Tampilkan Catatan!</span>
                                  @else
                                  <input class="ace" type="checkbox" id="po-tampilkan-catatan" value="1" name="catatan_retur_pembelian"/>
                                  <span class="lbl"> Tampilkan Catatan!</span>
                                  @endif
                                </label>
                              </span>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-sm-4">
                            <label class="col-sm-5 control-label" for="form-field-1">Penjualan Obat</label>

                            <div class="col-sm-7">
                              <select class="form-control" id="penjualan-obat-cetak" name="retur_penjualan_obat">
                                @if ($pengaturan->penjualan_obat == 0)
                                <option value="0" selected>INVOICE (A5)</option>
                                <option value="1">Tanpa Cetak</option>
                                @elseif ($pengaturan->
                                  penjualan_obat == 1)
                                  <option value="0">INVOICE (A5)</option>
                                  <option value="1" selected>Tanpa Cetak</option>
                                @endif
                              </select>
                            </div>
                          </div>

                          <div class="form-group col-sm-3">
                            <label class="col-sm-5 control-label no-padding-right" for="form-field-1-1">Margin Kiri</label>

                            <div class="col-sm-7">
                              <input type="text" id="penjualan-obat-kiri" value="{{$pengaturan->mki_penjualan}}" placeholder="Ukuran CM" class="form-control" />
                            </div>
                          </div>

                          <div class="form-group col-sm-3">
                            <label class="col-sm-5 control-label no-padding-right" for="form-field-1-1">Margin Kanan</label>

                            <div class="col-sm-7">
                              <input type="text" id="penjualan-obat-kanan" value="{{$pengaturan->mka_penjualan}}" placeholder="Ukuran CM" class="form-control" />
                            </div>
                          </div>
                          <div class="form-group col-sm-2">
                            <span class="">
                                <label class="middle">
                                  @if ($pengaturan->catatan_penjualan != 0)
                                  <input class="ace" type="checkbox" id="po-tampilkan-catatan" name="catatan_penjualan" value="0" checked/>
                                  <span class="lbl"> Tampilkan Catatan!</span>
                                  @else
                                  <input class="ace" type="checkbox" id="po-tampilkan-catatan" value="1" name="catatan_penjualan"/>
                                  <span class="lbl"> Tampilkan Catatan!</span>
                                  @endif
                                </label>
                              </span>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-sm-4">
                            <label class="col-sm-5 control-label" for="form-field-1">Retur Penjualan Obat</label>

                            <div class="col-sm-7">
                              <select class="form-control" id="retur-penjualan-cetak" name="retur_penjualan_obat">
                                @if ($pengaturan->retur_penjualan_obat == 0)
                                <option value="0" selected>INVOICE (A5)</option>
                                <option value="1">Tanpa Cetak</option>
                                @elseif ($pengaturan->
                                  retur_penjualan_obat == 1)
                                <option value="0">INVOICE (A5)</option>
                                <option value="1" selected>Tanpa Cetak</option>
                                @endif
                              </select>
                            </div>
                          </div>

                          <div class="form-group col-sm-3">
                            <label class="col-sm-5 control-label no-padding-right" for="form-field-1-1">Margin Kiri</label>

                            <div class="col-sm-7">
                              <input type="text" id="retur-penjualan-kiri" value="{{$pengaturan->mki_retur_penjualan}}" placeholder="Ukuran CM" class="form-control" />
                            </div>
                          </div>

                          <div class="form-group col-sm-3">
                            <label class="col-sm-5 control-label no-padding-right" for="form-field-1-1">Margin Kanan</label>

                            <div class="col-sm-7">
                              <input type="text" id="retur-penjualan-kanan" value="{{$pengaturan->mka_retur_penjualan}}" placeholder="Ukuran CM" class="form-control" />
                            </div>
                          </div>
                          <div class="form-group col-sm-2">
                            <span class="">
                                <label class="middle">
                                  @if ($pengaturan->catatan_retur_penjualan != 0)
                                  <input class="ace" type="checkbox" id="po-tampilkan-catatan" name="catatan_retur_penjualan" value="0" checked/>
                                  <span class="lbl"> Tampilkan Catatan!</span>
                                  @else
                                  <input class="ace" type="checkbox" id="po-tampilkan-catatan" value="1" name="catatan_retur_penjualan"/>
                                  <span class="lbl"> Tampilkan Catatan!</span>
                                  @endif
                                </label>
                              </span>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-sm-4">
                            <label class="col-sm-5 control-label" for="form-field-1">Pembayaran Piutang</label>

                            <div class="col-sm-7">
                              <select class="form-control" id="pembayaran-piutang-cetak" name="pembayaran_piutang">
                                @if ($pengaturan->pembayaran_piutang == 0)
                                <option value="0" selected>INVOICE (A5)</option>
                                <option value="1">Tanpa Cetak</option>
                                @elseif ($pengaturan->
                                  pembayaran_piutang == 1)
                                <option value="0">INVOICE (A5)</option>
                                <option value="1" selected>Tanpa Cetak</option>
                                @endif
                              </select>
                            </div>
                          </div>

                          <div class="form-group col-sm-3">
                            <label class="col-sm-5 control-label no-padding-right" for="form-field-1-1">Margin Kiri</label>

                            <div class="col-sm-7">
                              <input type="text" id="pembayaran-piutang-kiri" value="{{$pengaturan->mki_pembayaran_piutang}}"  placeholder="Ukuran CM" class="form-control" />
                            </div>
                          </div>

                          <div class="form-group col-sm-3">
                            <label class="col-sm-5 control-label no-padding-right" for="form-field-1-1">Margin Kanan</label>

                            <div class="col-sm-7">
                              <input type="text" id="pembayaran-piutang-kanan" value="{{$pengaturan->mka_pembayaran_piutang}}"  placeholder="Ukuran CM" class="form-control" />
                            </div>
                          </div>
                          <div class="form-group col-sm-2">
                            <span class="">
                                <label class="middle">
                                  @if ($pengaturan->catatan_pembayaran_piutang != 0)
                                  <input class="ace" type="checkbox" id="po-tampilkan-catatan" name="catatan_pembayaran_piutang" value="0" checked/>
                                  <span class="lbl"> Tampilkan Catatan!</span>
                                  @else
                                  <input class="ace" type="checkbox" id="po-tampilkan-catatan" value="1" name="catatan_pembayaran_piutang"/>
                                  <span class="lbl"> Tampilkan Catatan!</span>
                                  @endif
                                </label>
                              </span>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-sm-12">
                            <label class="col-sm-2 control-label" for="form-field-1">Catatan Promo</label>
                            <div class="col-sm-10">
                              <textarea class="form-control" value="{{$pengaturan->catatan_promo}}" id="promo" placeholder="Catatan Promo"></textarea>
                            </div>
                          </div>
                        </div>
                        <input type="submit" name="" value="Simpan" class="btn btn-success">
                      </form>
                    </div>

                    <div id="notifikasi" class="tab-pane">
                      <form id="pengaturan-notifikasi" class="form-horizontal" action="{{url('pengaturan-aplikasi/simpan-notifikasi')}}" role="form">
                        <div class="row">

                          <div class="form-group col-sm-12">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1">Obat Expired</label>

                            <div class="input-group col-sm-10">
                              <input type="number" name="obat_expired" id="obat-expired" value="{{$pengaturan->obat_expired}}" class="form-control" />
                              <span class="input-group-addon">
                                Bulan sebelum tanggal expired
                              </span>
                            </div>
                          </div>
                          <div class="form-group col-sm-12">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1">Piutang Jatuh Tempo</label>

                            <div class="input-group col-sm-10">
                              <input type="number" value="{{$pengaturan->piutang_jatuh_tempo}}" name="piutang_jatuh_tempo" id="piutang-tempo"  class="form-control" />
                              <span class="input-group-addon">
                                Bulan sebelum tanggal deadline
                              </span>
                            </div>
                          </div>
                          <div class="form-group col-sm-12">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1">Hutang Jatuh Tempo</label>

                            <div class="input-group col-sm-10">
                              <input type="number" value="{{$pengaturan->hutang_jatuh_tempo}}" name="hutang_jatuh_tempo" id="hutang-tempo"  class="form-control" />
                              <span class="input-group-addon">
                                Bulan sebelum tanggal deadline
                              </span>
                            </div>
                          </div>
                        </div>
                        <input type="submit" name="" value="Simpan" class="btn btn-success">
                      </form>
                    </div>

                    <div id="informasi" class="tab-pane">
                      <div class="col-sm-3">
                          <b>Anda berkunjung melalui</b>
                        </div>
                        <div class="col-sm-1">
                          <b>Desktop</b>
                        </div>
                    </div>

                    <div id="database" class="tab-pane">
                      <div class="row">
                        <div class="col-sm-2">
                          <b>Reset Database</b>
                        </div>
                        <div class="col-sm-1">
                          <a class="btn btn-danger btn-sm" href="#"><i class="fa fa-refresh"></i> Reset</a>
                        </div>
                      </div>
                    </div>

                    <div id="tuslah" class="tab-pane">
                      <form id="pengaturan-tuslah" class="form-horizontal" role="form" action="{{url('pengaturan-aplikasi/simpan-tuslah-embalase')}}" >
                        <div class="row">

                          <div class="form-group col-sm-12">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1">Tuslah</label>

                            <div class="col-sm-10">
                              <input type="number" id="tuslah" value="{{$pengaturan->tuslah}}" name="tuslah" class="form-control" />
                            </div>
                          </div>
                          <div class="form-group col-sm-12">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1">Embalase</label>

                            <div class="col-sm-10">
                              <input type="number" value="{{$pengaturan->embalase}}" name="embalase" id="embalase" class="form-control" />
                            </div>
                          </div>
                        </div>
                        <input type="submit" name="" value="Simpan" class="btn btn-success">
                      </form>
                    </div>

                    <div id="harga" class="tab-pane">
                      <form id="pengaturan-ppn" class="form-horizontal" role="form" action="{{url('pengaturan-aplikasi/simpan-harga-ppn')}}">
                        <div class="row">

                          <div class="form-group col-sm-12">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1">PPN</label>

                            <div class="input-group col-sm-10">
                              <input type="number" id="ppn" value="{{$pengaturan->ppn}}" name="ppn" class="form-control" />
                              <span class="input-group-addon">
                                %
                              </span>
                            </div>
                          </div>
                        </div>
                        <input type="submit" name="" value="Simpan" class="btn btn-success">
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script type="text/javascript">
$('.chosen-select').chosen();
    $("#rangepicker6").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
</script>
@stop
