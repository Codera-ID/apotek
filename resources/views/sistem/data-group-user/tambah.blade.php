@extends('layouts.home')
@section('css')
<link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
<link href="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.css')}}" rel="stylesheet" />
@stop
@section('content')
<style media="screen">
  label{
    font-weight: bold;
  }
</style>
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Sistem</a>
      </li>
      <li class="active">Tambah Grup User</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
          <h1>
            Tambah Grup User

          </h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <form class="form-horizontal" action="{{url('data-group-user/tambah')}}" method="post" enctype="multipart/form-data">
              {{csrf_field()}}

              <div class="col-sm-12">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Nama</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <input type="text" name="nama" value="" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Kode Grup</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <input type="text" name="kode_grup" value="" class="form-control" placeholder="Akan terisi otomatis" disabled>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Keterangan</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <textarea name="keterangan" rows="4" cols="40" class="form-control"></textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 no-padding-right">Pilih Akses</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      @foreach($menu as $menu)
                      <div class="checkbox">
                        <label><input type="checkbox" name="menu[]" value="{{$menu->id}}">{{$menu->menu}}</label>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-6">

              </div>
              </div>
              <input type="submit" name="" value="Ubah" class="btn btn-success">
              <input type="reset" name="" value="Reset" class="btn btn-default">
            </form>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script src="{{asset('vendors/holder/holder.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.js')}}"></script>
<script type="text/javascript">
$('.chosen-select').chosen();
    $("#rangepicker6").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
</script>
@stop
