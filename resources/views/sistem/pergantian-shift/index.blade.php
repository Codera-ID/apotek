@extends('layouts.home')
@section('css')
<link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Sistem</a>
      </li>
      <li class="active">Pergantian Shift</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
          <h1>
            Tutup Shift

          </h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <form class="form-horizontal" action="{{url('tutup-shift')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1"> Saldo Awal </label>
                      <div class="col-sm-8">
                        <input type="text" id="form-field-1" name="saldo_awal" value="0.00" class="col-xs-10" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1"> Hasil Penjualan </label>
                      <div class="col-sm-8">
                        <input type="text" id="form-field-1" name="hasil_penjualan" value="0.00" class="col-xs-10" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1"> Pembayaran Piutang </label>
                      <div class="col-sm-8">
                        <input type="text" id="form-field-1" name="pembayaran_piutang" value="0.00" class="col-xs-10" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1"> Jurnal Masuk </label>
                      <div class="col-sm-8">
                        <input type="text" id="form-field-1" name="jurnal_masuk" value="0.00" class="col-xs-10" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1"> Total Pendapatan </label>
                      <div class="col-sm-8">
                        <input type="text" id="form-field-1" name="total_pendapatan" value="0.00" class="col-xs-10" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1"> Retur Penjualan </label>
                      <div class="col-sm-8">
                        <input type="text" id="form-field-1" name="retur_penjualan" value="0.00" class="col-xs-10" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1"> Jurnal Keluar </label>
                      <div class="col-sm-8">
                        <input type="text" id="form-field-1" name="jurnal_keluar" value="0.00" class="col-xs-10" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1"> Total Pengeluaran </label>
                      <div class="col-sm-8">
                        <input type="text" id="form-field-1" name="total_pengeluaran" value="0.00" class="col-xs-10" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1"> Saldo Akhir </label>
                      <div class="col-sm-8">
                        <input type="text" id="form-field-1" name="saldo_akhir" value="0.00" class="col-xs-10" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1"> Saldo Kasir </label>
                      <div class="col-sm-8">
                        <input type="text" id="form-field-1" name="saldo_kasir" class="col-xs-10" required>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1"> Diserahkan Kepada </label>
                      <div class="col-sm-8">
                        <select class="col-xs-10" name="user">
                              @foreach($data as $key)
                                <option value="{{$key->id}}">{{$key->name}}</option>
                              @endforeach
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="form-field-1"> Keterangan </label>
                      <div class="col-sm-8">
                        <textarea class="col-xs-10" name="keterangan"></textarea>
                      </div>
                  </div>
                </div>
              </div>

              <input type="submit" name="" value="Proses" class="btn btn-success">
            </form>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script type="text/javascript">

</script>
@stop
