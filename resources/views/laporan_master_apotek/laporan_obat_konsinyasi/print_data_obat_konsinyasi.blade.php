@extends('layouts.app')
@section('css')
<style>
.head hr {
    border: none;
    height: 2px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}

.table, table td, .table th , .table>thead>tr{ 
    background-color: #fff !important; 
    background: #fff !important;
}

body{
  background-color: #fff !important; 
  background: #fff !important;
}

hr {
    border: none;
    height: 1px;
    /* Set the hr color */
    color: #333; /* old IE */
    background-color: #333; /* Modern Browsers */
}
</style>
@stop
@section('content')
<div class="main-content-inner">
  <div class="page-content">
    <div>
        <table>
          <tr>
            <td style="padding-right:20px">
            <img style="width:80px;" data-src="{{('images/'.$identitas->logo )}}" alt="..." src="{{('images/'.$identitas->logo )}}">
            </td>
            <td>
            <h3><b>{{$identitas->nama}}</b></h3>
                <h5>No. Izin Praktek : {{$identitas->no_registrasi}}</h5>
                <h5>{{$identitas->alamat}}</h5>
                <h5>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</h5>   
            </td>
          </tr>
        </table>
    </div><!-- /.page-header -->
    <div class="head"><hr></div>
    <br>
    <h2 class="center">Laporan Obat Konsinyasi</h2>
    <hr>
    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <table class="table table-bordered">
          <thead>
            <tr>
              <th class="center">#</th>
              <th>Kode Obat</th>
              <th>Kode Barcode</th>
              <th>Nama Obat</th>
              <th class="hidden-480">Golongan</th>
              <th class="hidden-480">Kategori</th>
              <th>Stok</th>
            </tr>
          </thead>

          <tbody>
            <?php $i=1; ?>
            @foreach($data as $key)
            <tr>
              <td class="center">{{$i}}</td>
              <td>{{$key->kd_obat}}</td>
              <td>{{$key->barcode_obat}}</td>
              <td>{{$key->nama_obat}}</td>
              <td>{{$key->nama_gol_obat}}</td>
              <td>{{$key->nama_kat_obat}}</td>
              <td>{{$key->stok}} {{$key->nama_sat}}</td>
            </tr>
            <?php $i++; ?>
            @endforeach

          </tbody>
        </table>
        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('footer')
@stop