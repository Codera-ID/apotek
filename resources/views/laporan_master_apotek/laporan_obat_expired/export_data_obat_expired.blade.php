<html>

    <tbody>
        <tr>
            <td>{{$identitas->nama}}</td>
        </tr>
        <tr>        
            <td>No. Izin Praktek : {{$identitas->no_registrasi}}</td>
        </tr>  
        <tr>                      
            <td>{{$identitas->alamat}}</td>
        </tr>        
        <tr>            
            <td>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</td>
        </tr>
        <tr>
            <td align="center">Laporan Supplier</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th class="center">#</th>
            <th>Kode</th>
            <th>Kode Barcode</th>
            <th>Nama Obat</th>
            <th class="hidden-480">Golongan</th>
            <th class="hidden-480">Kategori</th>
            <th>Tanggal Expired</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach($data as $key)
        <tr>
            <td class="center">{{$i}}</td>
            <td>{{$key->kd_obat}}</td>
            <td>{{$key->barcode_obat}}</td>
            <td>{{$key->nama_obat}}</td>
            <td>{{$key->nama_gol_obat}}</td>
            <td>{{$key->nama_kat_obat}}</td>
            <td>{{$key->tgl_expired}}</td>
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>

</html>