<html>

    <tbody>
        <tr>
            <td>{{$identitas->nama}}</td>
        </tr>
        <tr>        
            <td>No. Izin Praktek : {{$identitas->no_registrasi}}</td>
        </tr>  
        <tr>                      
            <td>{{$identitas->alamat}}</td>
        </tr>        
        <tr>            
            <td>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</td>
        </tr>
        <tr>
            <td align="center">Laporan Apoteker</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th align="center">#</th>
            <th>Nama</th>
            <th>No.SIK/No.SIPA</th>
            <th>No.STRA</th>
            <th>Alamat</th>
            <th>Kota</th>
            <th>Telepon</th>
            <th>
              <i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
              Tanggal Mulai Tugas
            </th>
            <th class="hidden-480">Status</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach($data as $key)
        <tr>
            <td class="center">{{$i}}</td>
            <td>{{$key->nama_apoteker}}</td>
            <td>{{$key->no_sik_sipa}}</td>
            <td>{{$key->no_stra}}</td>
            <td>{{$key->alamat}}</td>
            <td>{{$key->kota}}</td>
            <td>{{$key->no_tlpn}}</td>
            <td>{{$key->tanggal_mulai_tugas}}</td>
            <td>{{$key->status}}</td>
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>

</html>