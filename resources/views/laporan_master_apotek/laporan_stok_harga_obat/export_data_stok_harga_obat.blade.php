<html>

    <tbody>
        <tr>
            <td>{{$identitas->nama}}</td>
        </tr>
        <tr>        
            <td>No. Izin Praktek : {{$identitas->no_registrasi}}</td>
        </tr>  
        <tr>                      
            <td>{{$identitas->alamat}}</td>
        </tr>        
        <tr>            
            <td>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</td>
        </tr>
        <tr>
            <td align="center">Laporan Stok dan Harga Obat</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th class="center">#</th>
            <th>Gudang</th>
            <th>Kode Obat</th>
            <th>Nama Obat</th>
            <th>Stok Satuan 4</th>
            <th>Harga 1</th>
            <th>Harga 2</th>
            <th>Harga 3</th>
            <th>Stok Satuan 3</th>
            <th>Harga 1</th>
            <th>Harga 2</th>
            <th>Harga 3</th>
            <th>Stok Satuan 2</th>
            <th>Harga 1</th>
            <th>Harga 2</th>
            <th>Harga 3</th>
            <th>Stok Terkecil</th>
            <th>Harga 1</th>
            <th>Harga 2</th>
            <th>Harga 3</th>
            <th>Golongan</th>
            <th>Kategori</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach($data as $key)
        <tr>
            <td class="center">{{$i}}</td>
            <td>{{$key->gudang}}</td>
            <td>{{$key->kd_obat}}</td>
            <td>{{$key->nama_obat}}</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>{{$key->stok}}</td>
            <td>{{$key->harga_jual_1}}</td>
            <td>{{$key->harga_jual_2}}</td>
            <td>{{$key->harga_jual_3}}</td>
            <td>{{$key->nama_gol_obat}}</td>
            <td>{{$key->nama_kat_obat}}</td>
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>

</html>