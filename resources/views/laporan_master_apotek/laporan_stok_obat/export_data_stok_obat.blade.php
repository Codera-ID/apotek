<html>

    <tbody>
        <tr>
            <td>{{$identitas->nama}}</td>
        </tr>
        <tr>        
            <td>No. Izin Praktek : {{$identitas->no_registrasi}}</td>
        </tr>  
        <tr>                      
            <td>{{$identitas->alamat}}</td>
        </tr>        
        <tr>            
            <td>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</td>
        </tr>
        <tr>
            <td align="center">Laporan Stok Obat</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th class="center">#</th>
            <th>Gudang</th>
            <th>Kode Obat</th>
            <th>Nama Obat</th>
            <th class="hidden-480">Stok Satuan 4</th>
            <th class="hidden-480">Stok Satuan 3</th>
            <th class="hidden-480">Stok Satuan 2</th>
            <th class="hidden-480">Stok Terkecil</th>
            <th class="hidden-480">Golongan</th>
            <th class="hidden-480">Kategori</th>
            <th>Nominal</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach($data as $key)
        <tr>
            <td class="center">{{$i}}</td>
            <td>{{$key->gudang}}</td>
            <td>{{$key->kode_obat}}</td>
            <td>{{$key->nama_obat}}</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>{{$key->stok_nyata}} {{$key->nama_sat}}</td>
            <td>{{$key->nama_gol_obat}}</td>
            <td>{{$key->nama_kat_obat}}</td>
            <td>{{$key->hna}}</td>
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>

</html>