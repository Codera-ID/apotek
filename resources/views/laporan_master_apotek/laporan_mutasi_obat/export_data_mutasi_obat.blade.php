<html>

    <tbody>
        <tr>
            <td>{{$identitas->nama}}</td>
        </tr>
        <tr>        
            <td>No. Izin Praktek : {{$identitas->no_registrasi}}</td>
        </tr>  
        <tr>                      
            <td>{{$identitas->alamat}}</td>
        </tr>        
        <tr>            
            <td>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</td>
        </tr>
        <tr>
            <td align="center">Laporan Supplier</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th align="center">#</th>
            <th>Tanggal</th>
            <th>No Bukti</th>
            <th>Kode Obat</th>
            <th>Nama Obat</th>
            <th>Gudang</th>
            <th>Keterangan</th>
            <th>Masuk</th>
            <th>Keluar</th>
            <th>Stok Awal</th>
            <th>Stok Sisa</th>
            <th>Golongan</th>
            <th>Kategori</th>
            <th>Petugas</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach($data as $key)
        <tr>
            <td class="center">{{$i}}</td>
            <td>{{$key->kd_supplier}}</td>
            <td>{{$key->nama_supplier}}</td>
            <td>{{$key->alamat}}</td>
            <td>{{$key->kota}}</td>
            <td>{{$key->no_tlpn}}</td>
            <td>{{$key->status}}</td>
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>

</html>