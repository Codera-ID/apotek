@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Laporan Master Apotek</a>
      </li>
      <li class="active">Mutasi Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
    <div class="page-header">
      <h1>
        Laporan Mutasi Obat
      </h1>
      <div class="clearfix">
        <div class="col-xs-12">
            <form class="form-inline" action="{{url('laporan-data-mutasi-obat')}}" method="get" style="margin:10px 0px">
             <label class="control-label no-padding-right">Pilih Periode</label>
                <div class="input-daterange input-group">
                    <input type="text" class="form-control" name="start" value="{{ $start }}"/>
                    <span class="input-group-addon">
                    <i class="fa fa-exchange"></i>
                    </span>

                    <input type="text" class="form-control" name="end" value="{{ $end }}"/>
                </div>
                <button type="submit" class="btn btn-sm btn-default"> <i class="fa fa-search"></i> Cari </button>              
            </form>
        </div>
        <div class="pull-right tableTools-container">
          <div class="pull-right tableTools-container">
            <div class="dt-buttons btn-overlap btn-group">
              <a href="{{url('export-mutasi-obat/'.$start.'/'.$end)}}" class="dt-button buttons-csv buttons-html5 btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-database bigger-110 orange"></i> <span class="">Export</span></span></a>
              <a href="{{url('cetak-mutasi-obat/'.$start.'/'.$end)}}" class="dt-button buttons-print btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-print bigger-110 grey"></i> <span class="">Print</span></span></a>
            </div>
          </div>
        </div>
    </div><!-- /.page-header -->

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
          <div class="col-xs-12">
            <div class="table-header">
              Mutasi Obat
            </div>

            <!-- div.table-responsive -->

            <!-- div.dataTables_borderWrap -->
            <div>
              <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="center">#</th>
                    <th>Tanggal</th>
                    <th>No Bukti</th>
                    <th>Kode Obat</th>
                    <th>Nama Obat</th>
                    <th>Gudang</th>
                    <th>Keterangan</th>
                    <th>Masuk</th>
                    <th>Keluar</th>
                    <th>Stok Awal</th>
                    <th>Stok Sisa</th>
                    <th>Golongan</th>
                    <th>Kategori</th>
                    <th>Petugas</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $i=1; ?>
                  @foreach($data as $key)
                  <tr>
                    <td class="center">
                        <div class="hidden-sm hidden-xs action-buttons">
                            <a class="blue" href="{{url('export-kartu-stok-obat/'.$key->id)}}">
                                <i class="ace-icon fa fa-print"></i>
                            </a>
                        </div>
                    </td>
                    <td>{{$key->date}}</td>
                    <td>{{$key->no_bukti}}</td>
                    <td>{{$key->kd_obat}}</td>
                    <td>{{$key->nama_obat}}</td>
                    <td>{{$key->gudang}}</td>
                    <td>{{$key->keterangan}}</td>
                    <td>{{$key->stok_awal}}</td>
                    <td>{{$key->masuk}}</td>
                    <td>{{$key->keluar}}</td>
                    <td>{{$key->stok_akhir}}</td>
                    <td>{{$key->nama_gol_obat}}</td>
                    <td>{{$key->nama_kat_obat}}</td>
                    <td>{{$key->petugas}}</td>
                  </tr>
                  <?php $i++; ?>
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  //or change it into a date range picker
  $('.input-daterange').datepicker({
      autoclose:true,
      format: 'yyyy-mm-dd'
  });

  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
  }); 
});
</script>
@stop
