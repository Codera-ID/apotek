@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Laporan Master Apotek</a>
      </li>
      <li class="active">Data Pasien</li>
    </ul><!-- /.breadcrumb -->
  </div>
  @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
  @endif

  <div class="page-content">
    <div class="page-header">
      <h1>
        Laporan Data Pasien
      </h1>
      <div class="clearfix">
        <div class="pull-right tableTools-container">
          <div class="pull-right tableTools-container">
            <div class="dt-buttons btn-overlap btn-group">
                <a href="{{url('export-pasien')}}" class="dt-button buttons-csv buttons-html5 btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-database bigger-110 orange"></i> <span class="">Export</span></span></a>
                <a href="{{url('cetak-pasien')}}" class="dt-button buttons-print btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-print bigger-110 grey"></i> <span class="">Print</span></span></a>
            </div>
        </div>
      </div>
    </div><!-- /.page-header -->

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
          <div class="col-xs-12">
            <div class="table-header">
              Data Pasien
            </div>

            <!-- div.table-responsive -->

            <!-- div.dataTables_borderWrap -->
            <div>
              <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="center">#</th>
                    <th>No.RM</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>
                      <i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
                      Tanggal Lahir
                    </th>
                    <th>Umur</th>
                    <th>Alamat</th>
                    <th>Kota</th>
                    <th>Status Perkawinan</th>
                    <th>Email</th>
                    <th>Nama Ibu</th>
                    <th>Alergi Obat</th>
                    <th class="hidden-480">Status</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $i=1; ?>
                  @foreach($data as $key)
                  <tr>
                    <td class="center">{{$i}}</td>
                    <td>{{$key->id_rm}}</td>
                    <td>{{$key->nama_pasien}}</td>
                    <td>{{$key->jenis_kelamin}}</td>
                    <td>{{$key->tanggal_lahir}}</td>
                    <td>{{$key->umur}}</td>
                    <td>{{$key->alamat}}</td>
                    <td>{{$key->kota}}</td>
                    <td>{{$key->status_perkawinan}}</td>
                    <td>{{$key->email}}</td>
                    <td>{{$key->nama_ibu}}</td>
                    <td>{{$key->alergi_obat}}</td>
                    <td>{{$key->status}}</td>
                  </tr>
                  <?php $i++; ?>
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  var myTable = $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
  });
});
</script>
@stop
