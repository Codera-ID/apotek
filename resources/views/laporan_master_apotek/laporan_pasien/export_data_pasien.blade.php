<html>

    <tbody>
        <tr>
            <td>{{$identitas->nama}}</td>
        </tr>
        <tr>        
            <td>No. Izin Praktek : {{$identitas->no_registrasi}}</td>
        </tr>  
        <tr>                      
            <td>{{$identitas->alamat}}</td>
        </tr>        
        <tr>            
            <td>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</td>
        </tr>
        <tr>
            <td align="center">Laporan Pasien</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th class="center">#</th>
            <th>No.RM</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Tanggal Lahir</th>
            <th>Umur</th>
            <th>Alamat</th>
            <th>Kota</th>
            <th>Status Perkawinan</th>
            <th>Email</th>
            <th>Nama Ibu</th>
            <th>Alergi Obat</th>
            <th class="hidden-480">Status</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach($data as $key)
        <tr>
            <td class="center">{{$i}}</td>
            <td>{{$key->id_rm}}</td>
            <td>{{$key->nama_pasien}}</td>
            <td>{{$key->jenis_kelamin}}</td>
            <td>{{$key->tanggal_lahir}}</td>
            <td>{{$key->umur}}</td>
            <td>{{$key->alamat}}</td>
            <td>{{$key->kota}}</td>
            <td>{{$key->status_perkawinan}}</td>
            <td>{{$key->email}}</td>
            <td>{{$key->nama_ibu}}</td>
            <td>{{$key->alergi_obat}}</td>
            <td>{{$key->status}}</td>
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>

</html>