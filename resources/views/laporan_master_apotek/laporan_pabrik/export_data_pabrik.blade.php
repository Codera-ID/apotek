<html>

    <tbody>
        <tr>
            <td>{{$identitas->nama}}</td>
        </tr>
        <tr>        
            <td>No. Izin Praktek : {{$identitas->no_registrasi}}</td>
        </tr>  
        <tr>                      
            <td>{{$identitas->alamat}}</td>
        </tr>        
        <tr>            
            <td>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</td>
        </tr>
        <tr>
            <td align="center">Laporan Pabrik</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th align="center">#</th>
            <th>Kode</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Kota</th>
            <th class="hidden-480">Telepon</th>
            <th class="hidden-480">Status</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach($data as $key)
        <tr>
            <td class="center">{{$i}}</td>
            <td>{{$key->kd_pabrik}}</td>
            <td>{{$key->nama_pabrik}}</td>
            <td>{{$key->alamat}}</td>
            <td>{{$key->kota}}</td>
            <td>{{$key->no_tlpn}}</td>
            <td>{{$key->status}}</td>
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>

</html>