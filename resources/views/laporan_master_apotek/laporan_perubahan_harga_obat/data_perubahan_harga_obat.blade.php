@extends('layouts.home')
@section('css')
<style>
div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }

    .nowrap {
        white-space:nowrap;
    }
</style>
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Laporan Master Apotek</a>
      </li>
      <li class="active">Data Perubahan Harga Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>
  @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
  @endif

  <div class="page-content">
    <div class="page-header">
      <h1>
        Laporan Data Perubahan Harga Obat
      </h1>
      <div class="clearfix">
        <div class="pull-right tableTools-container">
          <a href="{{url('export-perubahan-harga')}}" class="dt-button buttons-csv buttons-html5 btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-database bigger-110 orange"></i> <span class="">Export</span></span></a>
          <a href="{{url('cetak-perubahan-harga')}}" class="dt-button buttons-print btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-print bigger-110 grey"></i> <span class="">Print</span></span></a>
        </div>
      </div>
    </div><!-- /.page-header -->

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
          <div class="col-xs-12">
            <div class="table-header">
              Data Perubahan Harga Obat
            </div>

            <!-- div.table-responsive -->

            <!-- div.dataTables_borderWrap -->
            <div>
              <table id="dynamic-table" class="nowrap table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="center">#</th>
                    <th>Tanggal</th>
                    <th>Kode Obat</th>                    
                    <th>Nama Obat</th>
                    <th>Satuan</th>
                    <th>Harga Beli</th>
                    <th>Harga Jual 1</th>
                    <th>Diskon Harga Jual 1</th>
                    <th>Harga Jual 2</th>
                    <th>Diskon Harga Jual 2</th>
                    <th>Harga Jual 3</th>
                    <th>Diskon Harga Jual 3</th>
                  </tr>
                </thead>

                <tbody>
                @foreach($data as $key)
                    <tr>
                      <td>{{$key->created_at}}</td>
                      <td>{{$key->kd_obat}}</td>
                      <td>{{$key->barcode_obat}}</td>
                      <td>{{$key->nama_obat}}</td>
                      <td>{{$key->jenis_obat}}</td>
                      <td>{{$key->hna}}</td>
                      <td>{{$key->harga_jual_1}}</td>
                      <td>{{$key->diskon_harga_1}}</td>
                      <td>{{$key->harga_jual_2}}</td>
                      <td>{{$key->diskon_harga_2}}</td>
                      <td>{{$key->harga_jual_3}}</td>
                      <td>{{$key->diskon_harga_3}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  var myTable = $('#dynamic-table').DataTable({
    "scrollX": true
  });
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
  });
});
</script>
@stop
