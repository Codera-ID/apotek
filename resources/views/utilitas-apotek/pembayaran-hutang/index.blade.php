@extends('layouts.home')

@section('css')
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Utilitas Apotek</a>
      </li>
      <li class="active">Pembayaran Hutang</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Pembayaran Hutang</h1>
          <br>
          <br>
          <div class="clearfix">
            <form class="form-horizontal" action="{{url('pembayaran-hutang')}}" method="get">
              <div class="pull-left col-xs-6">
                <label class="control-label no-padding-right">Pilih Periode</label>
              
                <div class="input-daterange input-group">
                  <input type="text" class="form-control" name="start" />
                  <span class="input-group-addon">
                    <i class="fa fa-exchange"></i>
                  </span>

                  <input type="text" class="form-control" name="end" />
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-purple btn-sm">
                      <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                      Cari
                    </button>
                  </span>
                </div>
              </div>
            </form>
          </div>
          <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
          </div>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <div class="table-responsive">
              <table  id="dynamic-table" class="table table-responsive table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Action</th>
                    <th>Tanggal</th>
                    <th>No. Faktur</th>
                    <th>No. PO</th>
                    <th>Supplier</th>
                    <th>Nilai Hutang</th>
                    <th>Jumlah Bayar</th>
                    <th>Jumlah Retur</th>
                    <th>Kekurangan</th>
                    <th>Jatuh Tempo</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $i=1; ?>
                  @foreach($data as $key)
                  <tr>
                    <td align="center">
                      <a class="green" href="{{url('pembayaran-hutang/'.$key->id_hutang)}}">
                            <i class="ace-icon fa fa-check bigger-130"></i>
                        </a>
                        <a class="blue" href="#">
                            <i class="ace-icon fa fa-search bigger-130"></i>
                        </a>
                    </td>
                    <td>{{$key->tgl_faktur}}</td>
                    <td>{{$key->no_faktur}}</td>
                    <td>{{$key->no_po}}</td>
                    <td>{{$key->nama_supplier}}</td>
                    <td>{{$key->total}}</td>
                    <td>{{$key->jumlah_bayar}}</td>
                    <td>{{$key->jumlah_retur}}</td>
                    <td>{{$key->sisa}}</td>
                    <td>{{$key->tanggal_jatuh_tempo}}</td>
                  </tr>
                  <?php $i++; ?>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  var myTable = $('#dynamic-table').DataTable({
          "aoColumns": [
					  { "bSortable": false },
					  null, null,null, null, null, null, null, null, null
					],
					"aaSorting": [],
  });
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
  });

$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
				new $.fn.dataTable.Buttons( myTable, {
					buttons: [
					  {
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange'></i> <span class=''>Export</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					  {
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					  {
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 grey'></i> <span class=''>Print</span>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false,
						message: 'This print was produced using the Print button for DataTables'
					  }		  
					]
				} );
				myTable.buttons().container().appendTo( $('.tableTools-container') );

        //or change it into a date range picker
				$('.input-daterange').datepicker({autoclose:true});
			
			
				//to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
				$('input[name=date-range-picker]').daterangepicker({
					'applyClass' : 'btn-sm btn-success',
					'cancelClass' : 'btn-sm btn-default',
					locale: {
						applyLabel: 'Apply',
						cancelLabel: 'Cancel',
					}
				})
				.prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
} );
</script>
@stop
