@extends('layouts.home')
@section('css')
<link href="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.css')}}" rel="stylesheet" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Utilitas</a>
      </li>
      <li class="active">Import Data Dari Excel</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Import Data Dari Excel</h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <div class="form-horizontal" >
                <fieldset>
                    <!-- Name input-->
                    <div class="col-sm-12">
                      <div class="col-sm-6">
                        <form class="" action="{{url('import-excel/pabrik')}}" method="post" enctype="multipart/form-data">
                          {{csrf_field()}}
                          <div class="form-group">
                              <label class="col-md-3 control-label" for="name">Pabrik</label>
                              <div class="col-md-9">
                                <input type="file" name="import_file" class="custom-file" />
                              </div>
                          </div>


                        <!-- Form actions -->
                      </div>
                      <div class="col-sm-6" style="padding:0px 100px">
                        <div class="form-group">
                          <button type="submit" name="button" class="btn btn-sm btn-primary w150">Import Pabrik</button>
                          <button type="button" name="button" onclick="window.open('{{asset('excel/1. Data Pabrik.xlsx')}}')" class="btn btn-sm btn-success">Download Template</button>
                        </div>
                      </div>
                      </form>
                    </div>
                    <form class="" action="{{url('import-excel/suplier')}}" method="post" enctype="multipart/form-data">
                      {{csrf_field()}}
                      <div class="col-sm-12">
                        <div class="col-sm-6">
                          <div class="form-group">
                              <label class="col-md-3 control-label" for="name">Suplier</label>
                              <div class="col-md-9">
                                <input type="file" name="import_file" class="custom-file" />
                              </div>
                          </div>
                          <!-- Form actions -->
                        </div>
                        <div class="col-sm-6" style="padding:0px 100px">
                          <div class="form-group">
                            <button type="submit" name="button" class="btn btn-sm btn-primary w150">Import Suplier</button>
                            <button type="button" name="button" onclick="window.open('{{asset('excel/2. Data Supplier.xlsx')}}')" class="btn btn-sm btn-success">Download Template</button>
                          </div>
                        </div>
                      </div>
                    </form>
                    <form class="" action="{{url('import-excel/gudang')}}" method="post" enctype="multipart/form-data">
                      {{csrf_field()}}
                      <div class="col-sm-12">
                        <div class="col-sm-6">
                          <div class="form-group">
                              <label class="col-md-3 control-label" for="name">Gudang</label>
                              <div class="col-md-9">
                                <input type="file" name="import_file" class="custom-file" />
                              </div>
                          </div>
                          <!-- Form actions -->
                        </div>
                        <div class="col-sm-6" style="padding:0px 100px">
                          <div class="form-group">
                            <button type="submit" name="button" class="btn btn-sm btn-primary w150">Import Gudang</button>
                            <button type="button" name="button" onclick="window.open('{{asset('excel/3. Data Gudang.xlsx')}}')" class="btn btn-sm btn-success">Download Template</button>
                          </div>
                        </div>
                      </div>
                    </form>

                    <form class="" action="{{url('import-excel/obat')}}" method="post" enctype="multipart/form-data">
                      {{csrf_field()}}
                      <div class="col-sm-12">
                        <div class="col-sm-6">
                          <div class="form-group">
                              <label class="col-md-3 control-label" for="name">Obat</label>
                              <div class="col-md-9">
                                <input type="file" name="import_file" class="custom-file" />
                              </div>
                          </div>
                          <!-- Form actions -->
                        </div>
                        <div class="col-sm-6" style="padding:0px 100px">
                          <div class="form-group">
                            <button type="submit" name="button" class="btn btn-sm btn-primary w150">Import Obat</button>
                            <button type="button" name="button" class="btn btn-sm btn-success" onclick="window.open('{{asset('excel/4.Data Obat.zip')}}')">Download Template</button>
                          </div>
                        </div>
                      </div>
                    </form>

                    <form class="" action="{{url('import-excel/apoteker')}}" method="post" enctype="multipart/form-data">
                      {{csrf_field()}}
                      <div class="col-sm-12">
                        <div class="col-sm-6">
                          <div class="form-group">
                              <label class="col-md-3 control-label" for="name">Apoteker</label>
                              <div class="col-md-9">
                                <input type="file" name="import_file" class="custom-file" />
                              </div>
                          </div>
                          <!-- Form actions -->
                        </div>
                        <div class="col-sm-6" style="padding:0px 100px">
                          <div class="form-group">
                            <button type="submit" name="" class="btn btn-sm btn-primary w150">Import Apoteker</button>
                            <button type="button" name="button" onclick="window.open('{{asset('excel/5. Data Apoteker.xlsx')}}')" class="btn btn-sm btn-success">Download Template</button>
                          </div>
                        </div>
                      </div>
                    </form>
                    <form class="" action="{{url('import-excel/pasien')}}" method="post" enctype="multipart/form-data">
                      {{csrf_field()}}
                      <div class="col-sm-12">
                        <div class="col-sm-6">
                          <div class="form-group">
                              <label class="col-md-3 control-label" for="name">Pasien</label>
                              <div class="col-md-9">
                                <input type="file" name="import_file" class="custom-file" />
                              </div>
                          </div>
                          <!-- Form actions -->
                        </div>
                        <div class="col-sm-6" style="padding:0px 100px">
                          <div class="form-group">
                            <button type="submit" name="button" class="btn btn-sm btn-primary w150">Import Pasien</button>
                            <button type="button" name="button" onclick="window.open('{{asset('excel/6. Data Pasien.xlsx')}}')" class="btn btn-sm btn-success">Download Template</button>
                          </div>
                        </div>
                      </div>
                    </form>
                </fieldset>
            </div>
          </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('vendors/holder/holder.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.js')}}"></script>
<script src="{{asset('js/ace-elements.min.js')}}"></script>
    <script src="{{asset('js/ace.min.js')}}"></script>
<script type="text/javascript">

  $('.custom-file').ace_file_input({
          no_file:'No File ...',
          btn_choose:'Choose',
          btn_change:'Change',
          droppable:false,
          onchange:null,
          thumbnail:false //| true | large
          //whitelist:'gif|png|jpg|jpeg'
          //blacklist:'exe|php'
          //onchange:''
          //
        });
</script>
@stop
