@extends('layouts.home')
@section('css')
<link href="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.css')}}" rel="stylesheet" />
<style>
div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }

    .nowrap {
        white-space:nowrap;
    }
</style>
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Utilitas</a>
      </li>
      <li class="active">Ubah Harga Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>
  <div class="page-content">
    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Ubah Harga Obat</h1>
          <!-- /.page-header -->
        </div>
        <div class="clearfix">
          <form class="" action="{{url('ubah-harga-obat/edit')}}" method="get">
          <div class="pull-left">
            <button type="submit" id="ubahButton" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-pencil"></span> Ubah</button>
          </div>
          <div class="pull-right">
            <div class="dt-buttons btn-overlap btn-group">
                <a href="{{url('ubah-harga-obat/excel')}}" class="dt-button buttons-csv buttons-html5 btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-database bigger-110 orange"></i> <span class="">Export</span></span></a>
            </div>
          </div>
        </div>
        <div class="space-4"></div>
        <div class="row">         
          <div class="col-xs-12">
            <div class="table-responsive">
              <table id="dynamic-table"  class="nowrap table table-responsive table-striped table-bordered table-hover" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <td> <input  id="check"  type="checkbox" name="" value=""> </td>
                    <th>Tanggal</th>
                    <th >Kode Obat</th>
                    <th >Kode Barcode</th>
                    <th >Nama Obat</th>
                    <th >Satuan</th>
                    <th >Golongan</th>
                    <th >Kategori</th>
                    <th >Jenis</th>
                    <th >Harga Beli</th>
                    <!-- <th >HPP</th> -->
                      <th >Harga Jual 1</th>
                      <th >Diskon Harga Jual 1</th>
                      <th >Harga Jual 2</th>
                      <th >Diskon Harga Jual 2</th>
                      <th >Harga Jual 3</th>
                      <th >Diskon Harga Jual 3</th>
                    </tr>
                </thead>

                <tbody>
                  @foreach($satuan as $sat)
                    <tr>
                      <td>  <input type="checkbox" name="check[]" value="{{$sat->kd_satuan}}"> </td>
                      <td>{{$sat->updated_at}}</td>
                      <td>{{$sat->kd_obat}}</td>
                      <td>{{$sat->barcode_obat}}</td>
                      <td>{{$sat->nama_obat}}</td>
                      <td>{{$sat->nama_sat_obat}}</td>
                      <td>{{$sat->nama_gol_obat}}</td>
                      <td>{{$sat->nama_kat_obat}}</td>
                      <td>{{$sat->jenis_obat}}</td>
                      <td>{{$sat->hna}}</td>
                      <td>{{$sat->harga_jual_1}}</td>
                      <td>{{$sat->diskon_harga_1}}</td>
                      <td>{{$sat->harga_jual_2}}</td>
                      <td>{{$sat->diskon_harga_2}}</td>
                      <td>{{$sat->harga_jual_3}}</td>
                      <td>{{$sat->diskon_harga_3}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        </form>
        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('vendors/holder/holder.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>

<script type="text/javascript">
$(document).ready( function () {
  var table = $('#dynamic-table').DataTable({
    "scrollX": true
  });
  $('#ubahButton').click( function() {
          var data = table.$('input, select').serialize();

          window.location.href = '{{url("ubah-harga-obat/edit?")}}'+data;
          return false;
      } );
} );
$("#check").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});

$('#id-input-file-1 , #id-input-file-2').ace_file_input({
          no_file:'No File ...',
          btn_choose:'Choose',
          btn_change:'Change',
          droppable:false,
          onchange:null,
          thumbnail:false //| true | large
          //whitelist:'gif|png|jpg|jpeg'
          //blacklist:'exe|php'
          //onchange:''
          //
        });
</script>

@stop
