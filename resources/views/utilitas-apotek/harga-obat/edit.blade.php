@extends('layouts.home')
@section('css')
<link href="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.css')}}" rel="stylesheet" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Utilitas</a>
      </li>
      <li class="active">Ubah Harga Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Ubah Harga Obat</h1>
          <div class="pull-right">

          </div>

        </div><!-- /.page-header -->
        <div class="">
          <form class="" action="{{url('ubah-harga-obat/edit')}}" method="post">
        </div>

        <div class="row">
          <div class="col-xs-12">
            <div class="table-responsive">
              <table  class="table table-responsive table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Action</th>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>Harga Beli</th>
                    <!-- <th>Harga HPP</th> -->
                    <th>Harga Jual 1</th>
                    <th>Diskon Harga Jual 1</th>
                    <th>Harga Jual 2</th>
                    <th>Diskon Harga Jual 2</th>
                    <th>Harga Jual 3</th>
                    <th>Diskon Harga Jual 3</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  {{csrf_field()}}
                  @foreach($satuan as $sat)
                  <input type="hidden" name="kd_satuan[]" value="{{$sat->kd_satuan}}" >
                  <tr>
                    <td>{{$i++}}</td>
                    <td> <a href="#md-footer-warning-{{$sat->kd_satuan}}" title="hapus" data-toggle="modal" class="btn btn-sm btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a> </td>
                    <td>{{$sat->kd_obat}}</td>
                    <td>{{$sat->nama_obat}}</td>
                    <td><input type="text" name="harga_beli[]" value="{{$sat->hna}}"></td>
                    <!-- <td><input type="text" name="hpp[]" value="{{$sat->hna}}"></td> -->
                    <td><input type="text" name="harga_jual_1[]" value="{{$sat->harga_jual_1}}"></td>
                    <td><input type="text" name="diskon_harga_1[]" value="{{$sat->diskon_harga_1}}"></td>
                    <td><input type="text" name="harga_jual_2[]" value="{{$sat->harga_jual_2}}"></td>
                    <td><input type="text" name="diskon_harga_2[]" value="{{$sat->diskon_harga_2}}"></td>
                    <td><input type="text" name="harga_jual_3[]" value="{{$sat->harga_jual_3}}"></td>
                    <td><input type="text" name="diskon_harga_3[]" value="{{$sat->diskon_harga_3}}"></td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <div class="space-4"></div>
            <div class="pull-left">
              <button type="submit" id="ubahButton" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-pencil"></span> Simpan</button>
            </div>
          </div>
        </div>
          </form>
        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('vendors/holder/holder.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/priceformat/jquery.priceformat.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
});
} );
$("#check").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});

</script>

@foreach($sat1 as $sat)
<script type="text/javascript">
$("#nilai{{$sat->kd_satuan}}").on("change",function(){
  var nilai = $("#nilai{{$sat->kd_satuan}}").val();
  var selisih = parseFloat(nilai) - {{$sat->stok}};
  $("#selisih{{$sat->kd_satuan}}").empty();
  $("#selisih{{$sat->kd_satuan}}").append(""+selisih.toFixed(2));
});
</script>
<div id="md-footer-warning-{{$sat->kd_satuan}}" tabindex="-1" role="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
      </div>
      <div class="modal-body">
        <div class="text-center">
          <div class="text-warning" style="color:#FFB752"><span class="modal-main-icon mdi mdi-alert-triangle" style="font-size: 80px;"></span></div>
          <h3>Warning!</h3>
          <p>Yakin Data Akan Di Hapus?</p>
          <div class="xs-mt-50">

            <form class="" action="{{url('/stok-opname/delete/'.$sat->kd_satuan)}}" method="post">
              {{csrf_field()}}
              <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
              <button type="submit"  class="btn btn-space btn-primary">Proceed</button>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endforeach
@stop
