<html>

    <tbody>
        <tr>
            <td>{{$identitas->nama}}</td>
        </tr>
        <tr>        
            <td>No. Izin Praktek : {{$identitas->no_registrasi}}</td>
        </tr>  
        <tr>                      
            <td>{{$identitas->alamat}}</td>
        </tr>        
        <tr>            
            <td>Telp. {{$identitas->telepon}}, Email : {{$identitas->email}}</td>
        </tr>
        <tr>
            <td align="center">DATA HARGA OBAT</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th>No</th>
            <th >Kode Obat</th>
            <th >Kode Barcode</th>
            <th >Nama Obat</th>
            <th >Satuan</th>
            <th >Golongan</th>
            <th >Kategori</th>
            <th >Jenis</th>
            <th >Harga Beli</th>
            <th >Harga Jual 1</th>
            <th >Diskon Harga Jual 1</th>
            <th >Harga Jual 2</th>
            <th >Diskon Harga Jual 2</th>
            <th >Harga Jual 3</th>
            <th >Diskon Harga Jual 3</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach($data as $key)
        <tr>
            <td class="center">{{$i}}</td>
            <td>{{$key->kd_obat}}</td>
            <td>{{$key->barcode_obat}}</td>
            <td>{{$key->nama_obat}}</td>
            <td>{{$key->nama_sat_obat}}</td>
            <td>{{$key->nama_gol_obat}}</td>
            <td>{{$key->nama_kat_obat}}</td>
            <td>{{$key->jenis_obat}}</td>
            <td>{{$key->hna}}</td>
            <td>{{$key->harga_jual_1}}</td>
            <td>{{$key->diskon_harga_1}}</td>
            <td>{{$key->harga_jual_2}}</td>
            <td>{{$key->diskon_harga_2}}</td>
            <td>{{$key->harga_jual_3}}</td>
            <td>{{$key->diskon_harga_3}}</td>
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>

</html>