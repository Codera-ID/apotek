@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Utilitas Apotek</a>
      </li>
      <li class="active">Pembayaran Piutang</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Utilitas Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Pembayaran Piutang
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Pembayaran Piutang</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('pembayaran-piutang-obat/bayar-hutang')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id_piutang" value="{{ $data->id_hutang }}">
            <div class="row">
              <div class="col-sm-12">
    						<div class="form-group">
    							<label class="col-sm-2 control-label" for="form-field-1"> Tanggal </label>
    								<div class="col-sm-10">
    									<input type="date" name="tanggal_pembayaran" value="{{$today}}" class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-2 control-label" for="form-field-1"> No Faktur </label>
    								<div class="col-sm-10">
    									<input type="text"  name="no_faktur" class="col-xs-10" value="{{$data->no_faktur}}" required readonly>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-2 control-label" for="form-field-1"> Nama Dokter </label>
    								<div class="col-sm-10">
    									<input type="text" name="supplier_name" class="col-xs-10" value="{{$data->nama_dokter}}"  required readonly>
    								</div>
    						</div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Nama Pasien </label>
                    <div class="col-sm-10">
                      <input type="text" name="supplier_name" class="col-xs-10" value="{{$data->nama_pasien}}"  required readonly>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Total </label>
                    <div class="col-sm-10">
                      <input type="number" id="total" name="total" class="col-xs-10" value="{{$data->total}}"  required readonly>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Pembayaran </label>
                    <div class="col-sm-10">
                      <select id="jenis_pembayaran" name="jenis_pembayaran" class="col-xs-10" required>
                        <option value="tunai">TUNAI</option>
                        <option value="kartu">KARTU</option>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Jumlah Tunai </label>
                    <div class="col-sm-10">
                      <input type="number" id="jumlah-tunai-value" name="jumlah_tunai" class="col-xs-12" required>
                    </div>
                </div>
                <div class="form-group hide" id="jumlah-kartu">
                  <label class="col-sm-2 control-label" for="form-field-1"> Jumlah Kartu </label>
                    <div class="col-sm-4">
                      <input type="number" id="jumlah-kartu-value" value="0" name="jumlah_kartu" class="col-xs-12">
                    </div>
                    <div class="col-sm-4">
                      <select class="col-xs-12" name="kartu">
                        <option value="Bank BRI">Bank BRI</option>
                        <option value="Bank Mandiri">Bank Mandiri</option>
                        <option value="Bank BNI">Bank BNI</option>
                        <option value="Bank Danamon">Bank Danamon</option>
                        <option value="Permata Bank">Permata Bank</option>
                        <option value="Bank BCA">Bank BCA</option>
                        <option value="Bank BNI">Bank BII</option>
                        <option value="Bank Panin">Bank Panin</option>
                        <option value="CIMB NIAGA">CIMB NIAGA</option>
                        <option value="Bank Lippo">Bank Lippo</option>
                        <option value="Bank NISP">Bank NISP</option>
                      </select>
                    </div>
                </div>
                <div class="form-group hide" id="nomor-kartu">
                  <label class="col-sm-2 control-label" for="form-field-1"> Nama Bank / No. Kartu </label>
                    <div class="col-sm-10">
                      <input type="text"  name="no_kartu" class="col-xs-10">
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Kembalian </label>
                    <div class="col-sm-10">
                      <input type="number" id="kembalian" name="kembalian" class="col-xs-10" required readonly>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Deadline</label>
                    <div class="col-sm-10">
                      <input type="date" name="tanggal_pembayaran" class="col-xs-10" value="{{$data->tanggal_pembayaran}}" required>
                    </div>
                </div>
              </div>
            </div>
						<div class="clearfix form-actions">
							<div class="col-md-6">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
									</button>
								</div>
                <div class="col-md-offset-4 col-md-2">
                  <a href="{{url('pembayaran-hutang')}}">
									<button class="btn btn-danger" type="button">
										<i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
											Kembali
									</button>
                </a>
								</div>
			         </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script>
$(document).ready(function(){
  $('#kembalian').val(0);

  $('#jenis_pembayaran').change(function() {
    if(this.value == 'tunai'){
      $('#jumlah-kartu').addClass('hide');
      $('#nomor-kartu').addClass('hide');
    }else{
      $('#jumlah-kartu').removeClass('hide');
      $('#nomor-kartu').removeClass('hide');
    }
  });

  $('#jumlah-tunai-value').change(function() {
    if( $('#jumlah-kartu-value').val() == undefined ){
      var jumlah_kartu = 0;
    }else {
      var jumlah_kartu = parseInt($('#jumlah-kartu-value').val());
    }
    var jumlah_tunai =  parseInt($('#jumlah-tunai-value').val());
    var total = parseInt($('#total').val());
    var kembalian = (jumlah_tunai + jumlah_kartu) - total;

      if(kembalian > 0){
        $('#kembalian').val(kembalian);
      }else{
        $('#kembalian').val(0);
      }
  });

  $('#jumlah-kartu-value').change(function() {
    if( $('#jumlah-kartu-value').val() == undefined ){
      var jumlah_kartu = 0;
    }else {
      var jumlah_kartu = parseInt($('#jumlah-kartu-value').val());
    }

    var jumlah_tunai =  parseInt($('#jumlah-tunai-value').val());
    var total = parseInt($('#total').val());
    var kembalian = (jumlah_tunai + jumlah_kartu) - total;

      if(kembalian > 0){
        $('#kembalian').val(kembalian);
      }else{
        $('#kembalian').val(0);
      }
  });

});
</script>
@stop
