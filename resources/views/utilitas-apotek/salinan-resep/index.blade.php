@extends('layouts.home')
@section('css')
<link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
<link href="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.css')}}" rel="stylesheet" />
@stop
@section('content')
<style media="screen">
  label{
    font-weight: bold;
  }
</style>
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Utilitas Apotek</a>
      </li>
      <li class="active">Salinan Resep</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
          <h1>
            Salinan Resep

          </h1>
        </div><!-- /.page-header -->
        <div class="row">
          <div class="col-xs-12">
            <form class="form-horizontal" action="{{url('salinan-resep/resep')}}" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="col-sm-12">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">No Faktur</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <select class="chosen-select form-control" name="">
                        @foreach($penjualan_obat as $apt)
                        <option value="{{$apt->id_apoteker}}">{{$apt->no_faktur}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Tgl. Resep</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <div class="row">
                        <div class="col-xs-8 col-sm-12">
                          <div class="input-group">
                            <input name="tgl_resep" class="form-control date-picker" value="{{$date}}" id="id-date-picker-1" type="text" data-date-format="dd-mm-yyyy" />
                            <span class="input-group-addon">
                              <i class="fa fa-calendar bigger-110"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Tgl. Pembuatan</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <div class="row">
                        <div class="col-xs-8 col-sm-12">
                          <div class="input-group">
                            <input name="tgl_pembuatan" class="form-control date-picker" value="{{$date}}" id="id-date-picker-1" type="text" data-date-format="dd-mm-yyyy" />
                            <span class="input-group-addon">
                              <i class="fa fa-calendar bigger-110"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 no-padding-right">Apoteker</label>
                  <div class="col-sm-9">
                    <select class="col-xs-10" name="apoteker">
                    @foreach($apoteker as $apt)
                    <option value="{{$apt->id_apoteker}}">{{$apt->nama_apoteker}}</option>
                    @endforeach
                    </select>
                    <span class="input-group-btn">
                      <a href="apoteker/tambah">
                        <button type="button" id="po" class="btn btn-info" style="font-size:14px; padding:0px; padding-left:8.5px; padding-right:8.5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"  title="Tambah data apoteker"><i class="glyphicon glyphicon-plus"></i></button>
                      </a>
                    </span>
                  </div>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="col-sm-2 no-padding-right">No. Resep</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <input type="text" name="telp" value="" class="form-control" name="no_resep">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 no-padding-right">Passien</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <select class="col-sm-10" name="passien">
                        <option value="">-Pilih Pasien-</option>
                        @foreach($pasien as $pas)
                        <option value="{{$pas->id_pasien}}">{{$pas->nama_pasien}}</option>
                        @endforeach
                      </select>
                      <span class="input-group-btn">
                        <a href="pasien/tambah">
                          <button type="button" id="po" class="btn btn-info" style="font-size:14px; padding:0px; padding-left:8.5px; padding-right:8.5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"  title="Tambah data pasien"><i class="glyphicon glyphicon-plus"></i></button>
                        </a>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Dokter</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <select class="col-sm-10" name="dokter">
                        <option value="">-Pilih Dokter-</option>
                        @foreach($dokter as $dok)
                        <option value="{{$dok->id_dokter}}">{{$dok->nama_dokter}}</option>
                        @endforeach
                      </select>
                      <span class="input-group-btn">
                        <a href="dokter/tambah">
                          <button type="button" id="po" class="btn btn-info" style="font-size:14px; padding:0px; padding-left:8.5px; padding-right:8.5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"  title="Tambah data dokter"><i class="glyphicon glyphicon-plus"></i></button>
                        </a>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2  no-padding-right">Struk</label>
                  <div class="col-sm-9">
                    <div class="col-sm-12" style="padding:0px">
                      <select class="form-control" name="struk">
                        <option value="tunai">Invoice (A5)</option>
                        <option value="struk58">Struk Kecil (58mm)</option>
                        <option value="struk76">Struk Kecil (76mm)</option>
                        <option value="tanpacetak">Tanpa Cetak (76mm)</option>
                      </select>
                    </div>
                  </div>
                </div>

              </div>
              </div>
              <div class="col-sm-12">
                <button type="button" href="#obat" data-toggle="modal" class="btn btn-primary" id="btn-obat"> <i class="glyphicon glyphicon-plus"></i> Obat</button>
                <button type="button" href="#racikan" data-toggle="modal" class="btn btn-primary" id="btn-racikan"> <i class="glyphicon glyphicon-plus"></i> Racikan</button>
                <button type="button" id="resep" data-toggle="modal" class="btn btn-primary resep"> <i class="glyphicon glyphicon-plus"></i> Resep</button>
                <button type="reset" name="button" class="btn btn-default"> <i class="glyphicon glyphicon-repeat"></i> Reset</button>
                <button type="submit" name="button" class="btn btn-success"> <i class="ace-icon fa fa-check bigger-110"></i> Proses</button>
              </div>
              <div class="col-sm-12 form-inline receipt" id="receipt" style="margin:10px 10px;">

              </div>
              <div class="col-sm-12" style="margin-top:10px;padding-top:10px;border-top:2px solid #438EB9">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th style="min-width:70px;">Action</th>
                      <th>Kode</th>
                      <th>Nama Obat</th>
                      <th>Jumlah dipenuhi</th>
                      <th>Jumlah tidak dipenuhi</th>
                      <th>Satuan</th>
                      <th>Keterangan</th>
                    </tr>
                  </thead>
                  <tbody id="baby">
                    <tr id="kosong">
                      <td  colspan="7"><div class="empty">Tidak ada data yang ditemukan, anda dapat menambahkan dengan memilih obat</div></td>
                    </tr>

                  </tbody>
                </table>
              </div>

            </form>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
<div id="obat" tabindex="-1" role="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
      </div>
      <div class="modal-body">
        <div class="text-center">
          <form  id="formobt">
          <table class="table table-bordered">
            <thead>
              <th>Pilih</th>
              <th>Kode Obat</th>
              <th>Obat</th>
              <th>Pabrik</th>
              <th>Kategori</th>
            </thead>
            <tbody>
              @foreach($obat as $obats)
                <tr>
                  <td> <input type="checkbox" name="obt[]" value="{{$obats->kd_obat}}"> </td>
                  <td>{{$obats->kd_obat}}</td>
                  <td>{{$obats->nama_obat}}</td>
                  <td>{{$obats->pabrik->nama_pabrik}}</td>
                  <td>{{$obats->kat_obat->nama_kat_obat}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
          <div class="xs-mt-50">
              <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
              <button id="btn-myobt" type="button"  class="btn btn-space btn-primary">Proceed</button>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="modal-racikan" tabindex="-1" role="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
      </div>
      <div class="modal-body">
        <form  class="" id="data-racikan">
        <div class="text-center" >
          <h1>Obat Racikan</h1>
          <table  class="table table-bordered">
            <thead>
              <th>Pilih</th>
              <th>Kode Obat</th>
              <th>Obat</th>
            </thead>
            <tbody>
              @foreach($racikan as $obats)
                <tr>
                  <td> <input  type="checkbox" name="racikan[]" value="{{$obats->kd_obat}}"> </td>
                  <td>{{$obats->kd_obat}}</td>
                  <td>{{$obats->nama_obat}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
          <div class="xs-mt-50">

              <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
              <button id="btn-add-racikan" type="button"  class="btn btn-space btn-primary">Proceed</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script src="{{asset('vendors/holder/holder.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.js')}}"></script>
<script type="text/javascript">
$('.chosen-select').chosen();
    $("#rangepicker6").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
    $('.date-picker').datepicker({
      autoclose: true,
      todayHighlight: true
    })
    $('#btn-obat').prop('readonly',true);
    $('#btn-obat').prop('disabled',true);
    $('#btn-racikan').prop('readonly',true);
    $('#btn-racikan').prop('disabled',true);
    $('.resep').on('click',function() {
      $('#btn-obat').prop('readonly',false);
      $('#btn-obat').prop('disabled',false);
      $('#btn-racikan').prop('readonly',false);
      $('#btn-racikan').prop('disabled',false);
      $('#resep').prop('readonly', true);
      $('#resep').prop('disabled', true);
      $('.receipt').append('<div id="brobro"><label>Resep</label><input id="resepid" type="text" name="resep" value="" class="form-control"><input type="button" name="" value="Simpan" class="btn btn-sm btn-danger" id="btn-resep-bro"><button id="empty" type="button" name="button" class="btn btn-sm btn-default">Cancel</button></div>');
      $('#empty').on('click',function(){
        $('#receipt').empty();
        $('#btn-obat').prop('readonly',true);
        $('#btn-obat').prop('disabled',true);
        $('#resep').prop('readonly', false);
        $('#resep').prop('disabled', false);
        $('#btn-racikan').prop('readonly',true);
        $('#btn-racikan').prop('disabled',true);
      });
      $('#btn-resep-bro').on('click',function(){
        var resepid = $('#resepid').val();
        console.log(resepid);
          $('#baby').prepend('<tr><td> <button type="button" name="button" class="btn btn-sm- btn-danger"> <input type="hidden" name="resep" value="'+resepid+'"><i class="glyphicon glyphicon-trash"></i> </button> </td><td>Resep</td><td>'+resepid+'</td><td> <input type="text" name="" value="" readonly> </td><td> <input type="text" name="" value="" readonly> </td><td> <select class="form-control" name="" readonly></select> </td><td> <input type="text" name="" value="" readonly> </td></tr>');
          $('#receipt').empty();
          $('#btn-obat').prop('readonly',true);
          $('#resep').prop('readonly', false);
          $('#btn-racikan').prop('readonly',true);
      });
      $('#btn-racikan').one('click',function(){
        $('#receipt').append('<div id="bro"> <button href="#modal-racikan" data-toggle="modal" type="button" id="cariracikanbutton" class="btn btn-primary btn-sm" ><i class="glyphicon glyphicon-search"></i> Cari Racikan</button><button id="empty-racikan" type="button" name="button" class="btn btn-sm btn-default">Cancel</button></div>' );
        $('#empty-racikan').on('click',function(){
          $('#bro').remove();

        });
      });
    });
    $('#btn-add-racikan').click( function() {
            var data = $('#data-racikan').serialize();
            var resepid = $('#resepid').val();
            //alert(data);
            $('#kosong').remove();
            $.getJSON("{{url('salinan-resep/data-racikan?')}}"+data, function(result){
              console.log(result);

              $.each(result, function(index, element) {

                  $('#baby').append('<tr><td> <button type="button" name="button" class="btn btn-sm- btn-danger"><i class="glyphicon glyphicon-trash"></i> </button> </td><td>'+element.kd_obat+'<input type="hidden" name="obat[]" value="'+element.nama_obat+'" ></td><td>'+element.nama_obat+'</td><td> <input type="text" name="jml[]" value="0" readonly> </td><td> <input type="text" name="jml2[]" value="0" readonly> </td><td> <input type="text" name="sat[]" value=" " > </td><td> <input type="text" name="ket[]" value="" readonly> </td></tr>');
              });
              $('#modal-racikan').modal('hide');
            });
                     //window.location.href = '{{url("ubah-harga-obat/edit?")}}'+data;
            return false;
        } );
  $('#btn-myobt').on('click',function(){
    var broi  = $('#formobt').serialize();
    var resepid = $('#resepid').val();
    $.getJSON("{{url('salinan-resep/data-obat?')}}"+broi, function(result){
      console.log(result);

      $.each(result, function(index, element) {
          $('#baby').append('<tr><td> <button type="button" name="button" class="btn btn-sm- btn-danger"> <i class="glyphicon glyphicon-trash"></i> </button> </td><td><input type="hidden" name="obat[]" value="'+element.nama_obat+'" >'+element.kd_obat+'</td><td>'+element.nama_obat+'</td><td> <input type="text" name="jml[]" value="0" > </td><td> <input type="text" name="jml2[]" value="0"> </td><td> <input type="text" name="sat[]" value="" > </td><td> <input type="text" name="ket[]" value="" > </td></tr>');
      });
      $('#obat').modal('hide');
    });
  });
</script>
@stop
