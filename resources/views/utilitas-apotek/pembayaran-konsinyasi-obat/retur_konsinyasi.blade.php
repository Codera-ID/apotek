@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Utilitas Apotek</a>
      </li>
      <li class="active">Retur Konsinyasi Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Utilitas Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Retur Konsinyasi Obat
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Retur Konsinyasi Obat</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('pembayaran-konsinyasi-obat/retur-konsinyasi')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id_konsinyasi" value="{{ $data->id_konsinyasi }}">
            <div class="row">
              <div class="col-sm-12">
    						<div class="form-group">
    							<label class="col-sm-2 control-label" for="form-field-1"> Tanggal </label>
    								<div class="col-sm-10">
    									<input type="date" name="tanggal_pembayaran" value="{{$today}}" class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-2 control-label" for="form-field-1"> No Faktur </label>
    								<div class="col-sm-10">
    									<input type="text"  name="no_faktur" class="col-xs-10" value="{{$data->no_faktur}}" required readonly>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-2 control-label" for="form-field-1"> Supplier </label>
    								<div class="col-sm-10">
    									<input type="text" name="supplier_name" class="col-xs-10" value="{{$data->nama_supplier}}"  required readonly>
    								</div>
    						</div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Sisa </label>
                    <div class="col-sm-10">
                      <input type="number" id="sisa" name="sisa" class="col-xs-10" value="{{$data->sisa}}"  required readonly>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Jumlah Retur </label>
                    <div class="col-sm-4">
                      <input type="number" id="jumlah-retur-value" name="jumlah_retur" class="col-xs-12" required>
                    </div>
                    <div class="col-sm-4">
                      <select class="col-xs-12" name="tunai">
                        <option value="{{$data->kd_sat_obat}}">{{$data->nama_sat_obat}}</option>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Sisa Akhir</label>
                    <div class="col-sm-10">
                      <input type="number" id="sisa_akhir" name="sisa_akhir" class="col-xs-10" value="{{$data->sisa}}" required readonly>
                    </div>
                </div>
              </div>
            </div>
						<div class="clearfix form-actions">
							<div class="col-md-6">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
									</button>
								</div>
                <div class="col-md-offset-4 col-md-2">
                  <a href="{{url('pembayaran-hutang')}}">
									<button class="btn btn-danger" type="button">
										<i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
											Kembali
									</button>
                </a>
								</div>
			         </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script>
$(document).ready(function(){

  $('#jumlah-retur-value').change(function() {

    var jumlah_retur =  parseInt($('#jumlah-retur-value').val());
    var sisa = parseInt($('#sisa').val());
    var sisa_akhir = sisa-jumlah_retur;
    
    $('#sisa_akhir').val(sisa_akhir);
  });

});
</script>
@stop
