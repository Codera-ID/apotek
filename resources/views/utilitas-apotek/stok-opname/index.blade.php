@extends('layouts.home')
@section('css')
<link href="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.css')}}" rel="stylesheet" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Utilitas Apotek</a>
      </li>
      <li class="active">Data Stock Opname</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Data Stok Opname</h1>
          <div class="pull-right">

          </div>

        </div><!-- /.page-header -->
        <div>
          <form class="" action="{{url('stok-opname/edit')}}" method="get">

          <div class="pull-left">
            <button type="button" id="ubahButton" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-pencil"></span> Proses</button>
          </div>
          <div class="pull-right">
            <div class="dt-buttons btn-overlap btn-group">
                <a href="{{url('stok-opname/excel')}}" class="dt-button buttons-csv buttons-html5 btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table"><span><i class="fa fa-database bigger-110 orange"></i> <span class="">Export</span></span></a>
            </div>
          </div>
        </div>
        <div class="space-4"></div>
              <div class="row">
                <div class="col-xs-12">
                  <div class="table-responsive">
                    <table id="dynamic-table"  class="table table-responsive table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th class="center">
                            <label class="pos-rel">
                              <input type="checkbox" class="ace"  name="" value="" />
                              <span class="lbl"></span>
                            </label>
                          </th>
                          <th >Kode Obat</th>
                          <th >Nama Obat</th>
                          <th >Nama Gudang</th>
                          <th >Golongan</th>
                          <th >Kategori</th>
                          <th >Stok Terkecil</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($opname as $opn)
                        <tr>
                          <td class="center">
                            <label class="pos-rel">
                              <input  type="checkbox" class="ace" name="check[]" value="{{$opn->kd_obat}}" >
                              <span class="lbl"></span>
                            </label>
                          </td>
                          <td>{{$opn->kd_obat}}</td>
                          <td>{{$opn->nama_obat}}</td>
                          <td>{{$opn->gudang}}</td>
                          <td>{{$opn->nama_gol_obat}}</td>
                          <td>{{$opn->nama_kat_obat}}</td>
                          <td>{{$opn->stok}} {{$opn->satuan}}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </form>
        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('vendors/holder/holder.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('vendors/jquery-export/xlsx.core.min.js')}}"></script>
<script src="{{asset('vendors/jquery-export/blob.min.js')}}"></script>
<script src="{{asset('vendors/jquery-export/FileSaver.js')}}"></script>
<script src="{{asset('vendors/jquery-export/tableExport.js')}}"></script>

<script type="text/javascript">
//And for the first simple table, which doesn't have TableTools or dataTables
				//select/deselect all rows according to table header checkbox
				var active_class = 'active';
        var myTable = $('#dynamic-table').DataTable({
                "aoColumns": [
                  { "bSortable": false },
                  null, null,null, null, null, null
                ],
                "aaSorting": [],
        });

        $('#ubahButton').click( function() {
            var data = myTable.$('input, select').serialize();

            window.location.href = '{{url("stok-opname/edit?")}}'+data;
            return false;
        });
				$('#dynamic-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
					var th_checked = this.checked;//checkbox inside "TH" table header
					
					$(this).closest('table').find('tbody > tr').each(function(){
						var row = this;
						if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
						else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
					});
				});
				
				//select/deselect a row when the checkbox is checked/unchecked
				$('#dynamic-table').on('click', 'td input[type=checkbox]' , function(){
					var $row = $(this).closest('tr');
					if($row.is('.detail-row ')) return;
					if(this.checked) $row.addClass(active_class);
					else $row.removeClass(active_class);
				});

</script>

@stop
