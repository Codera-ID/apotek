<html>

    <tbody>
        <tr>
            <td align="center">DATA STOK OPNAME</td>
        </tr>
    <tbody>

    <thead>
        <tr>
            <th>No</th>
            <th >Kode Obat</th>
            <th >Nama Obat</th>
            <th >Nama Gudang</th>
            <th >Golongan</th>
            <th >Kategori</th>
            <th >Stok Terkecil</th>
        </tr>
    </thead>

    <tbody>
        <?php $i=1; ?>
        @foreach($data as $key)
        <tr>
            <td class="center">{{$i}}</td>
            <td>{{$key->kode_obat}}</td>
            <td>{{$key->nama_obat}}</td>
            <td>{{$key->gudang}}</td>
            <td>{{$key->nama_gol_obat}}</td>
            <td>{{$key->nama_kat_obat}}</td>
            <td>{{$key->stok}} {{$key->satuan}}</td>
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>

</html>