@extends('layouts.home')
@section('css')
<link href="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.css')}}" rel="stylesheet" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Utitilitas Apotek</a>
      </li>
      <li class="active">Data Stock Opname</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Proses Stock Opname</h1>
          <div class="pull-right">

          </div>

        </div><!-- /.page-header -->
        <div class="">


          <form class="" action="{{url('stok-opname/edit')}}" method="post">



        </div>

        <div class="row">
          <div class="col-xs-12">
            <div class="table-responsive">
              <table  class="table table-responsive table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th style="text-align:center">Action</th>
                    <th >Kode Obat</th>
                    <th >Kode Barcode</th>
                    <th>Nama</th>
                    <th>Stok</th>
                    <th>Stok Nyata</th>
                    <th>Selisih</th>
                    <th>Satuan</th>
                    <th>Keterangan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  {{csrf_field()}}
                  @foreach($sat as $sat)
                  <input type="hidden" name="kd_satuan[]" value="{{$sat->kd_satuan}}" >
                    <tr>
                      <td>{{$i++}}</td>
                      <td> <a href="#md-footer-warning-{{$sat->kd_satuan}}" title="hapus" data-toggle="modal" class="btn btn-sm btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a> </td>
                      <td>{{$sat->kd_obat}}</td>
                      <td>{{$sat->barcode_obat}}</td>
                      <td>{{$sat->nama_obat}}</td>
                      <td>{{$sat->stok}}</td>
                        <td> <input id="nilai{{$sat->kd_satuan}}" class="price2" type="text" name="stok_baru[]" value="{{$sat->stok}}" style="width:70px;"> </td>
                        <td> <div  id="selisih{{$sat->kd_satuan}}"> </div> </td>
                        <td>{{$sat->nama_sat_obat}}</td>
                        <td><input type="text" name="keterangan[]" value=""></td>
                    </tr>

                  @endforeach
                </tbody>
              </table>
              <div class="pull-left">

                <button type="submit" id="ubahButton" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-pencil"></span> Proses</button>
              </div>
            </div>
          </div>
        </div>
          </form>
        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('vendors/holder/holder.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/priceformat/jquery.priceformat.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
});
} );
$("#check").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});

</script>

@foreach($sat1 as $sat)
<script type="text/javascript">
$("#nilai{{$sat->kd_satuan}}").on("change",function(){
  var nilai = $("#nilai{{$sat->kd_satuan}}").val();
  var selisih = parseFloat(nilai) - {{$sat->stok}};
  $("#selisih{{$sat->kd_satuan}}").empty();
  $("#selisih{{$sat->kd_satuan}}").append(""+selisih.toFixed(2));
});
</script>
<div id="md-footer-warning-{{$sat->kd_satuan}}" tabindex="-1" role="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
      </div>
      <div class="modal-body">
        <div class="text-center">
          <div class="text-warning" style="color:#FFB752"><span class="modal-main-icon mdi mdi-alert-triangle" style="font-size: 80px;"></span></div>
          <h3>Warning!</h3>
          <p>Yakin Data Akan Di Hapus?</p>
          <div class="xs-mt-50">

            <form class="" action="{{url('/stok-opname/delete/'.$sat->kd_satuan)}}" method="post">
              {{csrf_field()}}
              <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
              <button type="submit"  class="btn btn-space btn-primary">Proceed</button>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endforeach
@stop
