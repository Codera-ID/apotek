@extends('layouts.home')
@section('css')
<link href="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.css')}}" rel="stylesheet" />
@stop
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Utilitas</a>
      </li>
      <li class="active">Cetak Barcode</li>
    </ul><!-- /.breadcrumb -->
  </div>
  <div class="page-content">
    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header lighter blue col-sm-12">
          <h1 class="pull-left">Cetak Barcode</h1>
          <br>
          <div class="pull-right all-button">
          <a class="btn btn-primary btn-sm" href="{{url('barcode/obat')}}"><i class="fa fa-plus"></i>  Pilih Obat</a>
        </div>
        </div><!-- /.page-header -->
        <div class="">

        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="table-responsive">
              <table id="dynamic-table"  class="table table-responsive table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="center">#</th>
                    <th class="center">Action</th>                                                     
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>Barcode</th>                    
                    <th>Kategori</th>
                    <th>Satuan</th>
                    <th>Jenis</th>  
                    <th>Jumlah</th>                      
                  </tr>
                </thead>

                <tbody>
        <form class="" action="{{url('barcode/generate')}}" method="get">                
                  <?php $i = 1; ?>
                  @foreach($obat as $ob)
                   <tr>
                     <td>{{$i++}}</td>
                     <td class="center">
                        <a class="red" href="{{url('barcode/destroybarcode/'.$ob->kd_obat)}}">
                          <i class="ace-icon fa fa-trash-o bigger-130"></i>
                        </a>
                      </td>          
                     <td>{{$ob->kd_obat}}</td>
                     <td>{{$ob->nama_obat}}</td>
                     <td>{{$ob->barcode_obat}}</td>
                     <td>{{$ob->nama_kat_obat}}</td>
                     <td>{{$ob->nama_sat_obat}}</td>
                     <td>{{$ob->jenis_obat}}</td>
                     <td><input type="text" name="count[]" value="1"/><input type="hidden" name="barcode[]" value="{{$ob->barcode_obat}}"/></td>                                                                      
                   </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <br>
          
            <div class="pull-left">
              <button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-print"></span> Cetak</button>
            </div>
          </form>
        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('vendors/holder/holder.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/jasny-bootstrap/jasny-bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>

<script type="text/javascript">
//  var myTable = $('#dynamic-table').DataTable({
//   "aoColumns": [
//     { "bSortable": false },
//     null,  { "bSortable": false }, null,null, null, null, null, null
//   ],
//   "aaSorting": [],
// });
// $.extend( $.fn.dataTable.defaults, {
//   columnDefs: [{
//       targets: [ 3 ]
//   }]
// });
</script>

@stop
