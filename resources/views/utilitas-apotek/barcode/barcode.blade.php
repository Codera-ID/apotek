<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <script type="text/javascript" src="{{asset('assets\jsbarcode\JsBarcode.all.min.js')}}">

    </script>

    <style>
      div {
          text-align: justify;
      }

      div img {
          display: inline-block;
          width: 100px;
          height: 100px;
      }

      div:after {
          content: '';
          display: inline-block;
          width: 100%;
      }
    </style>
  </head>
  <body>
    <div>
    @foreach($obats as $ob)
    
      @for ($i = 0; $i < $ob->count; $i++)
        <img id="barcode{{ $ob->barcode }}"/>
        <script>
          JsBarcode("#barcode{{ $ob->barcode }}", {{$ob->barcode}});
        </script>
      @endfor
    @endforeach
    </div>
    <script type="text/javascript">
      window.print();
    </script>
  </body>
</html>
