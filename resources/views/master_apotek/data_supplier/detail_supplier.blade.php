@extends('layouts.home')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
  <ul class="breadcrumb">
    <li>
      <i class="ace-icon fa fa-home home-icon"></i>
      <a href="#">Home</a>
    </li>

    <li>
      <a href="#">Master Apotek</a>
    </li>
    <li class="active">Detail Supplier</li>
  </ul><!-- /.breadcrumb -->

</div>

<div class="page-content">
  <!-- /.ace-settings-container -->
    <div class="page-header">
      <h1>
        Master Apotek
        <small>
        <i class="ace-icon fa fa-angle-double-right"></i>
        Detail Supplier
        </small>
      </h1>
    </div><!-- /.page-header -->

    <div class="row data">
      <div class="col-xs-12">
        <div class="page-header-detail">
          <h1>Detail Supplier</h1>
        </div>
</div>
<table class="table table-striped detail">
    <tbody>
      <tr>
        <th>Kode </th>
        <td>{{$supplier->kd_supplier}}</td>
      </tr>
      <tr>
        <th>Nama </th>
        <td>{{$supplier->nama_supplier}}</td>
      </tr>
      <tr>
        <th>Alamat </th>
        <td>{{$supplier->alamat}}</td>
      </tr>
      <tr>
        <th>Kota</th>
        <td>{{$supplier->kota}}</td>
      </tr>
      <tr>
        <th>No. Telepon</th>
        <td>{{$supplier->no_tlpn}}</td>
      </tr>
      <tr>
        <th>No. Hp</th>
        <td>{{$supplier->no_hp}}</td>
      </tr>
      <tr>
        <th>No. Rekening</th>
        <td>{{$supplier->no_rek}}</td>
      </tr>
      <tr>
        <th>NPWP</th>
        <td>{{$supplier->npwp}}</td>
      </tr>
      <tr>
        <th>Tanggal Registrasi</th>
        <td>{{$supplier->created_at}}</td>
      </tr>
      <tr>
        <th>Status</th>
        <td>{{$supplier->status}}</td>
      </tr>
    </tbody>
  </table>
</div>
<div class="clearfix form-actions">
  <div class="col-md-6">
    <a href="{{url('supplier/ubah/'.$supplier->kd_supplier)}}">
    <button class="btn btn-warning" type="button">
      <i class="ace-icon fa fa-check bigger-110"></i>
        Ubah
      </button>
    </a>
    </div>
    <div class="col-md-offset-4 col-md-2">
      <a href="{{url('supplier')}}">
      <button class="btn btn-danger" type="button">
        <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
          Kembali
      </button>
    </a>
    </div>
   </div>

</body>
</html>

@stop
@section('js')
@stop
