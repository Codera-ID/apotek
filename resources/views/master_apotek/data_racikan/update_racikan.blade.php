@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Master Apotek</a>
      </li>
      <li class="active">Ubah Racikan</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Master Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Ubah Data Racikan
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Ubah Racikan</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('racikan/ubah')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Kode Racikan </label>
    								<div class="col-sm-8">
    									<input type="text"  name="kd_racikan" value="{{$racikan->kd_obat}}" class="col-xs-10" readonly required>
    								</div>
    						</div>
    						<div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Nama Racikan </label>
    								<div class="col-sm-8">
    									<input type="text"  name="nama_racikan" value="{{$racikan->nama_obat}}" class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Kategori </label>
    								<div class="col-sm-8">
    									<select name="kd_kat_obat" class="col-xs-10" required>
												@foreach($kat_obat as $key)
                          @if ($key->kd_kat_obat == $racikan->kategori)
												    <option value="{{$key->kd_kat_obat}}" selected>{{$key->nama_kat_obat}}</option>
                          @else
                            <option value="{{$key->kd_kat_obat}}">{{$key->nama_kat_obat}}</option>
                          @endif
												@endforeach
                      </select>
    								</div>
    						</div>
								<div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Obat </label>
    								<div class="col-sm-8">
    									<select name="kd_obat[]" id="obat" class="chosen-select col-xs-10" multiple="multiple">
												@foreach($obat as $key)
													@if(in_array($key->kd_obat, $r_obat))
														<option value="{{$key->kd_obat}}" selected>{{$key->nama_obat}}</option>													
													@else
														<option value="{{$key->kd_obat}}">{{$key->nama_obat}}</option>																																												
													@endif
												@endforeach
                      </select>
    								</div>
    						</div>
              </div>
          </div>
					<hr>
					<div class="clearfix form-actions">
						<div class="col-md-6">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
							</button>
							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
									Reset
							</button>
						</div>
            <div class="col-md-offset-4 col-md-2">
              <a href="{{url('racikan')}}">
								<button class="btn btn-danger" type="button">
									<i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
										Kembali
								</button>
               </a>
						</div>
			    </div>
			  </form>
				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>

<script>
	$('.chosen-select').chosen({allow_single_deselect:true}); 
</script>
@stop
