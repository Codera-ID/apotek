@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Master Apotek</a>
      </li>
      <li class="active">Tambah Gudang</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Master Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Tambah Data Gudang
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Tambah Gudang</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('gudang/tambah')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Kode </label>
    								<div class="col-sm-8">
    									<input type="text"  name="kd_gudang" class="col-xs-10" value="{{$kd_gudang}}" readonly>
    								</div>
    						</div>
    						<div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Nama </label>
    								<div class="col-sm-8">
    									<input type="text"  name="nama_gudang" class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Alamat </label>
    								<div class="col-sm-8">
    									<textarea name="alamat" class="col-xs-10"></textarea>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Kota </label>
    								<div class="col-sm-8">
    									<input type="text"  name="kota" class="col-xs-10" required>
    								</div>
    						</div>
              </div>
              <div class="col-sm-6">
    						<div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1">No. Telepon </label>
    								<div class="col-sm-9">
    									<input type="number"  name="no_tlpn"class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1">No. HP </label>
    								<div class="col-sm-9">
    									<input type="number"  name="no_hp" class="col-xs-10" required>
    								</div>
    						</div>
              </div>
          </div>
						<div class="clearfix form-actions">
							<div class="col-md-6">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
									</button>
								</div>
                <div class="col-md-offset-4 col-md-2">
                  <a href="{{url('gudang')}}">
									<button class="btn btn-danger" type="button">
										<i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
											Kembali
									</button>
                </a>
								</div>
			         </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
@stop
