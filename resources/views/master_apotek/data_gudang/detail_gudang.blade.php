@extends('layouts.home')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
  <ul class="breadcrumb">
    <li>
      <i class="ace-icon fa fa-home home-icon"></i>
      <a href="#">Home</a>
    </li>

    <li>
      <a href="#">Master Apotek</a>
    </li>
    <li class="active">Detail Gudang</li>
  </ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
  <!-- /.ace-settings-container -->
    <div class="page-header">
      <h1>
        Master Apotek
        <small>
        <i class="ace-icon fa fa-angle-double-right"></i>
        Detail Gudang
        </small>
      </h1>
    </div><!-- /.page-header -->

    <div class="row data">
      <div class="col-xs-12">
        <div class="page-header-detail">
          <h1>Detail Gudang</h1>
        </div>
</div>
<table class="table table-striped detail">
    <tbody>
      <tr>
        <th>Kode </th>
        <td>{{$gudang->kd_gudang}}</td>
      </tr>
      <tr>
        <th>Nama </th>
        <td>{{$gudang->nama_gudang}}</td>
      </tr>
      <tr>
        <th>Alamat </th>
        <td>{{$gudang->alamat}}</td>
      </tr>
      <tr>
        <th>Kota</th>
        <td>{{$gudang->kota}}</td>
      </tr>
      <tr>
        <th>No. Telepon</th>
        <td>{{$gudang->no_tlpn}}</td>
      </tr>
      <tr>
        <th>No. Hp</th>
        <td>{{$gudang->no_hp}}</td>
      </tr>
      <tr>
        <th>Tanggal Registrasi</th>
        <td>{{$gudang->created_at}}</td>
      </tr>
      <tr>
        <th>Status</th>
        <td>{{$gudang->status}}</td>
      </tr>
    </tbody>
  </table>
</div>
<div class="clearfix form-actions">
  <div class="col-md-6">
    <a href="{{url('gudang/ubah/'.$gudang->kd_gudang)}}">
    <button class="btn btn-warning" type="button">
      <i class="ace-icon fa fa-check bigger-110"></i>
        Ubah
      </button>
    </a>
    </div>
    <div class="col-md-offset-4 col-md-2">
      <a href="{{url('gudang')}}">
      <button class="btn btn-danger" type="button">
        <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
          Kembali
      </button>
    </a>
    </div>
   </div>

</body>
</html>

@stop
@section('js')
@stop
