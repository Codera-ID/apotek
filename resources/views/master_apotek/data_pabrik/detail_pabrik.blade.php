@extends('layouts.home')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
  <ul class="breadcrumb">
    <li>
      <i class="ace-icon fa fa-home home-icon"></i>
      <a href="#">Home</a>
    </li>

    <li>
      <a href="#">Master Apotek</a>
    </li>
    <li class="active">Detail Pabrik</li>
  </ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
  <!-- /.ace-settings-container -->
    <div class="page-header">
      <h1>
        Master Apotek
        <small>
        <i class="ace-icon fa fa-angle-double-right"></i>
        Detail Pabrik
        </small>
      </h1>
    </div><!-- /.page-header -->

    <div class="row data">
      <div class="col-xs-12">
        <div class="page-header-detail">
          <h1>Detail Pabrik</h1>
        </div>
      </div>
      <table class="table table-striped detail">
          <tbody>
            <tr>
              <th>Kode </th>
              <td>{{$pabrik->kd_pabrik}}</td>
            </tr>
            <tr>
              <th>Nama </th>
              <td>{{$pabrik->nama_pabrik}}</td>
            </tr>
            <tr>
              <th>Alamat </th>
              <td>{{$pabrik->alamat}}</td>
            </tr>
            <tr>
              <th>Kota</th>
              <td>{{$pabrik->kota}}</td>
            </tr>
            <tr>
              <th>No. Telepon</th>
              <td>{{$pabrik->no_tlpn}}</td>
            </tr>
            <tr>
              <th>No. Hp</th>
              <td>{{$pabrik->no_hp}}</td>
            </tr>
            <tr>
              <th>NPWP</th>
              <td>{{$pabrik->npwp}}</td>
            </tr>
            <tr>
              <th>Tanggal Registrasi</th>
              <td>{{$pabrik->created_at}}</td>
            </tr>
            <tr>
              <th>Status</th>
              <td>{{$pabrik->status}}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="clearfix form-actions">
        <div class="col-md-6">
          <a href="{{url('pabrik/ubah/'.$pabrik->kd_pabrik)}}">
          <button class="btn btn-warning" type="button">
            <i class="ace-icon fa fa-check bigger-110"></i>
              Ubah
            </button>
          </a>
          </div>
          <div class="col-md-offset-4 col-md-2">
            <a href="{{url('pabrik')}}">
            <button class="btn btn-danger" type="button">
              <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
                Kembali
            </button>
          </a>
          </div>
         </div>

</body>
</html>

@stop
@section('js')
@stop
