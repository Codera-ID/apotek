@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Master Apotek</a>
      </li>
      <li class="active">Update Apoteker</li>
    </ul><!-- /.breadcrumb -->

  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Master Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Update Data Apoteker
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Update Apoteker</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('apoteker/ubah')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id_apoteker" value="{{$apoteker->id_apoteker}}">
            <div class="row">
              <div class="col-sm-6">
    						<div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Nama </label>
    								<div class="col-sm-8">
    									<input type="text"  name="nama_apoteker" class="col-xs-10" value="{{$apoteker->nama_apoteker}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> No.SIK/No.SIPA </label>
    								<div class="col-sm-8">
    									<input type="text"  name="no_sik_sipa" class="col-xs-10" value="{{$apoteker->no_sik_sipa}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> No.STRA </label>
    								<div class="col-sm-8">
    									<input type="text"  name="no_stra" class="col-xs-10" value="{{$apoteker->no_stra}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Alamat </label>
    								<div class="col-sm-8">
    									<textarea name="alamat" class="col-xs-10">{{$apoteker->alamat}}</textarea>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Kota </label>
    								<div class="col-sm-8">
    									<input type="text"  name="kota" class="col-xs-10" value="{{$apoteker->kota}}" required>
    								</div>
    						</div>
              </div>
              <div class="col-sm-6">
    						<div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1">No. Telepon </label>
    								<div class="col-sm-9">
    									<input type="text"  name="no_tlpn" class="col-xs-10" value="{{$apoteker->no_tlpn}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Email </label>
    								<div class="col-sm-9">
    									<input type="email"  name="email" class="col-xs-10" value="{{$apoteker->email}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Tanggal Mulai Tugas </label>
    								<div class="col-sm-9">
    									<input type="date"  name="tgl_mulai" class="col-xs-10" value="{{$apoteker->tanggal_mulai_tugas}}" required>
    								</div>
    						</div>

                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Status </label>
    								<div class="col-sm-9">
    									<select name="status" class="col-xs-10" required>
                        <option value="Aktif" {{$apoteker->status == 'Aktif'? 'selected' : ''}}>Aktif</option>
                        <option value="Non Aktif" {{$apoteker->status == 'Non Aktif'? 'selected' : ''}}>Non Aktif</option>
                      </select>
    								</div>
    						</div>
              </div>
          </div>
						<div class="clearfix form-actions">
							<div class="col-md-6">
								<button class="btn btn-warning" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
										Ubah
									</button>
								</div>
                <div class="col-md-offset-4 col-md-2">
                  <a href="{{url('apoteker')}}">
									<button class="btn btn-danger" type="button">
										<i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
											Kembali
									</button>
                </a>
								</div>
			         </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
@stop
