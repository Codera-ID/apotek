@extends('layouts.home')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
  <ul class="breadcrumb">
    <li>
      <i class="ace-icon fa fa-home home-icon"></i>
      <a href="#">Home</a>
    </li>

    <li>
      <a href="#">Master Apotek</a>
    </li>
    <li class="active">Detail Apoteker</li>
  </ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
  <!-- /.ace-settings-container -->
    <div class="page-header">
      <h1>
        Master Apotek
        <small>
        <i class="ace-icon fa fa-angle-double-right"></i>
        Detail Apoteker
        </small>
      </h1>
    </div><!-- /.page-header -->

    <div class="row data">
      <div class="col-xs-12">
        <div class="page-header-detail">
          <h1>Detail Apoteker</h1>
        </div>
</div>
<table class="table table-striped detail">
    <tbody>
      <tr>
        <th>Nama :</th>
        <td>{{$apoteker->nama_apoteker}}</td>
      </tr>
      <tr>
        <th>No.SIK/No.SIPA</th>
        <td>{{$apoteker->no_sik_sipa}}</td>
      </tr>
      <tr>
        <th>No.STRA</th>
        <td>{{$apoteker->no_stra}}</td>
      </tr>
      <tr>
        <th>Alamat</th>
        <td>{{$apoteker->alamat}}</td>
      </tr>
      <tr>
        <th>Kota</th>
        <td>{{$apoteker->kota}}</td>
      </tr>
      <tr>
        <th>No. Telepon</th>
        <td>{{$apoteker->no_tlpn}}</td>
      </tr>
      <tr>
        <th>Email</th>
        <td>{{$apoteker->email}}</td>
      </tr>
      <tr>
        <th>Tanggal Mulai Tugas</th>
        <td>{{$apoteker->tanggal_mulai_tugas}}</td>
      </tr>
      <tr>
        <th>Tanggal Registrasi</th>
        <td>{{$apoteker->created_at}}</td>
      </tr>
      <tr>
        <th>Status</th>
        <td>{{$apoteker->status}}</td>
      </tr>
    </tbody>
  </table>
</div>
<div class="clearfix form-actions">
  <div class="col-md-6">
    <a href="{{url('apoteker/ubah/'.$apoteker->id_apoteker)}}">
    <button class="btn btn-warning" type="button">
      <i class="ace-icon fa fa-check bigger-110"></i>
        Ubah
      </button>
    </a>
    </div>
    <div class="col-md-offset-4 col-md-2">
      <a href="{{url('apoteker')}}">
      <button class="btn btn-danger" type="button">
        <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
          Kembali
      </button>
    </a>
    </div>
   </div>

</body>
</html>

@stop
@section('js')
@stop
