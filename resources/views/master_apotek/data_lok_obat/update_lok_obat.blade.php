@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Master Klinik</a>
      </li>
      <li class="active">Update Lokasi Obat</li>
    </ul><!-- /.breadcrumb -->

  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Master Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Update Data Lokasi Obat
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Update Lokasi Obat</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('lokasi-obat/ubah')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="kd_lok_obat" value="{{$lok_obat->kd_lok_obat}}">
            <div class="row">
              <div class="col-sm-12">
    						<div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Nama </label>
    								<div class="col-sm-8">
    									<input type="text"  name="nama_lok_obat" class="col-xs-10" value="{{$lok_obat->nama_lok_obat}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Status </label>
    								<div class="col-sm-8">
    									<select name="status" class="col-xs-10" required>
                        <option value="Aktif" {{$lok_obat->status == 'Aktif'? 'selected' : ''}}>Aktif</option>
                        <option value="Non Aktif" {{$lok_obat->status == 'Non Aktif'? 'selected' : ''}}>Non Aktif</option>
                      </select>
    								</div>
    						</div>
              </div>
          </div>
						<div class="clearfix form-actions">
							<div class="col-md-6">
								<button class="btn btn-warning" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
										Ubah
									</button>
								</div>
                <div class="col-md-offset-4 col-md-2">
                  <a href="{{url('lokasi-obat')}}">
									<button class="btn btn-danger" type="button">
										<i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
											Kembali
									</button>
                </a>
								</div>
			         </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
@stop
