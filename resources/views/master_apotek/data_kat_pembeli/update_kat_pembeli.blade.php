@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Master Apotek</a>
      </li>
      <li class="active">Data Kategori Pasien</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Master Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Ubah Data Kategori Pasien
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Ubah Kategori Pasien</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('kat-pasien/ubah')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="kd_kat_pasien" value="{{$data->kd_kat_pasien}}"/>
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> Nama Kategori </label>
                                    <div class="col-sm-8">
                                        <input type="text"  name="nama_kat_pembeli" class="col-xs-10" value="{{$data->nama_kategori}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> Info Kategori </label>
                                    <div class="col-sm-8">
                                        <textarea name="keterangan" class="col-xs-10">{{$data->info_kategori}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> Harga Jual </label>
                                    <div class="col-sm-8">
                                        <input type="number"  name="harga_jual" class="col-xs-10" value="{{$data->harga_jual}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> Diskon Harga Jual </label>
                                    <div class="col-sm-8">
                                        <input type="number"  name="harga_disc" class="col-xs-10" value="{{$data->disc_harga_jual}}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix form-actions">
                            <div class="col-md-6">
                                <button class="btn btn-info" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                        Submit
                                    </button>
                                </div>
                                <div class="col-md-offset-4 col-md-2">
                                <a href="{{url('kat-pasien')}}">
                                    <button class="btn btn-danger" type="button">
                                        <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
                                            Kembali
                                    </button>
                                </a>
                            </div>
                        </div>
			        </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
@stop
