@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Master Apotek</a>
      </li>
      <li class="active">Data Kategori Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>
  @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
  @endif

  <div class="page-content">
    <div class="page-header">
      <h1>
        Master Apotek
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          Data Kategori Pasien
        </small>
      </h1>
      <div class="pull-right all-button">
        <a class="btn btn-primary btn-sm" href="{{url('kat-pasien/tambah')}}"><i class="fa fa-plus"></i>  Tambah</a>
        <!-- <a class="btn btn-primary btn-sm" href="{{url('kategori-obat/print')}}"><i class="glyphicon glyphicon-print"></i> Print</a> -->
        <!-- <a class="btn btn-primary btn-sm" href="{{url('kategori-obat/export')}}"><i class="fa fa-upload"></i> Export</a> -->
      </div>
      <br>
    </div><!-- /.page-header -->

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
          <div class="col-xs-12">
            <div class="table-header">
              Data Kategori Pasien
            </div>

            <!-- div.table-responsive -->

            <!-- div.dataTables_borderWrap -->
            <div>
              <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="center">No.</th>
                    <th>Nama Kategori</th>
                    <th>Info Kategori</th>
                    <th>Harga Jual</th>
                    <th>Diskon Harga Jual (%)</th>                    
                    <th>Tanggal Diubah</th>                    
                    <th>Dibuat Oleh</th>                    
                    <th>Tanggal Dibuat</th>    
                    <th>Action</th>                                                        
                  </tr>
                </thead>

                <tbody>
                  <?php $i=1; ?>
                  @foreach($data as $key)
                  <tr>
                    <td class="center">{{$i}}</td>
                    <td>{{$key->nama_kategori}}</td>
                    <td>{{$key->info_kategori}}</td>
                    <td>{{$key->harga_jual}}</td>
                    <td>{{$key->disc_harga_jual}} %</td>
                    <td>{{$key->updated_at}}</td>
                    <td>{{$key->name}}</td>
                    <td>{{$key->created_at}}</td>                    
                    <td>
                      <div class="hidden-sm hidden-xs action-buttons">
                        <a class="green" href="{{url('kat-pasien/ubah/'.$key->kd_kat_pasien)}}">
                          <i class="ace-icon fa fa-pencil bigger-130"></i>
                        </a>

                        <a class="red" href="{{url('kat-pasien/hapus/'.$key->kd_kat_pasien)}}">
                          <i class="ace-icon fa fa-trash-o bigger-130"></i>
                        </a>
                      </div>
                    </td>
                  </tr>
                  <?php $i++; ?>
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable({
        "aoColumns": [
            { "bSortable": false },
            null, null,null, null, null, null, null,
            { "bSortable": false }
        ],
        "aaSorting": []
  });

  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
});
} );
</script>
@stop
