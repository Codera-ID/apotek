@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Master Apotek</a>
      </li>
      <li class="active">Tambah Pasien</li>
    </ul><!-- /.breadcrumb -->

    <div class="nav-search" id="nav-search">
      <form class="form-search">
        <span class="input-icon">
          <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
          <i class="ace-icon fa fa-search nav-search-icon"></i>
        </span>
      </form>
    </div><!-- /.nav-search -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Master Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Tambah Data Pasien
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Master Apotek</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('pasien/tambah')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="col-sm-6">
    						<div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> No.RM </label>
    								<div class="col-sm-8">
    									<input type="text" id="form-field-1" name="id_rm" class="col-xs-10" value="{{$id_rm}}" readonly>
    								</div>
    						</div>
    						<div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Nama </label>
    								<div class="col-sm-8">
    									<input type="text" id="form-field-1" name="nama_pasien" class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Golongan Darah </label>
    								<div class="col-sm-8">
    									<input type="text" id="form-field-1" name="gol_darah" class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Jenis Kelamin </label>
    								<div class="col-sm-8">
                      <input type="radio" id="form-field-1" name="jenis_kelamin" value="Laki-laki" class="col-xs-1" required>Laki-laki<br>
    									<input type="radio" id="form-field-1" name="jenis_kelamin" value="Perempuan" class="col-xs-1" required>Perempuan
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Tanggal Lahir</label>
    								<div class="col-sm-8">
    									<input type="text" id="tanggal_lahir" name="tgl_lahir" class="datepicker col-xs-10" required>
    								</div>
    						</div>
      						<div class="form-group">
      							<label class="col-sm-4 control-label" for="form-field-1">Umur </label>
      								<div class="col-sm-8">
      									<input type="number"  id="umur" name="umur"class="col-xs-10" required>
      								</div>
      						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Alamat </label>
    								<div class="col-sm-8">
    									<textarea name="alamat" class="col-xs-10"></textarea>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Kota </label>
    								<div class="col-sm-8">
    									<input type="text" id="form-field-1" name="kota" class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
                  <label class="col-sm-4 control-label" for="form-field-1">No. Telepon </label>
                    <div class="col-sm-8">
                      <input type="number" id="form-field-1" name="no_tlpn"class="col-xs-10" required>
                    </div>
                </div>
              </div>
              <div class="col-sm-6">
                
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1">Status Perkawinan </label>
    								<div class="col-sm-9">
                      <select name="status_perkawinan" class="col-xs-10">
                        <option value="">-Pilih Status Perkawinan-</option>
                        <option value="Belum Kawin">Belum Kawin</option>
                        <option value="Sudah Kawin">Sudah Kawin</option>
                        <option value="Janda">Janda</option>
                        <option value="Duda">Duda</option>
                      </select>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Pekerjaan </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="pekerjaan" class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Nama Ibu </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="nama_ibu" class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Nama Ayah </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="nama_ayah" class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> No. KK </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="no_kk" class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Email </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="email" class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Alergi Obat </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="alergi_obat" class="col-xs-10" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Pin BBM </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="pin_bbm" class="col-xs-10" required>
    								</div>
    						</div>
              </div>
          </div>
						<div class="clearfix form-actions">
							<div class="col-md-6">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
									</button>
								</div>
                <div class="col-md-offset-4 col-md-2">
                  <a href="{{url('pasien')}}">
									<button class="btn btn-danger" type="button">
										<i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
											Kembali
									</button>
                </a>
								</div>
			         </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script type="text/javascript">
  $(document).ready( function () {

		$('.datepicker').datepicker({
				format: 'yyyy-mm-dd',
		});

    // Menghitung umur otomatis
    $('#tanggal_lahir').change(function(){
      var date_input = new Date(this.value);
      var today = new Date();
      var year_input = date_input.getFullYear();
      var year = today.getFullYear();

      $('#umur').val(year-year_input);  
    });
  });
</script>
@stop
