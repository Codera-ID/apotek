@extends('layouts.home')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
  <ul class="breadcrumb">
    <li>
      <i class="ace-icon fa fa-home home-icon"></i>
      <a href="#">Home</a>
    </li>

    <li>
      <a href="#">Master Apotek</a>
    </li>
    <li class="active">Detail Pasien</li>
  </ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
  <!-- /.ace-settings-container -->
    <div class="page-header">
      <h1>
        Master Apotek
        <small>
        <i class="ace-icon fa fa-angle-double-right"></i>
        Detail Pasien
        </small>
      </h1>
    </div><!-- /.page-header -->

    <div class="row data">
      <div class="col-xs-12">
        <div class="page-header-detail">
          <h1>Detail Pasien</h1>
        </div>
</div>
<table class="table table-striped detail">
    <tbody>
      <tr>
        <th>No. RM :</th>
        <td>{{$pasien->id_rm}}</td>
      </tr>
      <tr>
        <th>Nama :</th>
        <td>{{$pasien->nama_pasien}}</td>
      </tr>
      <tr>
        <th>Golongan Darah</th>
        <td>{{$pasien->gol_darah}}</td>
      </tr>
      <tr>
        <th>Jenis Kelamin</th>
        <td>{{$pasien->jenis_kelamin}}</td>
      </tr>
      <tr>
        <th>Tanggal Lahir</th>
        <td>{{$pasien->tanggal_lahir}}</td>
      </tr>
      <tr>
        <th>Umur</th>
        <td>{{$pasien->umur}} Thn</td>
      </tr>
      <tr>
        <th>Alamat</th>
        <td>{{$pasien->alamat}}</td>
      </tr>
      <tr>
        <th>Kota</th>
        <td>{{$pasien->kota}}</td>
      </tr>
      <tr>
        <th>No. Telepon</th>
        <td>{{$pasien->no_tlpn}}</td>
      </tr>
      <tr>
        <th>Status Perkawinan</th>
        <td>{{$pasien->status_perkawinan}}</td>
      </tr>
      <tr>
        <th>Pekerjaan</th>
        <td>{{$pasien->pekerjaan}}</td>
      </tr>
      <tr>
        <th>Nama Ibu</th>
        <td>{{$pasien->nama_ibu}}</td>
      </tr>
      <tr>
        <th>Nama Ayah</th>
        <td>{{$pasien->nama_ayah}}</td>
      </tr>
      <tr>
        <th>No KK</th>
        <td>{{$pasien->no_kk}}</td>
      </tr>
      <tr>
        <th>Email</th>
        <td>{{$pasien->email}}</td>
      </tr>
      <tr>
        <th>Alergi Obat</th>
        <td>{{$pasien->alergi_obat}}</td>
      </tr>
      <tr>
        <th>Pin BBM</th>
        <td>{{$pasien->pin_bbm}}</td>
      </tr>
      <tr>
        <th>Tanggal Registrasi</th>
        <td>{{$pasien->tanggal_registrasi}}</td>
      </tr>
      <tr>
        <th>Status</th>
        <td>{{$pasien->status}}</td>
      </tr>
    </tbody>
  </table>
</div>
<div class="clearfix form-actions">
  <div class="col-md-6">
    <a href="{{url('pasien/ubah/'.$pasien->id_pasien)}}">
    <button class="btn btn-warning" type="button">
      <i class="ace-icon fa fa-check bigger-110"></i>
        Ubah
      </button>
    </a>
    </div>
    <div class="col-md-offset-4 col-md-2">
      <a href="{{url('pasien')}}">
      <button class="btn btn-danger" type="button">
        <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
          Kembali
      </button>
    </a>
    </div>
   </div>

</body>
</html>

@stop
@section('js')
@stop
