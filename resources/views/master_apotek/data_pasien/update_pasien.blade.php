@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Master Apotek</a>
      </li>
      <li class="active">Update Pasien</li>
    </ul><!-- /.breadcrumb -->

  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Master Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Update Data Pasien
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Update Pasien</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('pasien/ubah')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_pasien" value="{{$pasien->id_rm}}" />
            <div class="row">
              <div class="col-sm-6">
    						<div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> No.RM </label>
    								<div class="col-sm-8">
    									<input type="text" id="form-field-1" name="id_rm" class="col-xs-10" value="{{$pasien->id_rm}}" readonly>
    								</div>
    						</div>
    						<div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Nama </label>
    								<div class="col-sm-8">
    									<input type="text" id="form-field-1" name="nama_pasien" class="col-xs-10" value="{{$pasien->nama_pasien}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Golongan Darah </label>
    								<div class="col-sm-8">
    									<input type="text" id="form-field-1" name="gol_darah" class="col-xs-10" value="{{$pasien->gol_darah}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Jenis Kelamin </label>
    								<div class="col-sm-8">
                      <input type="radio" id="form-field-1" name="jenis_kelamin" value="Laki-laki" class="col-xs-1" required>Laki-laki<br>
    									<input type="radio" id="form-field-1" name="jenis_kelamin" value="Perempuan" class="col-xs-1" required>Perempuan
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Tanggal Lahir</label>
    								<div class="col-sm-8">
    									<input type="date" id="form-field-1" name="tgl_lahir" class="col-xs-10" value="{{$pasien->tanggal_lahir}}" required>
    								</div>
    						</div>
      						<div class="form-group">
      							<label class="col-sm-4 control-label" for="form-field-1">Umur </label>
      								<div class="col-sm-8">
      									<input type="number" id="form-field-1" name="umur"class="col-xs-10" value="{{$pasien->umur}}" required>
      								</div>
      						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Alamat </label>
    								<div class="col-sm-8">
    									<textarea name="alamat" class="col-xs-10">{{$pasien->alamat}}</textarea>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-4 control-label" for="form-field-1"> Kota </label>
    								<div class="col-sm-8">
    									<input type="text" id="form-field-1" name="kota" class="col-xs-10" value="{{$pasien->kota}}" required>
    								</div>
    						</div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1">No. Telepon </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="no_tlpn"class="col-xs-10" value="{{$pasien->no_tlpn}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1">Status Perkawinan </label>
    								<div class="col-sm-9">
                      <select name="status_perkawinan" class="col-xs-10">
                        <option value="">-Pilih Status Perkawinan-</option>
                        <option value="Belum Kawin" >Belum Kawin</option>
                        <option value="Sudah Kawin">Sudah Kawin</option>
                        <option value="Janda">Janda</option>
                        <option value="Duda">Duda</option>
                      </select>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Pekerjaan </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="pekerjaan" class="col-xs-10" value="{{$pasien->pekerjaan}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Nama Ibu </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="nama_ibu" class="col-xs-10" value="{{$pasien->nama_ibu}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Nama Ayah </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="nama_ayah" class="col-xs-10" value="{{$pasien->nama_ayah}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> No. KK </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="no_kk" class="col-xs-10" value="{{$pasien->no_kk}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Email </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="email" class="col-xs-10" value="{{$pasien->email}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Alergi Obat </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="alergi_obat" class="col-xs-10" value="{{$pasien->alergi_obat}}" required>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Pin BBM </label>
    								<div class="col-sm-9">
    									<input type="text" id="form-field-1" name="pin_bbm" class="col-xs-10" value="{{$pasien->pin_bbm}}" required>
    								</div>
    						</div>
              </div>
          </div>
						<div class="clearfix form-actions">
							<div class="col-md-6">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
									</button>
								</div>
                <div class="col-md-offset-4 col-md-2">
                  <a href="{{url('Pasien')}}">
									<button class="btn btn-danger" type="button">
										<i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
											Kembali
									</button>
                </a>
								</div>
			         </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
@stop
