@extends('layouts.home')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
  <ul class="breadcrumb">
    <li>
      <i class="ace-icon fa fa-home home-icon"></i>
      <a href="#">Home</a>
    </li>

    <li>
      <a href="#">Master Apotek</a>
    </li>
    <li class="active">Detail Obat</li>
  </ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
  <!-- /.ace-settings-container -->
    <div class="page-header">
      <h1>
        Master Apotek
        <small>
        <i class="ace-icon fa fa-angle-double-right"></i>
        Detail Obat
        </small>
      </h1>
    </div><!-- /.page-header -->

    <div class="row data">
      <div class="col-xs-12">
        <div class="page-header-detail">
          <h1>Detail Obat</h1>
        </div>
        <div class="tabbable">
          <ul class="nav nav-tabs" id="myTab">
            <li class="active">
              <a data-toggle="tab" href="#detail_obat">Detail Obat</a>
            </li>
            <li>
              <a data-toggle="tab" href="#detail_satuan">Detail Satuan</a>
            </li>
          </ul>

          <div class="tab-content">
            <div id="detail_obat" class="tab-pane fade in active">
              <div class="row data">
                <table class="table table-striped detail">
                    <tbody>
                      <tr>
                        <th>Kode </th>
                        <td>{{$obat->kd_obat}}</td>
                      </tr>
                      <tr>
                        <th>Nama </th>
                        <td>{{$obat->nama_obat}}</td>
                      </tr>
                      <tr>
                        <th>Pabrik </th>
                        <td>{{$obat->nama_pabrik}}</td>
                      </tr>
                      <tr>
                        <th>Golongan</th>
                        <td>{{$obat->nama_gol_obat}}</td>
                      </tr>
                      <tr>
                        <th>Kategori</th>
                        <td>{{$obat->nama_kat_obat}}</td>
                      </tr>
                      <tr>
                        <th>Jenis Obat</th>
                        <td>{{$obat->jenis_obat}}</td>
                      </tr>
                      <tr>
                        <th>Minimal Stok</th>
                        <td>{{$obat->minimal_stok}}</td>
                      </tr>
                      <tr>
                        <th>Indikasi</th>
                        <td>{{$obat->indikasi}}</td>
                      </tr>
                      <tr>
                        <th>Kandungan</th>
                        <td>{{$obat->kandungan}}</td>
                      </tr>
                      <tr>
                        <th>Dosis</th>
                        <td>{{$obat->dosis}}</td>
                      </tr>
                      <tr>
                        <th>Foto</th>
                        <td>{{$obat->foto}}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

            <div id="detail_satuan" class="tab-pane fade">
              <div class="row data">
                <table class="table table-striped detail">
                    <tbody>
                      <?php $i=1; ?>
                      @foreach($satuan as $item)
                      <tr>
                        <th colspan="4">Satuan <?php echo "$i"; ?></th>
                      </tr>
                      <tr>
                        <th>Satuan Terkecil </th>
                        <td colspan="3">{{$item->nama_sat_obat}}</td>
                      </tr>
                      <tr>
                        <th>Stok </th>
                        <td colspan="3">{{$item->stok}}</td>
                      </tr>
                      <tr>
                        <th>Lokasi </th>
                        <td colspan="3">{{$item->nama_lok_obat}}</td>
                      </tr>
                      <tr>
                        <th>HNA</th>
                        <td colspan="3">{{$item->hna}}</td>
                      </tr>
                      <tr>
                        <th>Harga Jual 1</th>
                        <td>{{$item->harga_jual_1}}</td>
                        <th>Diskon Harga 1</th>
                        <td>{{$item->diskon_harga_1}}</td>
                      </tr>
                      <tr>
                        <th>Harga Jual 2</th>
                        <td>{{$item->harga_jual_2}}</td>
                        <th>Diskon Harga 2</th>
                        <td>{{$item->diskon_harga_2}}</td>
                      </tr>
                      <tr>
                        <th>Harga Jual 3</th>
                        <td>{{$item->harga_jual_3}}</td>
                        <th>Diskon Harga 3</th>
                        <td>{{$item->diskon_harga_3}}</td>
                      </tr>
                      <tr>
                        <th>Barcode Obat</th>
                        <td colspan="3">{{$item->barcode_obat}}</td>
                      </tr>
                      <tr>
                        <th>No. Batch</th>
                        <td colspan="3">{{$item->no_batch}}</td>
                      </tr>
                      <tr>
                        <th>Tanggal Expired</th>
                        <td colspan="3">{{$item->tgl_expired}}</td>
                      </tr>
                      <?php $i++ ?>
                      @endforeach
                    </tbody>
                  </table>
            </div>
            </div>
          </div>
          </div>
        </div>
      </div>
      <div class="clearfix form-actions">
        <div class="col-md-6">
          <a href="{{url('obat/ubah/'.$obat->kd_obat)}}">
          <button class="btn btn-warning" type="button">
            <i class="ace-icon fa fa-check bigger-110"></i>
              Ubah
            </button>
          </a>
          </div>
          <div class="col-md-offset-4 col-md-2">
            <a href="{{url('obat')}}">
            <button class="btn btn-danger" type="button">
              <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
                Kembali
            </button>
          </a>
          </div>
         </div>

</body>
</html>

@stop
@section('js')
@stop
