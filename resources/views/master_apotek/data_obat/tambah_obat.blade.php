@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Master Apotek</a>
      </li>
      <li class="active">Tambah Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Master Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Tambah Data Obat
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Tambah Obat</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('obat/tambah')}}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="tabbable">
              <ul class="nav nav-tabs" id="myTab">
                <li class="active">
                  <a data-toggle="tab" href="#detail_obat">Detail Obat</a>
                </li>
                <li>
                  <a data-toggle="tab" href="#detail_satuan">Detail Satuan</a>
                </li>
              </ul>

              <div class="tab-content">
                <div id="detail_obat" class="tab-pane fade in active">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Kode </label>
          								<div class="col-sm-8">
          									<input type="text"  name="kd_obat" class="col-xs-10" value="{{$kd_obat}}" readonly>
          								</div>
          						</div>
          						<div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Nama </label>
          								<div class="col-sm-8">
          									<input type="text"  name="nama_obat" class="col-xs-10" required>
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Pabrik </label>
          								<div class="col-sm-8">
          									<select name="kd_pabrik" class="col-xs-10" required>
                              <option value="">-Pilih Pabrik-</option>
                              @foreach($pabrik as $key)
                                <option value="{{$key->kd_pabrik}}">{{$key->nama_pabrik}}</option>
                              @endforeach
                            </select>
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Golongan </label>
          								<div class="col-sm-8">
                            <select name="kd_gol_obat" class="col-xs-10" required>
                              <option value="">-Pilih Golongan-</option>
                              @foreach($gol_obat as $key)
                                <option value="{{$key->kd_gol_obat}}">{{$key->nama_gol_obat}}</option>
                              @endforeach
                            </select>
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Kategori </label>
          								<div class="col-sm-8">
                            <select name="kd_kat_obat" class="col-xs-10" required>
                              <option value="">-Pilih Kategori-</option>
                              @foreach($kat_obat as $key)
                                <option value="{{$key->kd_kat_obat}}">{{$key->nama_kat_obat}}</option>
                              @endforeach
                            </select>
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Jenis Obat </label>
          								<div class="col-sm-8">
                            <select name="jenis_obat" class="col-xs-10" required>
                              <option value="konsinyasi">KONSINYASI</option>
                              <option value="nonkonsinyasi">NON KONSINYASI</option>
                            </select>
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Minimal Stok </label>
          								<div class="col-sm-8">
          									<textarea name="minimal_stok" class="col-xs-10"></textarea>
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Indikasi </label>
          								<div class="col-sm-8">
          									<textarea name="indikasi" class="col-xs-10"></textarea>
          								</div>
          						</div>
                    </div>
                    <div class="col-sm-6">
          						<div class="form-group">
          							<label class="col-sm-3 control-label" for="form-field-1">Kandungan </label>
          								<div class="col-sm-9">
          									<textarea name="kandungan"class="col-xs-10"></textarea>
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-3 control-label" for="form-field-1">Dosis </label>
          								<div class="col-sm-9">
          									<textarea name="dosis" class="col-xs-10"></textarea>
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-3 control-label" for="form-field-1"> Foto </label>
                        <div class="col-md-9 single-right-left animated wow slideInUp" data-wow-delay=".5s">
                          <div class="form-group">
                            <div class="col-xs-10">
                              <input type="file" id="id-input-file-2" name="image" />
                            <div id=coba></div>
                            </div>
                          </div>
                          <div class="flexslider">
                           <div class="thumb-image"> <img id="img" src="{{asset('images/resolution.jpg')}}" alt="Gambar Tidak Tampil" data-imagezoom="true" class="img-responsive produk-image" style="height:220px;"> </div>
                          </div>
                        </div>
          						</div>
                    </div>
                </div>
                </div>

                <div id="detail_satuan" class="tab-pane fade">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Satuan Terkecil </label>
          								<div class="col-sm-8">
          									<select name="kd_sat_obat" id="kd_sat_obat" class="col-xs-10">
                              <option value="">-Pilih Satuan Obat-</option>
                              @foreach($sat_obat as $key)
                                <option value="{{$key->kd_sat_obat}}">{{$key->nama_sat_obat}}</option>
                              @endforeach
                            </select>
          								</div>
          						</div>
          						<div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Stok </label>
          								<div class="col-sm-7">
          									<input type="text" id="stok_obat" name="stok_obat" class="col-xs-8" required>
                            <span class="input-group-addon">
                              <label id="labelstock"></label>
                            </span>
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Lokasi </label>
          								<div class="col-sm-8">
          									<select name="kd_lok_obat" class="col-xs-10" required>
                              <option value="">-Pilih Lokasi-</option>
                              @foreach($lok_obat as $key)
                                <option value="{{$key->kd_lok_obat}}">{{$key->nama_lok_obat}}</option>
                              @endforeach
                            </select>
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> HNA </label>
          								<div class="col-sm-8">
                            <input type="text" id="hna"  name="hna" class="col-xs-10" required>
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Harga Jual 1 </label>
          								<div class="col-xs-3">
                            <input type="text" id="hrg_jual_1_persen" name="hrg_jual_1_persen" value="0" class="col-xs-7" required>
                            <span class="input-group-addon">
                              <label id="labelstnjml">%</label>
                            </span>
          								</div>
                          <div class="col-xs-4">
                            <input type="text" id="hrg_jual_1" name="hrg_jual_1" value="0" class="col-xs-10" required>
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Harga Jual 2 </label>
          								<div class="col-xs-3">
                            <input type="text" id="hrg_jual_2_persen" name="hrg_jual_2_persen" value="0" class="col-xs-7" required>
                            <span class="input-group-addon">
                              <label id="labelstnjml">%</label>
                            </span>
          								</div>
                          <div class="col-xs-4">
                            <input type="text" id="hrg_jual_2" name="hrg_jual_2" class="col-xs-10" value="0" required>
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Harga Jual 3 </label>
          								<div class="col-xs-3">
                            <input type="text" id="hrg_jual_3_persen" name="hrg_jual_3_persen" value="0" class="col-xs-7" required>
                            <span class="input-group-addon">
                              <label id="labelstnjml">%</label>
                            </span>
          								</div>
                          <div class="col-xs-4">
                            <input type="text" id="hrg_jual_3" name="hrg_jual_3" class="col-xs-10" value="0" required>
          								</div>
          						</div>
                    </div>
                    <div class="col-sm-6">
          						<div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1">Barcode Obat </label>
          								<div class="col-sm-8">
          									<input type="text" name="barcode_obat" class="col-xs-10">
          								</div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1">No. Batch </label>
                          <div class="col-sm-8">
                            <input type="text" name="no_batch" class="col-xs-10">
                          </div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1"> Tanggal Expired </label>
                        <div class="col-sm-8">
                          <input type="text" name="tgl_expired" class="datepicker col-xs-10">
                        </div>
          						</div>
                      <br><br>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1">Diskon Harga 1 </label>
                          <div class="col-sm-8">
                            <input type="text" name="dis_hrg_1" class="col-xs-7" value="0">
                            <span class="input-group-addon">
                              <label id="labelstnjml">%</label>
                            </span>
                          </div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1">Diskon Harga 2 </label>
                          <div class="col-sm-8">
                            <input type="text" name="dis_hrg_2" class="col-xs-7" value="0">
                            <span class="input-group-addon">
                              <label id="labelstnjml">%</label>
                            </span>
                          </div>
          						</div>
                      <div class="form-group">
          							<label class="col-sm-4 control-label" for="form-field-1">Diskon Harga 3 </label>
                          <div class="col-sm-8">
                            <input type="text" name="dis_hrg_3" class="col-xs-7" value="0">
                            <span class="input-group-addon">
                              <label id="labelstnjml">%</label>
                            </span>
                          </div>
          						</div>
                    </div>
                </div>
                </div>
              </div>
            </div>
						<div class="clearfix form-actions">
							<div class="col-md-6">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
									</button>
								</div>
                <div class="col-md-offset-4 col-md-2">
                  <a href="{{url('obat')}}">
									  <button class="btn btn-danger" type="button">
  										<i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
  											Kembali
									  </button>
                  </a>
							  </div>
			         </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script src="{{asset('js/ace-elements.min.js')}}"></script>

<script type="text/javascript">
  jQuery(function($) {

    $('#id-input-file-1 , #id-input-file-2').ace_file_input({
      no_file:'No File ...',
      btn_choose:'Choose',
      btn_change:'Change',
      droppable:false,
      onchange:null,
      thumbnail:false //| true | large
      //whitelist:'gif|png|jpg|jpeg'
      //blacklist:'exe|php'
      //onchange:''
      //
    });
    //pre-show a file name, for example a previously selected file
    //$('#id-input-file-1').ace_file_input('show_file_list', ['myfile.txt'])
    $('#id-input-file-2').change(function(){
      var fileInput = document.getElementById('id-input-file-2');
      var filePath = fileInput.value;
      var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
      if(!allowedExtensions.exec(filePath)){
          alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
          fileInput.value = '';
          return false;
      }else{
          //Image preview
          if (fileInput.files && fileInput.files[0]) {
              var reader = new FileReader();
              reader.onload = function(e) {
                  document.getElementById('img').src = e.target.result;
              };
              reader.readAsDataURL(fileInput.files[0]);
          }
      }
    });

    $("#kd_sat_obat").on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        $("#labelstock").text(optionSelected.text());
    });

    $("#hrg_jual_1_persen").change(function(){
      var hna = document.getElementById("hna").value;
      var persen = document.getElementById("hrg_jual_1_persen").value;
      var hrg_jual = ((parseInt(persen)/100)*parseInt(hna)) + parseInt(hna);
      document.getElementById("hrg_jual_1").value = hrg_jual;
    });
    $("#hrg_jual_2_persen").change(function(){
      var hna = document.getElementById("hna").value;
      var persen = document.getElementById("hrg_jual_2_persen").value;
      var hrg_jual = ((persen/100)*hna) + parseInt(hna);
      document.getElementById("hrg_jual_2").value = hrg_jual;
    });
    $("#hrg_jual_3_persen").change(function(){
      var hna = document.getElementById("hna").value;
      var persen = document.getElementById("hrg_jual_3_persen").value;
      var hrg_jual = ((persen/100)*hna) + parseInt(hna);
      document.getElementById("hrg_jual_3").value = hrg_jual;
    });
    $('.datepicker').datepicker({
				format: 'yyyy-mm-dd',
		});

  });
</script>
@stop
