@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Master Apotek</a>
      </li>
      <li class="active">Data Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>
  @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
  @endif

  <div class="page-content">
    <div class="page-header">
      <h1>
        Master Apotek
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          Data Obat
        </small>
      </h1>
      <div class="pull-right all-button">
        <a class="btn btn-primary btn-sm" href="{{url('obat/tambah')}}"><i class="fa fa-plus"></i>  Tambah</a>
        <!-- <a class="btn btn-primary btn-sm" href="{{url('obat/print')}}"><i class="glyphicon glyphicon-print"></i> Print</a>
        <a class="btn btn-primary btn-sm" href="{{url('obat/export')}}"><i class="fa fa-upload"></i> Export</a> -->
      </div>
      <br>
    </div><!-- /.page-header -->

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
          <div class="col-xs-12">
            <div class="table-header">
              Data Obat
            </div>

            <!-- div.table-responsive -->

            <!-- div.dataTables_borderWrap -->
            <div>
              <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="center">#</th>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>Pabrik</th>
                    <th class="hidden-480">Golongan</th>
                    <th class="hidden-480">Kategori</th>
                    <th>Kandungan</th>
                    <th>Jenis</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $i=1; ?>
                  @foreach($obat as $key)
                  <tr>
                    <td class="center">{{$i}}</td>
                    <td>{{$key->kd_obat}}</td>
                    <td>{{$key->nama_obat}}</td>
                    <td>{{$key->nama_pabrik}}</td>
                    <td>{{$key->nama_gol_obat}}</td>
                    <td>{{$key->nama_kat_obat}}</td>
                    <td>{{$key->kandungan}}</td>
                    <td>{{$key->jenis_obat}}</td>
                    <td>
                      <div class="hidden-sm hidden-xs action-buttons">
                        <a class="blue" href="{{url('obat/detail/'.$key->kd_obat)}}">
                          <i class="ace-icon fa fa-search-plus bigger-130"></i>
                        </a>

                        <a class="green" href="{{url('obat/ubah/'.$key->kd_obat)}}">
                          <i class="ace-icon fa fa-pencil bigger-130"></i>
                        </a>

                        <a class="red" href="{{url('obat/hapus/'.$key->kd_obat)}}">
                          <i class="ace-icon fa fa-trash-o bigger-130"></i>
                        </a>
                      </div>
                    </td>
                  </tr>
                  <?php $i++; ?>
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
});
$( "input[type=checkbox]" ).change(function(){
  var id= this.value;

  if(this.checked){
    var url = "obat/addobat/"+id;

    $.get(url);
  }else{
    var url = "obat/destroyobat/"+id;
    $.get(url);
  }
});

$.ajax({
    type : 'get',
    url : 'obat/cekobat',
    success: function (data) {
      $.each(data, function(key, value){
        $('input[type=checkbox]').each(function () {
          if (this.value == value) {
            $(this).prop( "checked", true );
          }
        });
      });
    }
  });
});
</script>
@stop
