@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Pencarian Data Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Transaksi Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Pencarian Data Obat
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Pencarian Data obat</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
          <div class="table-header">
            Data Obat
          </div>
          <div>
            <div class="table-responsive table_po">

            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th class="center">#</th>
                  <th>Kode</th>
                  <th>Nama Obat</th>
                  <th>Sisa Stok</th>
                  <th>Pabrik</th>
                  <th>Satuan</th>
                  <th>Golongan</th>
                  <th>Kategori</th>
                  <th>Lokasi</th>
                  <th>Kandungan</th>
                </tr>
              </thead>

              <tbody>
                <?php $i=1; ?>
                @foreach($obat as $key)
                <tr>
                  <td align="center"><input type="checkbox" class="pilih" name="pilih" id="coba" value="{{$key->kd_obat}}"/></td>
                  <td>{{$key->kd_obat}}</td>
                  <td>{{$key->nama_obat}}</td>
                  <td>{{$key->stok}}</td>
                  <td>{{$key->nama_pabrik}}</td>
                  <td>{{$key->nama_sat_obat}}</td>
                  <td>{{$key->nama_gol_obat}}</td>
                  <td>{{$key->nama_kat_obat}}</td>
                  <td>{{$key->nama_lok_obat}}</td>
                  <td>{{$key->kandungan}}</td>
                </tr>
                <?php $i++; ?>
                @endforeach

              </tbody>
            </table>
          </div>
          </div>
          <div class="clearfix form-actions">
              <div class="pull-right">
                <a href="{{url('mutasi-obat/tambah')}}">
                <button class="btn btn-danger" type="button">
                  <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
                    Kembali
                </button>
              </a>
              </div>
             </div>
             <div id="coba2"></div>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
});

$( "input[type=checkbox]" ).change(function(){
  var id= this.value;

  if(this.checked){
    var url = "obat/addobat/"+id;

    $.get(url);
  }else{
    var url = "obat/destroyobat/"+id;
    $.get(url);
  }
});

$.ajax({
    type : 'get',
    url : 'obat/cekobat',
success: function (data) {
  $.each(data, function(key, value){
    $('input[type=checkbox]').each(function () {
               if (this.value == value) {
                 $(this).prop( "checked", true );
               }
    });
  });
}
});
});
</script>
@stop
