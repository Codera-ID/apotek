@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Tambah Mutasi Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Transaksi Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Tambah Mutasi Obat
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
            <div class="page-header-detail">
    				<h1>Tambah Mutasi Obat</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('mutasi-obat/tambah-mutasi')}}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
						      <label class="col-sm-4 control-label" for="form-field-1"> Tanggal </label>
                  <div class="col-sm-8">
                    <input type="text" name="date" class="datepicker col-xs-12">
                  </div>
                </div>
                <div class="form-group">
						      <label class="col-sm-4 control-label" for="form-field-1"> No. Mutasi </label>
							    <div class="col-sm-8">
								    <input type="text" id="no_bukti" name="no_bukti" class="col-xs-12" required>
                    <input type="text" id="nama_petugas" name="petugas" value="{{Auth::user()->name}}" hidden class="col-xs-12">
							    </div>
					      </div>
                <div class="form-group">
						      <label class="col-sm-4 control-label" for="form-field-1"> Jenis Mutasi </label>
                  <div class="col-sm-8">
                    <select name="kd_mutasi" class="col-xs-12" required>
                      <option value="0">Mutasi Keluar</option>
                      <option value="1">Mutasi Masuk</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-4 control-label" for="form-field-1"> Keterangan </label>
                  <div class="col-sm-8">
                    <textarea name="keterangan" class="col-xs-12"></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix sub">
              <div class="col-md-12">
                <a href="{{url('mutasi-obat/obat')}}">
                  <button class="btn btn-info" type="button">
                    <i class="ace-icon fa fa-plus"></i>
                    Obat
                  </button>
                </a>
              </div>
            </div>
            <hr>
            <div class="table-responsive table_po">
              <table class="table">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Action</th>
                    <th>Kode Obat</th>
                    <th>Nama Obat</th>
                    <th>Jumlah</th>
                    <th>Satuan</th>
                </tr>
              </thead>
              <tbody>
              @if(count($obat) == 0)
                <tr>
                  <th colspan="6">Tekan tombol [+ Obat] untuk menambah data obat yang akan dipesan.</th>
                </tr>
              @else
              @php
                $i=1;
              @endphp
              @foreach($obat as $key)
              <tr>
                <th>{{$i}}</th>
                <th>
                  <a class="red" href="{{url('mutasi-obat/hapus-obat/'.$key->kd_obat)}}">
                    <button type="button" class="btn btn-danger" style="border-radius:10px;">
                           <i class="ace-icon fa fa-trash-o bigger-130"></i></button>
                         </a>
                       </th>
                       <th>{{$key->kd_obat}}</th>
                       <input type="hidden" name="{{'kd_obat'.$i}}" value="{{$key->kd_obat}}">
                       <th>{{$key->nama_obat}}</th>
                       <input type="hidden" name="{{'nama_obat'.$i}}" value="{{$key->nama_obat}}">
                       <td>
                         <input type="number" id="{{'jmlh'.$i}}" name="{{'jmlh'.$i}}" value="1" style="width:75px">
                       </td>
                       <td>
                         <?php $satuan = DB::table('satuan')->where('kd_obat','=',$key->kd_obat)
                                     ->join('satuan_obat', 'satuan.kd_sat_obat', '=', 'satuan_obat.kd_sat_obat')
                                     ->join('lokasi_obat', 'satuan.kd_lok_obat', '=', 'lokasi_obat.kd_lok_obat')
                                     ->get(); ?>
                        <select id="{{'satuan'.$i}}" class="satuan form-control select2-hidden-accessible" name="{{'kd_satuan'.$i}}">
                         @foreach($satuan as $item)
                         <option value="{{$item->kd_satuan}}">{{$item->nama_sat_obat}}</option>
                         @endforeach
                        </select></td>
                     </tr>
                     <input type="hidden" name="jmlh_detail" value="{{$i}}">
                     <?php $i++ ?>
                     @endforeach
                     @endif
                   </tbody>
                 </table>
               </div>
						<div class="clearfix form-actions">
							<div class="col-md-6">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
									</button>
								</div>
                <div class="col-md-offset-4 col-md-2">
                  <a href="{{url('mutasi-obat')}}">
                    <button class="btn btn-danger" type="button">
                      <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
                      Kembali
                    </button>
                  </a>
							  </div>
			      </div>
			    </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script type="text/javascript">
  $(document).ready( function () {

		$('.datepicker').datepicker({
				format: 'yyyy-mm-dd',
		});
  });
</script>
@stop