@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Retur Pembelian Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Transaksi Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Retur Pembelian Obat
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Retur Pembelian Obat</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal po" method="post" role="form" action="{{url('retur-pembelian/tambah/retur')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> No. Faktur </label>
    								<div class="col-sm-9">
                            <input id="pembelianobat-pono" class="col-xs-8" name="no_faktur" value="{{$id}}" type="text" readonly>
                            <span class="input-group-btn">
                              <a href="{{url('retur-pembelian/pembelian-obat')}}">
                                <button type="button" id="po" class="btn btn-info" style="font-size:12px; padding:3.60px; padding-left:7.5px; padding-right:7.5px;" title="Pilih data PO"><i class="glyphicon glyphicon-search"></i> Cari</button>
                              </a>
                            </span>
    								</div>
    						</div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Tanggal Faktur </label>
    								<div class="col-sm-9">
    									<input type="text"  name="tgl_fak" class="datepicker col-xs-10" value="{{$pembelian_obat[0]->tgl_faktur}}" readonly>
    								</div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Tanggal Retur </label>
    								<div class="col-sm-9">
    									<input type="text"  name="tgl_retur" class="datepicker col-xs-10" value="" required>
    								</div>
                </div>

              </div>
              <div class="col-sm-6">
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Supplier </label>
    								<div class="col-sm-9">
                      <input type="text" class="col-xs-10" value="{{$pembelian_obat[0]->nama_supplier}}" required readonly>
                      <input type="hidden"  name="kd_supplier" class="col-xs-10" value="{{$pembelian_obat[0]->kd_supplier}}" required readonly>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Gudang </label>
    								<div class="col-sm-9">
                      <input type="text" class="col-xs-10" value="{{$pembelian_obat[0]->nama_gudang}}" required readonly>
                      <input type="hidden"  name="kd_gudang" class="col-xs-10" value="{{$pembelian_obat[0]->kd_gudang}}" required readonly>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Jenis </label>
                  <div class="col-sm-3" id="jenis-form">
                    <input type="text"  name="jenis_pembayaran" class="col-xs-10" value="{{$pembelian_obat[0]->jenis_pembayaran}}" required readonly>
                  </div>
                  <div class="col-sm-6" id="kas">
                      <select class="col-xs-8" name="kas">
                        <option value="">Pilih Akun</option>
                        <option value="180" selected="">Kas Umum</option>
                        <option value="509">Kas Pusat</option>
                        <option value="199">Kas Kecil</option>
                        <option value="200">Bank BRI</option>
                        <option value="201">Bank Mandiri</option>
                        <option value="202">Bank BNI</option>
                        <option value="204">Bank Danamon</option>
                        <option value="182">Permata Bank</option>
                        <option value="187">Bank BCA</option>
                        <option value="428">Bank BII</option>
                        <option value="349">Bank Panin</option>
                        <option value="524">CIMB NIAGA</option>
                        <option value="528">Bank Lippo</option>
                        <option value="544">Bank NISP</option>
                      </select>
                    </div>
    						</div>
              </div>
          </div>
						<div class="clearfix sub">
							<div class="col-md-5">
                <a href="{{url('pembelian-obat/obat')}}">
								<button class="btn btn-info" type="button">
									<i class="ace-icon fa fa-exchange"></i>
										Obat
									</button>
                </a>
									&nbsp; &nbsp;
									<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
									</button>
                  &nbsp; &nbsp;
                  <button class="btn btn-success" type="submit">
  									<i class="ace-icon fa fa-check bigger-110"></i>
  										Submit
  									</button>
								</div>
                <div class="col-sm-7">
                  <div class="price pull-right">
                    @if(count($data) == 0)
                    <h1>Rp. <span>0,00</span></h1>
                    @else
                    <h1>Rp. <span id="total_bayar">0,00</span></h1>
                    <input type="hidden" class="total_bayar" name="total_bayar">
                    @endif
                  </div>
                </div>

			         </div>
               <hr>
                 <div class="table-responsive table_po">
                   <input type="text"  placeholder="ketik/scan barcode" class="col-xs-4" style="margin-bottom:20px;">
                 <table class="table">
                   <thead>
                     <tr>
                       <th>No</th>
                       <th>Action</th>
                       <th>Kode Obat</th>
                       <th>Nama Obat</th>
                       <th>Satuan</th>
                       <th>Jumlah</th>
                       <th>Harga Beli</th>
                       <th>Sub Total</th>
                       <th>Potongan</th>
                       <th>Total</th>
                     </tr>
                   </thead>
                   <tbody>
                     @if(count($data) == 0)
                     <tr>
                       <th colspan="10">Tekan tombol [+ Obat] untuk menambah data obat yang akan dipesan.</th>
                     </tr>
                     @else
                     <?php $i=1; ?>
                     @foreach($data as $key)
                     <tr>
                       <th>{{$i}}</th>
                       <th>
                         <a class="red" href="{{url('retur-pembelian/hapus_obat_retur/'.$key->no_faktur.'/'.$key->kd_obat)}}">
                           <button type="button" class="btn btn-danger" style="border-radius:10px;">
                           <i class="ace-icon fa fa-trash-o bigger-130"></i></button>
                         </a>
                       </th>
                       <th>{{$key->kd_obat}}</th>
                       <input type="hidden" name="{{'kd_obat'.$i}}" value="{{$key->kd_obat}}">
                       <th>{{$key->nama_obat}}</th>
                       <input type="hidden" name="{{'nama_obat'.$i}}" value="{{$key->nama_obat}}">
                       <td>{{$key->nama_sat_obat}}</td>
                       <input type="hidden" id="{{'kd_satuan'.$i}}" name="{{'kd_satuan'.$i}}" value="{{$key->kd_satuan}}">
                       <td>
                         <input type="number" id="{{'jmlh'.$i}}" name="{{'jmlh'.$i}}" value="1" style="width:75px" min="1" max="{{$key->jmlh}}">
                         <input type="hidden" id="{{'jumlah'.$i}}" value="{{$key->jmlh}}">
                       </td>
                        <th>
                          <input type="number" id="{{'hna'.$i}}" class="hna" name="{{'hna'.$i}}" value="{{$key->harga}}" style="width:75px" readonly>
                        </th>
                        <th>
                          <input type="text" id="{{'subtotal'.$i}}" class="subtotal" value="{{$key->harga}}" name="{{'subtotal'.$i}}" style="width:75px" readonly>
                        </th>
                        <th>
                          <input type="number" id="{{'potongan'.$i}}" class="potongan" value="0" name="{{'potongan'.$i}}" style="width:75px">
                        </th>
                        <th>
                          <input type="text" id="{{'total'.$i}}" class="total" name="{{'total'.$i}}" value="{{$key->harga}}" style="width:75px" readonly>
                        </th>
                     </tr>
                     <input type="hidden" name="jmlh_detail" value="{{$i}}">
                     <?php $i++ ?>
                     @endforeach
                     @endif
                   </tbody>
                 </table>
               </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script>
$(document).ready(function(){
  var x=1;
  var total_bayar = 0;
  $('tbody tr').each(function(){
    var total_bayar_id = "#total"+x;
    total_bayar = total_bayar + parseInt($(total_bayar_id).val());
    x++;
  });
  $('#total_bayar').html(total_bayar);
  $('.total_bayar').val(total_bayar);

  $('input[type=number]').change(function(){
      var id = $(this).attr("id");
      var lastChar = id.substr(id.length - 1);
      var jmlh_id = "#jmlh"+lastChar;
      var jumlah_id = "#jumlah"+lastChar;
      var jumlah = $(jumlah_id).val();
      var hna_id = "#hna"+lastChar;
      var subtotal_id = "#subtotal"+lastChar;
      var total_id = "#total"+lastChar;
      var potongan_id = "#potongan"+lastChar;
      var subtotal = $(subtotal_id).val();
      var jmlh = $(jmlh_id).val();
      if(parseInt(jmlh) > jumlah){
          jmlh = jumlah;
      }if(parseInt(jmlh) < 1){
        jmlh = 1;
      }
      var hna = $(hna_id).val();
      var subtotal = hna*jmlh;
      var potongan = $(potongan_id).val();
      var total = subtotal-potongan;
      $(subtotal_id).val(subtotal);
      $(jmlh_id).val(jmlh);
      $(total_id).val(total);
      var x=1;
      var total_bayar = 0;
      $('tbody tr').each(function(){
        var total_bayar_id = "#total"+x;
        total_bayar = total_bayar + parseInt($(total_bayar_id).val());
        x++;
      });
      $('#total_bayar').html(total_bayar);
      $('.total_bayar').val(total_bayar);
  });

  $('.datepicker').datepicker({
      format: 'yyyy-mm-dd',
  });

});
</script>
@stop
