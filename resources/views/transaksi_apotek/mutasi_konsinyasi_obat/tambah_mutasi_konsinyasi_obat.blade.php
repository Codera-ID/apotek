@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Tambah Mutasi Konsinyasi Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Transaksi Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Tambah Mutasi Konsinyasi Obat
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
            <div class="page-header-detail">
    				  <h1>Tambah Mutasi Konsinyasi Obat</h1>
            </div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('mutasi-konsinyasi-obat/tambah')}}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
          					<label class="col-sm-4 control-label" for="form-field-1"> Tanggal </label>
                    <div class="col-sm-8">
                      <input type="text" name="date" class="datepicker col-xs-8">
                    </div>
                  </div>
                  <div class="form-group">
          					<label class="col-sm-4 control-label" for="form-field-1"> No Bukti </label>
        						<div class="col-sm-8">
        							<input type="text" id="no_bukti" name="no_bukti" class="col-xs-8" required>
        						</div>
          				</div>
                  <div class="form-group">
      							<label class="col-sm-4 control-label" for="form-field-1"> Jenis Mutasi </label>
                    <div class="col-sm-8">
                      <select name="kd_mutasi" class="col-xs-10" required>
                        <option value="0">Mutasi Keluar</option>
                        <option value="1">Mutasi Masuk</option>                                                
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
      							<label class="col-sm-4 control-label" for="form-field-1"> Nama Obat</label>
                      <div class="col-sm-8">
                        <select name="kd_obat" class="col-xs-10" required>                                    
                          <option value="">-Pilih Obat-</option>
                          @foreach($obat as $key)
                              <option value="{{$key->kd_obat}}">{{$key->nama_obat}}</option>
                          @endforeach
                        </select>  
                      </div>                                                                          
                  </div>
                  <div class="form-group">
      							<label class="col-sm-4 control-label" for="form-field-1"> Jumlah </label>
      								<div class="col-sm-8">
      									<input type="text" id="jumlah" name="jumlah" class="col-xs-8" required>
      								</div>
      						</div>
                  <div class="form-group">
      							<label class="col-sm-4 control-label" for="form-field-1"> Keterangan </label>
      								<div class="col-sm-8">
      									<textarea name="keterangan" class="col-xs-10"></textarea>
      								</div>
      						</div>
                </div>
              </div>
						  <div class="clearfix form-actions">
  							<div class="col-md-6">
  								<button class="btn btn-info" type="submit">
  									<i class="ace-icon fa fa-check bigger-110"></i>
  										Submit
  									</button>
  									&nbsp; &nbsp; &nbsp;
  									<button class="btn" type="reset">
  									<i class="ace-icon fa fa-undo bigger-110"></i>
  											Reset
  									</button>
  								</div>
                  <div class="col-md-offset-4 col-md-2">
                    <a href="{{url('mutasi-konsinyasi-obat')}}">
                      <button class="btn btn-danger" type="button">
                        <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
                        Kembali
                      </button>
                    </a>
							    </div>
			          </div>
		          </form>
						<!-- PAGE CONTENT ENDS -->
			  </div><!-- /.col -->
	    </div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script type="text/javascript">
  $(document).ready( function () {

		$('.datepicker').datepicker({
				format: 'yyyy-mm-dd',
		});
  });
</script>
@stop