@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Mutasi Konsinyasi Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>
  @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
  @endif

  <div class="page-content">
    <div class="page-header">
      <h1>
        Transaksi Apotek
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          Mutasi Konsinyasi Obat
        </small>
      </h1>
      <div class="pull-right all-button">
        <a class="btn btn-primary btn-sm" href="{{url('mutasi-konsinyasi-obat/tambah-mutasi')}}"><i class="fa fa-plus"></i>  Tambah</a>
        <!-- <a class="btn btn-primary btn-sm" href="{{url('obat/print')}}"><i class="glyphicon glyphicon-print"></i> Print</a>
        <a class="btn btn-primary btn-sm" href="{{url('obat/export')}}"><i class="fa fa-upload"></i> Export</a> -->
      </div>
      <br>
    </div><!-- /.page-header -->

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
          <div class="col-xs-12">
            <div class="table-header">
              Mutasi Konsinyasi Obat
            </div>

            <!-- div.table-responsive -->

            <!-- div.dataTables_borderWrap -->
            <div>
              <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="center">#</th>
                    <th>Tanggal</th>
                    <th>No Bukti</th>
                    <th>Nama Obat</th>
                    <th>Keterangan</th>
                    <th>Masuk</th>
                    <th>Keluar</th>
                    <th>Stock</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $i=1; ?>
                  @foreach($data as $key)
                  <tr>
                    <td class="center">{{$i}}</td>
                    <td>{{$key->date}}</td>
                    <td>{{$key->no_bukti}}</td>
                    <td>{{$key->nama_obat}}</td>
                    <td>{{$key->keterangan}}</td>
                    <td>{{$key->masuk}}</td>
                    <td>{{$key->keluar}}</td>
                    <td>{{$key->stock}}</td>
                  </tr>
                  <?php $i++; ?>
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
});
} );
</script>
@stop
