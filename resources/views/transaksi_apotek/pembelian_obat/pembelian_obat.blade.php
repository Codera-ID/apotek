@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Pembelian Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Transaksi Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Tambah Data Pembelian Obat
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Pembelian Obat</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal po" method="post" role="form" action="{{url('pembelian-obat/tambah')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> PO No. </label>
    								<div class="col-sm-9">
                            <input id="pembelianobat-pono" class="col-xs-8" name="no_po" value="{{$id}}" type="text" readonly>
                            <span class="input-group-btn">
                              <a href="{{url('pembelian-obat/po')}}">
                                <button type="button" id="po" class="btn btn-info" style="font-size:12px; padding:3.60px; padding-left:7.5px; padding-right:7.5px;" title="Pilih data PO"><i class="glyphicon glyphicon-search"></i> Cari</button>
                              </a>
                            </span>
    								</div>
    						</div>
    						<div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> No.Faktur </label>
    								<div class="col-sm-9">
    									<input type="text" class="col-xs-10" name="no_fak" value="{{$faktur}}" required readonly>
    								</div>
    						</div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Tanggal Faktur </label>
    								<div class="col-sm-9">
    									<input type="text"  name="tgl_fak" class="datepicker col-xs-10" value="{{date('Y-m-d')}}" required readonly>
    								</div>
                </div>

              </div>
              <div class="col-sm-6">
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Supplier </label>
    								<div class="col-sm-9">
    									<select name="kd_supplier" class="col-xs-8" required>
                        <option value="">-Pilih Supplier-</option>
                        @foreach($supplier as $supp)
                        <option value="{{$supp->kd_supplier}}">{{$supp->nama_supplier}}</option>
                        @endforeach
                      </select>
                      <span class="input-group-btn">
                        <a href="supplier/tambah">
                          <button type="button" id="po" class="btn btn-info" style="font-size:14px; padding:0px; padding-left:8.5px; padding-right:8.5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"  title="Pilih data PO"><i class="glyphicon glyphicon-plus"></i></button>
                        </a>
                      </span>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Gudang </label>
    								<div class="col-sm-9">
    									<select name="kd_gudang" class="col-xs-8" required>
                        <option value="">-Pilih Gudang-</option>
                        @foreach($gudang as $gud)
                        <option value="{{$gud->kd_gudang}}">{{$gud->nama_gudang}}</option>
                        @endforeach
                      </select>
                      <span class="input-group-btn">
                        <a href="gudang/tambah">
                          <button type="button" id="po" class="btn btn-info" style="font-size:14px; padding:0px; padding-left:8.5px; padding-right:8.5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"  title="Pilih data PO"><i class="glyphicon glyphicon-plus"></i></button>
                        </a>
                      </span>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Jenis </label>
                  <div class="col-sm-3" id="jenis-form">
                      <select id="jenis_pembelian" name="jenis_pembayaran">
                      <option value="TUNAI" selected="">TUNAI</option>
                      <option value="HUTANG">HUTANG</option>
                      <option value="KONSINYASI">KONSINYASI</option>
                      </select>
                  </div>
                  <div class="col-sm-6" id="kas">
                      <select class="col-xs-8" name="kas">
                        <option value="">Pilih Akun</option>
                        <option value="Kas Umum" selected="">Kas Umum</option>
                        <option value="Kas Pusat">Kas Pusat</option>
                        <option value="Kas Kecil">Kas Kecil</option>
                        <option value="Bank BRI">Bank BRI</option>
                        <option value="Bank Mandiri">Bank Mandiri</option>
                        <option value="Bank BNI">Bank BNI</option>
                        <option value="Bank Danamon">Bank Danamon</option>
                        <option value="Permata Bank">Permata Bank</option>
                        <option value="Bank BCA">Bank BCA</option>
                        <option value="Bank BNI">Bank BII</option>
                        <option value="Bank Panin">Bank Panin</option>
                        <option value="CIMB NIAGA">CIMB NIAGA</option>
                        <option value="Bank Lippo">Bank Lippo</option>
                        <option value="Bank NISP">Bank NISP</option>
                      </select>
                  </div>
                  <div class="col-sm-6 hide" id="tanggal-jatuh-tempo">
                      <input type="text" name="tanggal_jatuh_tempo" class="datepicker col-xs-10">
                    </div>
    						</div>
              </div>
          </div>
						<div class="clearfix sub">
							<div class="col-md-5">
                <a id="url_obat" href="{{url('pembelian-obat/obat')}}">
								<button class="btn btn-info" type="button">
									<i class="ace-icon fa fa-plus"></i>
										Obat
									</button>
                </a>
									&nbsp; &nbsp;
									<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
									</button>
                  &nbsp; &nbsp;
                  <button class="btn btn-success" type="submit">
  									<i class="ace-icon fa fa-check bigger-110"></i>
  										Submit
  									</button>
								</div>
                <div class="col-sm-7">
                  <div class="price pull-right">
                    @if(count($data) == 0)
                    <h1>Rp. <span>0,00</span></h1>
                    @else
                    <h1>Rp. <span id="total_bayar">0,00</span></h1>
                    <input type="hidden" class="total_bayar" name="total_bayar">
                    @endif
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                      <label class="col-sm-3 control-label" for="pembelianobat-pemototal">Total</label>
                      <div class="col-sm-9">
                        <div class="form-group field-pembelianobat-pemototal">
                          <div class="col-sm-12">
                            <input type="text" id="pembelianobat-total" name="pembelian_total" value="0" class="form-control" readonly>
                            <div class="help-block">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label" for="pembelianobat-pemodiskon">Diskon</label>
                      <div class="col-sm-3">
                        <div class="form-group field-pembelianobat-pemodiskon">
                      <div class="col-sm-12">
                      <input type="number" id="pembelianobat-dis" class="kosong form-control" name="pembelianobat_dis">
                      <div class="help-block"></div>
                      </div>
                      </div>
                    </div>
                      <div class="col-sm-6">
                        <div class="form-group field-pembelianobat-pemodiskontext">
                      <div class="col-sm-12">
                      <input type="number" id="hasil-dis" class="kosong form-control" name="hasil_dis">
                      <div class="help-block"></div>
                      </div>
                      </div>
                      </div>
                      </div>
                  </div>
                <div class="col-md-5">
                  <div class="form-group">
                      <label class="col-sm-3 control-label" for="pembelianobat-pemopajak">Pajak</label>
                      <div class="col-sm-3">
                          <div class="form-group field-pembelianobat-pemopajak">
                          <div class="col-sm-12">
                          <input type="number" id="pembelianobat-pajak" class="kosong form-control" name="pembelianobat_pajak">
                          <div class="help-block"></div>
                          </div>
                          </div>
                        </div>
                      <div class="col-sm-6">
                        <div class="form-group field-pembelianobat-pemopajaktext">
                          <div class="col-sm-12">
                          <input type="number" id="hasil-pajak" class="kosong form-control" name="hasil_pajak">
                          <div class="help-block"></div>
                          </div>
                          </div>
                        </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="pembelianobat-pemobiaya">Biaya</label>
                            <div class="col-sm-9">
                            <div class="form-group field-pembelianobat-pemobiaya">
                            <div class="col-sm-12">
                            <input type="number" id="biaya" value="0" class="form-control" name="biaya">
                            <div class="help-block"></div>
                            </div>
                            </div>
                            </div>
                        </div>
            </div>
			         </div>
               <hr>
                <div class="table-responsive table_po">
                  <input type="text"  placeholder="ketik/scan barcode" class="col-xs-4" id=barcode style="margin-bottom:20px;">
                   <table class="table">
                     <thead>
                       <tr>
                         <th>#</th>
                         <th>Action</th>
                         <th>Kode Obat</th>
                         <th>Nama Obat</th>
                         <th>Jumlah</th>
                         <th>Satuan</th>
                         <th>Harga</th>
                         <th>Sub Total</th>
                         <th>Diskon (%)</th>
                         <th>Nominal Diskon</th>
                         <th>Margin Harga Jual 1 (%)</th>
                         <th>Harga Jual 1</th>
                         <th>Diskon Harga Jual 1 (%)</th>
                         <th>Tanggal Exp</th>
                         <th>No Batch</th>
                         <th>Total</th>
                       </tr>
                     </thead>
                     <tbody>
                       @if(count($data) == 0)
                       <tr>
                         <th colspan="10">Tekan tombol [+ Obat] untuk menambah data obat yang akan dipesan.</th>
                       </tr>
                       @else
                       <?php $i=1; ?>
                       @foreach($data as $key)
                       <tr>
                         <th>
                          <a href="#" data-toggle="collapse" data-target="{{'#detail'.$i}}">
                           <span class="glyphicon glyphicon-collapse-down"></span>
                           </a>
                         </th>
                         <th>
                           <a class="red" href="{{url('pembelian-obat/obat/hapuscheckbox/'.$key->kd_obat)}}">
                             <button type="button" class="btn btn-danger" style="border-radius:10px;">
                             <i class="ace-icon fa fa-trash-o bigger-130"></i></button>
                           </a>
                         </th>
                         <th>{{$key->kd_obat}}</th>
                         <input type="hidden" name="{{'kd_obat'.$i}}" value="{{$key->kd_obat}}">
                         <th><div style="width:150px">{{$key->nama_obat}}</div></th>
                         <input type="hidden" name="{{'nama_obat'.$i}}" value="{{$key->nama_obat}}">
                         <td>
                           <input type="number" id="{{'jmlh'.$i}}" name="{{'jmlh'.$i}}" value="1" style="width:75px">
                         </td>
                         <td>
                           <?php $satuan = DB::table('satuan')->where('kd_obat','=',$key->kd_obat)
                                       ->join('satuan_obat', 'satuan.kd_sat_obat', '=', 'satuan_obat.kd_sat_obat')
                                       ->join('lokasi_obat', 'satuan.kd_lok_obat', '=', 'lokasi_obat.kd_lok_obat')
                                       ->get(); ?>
                          <select id="{{'satuan'.$i}}" class="satuan form-control select2-hidden-accessible" name="{{'kd_satuan'.$i}}" style="width:75px">
                           @foreach($satuan as $item)
                           <option value="{{$item->kd_satuan}}">{{$item->nama_sat_obat}}</option>
                           @endforeach
                          </select></td>
                          <th>
                            <input type="number" id="{{'hna'.$i}}" class="hna" name="{{'hna'.$i}}" value="{{$satuan[0]->hna}}" style="width:75px">
                          </th>
                          <th>
                            <input type="text" id="{{'subtotal'.$i}}" class="subtotal" value="{{$satuan[0]->hna}}" name="{{'subtotal'.$i}}" style="width:75px" readonly>
                          </th>
                          <th>
                            <input type="number" id="{{'diskon'.$i}}" class="diskon" value="0" name="{{'diskon'.$i}}" style="width:75px">
                          </th>
                          <th>
                            <input type="number" id="{{'nom_dis'.$i}}" class="nom_dis" value="0" name="{{'nom_dis'.$i}}" style="width:110px">
                          </th>
                          <th>
                            <input type="number" id="{{'margin_hrg_jual'.$i}}" class="margin_hrg_jual" value="0" name="{{'margin_hrg_jual'.$i}}" style="width:150px">
                          </th>
                          <th>
                            <input type="number" id="{{'harga_jual_1'.$i}}" class="harga_jual" value="0" name="{{'harga_jual_1'.$i}}" style="width:110px">
                          </th>
                          <th>
                            <input type="text" id="{{'diskon_harga_jual'.$i}}" class="diskon_harga_jual" name="{{'diskon_harga_jual'.$i}}" value="0" style="width:150px">
                          </th>
                          <th>
                            <input type="text" id="{{'tgl_exp'.$i}}" class="datepicker tgl_exp" name="{{'tgl_exp'.$i}}" value="{{$satuan[0]->tgl_expired}}">
                          </th>
                          <th>
                            <input type="text" id="{{'no_batch'.$i}}" class="no_batch" name="{{'no_batch'.$i}}" value="{{$satuan[0]->hna}}" style="width:75px">
                          </th>
                          <th>
                            <input type="text" id="{{'total'.$i}}" class="total" name="{{'total'.$i}}" value="{{$satuan[0]->hna}}" style="width:75px" readonly>
                          </th>
                       </tr>
                       <input type="hidden" name="jmlh_detail" value="{{$i}}">
                       <?php $i++ ?>
                       @endforeach
                       @endif
                     </tbody>
                   </table>
                </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script>
$(document).ready(function(){
  var x=1;
  var total_bayar = 0;
  $('tbody tr').each(function(){
    var total_bayar_id = "#total"+x;
    total_bayar = total_bayar + parseInt($(total_bayar_id).val());
    x++;
  });
  $('#total_bayar').html(total_bayar);
  $('.total_bayar').val(total_bayar);
  $('#pembelianobat-total').val(total_bayar);
  $('#pembelianobat-dis').val(0);
  $('#pembelianobat-pajak').val(0);

  $('input[type=number]').change(function(){
      var id = $(this).attr("id");
      var lastChar = id.substr(id.length - 1);
      var jmlh_id = "#jmlh"+lastChar;
      var hna_id = "#hna"+lastChar;
      var subtotal_id = "#subtotal"+lastChar;
      var total_id = "#total"+lastChar;
      var diskon_id = "#diskon"+lastChar;
      var nom_dis_id = "#nom_dis"+lastChar;
      var margin_id = "#margin_hrg_jual"+lastChar;
      var harga_jual_1_id = "#harga_jual_1"+lastChar;
      var diskon = $(diskon_id).val() / 100;
      var subtotal = $(subtotal_id).val();
      var jmlh = $(jmlh_id).val();
      var hna = $(hna_id).val();
      var margin = $(margin_id).val() / 100;
      var harga_jual = margin*hna;
      var harga_jual_1 = parseInt(hna)+parseInt(harga_jual);
      var subtotal = hna*jmlh;
      var nominal_diskon = diskon*subtotal;
      var total = subtotal-nominal_diskon;
      $(harga_jual_1_id).val(harga_jual_1);
      $(subtotal_id).val(subtotal);
      $(total_id).val(total);
      $(nom_dis_id).val(nominal_diskon);
      var x=1;
      var total_bayar = 0;
      $('tbody tr').each(function(){
        var total_bayar_id = "#total"+x;
        total_bayar = total_bayar + parseInt($(total_bayar_id).val());
        x++;
      });

      $('#pembelianobat-total').val(total_bayar);
      var total_bayar_dis = $('#pembelianobat-total').val();
      var dis = $('#pembelianobat-dis').val();
      var hasil_dis = (dis/100)*total_bayar_dis;
      var total_bayar_hasil = total_bayar_dis - hasil_dis;
      $('#hasil-dis').val(hasil_dis);
      var total_bayar_pajak = $('#pembelianobat-total').val();
      var pajak = $('#pembelianobat-pajak').val();
      var hasil_pajak = (pajak/100)*total_bayar_pajak;
      var total_bayar_hasil_pajak = total_bayar_hasil + hasil_pajak;
      $('#hasil-pajak').val(hasil_pajak);
      var biaya = $('#biaya').val();
      var total_hasil_semua = total_bayar_hasil_pajak + parseInt(biaya);
      $('#total_bayar').html(total_hasil_semua);
      $('.total_bayar').val(total_hasil_semua);
  });

  $('.satuan').change(function(){
    var satuan_id = $(this).attr("id");
    var lastChar = satuan_id.substr(satuan_id.length - 1);
    var hna_id = "#hna"+lastChar;
    var subtotal_id = "#subtotal"+lastChar;
    var total_id = "#total"+lastChar;
    var jmlh_id = "#jmlh"+lastChar;
    var diskon_id = "#diskon"+lastChar;
    var nom_dis_id = "#nom_dis"+lastChar;
    var satuan = $(this).val();
    $.ajax({
        type : 'get',
        url : "{{url('pembelian-obat/obat/cekhargasatuan/')}}"+'/'+satuan,
    success: function (data) {
      $(hna_id).val(data);
      $(subtotal_id).val(0);
      var total_bayar = $('.total_bayar').val();
      var pembelianobat_total = $('#pembelianobat-total').val();
      var total = $(total_id).val();
      var total_semua = pembelianobat_total - total;
      var dis = $('#pembelianobat-dis').val();
      var hasil_dis = (dis/100)*total_semua;
      var total_bayar_hasil = total_semua - hasil_dis;
      $('#hasil-dis').val(hasil_dis);
      var pajak = $('#pembelianobat-pajak').val();
      var hasil_pajak = (pajak/100)*total_semua;
      var total_bayar_hasil_pajak = total_bayar_hasil + hasil_pajak;
      $('#hasil-pajak').val(hasil_pajak);
      var biaya = $('#biaya').val();
      var total_hasil_semua = total_bayar_hasil_pajak + parseInt(biaya);
      $('#total_bayar').html(total_hasil_semua);
      $('.total_bayar').val(0);
      $('#pembelianobat-total').val(total_semua);
      $(jmlh_id).val(0);
      $(total_id).val(0);
      $(diskon_id).val(0);
      $(nom_dis_id).val(0);

    }
    });

  });

  $('#jenis_pembelian').change(function() {
    if(this.value == 'TUNAI'){
      $('#kas').removeClass('hide');
      $('#tanggal-jatuh-tempo').addClass('hide');
      $('#url_obat').attr("href", "{{url('pembelian-obat/obat')}}")
    }else if(this.value == 'HUTANG'){
      $('#kas').addClass('hide');
      $('#tanggal-jatuh-tempo').removeClass('hide');
      $('#url_obat').attr("href", "{{url('pembelian-obat/obat')}}")
    }else{
      $('#kas').addClass('hide');
      $('#tanggal-jatuh-tempo').addClass('hide');
      $('#url_obat').attr("href", "{{url('pembelian-obat/obat/konsinyasi')}}");
    }
  });

  $('.datepicker').datepicker({
				format: 'yyyy-mm-dd',
		});
});
</script>
@stop
