@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Pencarian Data Purchase Order (PO)</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Transaksi Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Pencarian Data Purchase Order (PO)
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Pencarian Data Purchase Order (PO)</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
          <div class="table-header">
            Data Purchase Order (PO)
          </div>
          <div>
            <div class="table-responsive table_po">

            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>Action</th>
                  <th style="min-width:100px;">
                    <a href="/pembelian-obat/po?sort=potanggal" data-sort="potanggal">Tanggal</a>
                  </th>
                  <th>
                    <a href="/pembelian-obat/po?sort=pono" data-sort="pono">No. PO</a>
                  </th>
                  <th style="min-width:280px;">
                    <a href="/pembelian-obat/po?sort=supnama" data-sort="supnama">Supplier</a>
                  </th>
                  <th style="min-width:170px;">
                    <a href="/pembelian-obat/po?sort=pojenis" data-sort="pojenis">Jenis</a>
                  </th>
                  <th>
                    <a href="/pembelian-obat/po?sort=pototal" data-sort="pototal">Total</a>
                  </th>
                  <th>
                    <a href="/pembelian-obat/po?sort=poket" data-sort="poket">Keterangan</a>
                  </th>
                </tr>
              </thead>

              <tbody>
                <?php $i=1; ?>
                @foreach($po as $key)
                <tr>
                  <td align="center">
                    <a href="{{url('pembelian-obat/po/'.$key->no_po)}}">
                    <button type="button" class="btn btn-success" style="padding:0px" name="pilih" id="coba"/><i class="ace-icon fa fa-check bigger-110"></i></button>
                  </a>
                  </td>
                  <td>{{$key->created_at}}</td>
                  <td>{{$key->no_po}}</td>
                  <td>{{$key->nama_supplier}}</td>
                  <td>{{$key->jenis_obat}}</td>
                  <td>{{$key->total}}</td>
                  <td>{{$key->keterangan}}</td>
                </tr>
                <?php $i++; ?>
                @endforeach

              </tbody>
            </table>
          </div>
          </div>
          <div class="clearfix form-actions">
              <div class="pull-right">
                <a href="{{url('po')}}">
                <button class="btn btn-danger" type="button">
                  <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
                    Kembali
                </button>
              </a>
              </div>
             </div>
             <div id="coba2"></div>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
});

$( "input[type=checkbox]" ).change(function(){
  var id= this.value;

  if(this.checked){
    var url = "obat/addobat/"+id;

    $.get(url);
  }else{
    var url = "obat/destroyobat/"+id;
    $.get(url);
  }
});

$.ajax({
    type : 'get',
    url : 'obat/cekobat',
success: function (data) {
  $.each(data, function(key, value){
    $('input[type=checkbox]').each(function () {
               if (this.value == value) {
                 $(this).prop( "checked", true );
               }
    });
  });
}
});
});
</script>
@stop
