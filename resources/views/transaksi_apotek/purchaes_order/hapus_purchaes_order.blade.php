@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Data Purchase Order (PO)</li>
    </ul><!-- /.breadcrumb -->
  </div>
  @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
  @endif

  <div class="page-content">
    <div class="page-header">
      <h1>
        Transaksi Apotek
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          Hapus Purchase Order (PO)
        </small>
      </h1>
    </div><!-- /.page-header -->

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
          <div class="col-xs-12">
            <div class="table-header">
              Data Purchase Order (PO)
            </div>

            <!-- div.table-responsive -->

            <!-- div.dataTables_borderWrap -->
            <div>
              <table id="dynamic-table" class="table table-striped table-bordered table-hover data">
                <thead>
                  <tr>
                    <th class="center">#</th>
                    <th>Tanggal</th>
                    <th>No. PO</th>
                    <th>Supllier</th>
                    <th>Jenis</th>
                    <th>Total</th>
                    <th>Keterangan</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $i=0 ?>
                  @foreach($po as $key)
                  <tr>
                    <td class="center">
                      <a href="#" data-toggle="collapse" data-target="{{'#detail'.$i}}">
                        <span class="glyphicon glyphicon-collapse-down"></span>
                      </a>
                    </td>
                    <td>{{$key->tgl_po}}</td>
                    <td>{{$key->no_po}}</td>
                    <td>{{$key->nama_supplier}}</td>
                    <td>{{$key->jenis_obat}}</td>
                    <td>{{$key->total}}</td>
                    <td>{{$key->keterangan}}</td>
                    <td>
                      <a class="red" href="{{url('po/data-hapus/hapus/'.$key->no_po)}}">
                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                      </a>
                    </td>
                  </tr>
                  <tr id="{{'detail'.$i}}" class="collapse">
                      <td colspan="8">
                        <div class="skip-export kv-expanded-row w4" data-index="0" data-key="0" style="display: block;">
                          <div id="w8" class="grid-view">
                            <table class="table table-striped table-bordered">
                              <thead>
                              <tr>
                                <th style="width:15px;">No.</th><th style="width:140px;">Kode Obat</th>
                                <th style="width:180px;">Nama Obat</th><th style="width:80px; text-align:right;">Jumlah</th>
                                <th style="width:140px;">Satuan</th><th style="width:120px; text-align:right;">Harga</th>
                                <th style="width:80px; text-align:right;">Diskon</th>
                                <th style="width:120px; text-align:right;">Subtotal</th>
                              </tr>
                              </thead>
                              <tbody>
                              <?php $a=1;

                              $po_detail = DB::table('po_detail')->where('no_po',$key->no_po)
                                        ->join('obat', 'po_detail.kd_obat', '=', 'obat.kd_obat')
                                        ->join('satuan', 'po_detail.kd_satuan', '=', 'satuan.kd_satuan')->get();
                               ?>
                              @foreach($po_detail as $item)
                              <tr data-key="0">
                                <td>{{$a}}</td>
                                <td>{{$item->kd_obat}}</td>
                                <td>{{$item->nama_obat}}</td>
                                <td style="text-align:right">{{$item->jmlh}}</td>
                                <td>{{$item->kd_sat_obat}}</td>
                                <td style="text-align:right">{{$item->harga}}</td>
                                <td style="text-align:right">{{$item->diskon}} %</td>
                                <td style="text-align:right">{{$item->subtotal}}</td>
                                <?php $a++; ?>
                              </tr>
                              @endforeach
                            </tbody>
                            </table>
                          </div>
                        </div>
                      </td>
                  </tr>
                  <?php $i++ ?>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
});
} );
</script>
@stop
