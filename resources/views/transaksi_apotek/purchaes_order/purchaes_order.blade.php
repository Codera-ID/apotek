@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Purchase Order</li>
    </ul><!-- /.breadcrumb -->
  </div>
  @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
  @endif

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Transaksi Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Tambah Data PO
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Purchase Order (PO)</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal po" method="post" role="form" action="{{url('po/tambah')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Tanggal </label>
    								<div class="col-sm-9">
    									<input type="text"  name="tgl_po" class="datepicker col-xs-10" value="" required>
    								</div>
    						</div>
    						<div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> No.PO </label>
    								<div class="col-sm-9">
    									<input type="text" name="no_po" value="{{$no_po}}" placeholder="No.PO akan terisi secara otomatis" class="col-xs-10" readonly>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Supplier </label>
    								<div class="col-sm-9">
    									<select name="kd_supplier" class="col-xs-10" required>
                        <option value="">-Pilih Supplier-</option>
                        @foreach($supplier as $supp)
                        <option value="{{$supp->kd_supplier}}">{{$supp->nama_supplier}}</option>
                        @endforeach
                      </select>
    								</div>
    						</div>
              </div>
              <div class="col-sm-6">
    						<div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1">Jenis </label>
    								<div class="col-sm-9">
    									<select name="jenis_obat" class="col-xs-10">
                        <option value="Konsinyasi">Konsinyasi</option>
                        <option value="Non-Konsinyasi">Non-Konsinyasi</option>
                      </select>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1">Keterangan </label>
    								<div class="col-sm-9">
    									<textarea name="keterangan" class="col-xs-10" required></textarea>
    								</div>
    						</div>
              </div>
          </div>
						<div class="clearfix sub">
							<div class="col-md-5">
                <a href="{{url('po/obat')}}">
								<button class="btn btn-info" type="button">
									<i class="ace-icon fa fa-plus"></i>
										Obat
									</button>
                </a>
									&nbsp; &nbsp;
									<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
									</button>
                  &nbsp; &nbsp;
                  <button class="btn btn-success" type="submit">
  									<i class="ace-icon fa fa-check bigger-110"></i>
  										Submit
  									</button>
								</div>
                <div class="col-sm-7">
                  <div class="form-group">
      									<select name="jenis_invoice" class="col-sm-4">
                          <option value="cetak">Tanpa Cetak</option>
                          <option value="invoice">Invoice (A5)</option>
                        </select>
      						</div>
                  <div class="price pull-right">
                    @if(count($obat) == 0)
                    <h1>Rp. <span>0,00</span></h1>
                    @else
                    <h1>Rp. <span id="total_bayar">0,00</span></h1>
                    <input type="hidden" class="total_bayar" name="total_bayar">
                    @endif
                  </div>
                </div>
			         </div>
               <hr>
                 <div class="table-responsive table_po">
                   <input type="text"  placeholder="ketik/scan barcode" class="col-xs-4" style="margin-bottom:20px;">
                 <table class="table">
                   <thead>
                     <tr>
                       <th>No</th>
                       <th>Action</th>
                       <th>Kode Obat</th>
                       <th>Nama Obat</th>
                       <th>Jumlah</th>
                       <th>Satuan</th>
                       <th>Harga</th>
                       <th>Sub Total</th>
                       <th>Diskon</th>
                       <th>Nominal Diskon</th>
                       <th>Total</th>
                     </tr>
                   </thead>
                   <tbody>
                     @if(count($obat) == 0)
                     <tr>
                       <th colspan="10">Tekan tombol [+ Obat] untuk menambah data obat yang akan dipesan.</th>
                     </tr>
                     @else
                     <?php $i=1; ?>
                     @foreach($obat as $key)
                     <tr>
                       <th>{{$i}}</th>
                       <th>
                         <a class="red" href="{{url('po/obat/hapuscheckbox/'.$key->kd_obat)}}">
                           <button type="button" class="btn btn-danger" style="border-radius:10px;">
                           <i class="ace-icon fa fa-trash-o bigger-130"></i></button>
                         </a>
                       </th>
                       <th>{{$key->kd_obat}}</th>
                       <input type="hidden" name="{{'kd_obat'.$i}}" value="{{$key->kd_obat}}">
                       <th>{{$key->nama_obat}}</th>
                       <input type="hidden" name="{{'nama_obat'.$i}}" value="{{$key->nama_obat}}">
                       <td>
                         <input type="number" id="{{'jmlh'.$i}}" name="{{'jmlh'.$i}}" value="1" style="width:75px">
                       </td>
                       <td>
                         <?php $satuan = DB::table('satuan')->where('kd_obat','=',$key->kd_obat)
                                     ->join('satuan_obat', 'satuan.kd_sat_obat', '=', 'satuan_obat.kd_sat_obat')
                                     ->join('lokasi_obat', 'satuan.kd_lok_obat', '=', 'lokasi_obat.kd_lok_obat')
                                     ->get(); ?>
                        <select id="{{'satuan'.$i}}" class="satuan form-control select2-hidden-accessible" name="{{'kd_satuan'.$i}}">
                         @foreach($satuan as $item)
                         <option value="{{$item->kd_satuan}}">{{$item->nama_sat_obat}}</option>
                         @endforeach
                        </select></td>
                        <th>
                          <input type="number" id="{{'hna'.$i}}" class="hna" name="{{'hna'.$i}}" value="{{$satuan[0]->hna}}" style="width:75px">
                        </th>
                        <th>
                          <input type="text" id="{{'subtotal'.$i}}" class="subtotal" value="{{$satuan[0]->hna}}" name="{{'subtotal'.$i}}" style="width:75px" readonly>
                        </th>
                        <th>
                          <input type="number" id="{{'diskon'.$i}}" class="diskon" value="0" name="{{'diskon'.$i}}" style="width:75px">
                          <span class="input-group-addon" style="display: initial;">
                            <label id="labelstnjml">%</label>
                          </span>
                        </th>
                        <th>
                          <input type="number" id="{{'nom_dis'.$i}}" class="nom_dis" value="0" name="{{'nom_dis'.$i}}" style="width:75px">
                        </th>
                        <th>
                          <input type="text" id="{{'total'.$i}}" class="total" name="{{'total'.$i}}" value="{{$satuan[0]->hna}}" style="width:75px" readonly>
                        </th>
                     </tr>
                     <input type="hidden" name="jmlh_detail" value="{{$i}}">
                     <?php $i++ ?>
                     @endforeach
                     @endif
                   </tbody>
                 </table>
               </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script>
$(document).ready(function(){
  var x=1;
  var total_bayar = 0;
  $('tbody tr').each(function(){
    var total_bayar_id = "#total"+x;
    total_bayar = total_bayar + parseInt($(total_bayar_id).val());
    x++;
  });
  $('#total_bayar').html(total_bayar);
  $('.total_bayar').val(total_bayar);

  $('input[type=number]').change(function(){
      var id = $(this).attr("id");
      var lastChar = id.substr(id.length - 1);
      var jmlh_id = "#jmlh"+lastChar;
      var hna_id = "#hna"+lastChar;
      var subtotal_id = "#subtotal"+lastChar;
      var total_id = "#total"+lastChar;
      var diskon_id = "#diskon"+lastChar;
      var nom_dis_id = "#nom_dis"+lastChar;
      var diskon = $(diskon_id).val() / 100;
      var subtotal = $(subtotal_id).val();
      var jmlh = $(jmlh_id).val();
      var hna = $(hna_id).val();
      var subtotal = hna*jmlh;
      var nominal_diskon = diskon*subtotal;
      var total = subtotal-nominal_diskon;
      $(subtotal_id).val(subtotal);
      $(total_id).val(total);
      $(nom_dis_id).val(nominal_diskon);
      var x=1;
      var total_bayar = 0;
      $('tbody tr').each(function(){
        var total_bayar_id = "#total"+x;
        total_bayar = total_bayar + parseInt($(total_bayar_id).val());
        x++;
      });
      $('#total_bayar').html(total_bayar);
      $('.total_bayar').val(total_bayar);
  });

  $('select').change(function(){
    var satuan_id = $(this).attr("id");
    var lastChar = satuan_id.substr(satuan_id.length - 1);
    var jmlh_id = "#jmlh"+lastChar;
    var hna_id = "#hna"+lastChar;
    var total_id = "#total"+lastChar;
    var diskon_id = "#diskon"+lastChar;
    var nom_dis_id = "#nom_dis"+lastChar;
    var subtotal_id = "#subtotal"+lastChar;
    var satuan = $(this).val();
    $.ajax({
        type : 'get',
        url : 'po/obat/cekhargasatuan/'+satuan,
    success: function (data) {
      var total_bayar = $('.total_bayar').val();
      var total_bayar_satuan = $(total_id).val();
      var total_semua = total_bayar - total_bayar_satuan;
      $(hna_id).val(data);
      $(subtotal_id).val(0);
      $(jmlh_id).val(0);
      $(diskon_id).val(0);
      $(nom_dis_id).val(0);
      $(total_id).val(0);
      $('#total_bayar').html(total_semua);
      $('.total_bayar').val(total_semua);

    }
    });

  });

  $('.datepicker').datepicker({
				format: 'yyyy-mm-dd',
		});
});
</script>
@stop
