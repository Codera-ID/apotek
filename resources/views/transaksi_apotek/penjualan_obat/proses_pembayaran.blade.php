@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Proses Pembayaran</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Transaksi Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Proses Pembayaran
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Proses Pembayaran</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal" method="post" role="form" action="{{url('penjualan-obat/proses-pembayaran')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="no_faktur" value="{{ $data->no_fak }}">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
    							<label class="col-sm-2 control-label" for="form-field-1"> Subtotal </label>
    								<div class="col-sm-10">
    									<input type="number"  name="subtotal" class="col-xs-10" id="subtotal" value="{{$data->total_bayar}}" required readonly>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-2 control-label" for="form-field-1"> Diskon (%)</label>
    								<div class="col-sm-3">
    									<input type="number" name="diskon" id="diskon" class="col-xs-12" value="0" required>
    								</div>
                    <div class="col-sm-5">
                      <input type="number" name="total_diskon" class="col-xs-12" id="total_diskon" value="0" required readonly>
                    </div>
    						</div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Pajak (%)</label>
                    <div class="col-sm-3">
                      <input type="number" name="pajak" class="col-xs-12" id="pajak" value="0" required>
                    </div>
                    <div class="col-sm-5">
                      <input type="number" id="total_pajak" name="total_pajak" class="col-xs-12" value="0" required readonly>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Biaya Pengiriman </label>
                    <div class="col-sm-10">
                      <input type="number" id="biaya_pengiriman" name="biaya_pengiriman" class="col-xs-10" value="0" required>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Total </label>
                    <div class="col-sm-10">
                      <input type="number" id="total" name="total" class="col-xs-10" value="{{$data->total_bayar}}" required readonly>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Pembayaran </label>
                    <div class="col-sm-10">
                      <select id="jenis-pembayaran" name="pembayaran" class="col-xs-10" required>
                        <option value="tunai">TUNAI</option>
                        <option value="kartu">KARTU</option>
                        <option value="kredit">KREDIT</option>
                      </select>
                    </div>
                </div>
                <div class="form-group" id="jumlah-tunai">
                  <label class="col-sm-2 control-label" for="form-field-1"> Jumlah Tunai </label>
                    <div class="col-sm-10">
                      <input type="number" id="jumlah-tunai-value" name="jumlah_tunai" value="0" class="col-xs-10" required>
                    </div>
                </div>
                <div class="form-group kredit hide">
                  <label class="col-sm-2 control-label" for="form-field-1"> Uang Muka </label>
                    <div class="col-sm-10">
                      <input type="number" id="uang_muka" name="uang_muka" class="col-xs-10" required>
                    </div>
                </div>
                <div class="form-group hide" id="jumlah-kartu">
                  <label class="col-sm-2 control-label" for="form-field-1"> Jumlah Kartu </label>
                    <div class="col-sm-4">
                      <input type="number" id="jumlah-kartu-value" value="0" name="jumlah_kartu" class="col-xs-12">
                    </div>
                    <div class="col-sm-4">
                      <select class="col-xs-12" name="kartu">
                        <option value="Bank BRI">Bank BRI</option>
                        <option value="Bank Mandiri">Bank Mandiri</option>
                        <option value="Bank BNI">Bank BNI</option>
                        <option value="Bank Danamon">Bank Danamon</option>
                        <option value="Permata Bank">Permata Bank</option>
                        <option value="Bank BCA">Bank BCA</option>
                        <option value="Bank BNI">Bank BII</option>
                        <option value="Bank Panin">Bank Panin</option>
                        <option value="CIMB NIAGA">CIMB NIAGA</option>
                        <option value="Bank Lippo">Bank Lippo</option>
                        <option value="Bank NISP">Bank NISP</option>
                      </select>
                    </div>
                </div>
                <div class="form-group hide" id="nomor-kartu">
                  <label class="col-sm-2 control-label" for="form-field-1"> Nama Bank / No. Kartu </label>
                    <div class="col-sm-10">
                      <input type="text"  name="no_kartu" class="col-xs-10">
                    </div>
                </div>
                <div class="form-group" id="kembalian-html">
                  <label class="col-sm-2 control-label" for="form-field-1"> Kembalian </label>
                    <div class="col-sm-10">
                      <input type="number" id="kembalian" name="kembalian" class="col-xs-10" required readonly>
                    </div>
                </div>
                <div class="form-group kredit hide">
                  <label class="col-sm-2 control-label" for="form-field-1"> Jatuh Tempo</label>
                    <div class="col-sm-3">
                      <input type="date" id="deadline" name="tanggal_jatuh_tempo" class="col-xs-12" required>
                    </div>
                    <div class="input-group col-sm-4">
                      <input type="number" id="hari" name="hari" class="col-xs-10 form-control" value="0" required>
                      <span class="input-group-addon">
                        Hari
                      </span>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> No Faktur Pajak </label>
                    <div class="col-sm-10">
                      <input type="text"  name="faktur_pajak" class="col-xs-10">
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Pilihan Cetak </label>
                    <div class="col-sm-10">
                      <select id="jenis_cetak" name="jenis_pembayaran" class="col-xs-10" required>
                        <option value="tunai">Invoice (A5)</option>
                        <option value="struk58">Struk Kecil (58mm)</option>
                        <option value="struk76">Struk Kecil (76mm)</option>
                        <option value="tanpacetak">Tanpa Cetak (76mm)</option>
                      </select>
                    </div>
                </div>
              </div>
            </div>
						<div class="clearfix form-actions">
							<div class="col-md-6">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
									</button>
								</div>
                <div class="col-md-offset-4 col-md-2">
                  <a href="{{url('penjualan-obat')}}">
									<button class="btn btn-danger" type="button">
										<i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
											Kembali
									</button>
                </a>
								</div>
			         </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script>
$(document).ready(function(){
  $('#kembalian').val(0);

  $('#jenis-pembayaran').change(function() {
    if(this.value == 'tunai'){
      $('#jumlah-kartu').addClass('hide');
      $('#nomor-kartu').addClass('hide');
      $('.kredit').addClass('hide');
      $('#jumlah-tunai').removeClass('hide');
      $('#kembalian-html').removeClass('hide');
    }else if(this.value == 'kartu'){
      $('#jumlah-kartu').removeClass('hide');
      $('#nomor-kartu').removeClass('hide');
      $('.kredit').addClass('hide');
      $('#jumlah-tunai').removeClass('hide');
      $('#kembalian-html').removeClass('hide');
    }else if(this.value == 'kredit'){
      $('.kredit').removeClass('hide');
      $('#jumlah-kartu').addClass('hide');
      $('#nomor-kartu').addClass('hide');
      $('#jumlah-tunai').addClass('hide');
      $('#kembalian-html').addClass('hide');
    }
  });

  $('#jumlah-tunai-value').change(function() {
    if( $('#jumlah-kartu-value').val() == undefined ){
      var jumlah_kartu = 0;
    }else {
      var jumlah_kartu = parseInt($('#jumlah-kartu-value').val());
    }
    var jumlah_tunai =  parseInt($('#jumlah-tunai-value').val());
    var total = parseInt($('#total').val());
    var kembalian = (jumlah_tunai + jumlah_kartu) - total;

      if(kembalian > 0){
        $('#kembalian').val(kembalian);
      }else{
        $('#kembalian').val(0);
      }
  });

  $('#jumlah-kartu-value').change(function() {
    console.log($('#jumlah-kartu-value').val());
    if( $('#jumlah-kartu-value').val() == undefined ){
      var jumlah_kartu = 0;
    }else {
      var jumlah_kartu = parseInt($('#jumlah-kartu-value').val());
    }

    var jumlah_tunai =  parseInt($('#jumlah-tunai-value').val());
    var total = parseInt($('#total').val());
    var kembalian = (jumlah_tunai + jumlah_kartu) - total;

      if(kembalian > 0){
        $('#kembalian').val(kembalian);
      }else{
        $('#kembalian').val(0);
      }
      
  });

  $('#diskon').change(function(){
    var subtotal = $('#subtotal').val();
    var diskon = this.value;

    var total_diskon = subtotal * (diskon/100);
    $('#total_diskon').val(total_diskon);

    hitungTotalBayar();
  });

  $('#pajak').change(function(){
    var subtotal = $('#subtotal').val();
    var pajak = this.value;

    var total_pajak = subtotal * (pajak/100);
    $('#total_pajak').val(total_pajak);

    hitungTotalBayar();
  });

  $('#biaya_pengiriman').change(function(){
    hitungTotalBayar();
  });

  $('#deadline').change(function(){
    var deadline = new Date(this.value);
    var today = new Date();

    var timeDiff = Math.abs(deadline.getTime() - today.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

    $('#hari').val(diffDays);

  })

  //hitung semua
  function hitungTotalBayar(){
    var subtotal = parseInt($('#subtotal').val());
    var total_diskon = parseInt($('#total_diskon').val());
    var total_pajak = parseInt($('#total_pajak').val());
    var biaya_pengiriman = parseInt($('#biaya_pengiriman').val());
    var total = subtotal - total_diskon + total_pajak + biaya_pengiriman;

    $('#total').val(total);
  }

});
</script>
@stop
