@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Penjualan Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>
  @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
  @endif

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Transaksi Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Tambah Data Penjualan Obat
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Penjualan Obat</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal po" method="post" role="form" action="{{url('penjualan-obat/tampil-proses-pembayaran')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="col-sm-6">
    						<div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Kasir </label>
    								<div class="col-sm-9">
    									<input type="text" class="col-xs-10" name="kasir" value="{{ Auth::user()->name }}" required readonly>
                      <input type="hidden" class="total_bayar" name="total_bayar">
    								</div>
    						</div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Tanggal </label>
    								<div class="col-sm-9">
    									<input type="date"  name="tgl_penjualan" class="col-xs-10" required>
    								</div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> No. Faktur </label>
    								<div class="col-sm-9">
    									<input type="text"  name="no_fak" class="col-xs-10" value="{{ $no_faktur }}" placeholder="No. Faktur Terisi Secara Otomatis" readonly>
    								</div>
                </div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Gudang </label>
    								<div class="col-sm-9">
                      <select name="kd_gudang" class="col-xs-8" required readonly>
                        @foreach($gudang as $gud)
                        <option value="{{$gud->kd_gudang}}">{{$gud->nama_gudang}}</option>
                        @endforeach
                      </select>
                      <span class="input-group-btn">
                        <a href="gudang/tambah">
                          <button type="button" id="po" class="btn btn-info" style="font-size:14px; padding:0px; padding-left:8.5px; padding-right:8.5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"  title="Tambah data gudang"><i class="glyphicon glyphicon-plus"></i></button>
                        </a>
                      </span>
                    </div>
    						</div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> No. Resep </label>
                    <div class="col-sm-9">
                      <input type="text"  name="no_resep" class="col-xs-10" >
                    </div>
                </div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Pasien </label>
    								<div class="col-sm-9">
    									<select id="pasien" name="kd_pasien" class="col-xs-8" required>
                        <option value="">-Pilih Pasien-</option>
                        @foreach($pasien as $pas)
                        <option value="{{$pas->id_rm}}">{{$pas->nama_pasien}}</option>
                        @endforeach
                      </select>
                      <span class="input-group-btn">
                        <a href="pasien/tambah">
                          <button type="button" id="po" class="btn btn-info" style="font-size:14px; padding:0px; padding-left:8.5px; padding-right:8.5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"  title="Tambah data pasien"><i class="glyphicon glyphicon-plus"></i></button>
                        </a>
                      </span>
    								</div>
    						</div>
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> Dokter </label>
    								<div class="col-sm-9">
    									<select name="kd_dokter" class="col-xs-8" required>
                        <option value="">-Pilih Dokter-</option>
                        @foreach($dokter as $dok)
                        <option value="{{$dok->id_dokter}}">{{$dok->nama_dokter}}</option>
                        @endforeach
                      </select>
                      <span class="input-group-btn">
                        <a href="dokter/tambah">
                          <button type="button" id="po" class="btn btn-info" style="font-size:14px; padding:0px; padding-left:8.5px; padding-right:8.5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"  title="Tambah data dokter"><i class="glyphicon glyphicon-plus"></i></button>
                        </a>
                      </span>
    								</div>
    						</div>
              </div>
            </div>
						<div class="clearfix sub">
							<div class="col-md-5">
                <a href="{{url('penjualan-obat/tambah-obat')}}">
    							<button class="btn btn-info" type="button">
    								<i class="ace-icon fa fa-plus"></i>
    								Obat
    							</button>
                </a>
								&nbsp; &nbsp;
                <button class="btn btn-info" type="button" id="resep">
                  <i class="ace-icon fa fa-plus"></i>
                    Resep
                </button>
								<button class="btn btn-default" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
										Reset
								</button>
                &nbsp; &nbsp;
                <button class="btn btn-success" type="submit">
  								<i class="ace-icon fa fa-check bigger-110"></i>
  									Bayar
  							</button>
							</div>
              <hr>
            </div>
            <div class="table-responsive" style="margin-top:20px;">
              <input type="text"  placeholder="ketik/scan barcode" class="col-xs-4" style="margin-bottom:20px;">
              <table class="table">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Action</th>
                    <th>Kode Obat</th>
                    <th>Nama Obat</th>
                    <th>Jumlah</th>
                    <th>Satuan</th>
                    <th>Harga</th>
                    <th>Sub Total</th>
                    <th>Diskon</th>
                    <th>Nominal Diskon</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  @if(count($data) == 0)
                  <tr>
                    <th colspan="10">Tekan tombol [+ Obat] untuk menambah data obat yang akan dipesan.</th>
                  </tr>
                  @else
                  <?php $i=1; ?>
                  @foreach($data as $key)
                  <tr>
                    <th>{{$i}}</th>
                    <th>
                      <a class="red" href="{{url('penjualan-obat/obat/hapus-obat/'.$key->kd_obat)}}">
                        <button type="button" class="btn btn-danger" style="border-radius:10px;">
                        <i class="ace-icon fa fa-trash-o bigger-130"></i></button>
                      </a>
                    </th>
                    <th>{{$key->kd_obat}}</th>
                    <input type="hidden" name="{{'kd_obat'.$i}}" value="{{$key->kd_obat}}">
                    <th>{{$key->nama_obat}}</th>
                    <input type="hidden" name="{{'nama_obat'.$i}}" value="{{$key->nama_obat}}">
                    <td>
                      <input type="number" id="{{'jmlh'.$i}}" name="{{'jmlh'.$i}}" value="1" style="width:75px">
                    </td>
                    <td>
                      <?php $satuan = DB::table('satuan')->where('kd_obat','=',$key->kd_obat)
                                   ->join('satuan_obat', 'satuan.kd_sat_obat', '=', 'satuan_obat.kd_sat_obat')
                                   ->join('lokasi_obat', 'satuan.kd_lok_obat', '=', 'lokasi_obat.kd_lok_obat')
                                   ->get(); ?>
                      <select id="{{'satuan'.$i}}" class="satuan form-control select2-hidden-accessible" name="{{'kd_satuan'.$i}}">
                       @foreach($satuan as $item)
                       <option value="{{$item->kd_satuan}}">{{$item->nama_sat_obat}}</option>
                       @endforeach
                      </select>
                    </td>
                    <th>
                      <input type="number" id="{{'hna'.$i}}" class="hna" name="{{'hna'.$i}}" value="{{$satuan[0]->hna}}" style="width:75px">
                    </th>
                    <th>
                      <input type="text" id="{{'subtotal'.$i}}" class="subtotal" value="{{$satuan[0]->hna}}" name="{{'subtotal'.$i}}" style="width:75px" readonly>
                    </th>
                    <th>
                      <input type="number" id="{{'diskon'.$i}}" class="diskon" value="0" name="{{'diskon'.$i}}" style="width:75px" readonly>
                      <span class="input-group-addon" style="display: initial;">
                        <label id="labelstnjml">%</label>
                      </span>
                    </th>
                    <th>
                        <input type="number" id="{{'nom_dis'.$i}}" class="nom_dis" value="0" name="{{'nom_dis'.$i}}" style="width:75px">
                    </th>
                    <th>
                        <input type="text" id="{{'total'.$i}}" class="total" name="{{'total'.$i}}" value="{{$satuan[0]->hna}}" style="width:75px" readonly>
                    </th>
                  </tr>
                  <input type="hidden" id="jumlah" name="jmlh_detail" value="{{$i}}">
                  <?php $i++ ?>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div>
            
			    </form>
					<!-- PAGE CONTENT ENDS -->
			  </div><!-- /.col -->
		  </div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script>
$(document).ready(function(){
  var x=1;
  var total_bayar = 0;
  $('tbody tr').each(function(){
    var total_bayar_id = "#total"+x;
    total_bayar = total_bayar + parseInt($(total_bayar_id).val());
    x++;
  });
  $('#total_bayar').html(total_bayar);
  $('.total_bayar').val(total_bayar);
  $('#penjualanobat-total').val(total_bayar);
  $('#penjualanobat-dis').val(0);
  $('#penjualanobat-pajak').val(0); 
  $('#resep').on('click',function() {
    $('#resep').prop('readonly', true);
    $('#resep').prop('disabled', true);
  });

  $('input[type=number]').change(function(){
      var id = $(this).attr("id");
      var lastChar = id.substr(id.length - 1);
      var jmlh_id = "#jmlh"+lastChar;
      var hna_id = "#hna"+lastChar;
      var subtotal_id = "#subtotal"+lastChar;
      var total_id = "#total"+lastChar;
      var diskon_id = "#diskon"+lastChar;
      var nom_dis_id = "#nom_dis"+lastChar;
      var margin_id = "#margin_hrg_jual"+lastChar;
      var harga_jual_1_id = "#harga_jual_1"+lastChar;
      var diskon = $(diskon_id).val() / 100;
      var subtotal = $(subtotal_id).val();
      var jmlh = $(jmlh_id).val();
      var hna = $(hna_id).val();
      var margin = $(margin_id).val() / 100;
      var harga_jual = margin*hna;
      var harga_jual_1 = parseInt(hna)+parseInt(harga_jual);
      var subtotal = hna*jmlh;
      var nominal_diskon = diskon*subtotal;
      var total = subtotal-nominal_diskon;
      $(harga_jual_1_id).val(harga_jual_1);
      $(subtotal_id).val(subtotal);
      $(total_id).val(total);
      $(nom_dis_id).val(nominal_diskon);
      var x=1;
      var total_bayar = 0;
      $('tbody tr').each(function(){
        var total_bayar_id = "#total"+x;
        total_bayar = total_bayar + parseInt($(total_bayar_id).val());
        x++;
      });

      $('#penjualanobat-total').val(total_bayar);
      var total_bayar_dis = $('#penjualanobat-total').val();
      var dis = $('#penjualanobat-dis').val();
      var hasil_dis = (dis/100)*total_bayar_dis;
      var total_bayar_hasil = total_bayar_dis - hasil_dis;
      $('#hasil-dis').val(hasil_dis);
      var total_bayar_pajak = $('#penjualanobat-total').val();
      var pajak = $('#penjualanobat-pajak').val();
      var hasil_pajak = (pajak/100)*total_bayar_pajak;
      var total_bayar_hasil_pajak = total_bayar_hasil + hasil_pajak;
      $('#hasil-pajak').val(hasil_pajak);
      var biaya = $('#biaya').val();
      var total_hasil_semua = total_bayar_hasil_pajak + parseInt(biaya);
      $('#total_bayar').html(total_hasil_semua);
      $('.total_bayar').val(total_hasil_semua);
  });

  $('select').change(function(){
    var satuan_id = $(this).attr("id");
    var lastChar = satuan_id.substr(satuan_id.length - 1);
    var hna_id = "#hna"+lastChar;
    var subtotal_id = "#subtotal"+lastChar;
    var total_id = "#total"+lastChar;
    var jmlh_id = "#jmlh"+lastChar;
    var diskon_id = "#diskon"+lastChar;
    var nom_dis_id = "#nom_dis"+lastChar;
    var satuan = $(this).val();
    $.ajax({
        type : 'get',
        url : "{{url('pembelian-obat/obat/cekhargasatuan/')}}"+'/'+satuan,
    success: function (data) {
      $(hna_id).val(data);
      $(subtotal_id).val(0);
      var total_bayar = $('.total_bayar').val();
      var penjualanobat_total = $('#penjualanobat-total').val();
      var total = $(total_id).val();
      var total_semua = penjualanobat_total - total;
      var dis = $('#penjualanobat-dis').val();
      var hasil_dis = (dis/100)*total_semua;
      var total_bayar_hasil = total_semua - hasil_dis;
      $('#hasil-dis').val(hasil_dis);
      var pajak = $('#penjualanobat-pajak').val();
      var hasil_pajak = (pajak/100)*total_semua;
      var total_bayar_hasil_pajak = total_bayar_hasil + hasil_pajak;
      $('#hasil-pajak').val(hasil_pajak);
      var biaya = $('#biaya').val();
      var total_hasil_semua = total_bayar_hasil_pajak + parseInt(biaya);
      $('#total_bayar').html(total_hasil_semua);
      $('.total_bayar').val(0);
      $('#penjualanobat-total').val(total_semua);
      $(jmlh_id).val(0);
      $(total_id).val(0);
      $(diskon_id).val(0);
      $(nom_dis_id).val(0);

    }
    });
    });

  function hitung_semua(i) {
      var lastChar = i;
      var jmlh_id = "#jmlh"+lastChar;
      var hna_id = "#hna"+lastChar;
      var subtotal_id = "#subtotal"+lastChar;
      var total_id = "#total"+lastChar;
      var diskon_id = "#diskon"+lastChar;
      var nom_dis_id = "#nom_dis"+lastChar;
      var margin_id = "#margin_hrg_jual"+lastChar;
      var harga_jual_1_id = "#harga_jual_1"+lastChar;
      var diskon = $(diskon_id).val() / 100;
      var subtotal = $(subtotal_id).val();
      var jmlh = $(jmlh_id).val();
      var hna = $(hna_id).val();
      var margin = $(margin_id).val() / 100;
      var harga_jual = margin*hna;
      var harga_jual_1 = parseInt(hna)+parseInt(harga_jual);
      var subtotal = hna*jmlh;
      var nominal_diskon = diskon*subtotal;
      var total = subtotal-nominal_diskon;
      $(harga_jual_1_id).val(harga_jual_1);
      $(subtotal_id).val(subtotal);
      $(total_id).val(total);
      $(nom_dis_id).val(nominal_diskon);
      var x=1;
      var total_bayar = 0;
      $('tbody tr').each(function(){
        var total_bayar_id = "#total"+x;
        total_bayar = total_bayar + parseInt($(total_bayar_id).val());
        x++;
      });

      $('#penjualanobat-total').val(total_bayar);
      var total_bayar_dis = $('#penjualanobat-total').val();
      var dis = $('#penjualanobat-dis').val();
      var hasil_dis = (dis/100)*total_bayar_dis;
      var total_bayar_hasil = total_bayar_dis - hasil_dis;
      $('#hasil-dis').val(hasil_dis);
      var total_bayar_pajak = $('#penjualanobat-total').val();
      var pajak = $('#penjualanobat-pajak').val();
      var hasil_pajak = (pajak/100)*total_bayar_pajak;
      var total_bayar_hasil_pajak = total_bayar_hasil + hasil_pajak;
      $('#hasil-pajak').val(hasil_pajak);
      var biaya = $('#biaya').val();
      var total_hasil_semua = total_bayar_hasil_pajak + parseInt(biaya);
      $('#total_bayar').html(total_hasil_semua);
      $('.total_bayar').val(total_hasil_semua);
  }

});
</script>
@stop
