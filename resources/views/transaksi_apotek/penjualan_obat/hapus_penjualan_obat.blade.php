@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Data Penjualan Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>
  @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
  @endif

  <div class="page-content">
    <div class="page-header">
      <h1>
        Transaksi Apotek
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          Hapus Penjualan Obat
        </small>
      </h1>
    </div><!-- /.page-header -->

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
          <div class="col-xs-12">
            <div class="table-header">
              Data Penjualan Obat
            </div>

            <!-- div.table-responsive -->

            <!-- div.dataTables_borderWrap -->
            <div>
              <table id="dynamic-table" class="table table-striped table-bordered table-hover data">
                <thead>
                  <tr>
                    <th class="center">#</th>
                    <th>Tanggal</th>
                    <th>No. Faktur</th>
                    <th>Pasien</th>
                    <th>Dokter</th>
                    <th>Jenis</th>
                    <th>Gudang</th>
                    <th>Diskon(%)</th>
                    <th>Pajak(%)</th>
                    <th>Grand Total</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $i=0 ?>
                  @foreach($penjualan_obat as $key)
                  <tr>
                    <td class="center">
                      <a href="#" data-toggle="collapse" data-target="{{'#detail'.$i}}">
                        <span class="glyphicon glyphicon-collapse-down"></span>
                      </a>
                    </td>
                    <td>{{$key->created_at}}</td>
                    <td>{{$key->no_faktur}}</td>
                    <td>{{$key->nama_pasien}}</td>
                    <td>{{$key->nama_dokter}}</td>
                    <td>{{$key->jenis_pembayaran}}</td>
                    <td>{{$key->nama_gudang}}</td>
                    <td>{{$key->disc_harga_jual}}%</td>
                    <td>{{$key->pajak}}</td>
                    <td>{{$key->total}}</td>
                    <td>
                      <a class="red" href="{{url('penjualan-obat/data-hapus/'.$key->no_faktur)}}">
                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                      </a>
                    </td>
                  </tr>
                  <tr id="{{'detail'.$i}}" class="collapse">
                      <td colspan=11>
                        <div class="skip-export kv-expanded-row w4" data-index="0" data-key="0" style="display: block;">
                          <div id="w8" class="grid-view">
                            <table class="table table-striped table-bordered">
                              <thead>
                              <tr>
                                <th style="width:15px;">No.</th>
                                <th style="width:140px;">Kode Obat</th>
                                <th style="width:180px;">Nama Obat</th>
                                <th style="width:80px; text-align:right;">Jumlah</th>
                                <th style="width:140px;">Satuan</th>
                                <th style="width:120px; text-align:right;">Harga</th>
                                <th style="width:80px; text-align:right;">Diskon</th>
                                <th style="width:80px; text-align:right;">Bonus</th>
                                <th style="width:80px; text-align:right;">No. Batch</th>
                              </tr>
                              </thead>
                              <tbody>
                                <?php $a=1;

                                $penjualan_detail = DB::table('penjualan_detail')->where('no_faktur',$key->no_faktur)
                                          ->join('obat', 'penjualan_detail.kd_obat', '=', 'obat.kd_obat')
                                          ->join('satuan', 'penjualan_detail.kd_satuan', '=', 'satuan.kd_satuan')
                                          ->get();
                                 ?>
                                @foreach($penjualan_detail as $item)
                              <tr data-key="0">
                                <td>{{$a}}</td>
                                <td>{{$item->kd_obat}}</td>
                                <td>{{$item->nama_obat}}</td>
                                <td style="text-align:right">{{$item->jmlh}}</td>
                                <td>{{$item->nama_sat}}</td>
                                <td style="text-align:right">{{$item->harga}}</td>
                                <td style="text-align:right">{{$item->diskon}} %</td>
                                <td style="text-align:right">0 %</td>
                                <td style="text-align:right">{{$item->no_batch}}</td>
                              </tr>
                              <?php $a++ ?>
                              @endforeach
                            </tbody>
                            </table>
                          </div>
                        </div>
                      </td>
                  </tr>
                  <?php $i++ ?>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
});
} );
</script>
@stop
