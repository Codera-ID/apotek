@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Data Retur Penjualan Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>
  @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
  @endif

  <div class="page-content">
    <div class="page-header">
      <h1>
        Transaksi Apotek
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          Retur Penjualan Obat
        </small>
      </h1>
      <div class="clearfix">
        <form class="form-horizontal" action="{{url('retur-penjualan/data-hapus')}}" method="get">
              <div class="pull-left col-xs-4">                
                  <label class="control-label no-padding-right">Pilih Periode</label>
                  <div class="input-daterange input-group">
                      <input type="text" class="form-control" name="start" value="{{ $start_date }}"/>
                      <span class="input-group-addon">
                      <i class="fa fa-exchange"></i>
                      </span>

                      <input type="text" class="form-control" name="end" value="{{ $end_date }}"/>
                  </div>
                  <br>
                  <span class="input-group-btn">
                      <button type="submit" class="btn btn-purple btn-sm">
                          <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                          Cari
                      </button>
                  </span>
              </div>
          </form>
        </div>

      <div class="pull-right all-button">
        <a class="btn btn-primary btn-sm" href="{{url('retur-penjualan/penjualan-obat')}}"><i class="fa fa-plus"></i>  Tambah</a>
      </div>
      <br>
    </div><!-- /.page-header -->

    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
          <div class="col-xs-12">
            <div class="table-header">
              Data Retur Penjualan Obat
            </div>

            <!-- div.table-responsive -->

            <!-- div.dataTables_borderWrap -->
            <div>
              <table id="dynamic-table" class="table table-striped table-bordered table-hover data">
                <thead>
                  <tr>
                    <th class="center">#</th>
                    <th>Tanggal Retur</th>
                    <th>No. Faktur</th>
                    <th>Pasien</th>
                    <th>Dokter</th>
                    <th>No Resep</th>
                    <th>Jenis Pembayaran</th>
                    <th>Grand Total</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $index = 0;?>
                  @foreach($retur_penjualan as $key)
                  <?php $index++;?>
                  <tr>
                    <td class="center">
                      <a class="detail-obat" href="#" data-toggle="collapse" data-target=".detail{{$index}}">
                        <span class="glyphicon glyphicon-collapse-down"></span>
                      </a>
                    </td>
                    <td>{{$key->tanggal_retur}}</td>
                    <td>{{$key->no_faktur}}</td>
                    <td>{{$key->nama_pasien}}</td>
                    <td>{{$key->nama_dokter}}</td>
                    <td>{{$key->no_resep}}</td>
                    <td>{{$key->jenis_pembayaran}}</td>
                    <td>{{$key->total}}</td>
                    <td>
                      <div class="hidden-sm hidden-xs action-buttons">
                        <a class="red" href="{{url('retur-penjualan/data-hapus/'.$key->no_retur_penjualan)}}">
                          <i class="ace-icon fa fa-trash-o bigger-130"></i>
                        </a>
                      </div>
                    </td>
                  </tr>
                  <tr class="detail{{$index}} collapse" >
                      <td colspan=11>
                        <div class="skip-export kv-expanded-row w4" data-index="0" data-key="0" style="display: block;">
                          <div id="w8" class="grid-view">
                            <table class="table table-striped table-bordered">
                              <thead>
                              <tr>
                                <th style="width:15px;">No.</th>
                                <th style="width:180px;">Nama Obat</th>
                                <th style="width:140px;">Satuan</th>
                                <th style="width:80px; text-align:right;">Jumlah</th>
                                <th style="width:120px; text-align:right;">Harga Rata Rata</th>
                                <th style="width:80px; text-align:right;">Potongan</th>
                                <th style="width:120px; text-align:right;">Total</th>
                              </tr>
                              </thead>
                              <tbody>
                                <?php
                                $a=1;
                                $detail_retur_penjualan = DB::table('detail_retur_penjualan')
                                          ->join('retur_penjualan_obat', 'detail_retur_penjualan.no_retur_penjualan', '=',
                                            'retur_penjualan_obat.no_retur_penjualan')
                                          ->join('penjualan_detail', 'detail_retur_penjualan.kd_obat', '=', 'penjualan_detail.kd_obat')
                                          ->join('satuan', 'penjualan_detail.kd_satuan', '=', 'satuan.kd_satuan')
                                          ->join('obat', 'obat.kd_obat', '=', 'satuan.kd_obat')
                                          ->where('detail_retur_penjualan.no_retur_penjualan',$key->no_retur_penjualan)
                                          ->where('penjualan_detail.no_faktur', $key->no_faktur)
                                          ->where('penjualan_detail.retur', '=', 'true')
                                          ->select('detail_retur_penjualan.*', 'satuan.nama_sat', 'obat.nama_obat')
                                          ->get();
                                 ?>
                                 @foreach($detail_retur_penjualan as $item)
                                  <tr>
                                    <td>{{$a}}</td>
                                    <td>{{$item->nama_obat}}</td>
                                    <td>{{$item->nama_sat}}</td>
                                    <td style="text-align:right">{{$item->jmlh}}</td>
                                    <td style="text-align:right">{{$item->harga_rata_rata}}</td>
                                    <td style="text-align:right">{{$item->potongan}}</td>
                                    <td style="text-align:right">{{$item->total}}</td>
                                  </tr>
                                  <?php $a++ ?>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  
  //or change it into a date range picker
  $('.input-daterange').datepicker({
              autoclose:true,
              format: 'yyyy-mm-dd'
          });

  //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
  $('input[name=date-range-picker]').daterangepicker({
    'applyClass' : 'btn-sm btn-success',
    'cancelClass' : 'btn-sm btn-default',
    locale: {
      applyLabel: 'Apply',
      cancelLabel: 'Cancel',
    }
  })
  .prev().on(ace.click_event, function(){
    $(this).next().focus();
  });
  
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
  });

  
});

</script>
@stop
