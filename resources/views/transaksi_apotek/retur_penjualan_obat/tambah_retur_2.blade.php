@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Retur Penjualan Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Transaksi Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Retur Penjualan Obat
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Retur Penjualan Obat</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal po" method="post" role="form" action="{{url('retur-penjualan/tambah/retur')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> No. Faktur </label>
    								<div class="col-sm-9">
                            <input id="pembelianobat-pono" class="col-xs-8" name="no_faktur" value="" type="text" readonly>
                            <span class="input-group-btn">
                              <a href="{{url('retur-penjualan/penjualan-obat')}}">
                                <button type="button" id="po" class="btn btn-info" style="font-size:12px; padding:3.60px; padding-left:7.5px; padding-right:7.5px;" title="Pilih data PO"><i class="glyphicon glyphicon-search"></i> Cari</button>
                              </a>
                            </span>
    								</div>
    						</div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Tanggal Faktur </label>
    								<div class="col-sm-9">
    									<input type="date"  name="tgl_fak" class="col-xs-10" value="" readonly>
    								</div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Tanggal Retur </label>
    								<div class="col-sm-9">
    									<input type="date"  name="tgl_retur" class="col-xs-10" value="" required>
    								</div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Gudang </label>
                    <div class="col-sm-9">
                      <select name="kd_gudang" class="col-xs-8" required readonly disabled>
                        <option value=""></option>
                      </select>
                    </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> No. Resep </label>
                    <div class="col-sm-9">
                      <input type="text"  name="no_resep" value="" class="col-xs-10" readonly>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Pasien </label>
                    <div class="col-sm-9">
                      <select id="pasien" name="kd_pasien" class="col-xs-8" required readonly disabled>
                        <option value=""></option>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Dokter </label>
                    <div class="col-sm-9">
                      <select name="kd_dokter" class="col-xs-8" required readonly disabled>
                        <option value=""></option>
                      </select>
                    </div>
                </div>
              </div>
          </div>
						<div class="clearfix sub">
							<div class="col-md-5">
									<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
									</button>
                  &nbsp; &nbsp;
                  <button class="btn btn-success" type="submit">
  									<i class="ace-icon fa fa-check bigger-110"></i>
  										Submit
  									</button>
								</div>
                <div class="col-sm-7">
                  <div class="price pull-right">
                    <h1>Rp. <span>0,00</span></h1>
                    <input type="hidden" class="total_bayar" name="total_bayar">
                  </div>
                </div>

			         </div>
               <hr>
                 <div class="table-responsive table_po">
                   <input type="text"  placeholder="ketik/scan barcode" class="col-xs-4" style="margin-bottom:20px;">
                 <table class="table">
                   <thead>
                     <tr>
                       <th>No</th>
                       <th>Action</th>
                       <th>Kode Obat</th>
                       <th>Nama Obat</th>
                       <th>Satuan</th>
                       <th>Jumlah</th>
                       <th>Harga Beli</th>
                       <th>Sub Total</th>
                       <th>Potongan</th>
                       <th>Total</th>
                     </tr>
                   </thead>
                   <tbody>
                     <tr>
                       <th colspan="10">Tekan tombol [+ Obat] untuk menambah data obat yang akan dipesan.</th>
                     </tr>
                   </tbody>
                 </table>
               </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script>
$(document).ready(function(){

});
</script>
@stop
