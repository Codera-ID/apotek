@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Retur Penjualan Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Transaksi Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Retur Penjualan Obat
					</small>
				</h1>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">
          <div class="page-header-detail">
    				<h1>Retur Penjualan Obat</h1>
    			</div>
					<!-- PAGE CONTENT BEGINS -->
					<form class="form-horizontal po" method="post" role="form" action="{{url('retur-penjualan/tambah/retur')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
    							<label class="col-sm-3 control-label" for="form-field-1"> No. Faktur </label>
    								<div class="col-sm-9">
                            <input id="pembelianobat-pono" class="col-xs-8" name="no_faktur" value="{{$id}}" type="text" readonly>
                            <span class="input-group-btn">
                              <a href="{{url('retur-penjualan/penjualan-obat')}}">
                                <button type="button" id="po" class="btn btn-info" style="font-size:12px; padding:3.60px; padding-left:7.5px; padding-right:7.5px;" title="Pilih data PO"><i class="glyphicon glyphicon-search"></i> Cari</button>
                              </a>
                            </span>
    								</div>
    						</div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Tanggal Faktur </label>
    								<div class="col-sm-9">
    									<input type="date"  name="tgl_fak" class="col-xs-10" value="{{$penjualan_obat[0]->tgl_faktur}}" readonly>
    								</div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Tanggal Retur </label>
    								<div class="col-sm-9">
    									<input type="date"  name="tgl_retur" class="col-xs-10" value="" required>
    								</div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Gudang </label>
                    <div class="col-sm-9">
                      <select name="kd_gudang" class="col-xs-8" required readonly disabled>
                        <option value="{{$penjualan_obat[0]->kd_gudang}}">{{$penjualan_obat[0]->nama_gudang}}</option>
                      </select>
                    </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> No. Resep </label>
                    <div class="col-sm-9">
                      <input type="text"  name="no_resep" value="{{$penjualan_obat[0]->no_resep}}" class="col-xs-10" readonly>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Pasien </label>
                    <div class="col-sm-9">
                      <select id="pasien" name="kd_pasien" class="col-xs-8" required readonly disabled>
                        <option value="{{$penjualan_obat[0]->id_rm}}">{{$penjualan_obat[0]->nama_pasien}}</option>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="form-field-1"> Dokter </label>
                    <div class="col-sm-9">
                      <select name="kd_dokter" class="col-xs-8" required readonly disabled>
                        <option value="{{$penjualan_obat[0]->id_dokter}}">{{$penjualan_obat[0]->nama_dokter}}</option>
                      </select>
                    </div>
                </div>
              </div>
          </div>
						<div class="clearfix sub">
							<div class="col-md-5">
									<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
									</button>
                  &nbsp; &nbsp;
                  <button class="btn btn-success" type="submit">
  									<i class="ace-icon fa fa-check bigger-110"></i>
  										Submit
  									</button>
								</div>
                <div class="col-sm-7">
                  <div class="price pull-right">
                    @if(count($data) == 0)
                    <h1>Rp. <span>0,00</span></h1>
                    @else
                    <h1>Rp. <span id="total_bayar">0,00</span></h1>
                    <input type="hidden" class="total_bayar" name="total_bayar">
                    @endif
                  </div>
                </div>

			         </div>
               <hr>
                 <div class="table-responsive table_po">
                   <input type="text"  placeholder="ketik/scan barcode" class="col-xs-4" style="margin-bottom:20px;">
                 <table class="table">
                   <thead>
                     <tr>
                       <th>No</th>
                       <th>Action</th>
                       <th>Kode Obat</th>
                       <th>Nama Obat</th>
                       <th>Satuan</th>
                       <th>Jumlah</th>
                       <th>Harga Beli</th>
                       <th>Sub Total</th>
                       <th>Potongan</th>
                       <th>Total</th>
                     </tr>
                   </thead>
                   <tbody>
                     @if(count($data) == 0)
                     <tr>
                       <th colspan="10">Tekan tombol [+ Obat] untuk menambah data obat yang akan dipesan.</th>
                     </tr>
                     @else
                     <?php $i=1; ?>
                     @foreach($data as $key)
                     <tr>
                       <th>{{$i}}</th>
                       <th>
                         <a class="red" href="{{url('retur-penjualan/hapus_obat_retur/'.$key->no_faktur.'/'.$key->kd_obat)}}">
                           <button type="button" class="btn btn-danger" style="border-radius:10px;">
                           <i class="ace-icon fa fa-trash-o bigger-130"></i></button>
                         </a>
                       </th>
                       <th>{{$key->kd_obat}}</th>
                       <input type="hidden" name="{{'kd_obat'.$i}}" value="{{$key->kd_obat}}">
                       <th>{{$key->nama_obat}}</th>
                       <input type="hidden" name="{{'nama_obat'.$i}}" value="{{$key->nama_obat}}">
                       <td>{{$key->nama_sat_obat}}</td>
                       <input type="hidden" id="{{'kd_satuan'.$i}}" name="{{'kd_satuan'.$i}}" value="{{$key->kd_satuan}}">
                       <td>
                         <input type="number" id="{{'jmlh'.$i}}" name="{{'jmlh'.$i}}" value="1" style="width:75px" min="1" max="{{$key->jmlh}}">
                         <input type="hidden" id="{{'jumlah'.$i}}" value="{{$key->jmlh}}">
                       </td>
                        <th>
                          <input type="number" id="{{'hna'.$i}}" class="hna" name="{{'hna'.$i}}" value="{{$key->harga*(1-($key->diskon/100))}}" style="width:75px" readonly>
                        </th>
                        <th>
                          <input type="text" id="{{'subtotal'.$i}}" class="subtotal" value="{{$key->harga*(1-($key->diskon/100))}}" name="{{'subtotal'.$i}}" style="width:75px" readonly>
                        </th>
                        <th>
                          <input type="number" id="{{'potongan'.$i}}" class="potongan" value="{{$key->diskon}}" name="{{'potongan'.$i}}" style="width:75px">
                        </th>
                        <th>
                          <input type="text" id="{{'total'.$i}}" class="total" name="{{'total'.$i}}" value="{{$key->harga*(1-($key->diskon/100))}}" style="width:75px" readonly>
                        </th>
                     </tr>
                     <input type="hidden" name="jmlh_detail" value="{{$i}}">
                     <?php $i++ ?>
                     @endforeach
                     @endif
                   </tbody>
                 </table>
               </div>
			      </form>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script>
$(document).ready(function(){
  var x=1;
  var total_bayar = 0;
  $('tbody tr').each(function(){
    var total_bayar_id = "#total"+x;
    total_bayar = total_bayar + parseInt($(total_bayar_id).val());
    x++;
  });
  $('#total_bayar').html(total_bayar);
  $('.total_bayar').val(total_bayar);

  $('input[type=number]').change(function(){
      var id = $(this).attr("id");
      var lastChar = id.substr(id.length - 1);
      var jmlh_id = "#jmlh"+lastChar;
      var jumlah_id = "#jumlah"+lastChar;
      var jumlah = $(jumlah_id).val();
      var hna_id = "#hna"+lastChar;
      var subtotal_id = "#subtotal"+lastChar;
      var total_id = "#total"+lastChar;
      var potongan_id = "#potongan"+lastChar;
      var subtotal = $(subtotal_id).val();
      var jmlh = $(jmlh_id).val();
      if(parseInt(jmlh) > jumlah){
          jmlh = jumlah;
      }if(parseInt(jmlh) < 1){
        jmlh = 1;
      }
      var hna = $(hna_id).val();
      var subtotal = hna*jmlh;
      var potongan = $(potongan_id).val();
      var total = subtotal-potongan;
      $(subtotal_id).val(subtotal);
      $(jmlh_id).val(jmlh);
      $(total_id).val(total);
      var x=1;
      var total_bayar = 0;
      $('tbody tr').each(function(){
        var total_bayar_id = "#total"+x;
        total_bayar = total_bayar + parseInt($(total_bayar_id).val());
        x++;
      });
      $('#total_bayar').html(total_bayar);
      $('.total_bayar').val(total_bayar);
  });

});
</script>
@stop
