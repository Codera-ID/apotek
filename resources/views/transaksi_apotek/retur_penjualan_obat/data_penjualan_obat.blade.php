@extends('layouts.home')
@section('content')
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home</a>
      </li>

      <li>
        <a href="#">Transaksi Apotek</a>
      </li>
      <li class="active">Pencarian Data Penjualan Obat</li>
    </ul><!-- /.breadcrumb -->
  </div>

  <div class="page-content">
		<!-- /.ace-settings-container -->
  		<div class="page-header">
				<h1>
					Transaksi Apotek
				  <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Pencarian Data Penjualan Obat
					</small>
				</h1>
        <br>
        <div class="clearfix">
          <div class="col-xs-6">
            <form class="form-inline" action="{{url('retur-penjualan/penjualan-obat')}}" method="get" style="margin:10px 0px">
              <div class="row">
                <label class="control-label no-padding-right">Pilih Periode</label>
                <div class="input-daterange input-group">
                    <input type="text" class="form-control" name="start" value="{{$start_date}}"/>
                    <span class="input-group-addon">
                    <i class="fa fa-exchange"></i>
                    </span>
                    <input type="text" class="form-control" name="end" value="{{$end_date}}"/>
                </div>
                <button type="submit" class="btn btn-sm btn-default"> <i class="fa fa-search"></i> Cari </button>
              </div>
            </form>
          </div>
        </div>
			</div><!-- /.page-header -->

      <div class="row">
				<div class="col-xs-12">


					<!-- PAGE CONTENT BEGINS -->
          <div class="table-header">
            Data Penjualan Obat
          </div>
          <div>
            <div class="table-responsive table_po">

            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>Action</th>
                  <th>Tanggal</th>
                  <th>No. Faktur</th>
                  <th>Pasien</th>
                  <th>Dokter</th>
                  <th>Jenis</th>
                  <th>Gudang</th>
                  <th>Diskon(%)</th>
                  <th>Pajak(%)</th>
                  <th>Grand Total</th>
                </tr>
              </thead>

              <tbody>
                @foreach($penjualan_obat as $key)
                <tr>
                  <td align="center">
                    <a href="{{url('retur-penjualan/update_retur/'.$key->no_faktur)}}">
                    <button type="button" class="btn btn-success" style="padding:0px" name="pilih" id="coba"/><i class="ace-icon fa fa-check bigger-110"></i></button>
                  </a>
                  </td>
                  <td>{{$key->tgl_faktur}}</td>
                  <td>{{$key->no_faktur}}</td>
                  <td>{{$key->nama_pasien}}</td>
                  <td>{{$key->nama_dokter}}</td>
                  <td>{{$key->jenis_pembayaran}}</td>
                  <td>{{$key->nama_gudang}}</td>
                  <td>{{$key->diskon}}%</td>
                  <td>{{$key->pajak}}</td>
                  <td>{{$key->total}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          </div>
          <div class="clearfix form-actions">
              <div class="pull-right">
                <a href="{{url('retur-penjualan')}}">
                <button class="btn btn-danger" type="button">
                  <i class="ace-icon glyphicon glyphicon-step-backward bigger-110"></i>
                    Kembali
                </button>
              </a>
              </div>
             </div>
             <div id="coba2"></div>
						<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>
@stop
@section('js')
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $('#dynamic-table').DataTable();
  $.extend( $.fn.dataTable.defaults, {
        columnDefs: [{
            targets: [ 3 ]
        }]
  });

  $( "input[type=checkbox]" ).change(function(){
    var id= this.value;

    if(this.checked){
      var url = "obat/addobat/"+id;

      $.get(url);
    }else{
      var url = "obat/destroyobat/"+id;
      $.get(url);
    }
  });

  $.ajax({
      type : 'get',
      url : 'obat/cekobat',
  success: function (data) {
    $.each(data, function(key, value){
      $('input[type=checkbox]').each(function () {
                 if (this.value == value) {
                   $(this).prop( "checked", true );
                 }
      });
    });
    }
  });

  //or change it into a date range picker
  $('.input-daterange').datepicker({
              autoclose:true,
              format: 'yyyy-mm-dd'
          });
      
  //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
  $('input[name=date-range-picker]').daterangepicker({
    'applyClass' : 'btn-sm btn-success',
    'cancelClass' : 'btn-sm btn-default',
    locale: {
      applyLabel: 'Apply',
      cancelLabel: 'Cancel',
    }
  })
  .prev().on(ace.click_event, function(){
    $(this).next().focus();
  });
  
});
</script>
@stop
