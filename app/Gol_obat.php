<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gol_obat extends Model
{
  protected $table = 'golongan_obat';

  public function obat()
    {
        return $this->hasMany('App\Obat');
    }
}
