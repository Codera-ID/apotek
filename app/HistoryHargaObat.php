<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryHargaObat extends Model
{
    protected $table = 'history_harga_obat';
    protected $primaryKey = 'id';
    public function morp()
    {
        return $this->morphTo();
    }
}
