<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obat extends Model
{
  protected $table = 'obat';

  public function gol_obat()
    {
        return $this->belongsTo('App\Gol_obat','kd_gol_obat','kd_gol_obat');
    }

    public function kat_obat()
    {
        return $this->belongsTo('App\Kat_obat','kd_kat_obat','kd_kat_obat');
    }

    public function pabrik()
    {
        return $this->belongsTo('App\Pabrik','kd_pabrik','kd_pabrik');
    }
    public function sat(){
      return $this->hasMany('App\Satuan','kd_obat','kd_obat');
    }
    public function gud()
    {
      # code...
      return $this->hasOne('App\Lok_obat','kd_lok_obat','kd_lok_obat');
    }
    public function mop()
    {
        return $this->morphMany('App\Satuan', 'morp');
    }
    public function mutasi()
    {
        return $this->hasMany('App\MutasiObat','kd_obat','kd_obat');        
    }
}
