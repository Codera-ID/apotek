<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JurnalSingkat extends Model
{
  protected $table = 'jurnal_singkat';
  public $timestamps = false;
  protected $primaryKey = 'id_jurnal';
  public function kredit(){
    return $this->hasOne('App\JurnalSingkat','kode_akun_kredit','kode_akun');
  }
  public function debit(){
    return $this->hasOne('App\JurnalSingkat','kode_akun_debit','kode_akun');
  }
}
