<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosAkunKeuangan extends Model
{
  protected $table = 'pos_akun_keuangan';
  public $timestamps = false;
  protected $primaryKey = 'kode_pos_akun';
}
