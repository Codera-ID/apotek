<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MutasiObat extends Model
{
    protected $table = 'mutasi_obats';
    
    public function obat()
    {
        return $this->belongsTo('App\Obat');        
    }

}
