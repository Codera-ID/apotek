<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalShift extends Model
{
  protected $table = 'jadwal_shift';
  public $timestamps = false;
  protected $primaryKey = 'id_js';
}
