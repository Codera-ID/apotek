<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KelompokAkun extends Model
{
  protected $table = 'kelompok_akun_keuangan';
  public $timestamps = false;
  protected $primaryKey = 'kode_kelompok_akun';
}
