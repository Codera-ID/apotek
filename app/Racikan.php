<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Racikan extends Model
{
  protected $table = 'racikan';

  public function kat_obat()
  {
      return $this->belongsTo('App\Kat_obat','kd_kat_obat','kd_kat_obat');
  }

}
