<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AksesMenu extends Model
{
  protected $table = 'akses_menu';
  public $timestamps = false;
  protected $primaryKey = 'id';
}
