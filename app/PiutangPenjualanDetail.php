<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PiutangPenjualanDetail extends Model
{
  protected $table = 'piutang_penjualan_detail';
}
