<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apoteker extends Model
{
  protected $table = 'apoteker';
}
