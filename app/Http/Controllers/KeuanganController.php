<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\KelompokAkun;
use App\PosAkunKeuangan;
use App\AkunKeuangan;
use App\JurnalSingkat;
use App\JurnalKeuangan;
use Response;
use DB;
use Auth;

class KeuanganController extends Controller
{
    public function indexAkun()
    {
      $akun = DB::table('akun_keuangan')
              ->join('pos_akun_keuangan','akun_keuangan.kode_pos_akun','pos_akun_keuangan.kode_pos_akun')
              ->join('kelompok_akun_keuangan','pos_akun_keuangan.kode_kelompok_akun','kelompok_akun_keuangan.kode_kelompok_akun')
              ->get();
      return view('keuangan.akun.index',['akun'=>$akun]);
    }
    public function createAkun()
    {
      $kelompokakun = KelompokAkun::all();
      return view('keuangan.akun.tambah',['kelompokakun'=>$kelompokakun]);
    }
    public function deleteAkun($id)
    {
      $kelompokakun = AkunKeuangan::where('kode_akun', $id)->delete();
      return redirect('akun')->with('message', '<div class="alert alert-success">Berhasil Menghapus Data</div>');
    }
    public function ubahAkun($id){
      $kelompokakun = KelompokAkun::all();    
      $akun = AkunKeuangan::where('kode_akun',$id)
                              ->join('pos_akun_keuangan', 'pos_akun_keuangan.kode_pos_akun', '=', 'akun_keuangan.kode_pos_akun')
                              ->first();
      $posakun = PosAkunKeuangan::where('kode_kelompok_akun', $akun->kode_kelompok_akun)->get();

      return view('keuangan.akun.ubah',['akun'=>$akun, 'kelompokakun'=>$kelompokakun, 'posakun' => $posakun]);      
    }
    public function prosesAkun(Request $request){
      # code...
      $nama = $request->nama_akun;
      $kode_pos = $request->posakun;
      $saldo_normal = $request->saldo_normal;

      AkunKeuangan::where('kode_akun', $request->kode_akun)->update([
          'nama_akun' => $nama,
          'kode_pos_akun' => $kode_pos,
          'saldo_normal' => $saldo_normal
      ]);
      
      return redirect('akun')->with('message', '<div class="alert alert-success">Berhasil Mengubah Data</div>');      
    }
    public function postAkun(Request $request)
    {
      # code...
      $nama = $request->nama_akun;
      $kode_pos = $request->posakun;
      $saldo_normal = $request->saldo_normal;
      $kodeakun = $request->kodeakun;
      if (isset($request->postakun2)) {
        # code...
      }else{
        $kode_akun = $kode_pos."".$kodeakun;
        $akun = new AkunKeuangan;
        $akun->nama_akun = $nama;
        $akun->kode_akun = $kode_akun;
        $akun->kode_pos_akun = $kode_pos;
        $akun->saldo_normal = $saldo_normal;
        $akun->save();
        return redirect('akun')->with('message', '<div class="alert alert-success">Berhasil Menambah Data</div>');
      }
    }
    public function getPosAkun(Request $request)
    {
      $id = $request->input('id');
      $posakun = PosAkunKeuangan::where('kode_kelompok_akun',$id)->get();
      return Response::make($posakun);
    }
    public function getShortJurnal(Request $request)
    {
      $id = $request->input('id');
      $posakun = JurnalSingkat::where('id_jurnal',$id)->first();
      $akunkredit = AkunKeuangan::where('kode_akun', $posakun->kode_akun_kredit)->get();
      $akundebit = AkunKeuangan::where('kode_akun', $posakun->kode_akun_debit)->get();      
      return Response::make(['posakun' => $posakun, 'debit' => $akundebit, 'kredit' => $akunkredit]);
    }
    public function getsaldo(Request $request)
    {
      if ($request->input('kode') == 0) {
        # code...
        $id = $request->input('id');
        $posakun = KelompokAkun::where('kode_kelompok_akun',$id)->first();
        return Response::make($posakun);
      }

    }
    //jurnal singkat
    public function indexJurnalSingkat()
    {
      $jurnal = JurnalSingkat::all();
      return view('keuangan.jurnal-singkat.index',['jurnal'=>$jurnal]);
    }
    public function tambahJurnalSingkat()
    {
      $debit = AkunKeuangan::where('saldo_normal', 'Debit')->orderBy('kode_akun','ASC')->get();
      $kredit = AkunKeuangan::where('saldo_normal', 'Kredit')->orderBy('kode_akun','ASC')->get();      

      //print_r($akunb);
      return view('keuangan.jurnal-singkat.tambah',['debit'=>$debit, 'kredit'=>$kredit]);
    }
    public function postJurnalSingkat(Request $request)
    {
      $jurnal = new JurnalSingkat;
      $jurnal->kode_akun_debit = $request->akun_debit;
      $jurnal->kode_akun_kredit = $request->akun_kredit;
      $jurnal->jenis = $request->jenis;
      $jurnal->deskripsi = $request->deskripsi;
      $jurnal->save();

      //return view('keuangan.jurnal-singkat.tambah',['akunk'=>$akunk]);
      return redirect('jurnal-singkat')->with('message', '<div class="alert alert-success">Berhasil Menambah Data</div>');
    }
    public function hapusJurnalSingkat($id){
      JurnalSingkat::where('id_jurnal', $id)->delete();

      return redirect('jurnal-singkat');
    }
    //jurnal keuangan
    public function indexJurnalKeuangan(Request $req)
    {

      $start_date = $req->start;
      $end_date = $req->end;

      $query = JurnalKeuangan::join('users', 'users.id', '=', 'created_by');      

      if($start_date != null){
        $query->where('tanggal', '>=', $start_date);        
      }

      if($end_date != null){
        $query->where('tanggal', '<=', $end_date);        
      }

      $data = $query->get();

      return view('keuangan.jurnal-keuangan.index', ['data' => $data, 'start' => $start_date, 'end' => $end_date]);
    }
    public function tambahJurnalKeuangan()
    {
      $jurnal = JurnalSingkat::all();

      return view('keuangan.jurnal-keuangan.tambah', ['jurnal' => $jurnal]);
    }
    public function hapusJurnalKeuangan($id){
      JurnalKeuangan::where('kd_jurnal', $id)->delete();

      return redirect('jurnal-keuangan');
    }
    public function prosesJurnalKeuangan(Request $req){
      $jurnal = new JurnalKeuangan;
      $jurnal->tanggal = $req->tanggal;
      $jurnal->kd_jurnal_singkat = $req->jurnal_id;
      $jurnal->nobukti = $req->no_bukti;
      $jurnal->deskripsi = $req->deskripsi;
      $jurnal->nominal = $req->nominal;
      $jurnal->created_by = Auth::id();
      $jurnal->save();

      return redirect('jurnal-keuangan');
    }
    public function indexLapJurnalKeuangan(Request $req)
    {
      $start_date = $req->start;
      $end_date = $req->end;

      $query = JurnalKeuangan::join('jurnal_singkat', 'jurnal_singkat.id_jurnal', '=', 'kd_jurnal_singkat');      

      if($start_date != null){
        $query->where('tanggal', '>=', $start_date);        
      }

      if($end_date != null){
        $query->where('tanggal', '<=', $end_date);        
      }

      $data = $query->get();

      $results = array();
      foreach ($data as $key => $jurnal) {
        $debit = AkunKeuangan::where('kode_akun', $jurnal->kode_akun_debit)->first();
        if($debit){
          $result = new \stdClass();
          $result->tanggal = $jurnal->tanggal;
          $result->nobukti = $jurnal->nobukti;
          $result->kode_akun = $debit->kode_akun;
          $result->nama_akun = $debit->nama_akun;          
          $result->nominal_debit = $jurnal->nominal;
          $result->nominal_kredit = 0;                  
          array_push($results, $result);        
        }

        $credit = AkunKeuangan::where('kode_akun', $jurnal->kode_akun_kredit)->first();
        if($credit){
          $result = new \stdClass();
          $result->tanggal = $jurnal->tanggal;
          $result->nobukti = $jurnal->nobukti;
          $result->kode_akun = $credit->kode_akun;
          $result->nama_akun = $credit->nama_akun;          
          $result->nominal_debit = 0;
          $result->nominal_kredit = $jurnal->nominal;                  
          array_push($results, $result);       
        }
      }

      return view('keuangan.laporan-jurnal-keuangan.index',  ['data' => $results, 'start' => $start_date, 'end' => $end_date]);
    }
    public function indexLapBukuBesar(Request $req)
    {
      $start_date = $req->start;
      $end_date = $req->end;

      $query = JurnalKeuangan::join('jurnal_singkat', 'jurnal_singkat.id_jurnal', '=', 'kd_jurnal_singkat');      
      
      if($start_date != null){
        $query->where('tanggal', '>=', $start_date);        
      }

      if($end_date != null){
        $query->where('tanggal', '<=', $end_date);        
      }

      $data = $query->get();

      $results = array();
      foreach ($data as $key => $jurnal) {
          $debit = AkunKeuangan::where('kode_akun', $jurnal->kode_akun_debit)
          ->join('pos_akun_keuangan', 'akun_keuangan.kode_pos_akun', '=', 'pos_akun_keuangan.kode_pos_akun')
          ->join('kelompok_akun_keuangan', 'kelompok_akun_keuangan.kode_kelompok_akun', '=', 'pos_akun_keuangan.kode_kelompok_akun')
          ->first();
          
          if($debit){
            $result = new \stdClass();
            $result->kelompok_akun = $debit->nama_kelompok_akun;
            $result->akun_pos = $debit->nama_pos_akun;
            $result->nobukti = $debit->nobukti;
            $result->deskripsi = $jurnal->deskripsi;
            $result->tanggal = $jurnal->tanggal;
            $result->nobukti = $jurnal->nobukti;
            $result->kode_akun = $debit->kode_akun;
            $result->nama_akun = $debit->nama_akun;          
            $result->nominal_debit = $jurnal->nominal;
            $result->nominal_kredit = 0;                  
            array_push($results, $result);        
          }

          $credit = AkunKeuangan::where('kode_akun', $jurnal->kode_akun_kredit)
                  ->join('pos_akun_keuangan', 'akun_keuangan.kode_pos_akun', '=', 'pos_akun_keuangan.kode_pos_akun')
                  ->join('kelompok_akun_keuangan', 'kelompok_akun_keuangan.kode_kelompok_akun', '=', 'pos_akun_keuangan.kode_kelompok_akun')
                  ->first();

          if($credit){
            $result = new \stdClass();
            $result->kelompok_akun = $credit->nama_kelompok_akun;
            $result->akun_pos = $credit->nama_pos_akun;
            $result->nobukti = $credit->nobukti;
            $result->deskripsi = $jurnal->deskripsi;
            $result->tanggal = $jurnal->tanggal;
            $result->nobukti = $jurnal->nobukti;
            $result->kode_akun = $credit->kode_akun;
            $result->nama_akun = $credit->nama_akun;       
            $result->nominal_debit = 0;
            $result->nominal_kredit = $jurnal->nominal;

            array_push($results, $result);
          }                 
      }

      return view('keuangan.laporan-buku-besar.index', ['data' => $results, 'start' => $start_date, 'end' => $end_date]);
    }
    public function indexLapLaba()
    {
      return view('keuangan.laporan-laba.index');
    }
    public function indexLapPenambahanModal()
    {
      return view('keuangan.laporan-penambahan-modal.index');
    }
    public function indexLapPajak()
    {
      return view('keuangan.laporan-pajak.index');
    }
    public function indexLapNeraca()
    {
      return view('keuangan.laporan-neraca.index');
    }
    public function indexLapTipeAkun()
    {
      return view('keuangan.laporan-tipe-akun.index');
    }
    public function indexLapDetailAkun()
    {
      return view('keuangan.laporan-detail-akun.index');
    }
    public function indexLapKeuangan(Request $req){
      $start_date = $req->start;
      $end_date = $req->end;
      
      $query = DB::table('akun_keuangan')
          ->join('pos_akun_keuangan','akun_keuangan.kode_pos_akun','pos_akun_keuangan.kode_pos_akun')
          ->join('kelompok_akun_keuangan','pos_akun_keuangan.kode_kelompok_akun','kelompok_akun_keuangan.kode_kelompok_akun');      
      
      if($start_date != null){
        $query->where('created_at', '>=', $start_date);        
      }

      if($end_date != null){
        $query->where('created_at', '<=', $end_date);        
      }

      $data = $query->get();

      return view('keuangan.laporan-keuangan.index',['akun'=>$data, 'start' => $start_date, 'end' => $end_date]);
      
    }

}
