<?php

namespace App\Http\Controllers;

use App\Supplier;
use App\Gol_obat;
use App\Kat_obat;
use App\Obat;
use App\Apoteker;
use App\Pasien;
use App\Sat_obat;
use App\Racikan;
use App\Identitas;
use App\Lok_obat;
use App\Gudang;
use App\Pabrik;
use App\Harga_jual;
use App\Diskon_harga;
use App\Satuan;
use App\HistoryHargaObat;
use Log;
use PDF;
use Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class LaporanapotekController extends Controller
{
    function laporan_data_pabrik(){

      $data = Pabrik::all();

      return view('laporan_master_apotek.laporan_pabrik.data_pabrik',['data' => $data]);
    }

    function cetak_data_pabrik(){
      $data = Pabrik::all();
      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_master_apotek.laporan_pabrik.print_data_pabrik', ['data' => $data, 'identitas' => $ident]);
      // return view('laporan_master_apotek.laporan_pabrik.print_data_pabrik', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-pabrik.pdf');
    }

    function export_data_pabrik(){
      $excel = Excel::create('Laporan Pabrik', function($excel) {
        
            // Set the title
          $excel->setTitle('Laporan Pabrik');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('Laporan Data Pabrik');

          $excel->sheet('Sheet1', function($sheet) {

              $data = Pabrik::all();
              $ident = Identitas::first();
              
              $sheet->loadView('laporan_master_apotek.laporan_pabrik.export_data_pabrik', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:H'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:H1');
              $sheet->mergeCells('A2:H2');
              $sheet->mergeCells('A3:H3');
              $sheet->mergeCells('A4:H4');
              $sheet->mergeCells('A5:H5');
              $sheet->getStyle('A1:H4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    function laporan_data_supplier(){

      $data = Supplier::all();

      return view('laporan_master_apotek.laporan_supplier.data_supplier',['data' => $data]);
    }

    function cetak_data_supplier(){
      $data = Supplier::all();
      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_master_apotek.laporan_supplier.print_data_supplier', ['data' => $data, 'identitas' => $ident]);
      // return view('laporan_master_apotek.laporan_pabrik.print_data_pabrik', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-supplier.pdf');
    }

    function export_data_supplier(){
      $excel = Excel::create('Laporan Supplier', function($excel) {
        
            // Set the title
          $excel->setTitle('Laporan Supplier');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('Laporan Data Supplier');

          $excel->sheet('Sheet1', function($sheet) {

              $data = Supplier::all();
              $ident = Identitas::first();
              
              $sheet->loadView('laporan_master_apotek.laporan_supplier.export_data_supplier', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:G'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:G1');
              $sheet->mergeCells('A2:G2');
              $sheet->mergeCells('A3:G3');
              $sheet->mergeCells('A4:G4');
              $sheet->mergeCells('A5:G5');
              $sheet->getStyle('A1:G4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    function laporan_data_gudang(){

      $data = Gudang::all();

      return view('laporan_master_apotek.laporan_gudang.data_gudang',['data' => $data]);
    }

    function cetak_data_gudang(){
      $data = Gudang::all();
      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_master_apotek.laporan_gudang.print_data_gudang', ['data' => $data, 'identitas' => $ident]);
      // return view('laporan_master_apotek.laporan_pabrik.print_data_pabrik', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-gudang.pdf');
    }

    function export_data_gudang(){
      $excel = Excel::create('Laporan Gudang', function($excel) {
        
            // Set the title
          $excel->setTitle('Laporan Gudang');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('Laporan Data Gudang');

          $excel->sheet('Sheet1', function($sheet) {

              $data = Gudang::all();
              $ident = Identitas::first();
              
              $sheet->loadView('laporan_master_apotek.laporan_gudang.export_data_gudang', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:G'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:G1');
              $sheet->mergeCells('A2:G2');
              $sheet->mergeCells('A3:G3');
              $sheet->mergeCells('A4:G4');
              $sheet->mergeCells('A5:G5');
              $sheet->getStyle('A1:G4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    function laporan_data_apoteker(){

      $data = Apoteker::all();

      return view('laporan_master_apotek.laporan_apoteker.data_apoteker',['data' => $data]);
    }

    function cetak_data_apoteker(){
      $data = Apoteker::all();
      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_master_apotek.laporan_apoteker.print_data_apoteker', ['data' => $data, 'identitas' => $ident]);
      // return view('laporan_master_apotek.laporan_pabrik.print_data_pabrik', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-apoteker.pdf');
    }

    function export_data_apoteker(){
      $excel = Excel::create('Laporan Apoteker', function($excel) {
        
            // Set the title
          $excel->setTitle('Laporan Apoteker');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('Laporan Data Apoteker');

          $excel->sheet('Sheet1', function($sheet) {

              $data = Apoteker::all();
              $ident = Identitas::first();
              
              $sheet->loadView('laporan_master_apotek.laporan_apoteker.export_data_apoteker', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:I'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:I1');
              $sheet->mergeCells('A2:I2');
              $sheet->mergeCells('A3:I3');
              $sheet->mergeCells('A4:I4');
              $sheet->mergeCells('A5:I5');
              $sheet->getStyle('A1:I4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    function laporan_data_pasien(){

      $data = Pasien::all();

      return view('laporan_master_apotek.laporan_pasien.data_pasien',['data' => $data]);
    }

    function cetak_data_pasien(){
      $data = Pasien::all();
      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_master_apotek.laporan_pasien.print_data_pasien', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-pasien.pdf');
    }

    function export_data_pasien(){
      $excel = Excel::create('Laporan Pasien', function($excel) {
        
            // Set the title
          $excel->setTitle('Laporan Pasien');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('Laporan Data Pasien');

          $excel->sheet('Sheet1', function($sheet) {

              $data = Pasien::all();
              $ident = Identitas::first();
              
              $sheet->loadView('laporan_master_apotek.laporan_pasien.export_data_pasien', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:M'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:M1');
              $sheet->mergeCells('A2:M2');
              $sheet->mergeCells('A3:M3');
              $sheet->mergeCells('A4:M4');
              $sheet->mergeCells('A5:M5');
              $sheet->getStyle('A1:M4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    function laporan_data_obat_expired(){
      $tanggal = date('Y-m-d');

      $data = DB::table('obat')
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->orderBy('satuan.tgl_expired', 'desc')
                  ->get();

      return view('laporan_master_apotek.laporan_obat_expired.data_obat_expired',['data' => $data]);
    }

    function cetak_data_obat_expired(){
      $data = DB::table('obat')
            ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
            ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
            ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
            ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
            ->orderBy('satuan.tgl_expired', 'desc')
            ->get();
      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_master_apotek.laporan_obat_expired.print_data_obat_expired', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-obat-expired.pdf');
    }

    function export_data_obat_expired(){
      $excel = Excel::create('Laporan Obat Expired', function($excel) {
        
            // Set the title
          $excel->setTitle('Laporan Obat Expired');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('Laporan Data Obat Expired');

          $excel->sheet('Sheet1', function($sheet) {

              $data = DB::table('obat')
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->orderBy('satuan.tgl_expired', 'desc')
                  ->get();
              $ident = Identitas::first();
              
              $sheet->loadView('laporan_master_apotek.laporan_obat_expired.export_data_obat_expired', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:G'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:G1');
              $sheet->mergeCells('A2:G2');
              $sheet->mergeCells('A3:G3');
              $sheet->mergeCells('A4:G4');
              $sheet->mergeCells('A5:G5');
              $sheet->getStyle('A1:G4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    function laporan_data_obat_habis(){

      $data = DB::table('obat')
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->whereRaw('satuan.stok < obat.minimal_stok')
                  ->get();

      //Log::info(print_r($data, true));

      return view('laporan_master_apotek.laporan_obat_habis.data_obat_habis',['data' => $data]);
    }

    function cetak_data_obat_habis(){
      $data = DB::table('obat')
            ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
            ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
            ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
            ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
            ->whereRaw('satuan.stok < obat.minimal_stok')
            ->get();

      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_master_apotek.laporan_obat_habis.print_data_obat_habis', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-obat-habis.pdf');
    }

    function export_data_obat_habis(){
      $excel = Excel::create('Laporan Obat Habis', function($excel) {
        
            // Set the title
          $excel->setTitle('Laporan Obat Habis');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('Laporan Data Obat Habis');

          $excel->sheet('Sheet1', function($sheet) {

              $data = DB::table('obat')
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->whereRaw('satuan.stok < obat.minimal_stok')
                  ->get();

              $ident = Identitas::first();
              
              $sheet->loadView('laporan_master_apotek.laporan_obat_habis.export_data_obat_habis', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:H'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:H1');
              $sheet->mergeCells('A2:H2');
              $sheet->mergeCells('A3:H3');
              $sheet->mergeCells('A4:H4');
              $sheet->mergeCells('A5:H5');
              $sheet->getStyle('A1:H4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    function laporan_data_obat_konsinyasi(){

      $data = DB::table('obat')
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->whereRaw('obat.jenis_obat = "konsinyasi"')
                  ->get();

      //Log::info(print_r($data, true));

      return view('laporan_master_apotek.laporan_obat_konsinyasi.data_obat_konsinyasi',['data' => $data]);
    }

    function cetak_data_obat_konsinyasi(){
      $data = DB::table('obat')
            ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
            ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
            ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
            ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
            ->whereRaw('obat.jenis_obat = "konsinyasi"')
            ->get();

      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_master_apotek.laporan_obat_konsinyasi.print_data_obat_konsinyasi', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-obat-konsinyasi.pdf');
    }

    function export_data_obat_konsinyasi(){
      $excel = Excel::create('Laporan Obat konsinyasi', function($excel) {
        
            // Set the title
          $excel->setTitle('Laporan Obat konsinyasi');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('Laporan Data Obat konsinyasi');

          $excel->sheet('Sheet1', function($sheet) {

              $data = DB::table('obat')
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->whereRaw('obat.jenis_obat = "konsinyasi"')
                  ->get();

              $ident = Identitas::first();
              
              $sheet->loadView('laporan_master_apotek.laporan_obat_konsinyasi.export_data_obat_konsinyasi', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:G'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:G1');
              $sheet->mergeCells('A2:G2');
              $sheet->mergeCells('A3:G3');
              $sheet->mergeCells('A4:G4');
              $sheet->mergeCells('A5:G5');
              $sheet->getStyle('A1:G4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    function laporan_data_stok_harga_obat(){

      $data = DB::table('obat')
                  ->join('opname', 'obat.kd_obat', '=', 'opname.kode_obat')
                  ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->select('obat.*', 'satuan.*', 'opname.gudang', 'kategori_obat.nama_kat_obat', 'golongan_obat.nama_gol_obat')
                  ->get();

      //Log::info(print_r($data, true));

      return view('laporan_master_apotek.laporan_stok_harga_obat.data_stok_harga_obat',['data' => $data]);
    }

    function cetak_data_stok_harga_obat(){
      $data = DB::table('obat')
              ->join('opname', 'obat.kd_obat', '=', 'opname.kode_obat')
              ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
              ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
              ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
              ->select('obat.*', 'satuan.*', 'opname.gudang', 'kategori_obat.nama_kat_obat', 'golongan_obat.nama_gol_obat')
              ->get();

      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_master_apotek.laporan_stok_harga_obat.print_data_stok_harga_obat', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-stok-harga-obat.pdf');
    }

    function export_data_stok_harga_obat(){
      $excel = Excel::create('Laporan Stok dan Harga Obat', function($excel) {
        
            // Set the title
          $excel->setTitle('Laporan Stok dan Harga Obat');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('Laporan Data Stok dan Harga Obat');

          $excel->sheet('Sheet1', function($sheet) {

              $data = DB::table('obat')
                  ->join('opname', 'obat.kd_obat', '=', 'opname.kode_obat')
                  ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->select('obat.*', 'satuan.*', 'opname.gudang', 'kategori_obat.nama_kat_obat', 'golongan_obat.nama_gol_obat')
                  ->get();

              $ident = Identitas::first();
              
              $sheet->loadView('laporan_master_apotek.laporan_stok_harga_obat.export_data_stok_harga_obat', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:V'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:V1');
              $sheet->mergeCells('A2:V2');
              $sheet->mergeCells('A3:V3');
              $sheet->mergeCells('A4:V4');
              $sheet->mergeCells('A5:V5');
              $sheet->getStyle('A1:V4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    function laporan_data_perubahan_harga_obat(){

      $data = HistoryHargaObat::join('obat', 'obat.kd_obat', '=', 'history_harga_obat.kd_obat')
              ->get();

      return view('laporan_master_apotek.laporan_perubahan_harga_obat.data_perubahan_harga_obat',['data' => $data]);      
    }

    function cetak_data_perubahan_harga_obat(){
      $data = HistoryHargaObat::join('obat', 'obat.kd_obat', '=', 'history_harga_obat.kd_obat')
              ->get();

      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_master_apotek.laporan_perubahan_harga_obat.print_data_perubahan_harga_obat', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-perubahan-harga-obat.pdf');
    }

    function export_data_perubahan_harga_obat(){
      $excel = Excel::create('Laporan Perubahan Harga Obat', function($excel) {
        
            // Set the title
          $excel->setTitle('Laporan Perubahan Harga Obat');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('Laporan Data Perubahan Harga Obat');

          $excel->sheet('Sheet1', function($sheet) {

              $data = HistoryHargaObat::join('obat', 'obat.kd_obat', '=', 'history_harga_obat.kd_obat')
              ->get();

              $ident = Identitas::first();
              
              $sheet->loadView('laporan_master_apotek.laporan_perubahan_harga_obat.export_data_perubahan_harga_obat', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:L'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:L1');
              $sheet->mergeCells('A2:L2');
              $sheet->mergeCells('A3:L3');
              $sheet->mergeCells('A4:L4');
              $sheet->mergeCells('A5:L5');
              $sheet->getStyle('A1:L4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    function laporan_data_stok_obat(){

      $data = DB::table('opname')
                  ->join('obat', 'opname.kode_obat', '=', 'obat.kd_obat')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->join('satuan', 'satuan.kd_obat', '=', 'obat.kd_obat')
                  ->orderBy('opname.gudang', 'desc')
                  ->get();

      //Log::info(print_r($data, true));

      return view('laporan_master_apotek.laporan_stok_obat.data_stok_obat',['data' => $data]);
    }

    function cetak_data_stok_obat(){
      $data = DB::table('opname')
              ->join('obat', 'opname.kode_obat', '=', 'obat.kd_obat')
              ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
              ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
              ->join('satuan', 'satuan.kd_obat', '=', 'obat.kd_obat')
              ->orderBy('opname.gudang', 'desc')
              ->get();

      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_master_apotek.laporan_stok_obat.print_data_stok_obat', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-stok-obat.pdf');
    }

    function export_data_stok_obat(){
      $excel = Excel::create('Laporan Stok Obat', function($excel) {
        
            // Set the title
          $excel->setTitle('Laporan Stok Obat');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('Laporan Data Stok Obat');

          $excel->sheet('Sheet1', function($sheet) {

              $data = DB::table('opname')
                  ->join('obat', 'opname.kode_obat', '=', 'obat.kd_obat')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->join('satuan', 'satuan.kd_obat', '=', 'obat.kd_obat')
                  ->orderBy('opname.gudang', 'desc')
                  ->get();

              $ident = Identitas::first();
              
              $sheet->loadView('laporan_master_apotek.laporan_stok_obat.export_data_stok_obat', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:K'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:K1');
              $sheet->mergeCells('A2:K2');
              $sheet->mergeCells('A3:K3');
              $sheet->mergeCells('A4:K4');
              $sheet->mergeCells('A5:K5');
              $sheet->getStyle('A1:K4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    function laporan_data_mutasi_obat(Request $req){
      $start_date = $req->start;
      $end_date = $req->end;

      $query = DB::table('mutasi_obats')
      ->join('obat', 'obat.kd_obat', '=', 'mutasi_obats.kd_obat')
      ->join('opname', 'obat.kd_obat', '=', 'opname.kode_obat')
      ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
      ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
      ->select('mutasi_obats.*','obat.nama_obat', 'opname.gudang', 'kategori_obat.nama_kat_obat', 'golongan_obat.nama_gol_obat');

      if($start_date != null){
        $query->where('date', '>=', $start_date);        
      }

      if($end_date != null){
        $query->where('date', '<=', $end_date);        
      }

      $data = $query->get();

      return view('laporan_master_apotek.laporan_mutasi_obat.data_mutasi_obat', ['data'=>$data, 'start'=>$start_date, 'end'=>$end_date]);
    }

    function cetak_data_mutasi_obat($start_date, $end_date){
      
      $query = DB::table('mutasi_obats')
            ->join('obat', 'obat.kd_obat', '=', 'mutasi_obats.kd_obat')
            ->join('opname', 'obat.kd_obat', '=', 'opname.kode_obat')
            ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
            ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
            ->select('mutasi_obats.*', 'opname.gudang', 'kategori_obat.nama_kat_obat', 'golongan_obat.nama_gol_obat');

      if($start_date != null){
        $query->where('date', '>=', $start_date);        
      }

      if($end_date != null){
        $query->where('date', '<=', $end_date);        
      }
      $data = $query->get();

      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_master_apotek.laporan_mutasi_obat.print_data_mutasi_obat', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-mutasi-obat.pdf');
    }

    function export_data_mutasi_obat($start_date, $end_date){

      $excel = Excel::create('Laporan Mutasi Obat', function($excel) {
        
            // Set the title
          $excel->setTitle('Laporan Mutasi Obat');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('Laporan Data Mutasi Obat');

          $excel->sheet('Sheet1', function($sheet) {

              $query = DB::table('mutasi_obats')
                  ->join('obat', 'obat.kd_obat', '=', 'mutasi_obats.kd_obat')
                  ->join('opname', 'obat.kd_obat', '=', 'opname.kode_obat')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->select('mutasi_obats.*', 'opname.gudang', 'kategori_obat.nama_kat_obat', 'golongan_obat.nama_gol_obat');

              if($start_date != null){
                $query->where('date', '>=', $start_date);        
              }

              if($end_date != null){
                $query->where('date', '<=', $end_date);        
              }

              $data = $query->get();

              $ident = Identitas::first();
              
              $sheet->loadView('laporan_master_apotek.laporan_mutasi_obat.export_data_mutasi_obat', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:M'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:M1');
              $sheet->mergeCells('A2:M2');
              $sheet->mergeCells('A3:M3');
              $sheet->mergeCells('A4:M4');
              $sheet->mergeCells('A5:M5');
              $sheet->getStyle('A1:M4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    
}
