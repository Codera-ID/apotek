<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     function index(){
       $admin = DB::table('users')->get();

       return view('index',['admin' => $admin]);
     }

     function data(){

       $admin = DB::table('users')->get();

       echo count($admin);

     }

    function get_data_pembelian_obat(){
        $kd_supplier = DB::table('supplier')
                    ->select('kd_supplier')
                    ->get();

        $data = DB::table('pembelian_obat')
                ->selectRaw('year(tgl_faktur) as penjualan_tahun, MONTH(tgl_faktur) as bulan, sum(total) as total_pembelian, kd_supplier')
                ->where('kd_supplier', $kd_supplier[2]->kd_supplier)
                ->groupBy(DB::RAW('year(tgl_faktur), month(tgl_faktur)'))
                ->orderBy(DB::RAW('year(tgl_faktur), MONTH(tgl_faktur)'))
                ->get();

        //Log::info(print_r($data, true));

        return $data;
     }

     function get_data_penjualan_obat(){
        $data = DB::table('penjualan_obat')
                ->selectRaw('year(tgl_faktur) as penjualan_tahun, MONTH(tgl_faktur) as bulan, sum(total) as total_penjualan')
                ->groupBy(DB::RAW('year(tgl_faktur), monthname(tgl_faktur)'))
                ->orderBy(DB::RAW('year(tgl_faktur), MONTHNAME(tgl_faktur)'))
                ->get();

        Log::info(print_r($data, true));

        return $data;
    }

    function get_data_obat_terlaris(){
        
        $monthname = 'December';
        $data = DB::table('penjualan_obat')
                  ->selectRaw('sum(penjualan_detail.jmlh) as jumlah_obat, obat.nama_obat, monthname(penjualan_obat.tgl_faktur) as bulan')
                  ->join('penjualan_detail', 'penjualan_obat.no_faktur', '=', 'penjualan_detail.no_faktur')
                  ->join('obat', 'penjualan_detail.kd_obat', '=', 'obat.kd_obat')
                  ->whereRaw('monthname(penjualan_obat.tgl_faktur) LIKE "%'.$monthname.'%"')
                  ->groupBy(DB::RAW('obat.kd_obat, obat.nama_obat, year(penjualan_obat.tgl_faktur), month(penjualan_obat.tgl_faktur)'))
                  ->orderBy(DB::RAW('sum(penjualan_detail.jmlh)'))
                  ->get();

        // Log::info(print_r($data, true));

        return $data;
    }
}
