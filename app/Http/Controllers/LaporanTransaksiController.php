<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PO;
use App\Identitas;
use PDF;
use Excel;
use Illuminate\Support\Facades\DB;

class LaporanTransaksiController extends Controller
{
    public function laporan_data_purchase_order(Request $req)
    {
      $jenis_invoice = 1;
      $query = DB::table('purchaes_order')
              ->join('po_detail', 'po_detail.no_po', '=', 'purchaes_order.no_po')
              ->join('supplier', 'supplier.kd_supplier', '=', 'purchaes_order.kd_supplier');

      $start_date = $req->start;
      $end_date = $req->end;
      //$jenis_invoice = $req->jenis_invoice;

      if($start_date != null){
        $query->where('tgl_po', '>=', $start_date);        
      }

      if($end_date != null){
        $query->where('tgl_po', '<=', $end_date);        
      }

      if($jenis_invoice == null){
        $jenis_invoice = 'cetak';
      }

      //$query->where('jenis_invoice', '=', $jenis_invoice);        
      $data = $query->get();

      return view('laporan_transaksi_apotek.data_po.data_po', ['data' => $data, 'start_date' => $start_date, 'end_date' => $end_date]);
    }

    public function cetak_purchase_order(){
      $data = DB::table('purchaes_order')
              ->join('po_detail', 'po_detail.no_po', '=', 'purchaes_order.no_po')
              ->join('supplier', 'supplier.kd_supplier', '=', 'purchaes_order.kd_supplier')
              ->get();

      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_transaksi_apotek.data_po.print_data_po', ['data' => $data, 'identitas' => $ident]);
      // return view('laporan_master_apotek.laporan_pabrik.print_data_pabrik', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-purchase-order.pdf');
    }

    public function cetak_detail_purchase_order(){
      $data = DB::table('purchaes_order')
              ->join('po_detail', 'po_detail.no_po', '=', 'purchaes_order.no_po')
              ->join('supplier', 'supplier.kd_supplier', '=', 'purchaes_order.kd_supplier')
              ->get();

      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_transaksi_apotek.data_po.print_detail_data_po', ['data' => $data, 'identitas' => $ident]);
      // return view('laporan_master_apotek.laporan_pabrik.print_data_pabrik', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-detail-purchase-order.pdf');
    }
    public function cetak_faktur_purchase_order($id){
      $data = DB::table('purchaes_order')
              ->join('po_detail', 'po_detail.no_po', '=', 'purchaes_order.no_po')
              ->join('supplier', 'supplier.kd_supplier', '=', 'purchaes_order.kd_supplier')
              ->where('purchaes_order.no_po', '=', $id)
              ->get();

      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_transaksi_apotek.data_po.print_faktur_data_po', ['data' => $data, 'identitas' => $ident, 'id' => $id]);
      // return view('laporan_master_apotek.laporan_pabrik.print_data_pabrik', ['data' => $data, 'identitas' => $ident]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-faktur-purchase-order-'.$id.'.pdf');
    }

    public function export_purchase_order(){
      $excel = Excel::create('Laporan Purchase Order', function($excel) {
        
            // Set the title
          $excel->setTitle('LAPORAN REKAP PURCHASE ORDER');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('LAPORAN REKAP PURCHASE ORDER');

          $excel->sheet('Sheet1', function($sheet) {

              $data = DB::table('purchaes_order')
              ->join('po_detail', 'po_detail.no_po', '=', 'purchaes_order.no_po')
              ->join('supplier', 'supplier.kd_supplier', '=', 'purchaes_order.kd_supplier')
              ->get();
              $ident = Identitas::first();
              
              $sheet->loadView('laporan_transaksi_apotek.data_po.export_data_po', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:H'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:H1');
              $sheet->mergeCells('A2:H2');
              $sheet->mergeCells('A3:H3');
              $sheet->mergeCells('A4:H4');
              $sheet->mergeCells('A5:H5');
              $sheet->getStyle('A1:H4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    public function export_detail_purchase_order(){
      $excel = Excel::create('Laporan Detail Purchase Order', function($excel) {
        
            // Set the title
          $excel->setTitle('LAPORAN REKAP DETAIL PURCHASE ORDER');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('LAPORAN REKAP DETAIL PURCHASE ORDER');

          $excel->sheet('Sheet1', function($sheet) {

              $data = DB::table('purchaes_order')
              ->join('po_detail', 'po_detail.no_po', '=', 'purchaes_order.no_po')
              ->join('supplier', 'supplier.kd_supplier', '=', 'purchaes_order.kd_supplier')
              ->get();
              $ident = Identitas::first();
              
              $sheet->loadView('laporan_transaksi_apotek.data_po.export_detail_data_po', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 10;
              $border = 'A6:H'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:H1');
              $sheet->mergeCells('A2:H2');
              $sheet->mergeCells('A3:H3');
              $sheet->mergeCells('A4:H4');
              $sheet->mergeCells('A5:H5');
              $sheet->getStyle('A1:H4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    public function laporan_obat_purchase_order($id){

      $data = DB::table('po_detail')
                  ->join('obat', 'obat.kd_obat', '=' , 'po_detail.kd_obat')
                  ->join('satuan', 'satuan.kd_obat', '=', 'obat.kd_obat')
                  ->join('satuan_obat', 'satuan_obat.kd_sat_obat', '=', 'satuan.kd_sat_obat')
                  ->get();

      return view('laporan_transaksi_apotek.data_po.detail_data_po', ['obat' => $data, 'no_po' => $id]);                  
    }

    public function laporan_data_pembelian_obat(Request $req)
    {
      $query = DB::table('pembelian_obat')
                  ->join('supplier', 'pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->join('gudang', 'pembelian_obat.kd_gudang', '=', 'gudang.kd_gudang')
                  ->select('pembelian_obat.*', 'supplier.nama_supplier', 'gudang.nama_gudang')
                  ->orderBy('pembelian_obat.created_at', 'desc');

      $start_date = $req->start;
      $end_date = $req->end;

      if($start_date == null){
        $start_date = date('Y-m-d');
      }

      if($end_date == null){
        $end_date = date('Y-m-d');
      }
      
      $query->where('tgl_faktur', '>=', $start_date); 
      $query->where('tgl_faktur', '<=', $end_date); 
      $data = $query->get();

      return view('laporan_transaksi_apotek.data_pembelian.data_pembelian', ['data' => $data, 'start_date' => $start_date, 'end_date' => $end_date]);
    }

    public function cetak_pembelian_obat($start, $end){
      $query = DB::table('pembelian_obat')
                  ->join('supplier', 'pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->join('gudang', 'pembelian_obat.kd_gudang', '=', 'gudang.kd_gudang')
                  ->select('pembelian_obat.*', 'supplier.nama_supplier', 'gudang.nama_gudang')
                  ->orderBy('pembelian_obat.created_at', 'desc');

      $start_date = $start;
      $end_date = $end;

      if($start_date == null){
        $start_date = date('Y-m-d');
      }

      if($end_date == null){
        $end_date = date('Y-m-d');
      }
      
      $query->where('tgl_faktur', '>=', $start_date); 
      $query->where('tgl_faktur', '<=', $end_date); 
      $data = $query->get();

      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_transaksi_apotek.data_pembelian.print_data_pembelian', ['data' => $data, 'identitas' => $ident, 'start_date' => $start_date, 'end_date' => $end_date]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-pembelian-obat.pdf');
    }

    public function cetak_detail_pembelian_obat($start, $end){
      $query = DB::table('pembelian_obat')
                  ->join('supplier', 'pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->join('gudang', 'pembelian_obat.kd_gudang', '=', 'gudang.kd_gudang')
                  ->select('pembelian_obat.*', 'supplier.nama_supplier', 'gudang.nama_gudang')
                  ->orderBy('pembelian_obat.created_at', 'desc');

      $start_date = $start;
      $end_date = $end;

      if($start_date == null){
        $start_date = date('Y-m-d');
      }

      if($end_date == null){
        $end_date = date('Y-m-d');
      }
      
      $query->where('tgl_faktur', '>=', $start_date); 
      $query->where('tgl_faktur', '<=', $end_date); 
      $data = $query->get();

      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_transaksi_apotek.data_pembelian.print_detail_data_pembelian', ['data' => $data, 'identitas' => $ident, 'start_date' => $start_date, 'end_date' => $end_date]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-detail-pembelian-obat.pdf');
    }

    public function export_pembelian_obat($start, $end){
      $excel = Excel::create('Laporan Pembelian Obat', function($excel) use($start,$end) {
        
              // Set the title
              $excel->setTitle('LAPORAN REKAP PEMBELIAN OBAT');
          
              // Chain the setters
              $excel->setCreator('Apotik')
                    ->setCompany('Apotik');
          
              // Call them separately
              $excel->setDescription('LAPORAN REKAP PEMBELIAN OBAT');

              $excel->sheet('Sheet1', function($sheet) use($start, $end) {

              $query = DB::table('pembelian_obat')
                  ->join('supplier', 'pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->join('gudang', 'pembelian_obat.kd_gudang', '=', 'gudang.kd_gudang')
                  ->select('pembelian_obat.*', 'supplier.nama_supplier', 'gudang.nama_gudang')
                  ->orderBy('pembelian_obat.created_at', 'desc');

              $start_date = $start;
              $end_date = $end;

              if($start_date == null){
                $start_date = date('Y-m-d');
              }

              if($end_date == null){
                $end_date = date('Y-m-d');
              }
              
              $query->where('tgl_faktur', '>=', $start_date); 
              $query->where('tgl_faktur', '<=', $end_date); 
              $data = $query->get();
              $ident = Identitas::first();
              
              $sheet->loadView('laporan_transaksi_apotek.data_pembelian.export_data_pembelian', ['data' => $data, 'identitas' => $ident, 'start_date' => $start_date, 'end_date' => $end_date]);

              $end = $data->count() + 7;
              $border = 'A7:J'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:J1');
              $sheet->mergeCells('A2:J2');
              $sheet->mergeCells('A3:J3');
              $sheet->mergeCells('A4:J4');
              $sheet->mergeCells('A5:J5');
              $sheet->mergeCells('A6:J6');
              $sheet->getStyle('A1:J4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5:A6')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    public function export_detail_pembelian_obat($start, $end){
      $excel = Excel::create('Laporan Detail Pembelian Obat', function($excel) use($start,$end) {
        
              // Set the title
              $excel->setTitle('LAPORAN REKAP DETAIL PEMBELIAN OBAT');
          
              // Chain the setters
              $excel->setCreator('Apotik')
                    ->setCompany('Apotik');
          
              // Call them separately
              $excel->setDescription('LAPORAN REKAP DETAIL PEMBELIAN OBAT');

              $excel->sheet('Sheet1', function($sheet) use($start, $end) {

              $query = DB::table('pembelian_obat')
                  ->join('supplier', 'pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->join('gudang', 'pembelian_obat.kd_gudang', '=', 'gudang.kd_gudang')
                  ->select('pembelian_obat.*', 'supplier.nama_supplier', 'gudang.nama_gudang')
                  ->orderBy('pembelian_obat.created_at', 'desc');

              $start_date = $start;
              $end_date = $end;

              if($start_date == null){
                $start_date = date('Y-m-d');
              }

              if($end_date == null){
                $end_date = date('Y-m-d');
              }
              
              $query->where('tgl_faktur', '>=', $start_date); 
              $query->where('tgl_faktur', '<=', $end_date); 
              $data = $query->get();
              $rows_detail = 0;

              foreach ($data as $key) {
                $pembelian_detail = DB::table('pembelian_detail')->where('no_faktur',$key->no_faktur)->get();
                $rows_detail += $pembelian_detail->count() + 6;
                Log::info(print_r($rows_detail, true));
              }

              $ident = Identitas::first();
              
              $sheet->loadView('laporan_transaksi_apotek.data_pembelian.export_detail_data_pembelian', ['data' => $data, 'identitas' => $ident, 'start_date' => $start_date, 'end_date' => $end_date]);

              $end = $data->count() + $rows_detail + 7;
              Log::info(print_r($end, true));
              $border = 'A7:I'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:I1');
              $sheet->mergeCells('A2:I2');
              $sheet->mergeCells('A3:I3');
              $sheet->mergeCells('A4:I4');
              $sheet->mergeCells('A5:I5');
              $sheet->mergeCells('A6:I6');
              $sheet->getStyle('A1:I4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5:A6')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }
    
    public function laporan_data_retur_pembelian(Request $req)
    {
      $query = DB::table('retur_pembelian_obat')
                  ->join('pembelian_obat', 'retur_pembelian_obat.no_faktur', '=', 'pembelian_obat.no_faktur')
                  ->join('supplier', 'retur_pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->select('retur_pembelian_obat.*', 'pembelian_obat.no_po', 'supplier.nama_supplier')
                  ->orderBy('retur_pembelian_obat.created_at', 'desc');

      $start_date = $req->start;
      $end_date = $req->end;

      if($start_date == null){
        $start_date = date('Y-m-d');
      }

      if($end_date == null){
        $end_date = date('Y-m-d');
      }
      
      $query->where('tgl_faktur', '>=', $start_date); 
      $query->where('tgl_faktur', '<=', $end_date); 
      $data = $query->get();

      return view('laporan_transaksi_apotek.data_retur_pembelian.data_retur_pembelian', ['data' => $data, 'start_date' => $start_date, 'end_date' => $end_date]);
    }

    public function export_retur_pembelian_obat($start, $end){
      $excel = Excel::create('Laporan Retur Pembelian Obat', function($excel) use($start,$end) {
        
              // Set the title
              $excel->setTitle('LAPORAN REKAP RETUR PEMBELIAN OBAT');
          
              // Chain the setters
              $excel->setCreator('Apotik')
                    ->setCompany('Apotik');
          
              // Call them separately
              $excel->setDescription('LAPORAN REKAP RETUR PEMBELIAN OBAT');

              $excel->sheet('Sheet1', function($sheet) use($start, $end) {

              $query = DB::table('retur_pembelian_obat')
                  ->join('pembelian_obat', 'retur_pembelian_obat.no_faktur', '=', 'pembelian_obat.no_faktur')
                  ->join('supplier', 'retur_pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->select('retur_pembelian_obat.*', 'pembelian_obat.no_po', 'supplier.nama_supplier')
                  ->orderBy('retur_pembelian_obat.created_at', 'desc');

              $start_date = $start;
              $end_date = $end;

              if($start_date == null){
                $start_date = date('Y-m-d');
              }

              if($end_date == null){
                $end_date = date('Y-m-d');
              }
              
              $query->where('tgl_faktur', '>=', $start_date); 
              $query->where('tgl_faktur', '<=', $end_date); 
              $data = $query->get();

              $ident = Identitas::first();
              
              $sheet->loadView('laporan_transaksi_apotek.data_retur_pembelian.export_data_retur_pembelian', ['data' => $data, 'identitas' => $ident, 'start_date' => $start_date, 'end_date' => $end_date]);

              $end = $data->count() + 7;
              $border = 'A7:G'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:G1');
              $sheet->mergeCells('A2:G2');
              $sheet->mergeCells('A3:G3');
              $sheet->mergeCells('A4:G4');
              $sheet->mergeCells('A5:G5');
              $sheet->mergeCells('A6:G6');
              $sheet->getStyle('A1:G4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5:A6')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    public function export_detail_retur_pembelian_obat($start, $end){
      $excel = Excel::create('Laporan Detail Retur Pembelian Obat', function($excel) use($start,$end) {
        
              // Set the title
              $excel->setTitle('LAPORAN REKAP RETUR DETAIL PEMBELIAN OBAT');
          
              // Chain the setters
              $excel->setCreator('Apotik')
                    ->setCompany('Apotik');
          
              // Call them separately
              $excel->setDescription('LAPORAN REKAP DETAIL RETUR PEMBELIAN OBAT');

              $excel->sheet('Sheet1', function($sheet) use($start, $end) {

              $query = DB::table('retur_pembelian_obat')
                  ->join('pembelian_obat', 'retur_pembelian_obat.no_faktur', '=', 'pembelian_obat.no_faktur')
                  ->join('supplier', 'retur_pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->select('retur_pembelian_obat.*', 'pembelian_obat.no_po', 'supplier.nama_supplier')
                  ->orderBy('retur_pembelian_obat.created_at', 'desc');

              $start_date = $start;
              $end_date = $end;

              if($start_date == null){
                $start_date = date('Y-m-d');
              }

              if($end_date == null){
                $end_date = date('Y-m-d');
              }
              
              $query->where('tgl_faktur', '>=', $start_date); 
              $query->where('tgl_faktur', '<=', $end_date); 
              $data = $query->get();
              $rows_detail = 0;

              foreach ($data as $key) {
                $details = DB::table('retur_pembelian_obat')->where('no_faktur',$key->no_faktur)
                  ->join('detail_retur_pembelian', 'detail_retur_pembelian.no_retur_pembelian', '=', 'retur_pembelian_obat.no_retur_pembelian')->get();
                $rows_detail += $details->count() + 1;
                //Log::info(print_r($rows_detail, true));
              }

              $ident = Identitas::first();
              
              $sheet->loadView('laporan_transaksi_apotek.data_retur_pembelian.export_data_detail_retur_pembelian', ['data' => $data, 'identitas' => $ident, 'start_date' => $start_date, 'end_date' => $end_date]);

              $end = $data->count() + $rows_detail + 7;
              //Log::info(print_r($end, true));
              $border = 'A7:G'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:G1');
              $sheet->mergeCells('A2:G2');
              $sheet->mergeCells('A3:G3');
              $sheet->mergeCells('A4:G4');
              $sheet->mergeCells('A5:G5');
              $sheet->mergeCells('A6:G6');
              $sheet->getStyle('A1:G4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5:A6')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    public function cetak_retur_pembelian_obat($start, $end){
      $query = DB::table('retur_pembelian_obat')
                  ->join('pembelian_obat', 'retur_pembelian_obat.no_faktur', '=', 'pembelian_obat.no_faktur')
                  ->join('supplier', 'retur_pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->select('retur_pembelian_obat.*', 'pembelian_obat.no_po', 'supplier.nama_supplier')
                  ->orderBy('retur_pembelian_obat.created_at', 'desc');

      $start_date = $start;
      $end_date = $end;

      if($start_date == null){
        $start_date = date('Y-m-d');
      }

      if($end_date == null){
        $end_date = date('Y-m-d');
      }
      
      $query->where('tgl_faktur', '>=', $start_date); 
      $query->where('tgl_faktur', '<=', $end_date); 
      $data = $query->get();

      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_transaksi_apotek.data_retur_pembelian.print_data_retur_pembelian', ['data' => $data, 'identitas' => $ident, 'start_date' => $start_date, 'end_date' => $end_date]);
      
      return $pdf->setPaper('a4', 'potrait')->download('laporan-retur-pembelian-obat.pdf');
    }

    public function cetak_detail_retur_pembelian_obat($start, $end){
      $query = DB::table('retur_pembelian_obat')
                  ->join('pembelian_obat', 'retur_pembelian_obat.no_faktur', '=', 'pembelian_obat.no_faktur')
                  ->join('supplier', 'retur_pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->select('retur_pembelian_obat.*', 'pembelian_obat.no_po', 'supplier.nama_supplier')
                  ->orderBy('retur_pembelian_obat.created_at', 'desc');

      $start_date = $start;
      $end_date = $end;

      if($start_date == null){
        $start_date = date('Y-m-d');
      }

      if($end_date == null){
        $end_date = date('Y-m-d');
      }
      
      $query->where('tgl_faktur', '>=', $start_date); 
      $query->where('tgl_faktur', '<=', $end_date); 
      $data = $query->get();

      $ident = Identitas::first();

      $pdf = PDF::loadView('laporan_transaksi_apotek.data_retur_pembelian.print_data_detail_retur_pembelian', ['data' => $data, 'identitas' => $ident, 'start_date' => $start_date, 'end_date' => $end_date]);
      
      return $pdf->setPaper('a4', 'landscape')->download('laporan-detail-retur-pembelian-obat.pdf');
    }


    public function laporan_obat_retur_pembelian($no_retur,$no_faktur){

      $data =  DB::table('detail_retur_pembelian')
                ->join('retur_pembelian_obat', 'detail_retur_pembelian.no_retur_pembelian', '=',
                  'retur_pembelian_obat.no_retur_pembelian')
                ->join('pembelian_detail', 'detail_retur_pembelian.kd_obat', '=', 'pembelian_detail.kd_obat')
                ->join('satuan', 'pembelian_detail.kd_satuan', '=', 'satuan.kd_satuan')
                ->join('obat', 'obat.kd_obat', '=', 'satuan.kd_obat')
                ->where('detail_retur_pembelian.no_retur_pembelian',$no_retur)
                ->where('pembelian_detail.no_faktur', $no_faktur)
                ->where('pembelian_detail.retur', '=', 'true')
                ->select('detail_retur_pembelian.*', 'satuan.nama_sat', 'obat.nama_obat')
                ->get();

      return view('laporan_transaksi_apotek.data_retur_pembelian.detail_data_retur_pembelian', ['obat' => $data, 'no_retur' => $no_retur, 'no_faktur' => $no_faktur]);                  
    }

    public function laporan_data_penjualan_obat(Request $req)
    {
      $query = DB::table('penjualan_obat')
                  ->join('gudang', 'penjualan_obat.kd_gudang', '=', 'gudang.kd_gudang')
                  ->join('dokter', 'penjualan_obat.id_dokter', '=', 'dokter.id_dokter')
                  ->join('pasien', 'penjualan_obat.id_rm', '=', 'pasien.id_rm')
                  ->join('kategori_pasien', 'pasien.kd_kat_pasien', '=', 'kategori_pasien.kd_kat_pasien')
                  ->select('penjualan_obat.*', 'gudang.nama_gudang', 'dokter.nama_dokter', 'pasien.nama_pasien', 'kategori_pasien.disc_harga_jual')
                  ->orderBy('penjualan_obat.created_at', 'desc');

      $start_date = $req->start;
      $end_date = $req->end;

      if($start_date != null){
        $query->where('tgl_faktur', '>=', $start_date);        
      }

      if($end_date != null){
        $query->where('tgl_faktur', '<=', $end_date);        
      }
       
      $data = $query->get();

      return view('laporan_transaksi_apotek.data_penjualan.data_penjualan', ['data' => $data, 'start_date' => $start_date, 'end_date' => $end_date]);
    }

    public function laporan_obat_penjualan_obat($id){

      $data = DB::table('penjualan_detail')->where('no_faktur',$id)
          ->join('obat', 'penjualan_detail.kd_obat', '=', 'obat.kd_obat')
          ->join('satuan', 'penjualan_detail.kd_satuan', '=', 'satuan.kd_satuan')
          ->get();

      return view('laporan_transaksi_apotek.data_penjualan.detail_data_penjualan', ['obat' => $data, 'no_faktur' => $id]);                  
    }

    public function laporan_data_retur_penjualan(Request $req)
    {
      $query = DB::table('retur_penjualan_obat')
                  ->join('penjualan_obat', 'retur_penjualan_obat.no_faktur', '=', 'penjualan_obat.no_faktur')
                  ->join('dokter', 'penjualan_obat.id_dokter', '=', 'dokter.id_dokter')
                  ->join('pasien', 'penjualan_obat.id_rm', '=', 'pasien.id_rm')
                  ->select('retur_penjualan_obat.*', 'dokter.nama_dokter', 'pasien.nama_pasien', 'penjualan_obat.*')
                  ->orderBy('retur_penjualan_obat.created_at', 'desc');


      $start_date = $req->start;
      $end_date = $req->end;

      if($start_date != null){
        $query->where('tanggal_retur', '>=', $start_date);        
      }

      if($end_date != null){
        $query->where('tanggal_retur', '<=', $end_date);        
      }
       
      $data = $query->get();

      return view('laporan_transaksi_apotek.data_retur_penjualan.data_retur_penjualan', ['data' => $data, 'start_date' => $start_date, 'end_date' => $end_date]);
    }

    public function laporan_obat_retur_penjualan($no_retur,$no_faktur){

      $data =  DB::table('detail_retur_penjualan')
                  ->join('retur_penjualan_obat', 'detail_retur_penjualan.no_retur_penjualan', '=',
                    'retur_penjualan_obat.no_retur_penjualan')
                  ->join('penjualan_detail', 'detail_retur_penjualan.kd_obat', '=', 'penjualan_detail.kd_obat')
                  ->join('satuan', 'penjualan_detail.kd_satuan', '=', 'satuan.kd_satuan')
                  ->join('obat', 'obat.kd_obat', '=', 'satuan.kd_obat')
                  ->where('detail_retur_penjualan.no_retur_penjualan',$no_retur)
                  ->where('penjualan_detail.no_faktur', $no_faktur)
                  ->where('penjualan_detail.retur', '=', 'true')
                  ->select('detail_retur_penjualan.*', 'satuan.nama_sat', 'obat.nama_obat')
                  ->get();

      return view('laporan_transaksi_apotek.data_retur_penjualan.detail_data_retur_penjualan', ['obat' => $data, 'no_retur' => $no_retur, 'no_faktur' => $no_faktur]);                  
    }

     public function laporan_data_statistik_penjualan(Request $req)
     {
      $query = DB::table('penjualan_obat')
                  ->selectRaw('count(penjualan_obat.no_faktur) as jumlah_transaksi, sum(penjualan_detail.jmlh) as jumlah_terjual, obat.kd_obat, obat.nama_obat')
                  ->join('penjualan_detail', 'penjualan_obat.no_faktur', '=', 'penjualan_detail.no_faktur')
                  ->join('obat', 'penjualan_detail.kd_obat', '=', 'obat.kd_obat')
                  ->groupBy('obat.kd_obat')
                  ->groupBy('obat.nama_obat');

      $start_date = $req->start;
      $end_date = $req->end;

      if($start_date != null){
        $query->where('tanggal_retur', '>=', $start_date);        
      }

      if($end_date != null){
        $query->where('tanggal_retur', '<=', $end_date);        
      }
       
      $data = $query->get();

      return view('laporan_transaksi_apotek.data_statistik_penjualan.data_statistik_penjualan', ['data' => $data, 'start_date' => $start_date, 'end_date' => $end_date]);
    }
}
