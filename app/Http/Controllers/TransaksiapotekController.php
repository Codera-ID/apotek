<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Obat;
use App\PO;
use App\Detail_PO;
use App\Gudang;
use App\Supplier;
use App\Satuan;
use App\Pasien;
use App\Kategori_pasien;
use App\Dokter;
use App\Pembelian_detail;
use App\Pembelian_obat;
use App\Retur_pembelian;
use App\Detail_retur_pembelian;
use App\Penjualan_obat;
use App\Penjualan_detail;
use App\Retur_penjualan;
use App\Detail_retur_penjualan;
use App\MutasiObat;
use App\MutasiKonsinyasiObat;
use App\HutangPembelian;
use App\KonsinyasiPembelian;
use App\PenjualanPembayaran;
use App\PiutangPenjualan;
use Illuminate\Support\Facades\DB;
use Session;
use Log;

class TransaksiapotekController extends Controller
{
    //obat checkbox

    function add_obat_session($id){
      Obat::where('kd_obat',$id)->update([
        'checkbox' => 'true',
      ]);
    }

    function destroy_obat_session($id){

      Obat::where('kd_obat',$id)->update([
        'checkbox' => 'false',
      ]);

    }

    function cek_obat_session(){
      $obat = Obat::where('checkbox','true')->get();
      if(count($obat)>0){
      foreach ($obat as $key ) {
        $checkbox[] = $key->kd_obat;
      }
      return $checkbox;
      }else{
        $checkbox[]='';
        return $checkbox;
      }
    }

    function cek_harga_satuan($id){
      $satuan = DB::table('satuan')->where('kd_satuan','=',$id)
                  ->join('satuan_obat', 'satuan.kd_sat_obat', '=', 'satuan_obat.kd_sat_obat')
                  ->join('lokasi_obat', 'satuan.kd_lok_obat', '=', 'lokasi_obat.kd_lok_obat')
                  ->get();
      foreach($satuan as $key){
        $hna[] = $key->hna;
      }

      return $hna;
    }


    //purchaes order
    function hapuscheckbox_po($id){
      Obat::where('kd_obat',$id)->update([
        'checkbox' => 'false',
      ]);

      return redirect('po');
    }

    function tampil_purchaes_order(){
      $obat = Obat::where('checkbox','true')->get();
      $supplier = Supplier::all();

      $kd_po_max = PO::max('no_po');
      $first_kd_po = "PO";
      $kd= (integer) substr($kd_po_max,11);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) substr($date,2);
      $no_po = "$first_kd_po" .$date_value. sprintf("%04d",$kd);

      return view('transaksi_apotek.purchaes_order.purchaes_order',['obat'=>$obat,'supplier'=>$supplier, 'no_po'=>$no_po]);
    }

    function tampil_obat_purchaes_order(){
      $obat = DB::table('obat')
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('satuan','satuan.kd_obat','=','obat.kd_obat')
                  ->join('satuan_obat','satuan_obat.kd_sat_obat','=','satuan.kd_sat_obat')        
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->join('lokasi_obat', 'lokasi_obat.kd_lok_obat', '=', 'satuan.kd_lok_obat')
                  ->groupBy('obat.kd_obat')
                  ->get();


      return view('transaksi_apotek.purchaes_order.data_obat',['obat' => $obat]);
    }

    function tambah_po(Request $req){

      $po = new PO;
      $po->no_po = $req->no_po;
      $po->tgl_po = $req->tgl_po;
      $po->kd_supplier = $req->kd_supplier;
      $po->jenis_obat = $req->jenis_obat;
      $po->total = $req->total_bayar;
      $po->jenis_invoice = $req->jenis_invoice;
      $po->keterangan = $req->keterangan;
      $po->save();
      $jmlh_detail =(integer) $req->jmlh_detail;
      for($i=1;$i < $jmlh_detail+1;$i++){
        $kd_obat="kd_obat".$i;
        $jmlh ="jmlh".$i;
        $kd_satuan ="kd_satuan".$i;
        $hna ="hna".$i;
        $subtotal ="subtotal".$i;
        $diskon ="diskon".$i;
        $nom_dis ="nom_dis".$i;
        $total ="total".$i;

        $detail_po = new Detail_PO;

        $detail_po->no_po = $req->no_po;
        $detail_po->kd_obat= $req->$kd_obat;
        $detail_po->jmlh = $req->$jmlh;
        $detail_po->kd_satuan = $req->$kd_satuan;
        $detail_po->harga = $req->$hna;
        $detail_po->subtotal = $req->$subtotal;
        $detail_po->diskon = $req->$diskon;

        $detail_po->save();
        Obat::where('kd_obat',$req->$kd_obat)->update([
          'checkbox' => 'false',
        ]);
      }

      return redirect('po')->with('message', 'Tambah Berhasil.');;
    }

    function tampil_hapus_po(){
      $po = DB::table('purchaes_order')
                ->join('supplier', 'purchaes_order.kd_supplier', '=', 'supplier.kd_supplier')->get();


      return view('transaksi_apotek.purchaes_order.hapus_purchaes_order',['po' => $po]);
    }

    function proses_hapus_po($id){

      $detail_po = Detail_PO::where('no_po',$id)->get();
      foreach ($detail_po as $key) {
        Detail_PO::where('no_po_detail',$key->no_po_detail)->delete();
      }

      PO::where('no_po',$id)->delete();


      return redirect('po/data-hapus')->with('message', 'Delete Berhasil');
    }

    //pembelian obat
    function hapuscheckbox_pembelian_obat($id){
      Obat::where('kd_obat',$id)->update([
        'checkbox' => 'false',
      ]);

      return redirect('pembelian-obat');
    }

    function hapus_po_pembelian_obat($id){

      Detail_PO::where('no_po_detail',$key->no_po_detail)->delete();

      return redirect('pembelian-obat');
    }

    function tampil_pembelian_obat(){
      $faktur = Pembelian_obat::max('no_faktur');
      $first_no_faktur = "FAK";
      $kd= (integer) substr($faktur,10);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) substr($date,2);
      $faktur = "$first_no_faktur" .$date_value. sprintf("%04d",$kd);

      $data = Obat::where('checkbox','true')->get();
      $supplier = Supplier::all();
      $gudang = Gudang::all();
      $id=false;

      return view('transaksi_apotek.pembelian_obat.pembelian_obat',['data'=>$data,'supplier'=>$supplier,'id'=>$id, 'gudang' => $gudang, 'faktur' => $faktur]);
    }

    function tampil_pembelian_obat_po_id($id){
      $faktur = Pembelian_obat::max('no_faktur');
      $first_no_faktur = "FAK";
      $kd= (integer) substr($faktur,10);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) substr($date,2);
      $faktur = "$first_no_faktur" .$date_value. sprintf("%04d",$kd);

      $po_detail = DB::table('po_detail')->where('no_po',$id)->get();
      foreach($po_detail as $key){
        Obat::where('kd_obat',$key->kd_obat)->update([
          'checkbox' => 'true',
        ]);
      }
      $data = Obat::where('checkbox','true')->get();

      $gudang = Gudang::all();
      $supplier = Supplier::all();
      return view('transaksi_apotek.pembelian_obat.pembelian_obat',['data'=>$data,'supplier'=>$supplier, 'gudang' => $gudang, 'id'=>$id, 'faktur' => $faktur]);
    }

    function tampil_pembelian_obat_po(){
      $po = DB::table('purchaes_order')
                  ->join('supplier', 'purchaes_order.kd_supplier', '=', 'supplier.kd_supplier')
                  ->get();
      return view('transaksi_apotek.pembelian_obat.data_po',['po'=>$po]);
    }

    function tampil_obat_pemebelian_obat(){
      $obat = DB::table('obat')
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->whereRaw("obat.jenis_obat = 'nonkonsinyasi'")
                  ->get();


      return view('transaksi_apotek.pembelian_obat.data_obat',['obat' => $obat]);
    }

    function tampil_pembelian_konsinyasi_obat(){
      $obat = DB::table('obat')
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->whereRaw("obat.jenis_obat = 'konsinyasi'")
                  ->get();


      return view('transaksi_apotek.pembelian_obat.data_obat',['obat' => $obat]);
    }

    function tampil_hapus_pembelian_obat(){
      $pembelian_obat = DB::table('pembelian_obat')
                  ->join('supplier', 'pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->join('gudang', 'pembelian_obat.kd_gudang', '=', 'gudang.kd_gudang')
                  ->select('pembelian_obat.*', 'supplier.nama_supplier', 'gudang.nama_gudang')
                  ->orderBy('pembelian_obat.created_at', 'desc')
                  ->get();

      return view('transaksi_apotek.pembelian_obat.hapus_pembelian_obat',['pembelian_obat' => $pembelian_obat]);
    }

    function proses_hapus_pembelian_obat($id){
      $pembelian_detail = Pembelian_detail::where('no_faktur',$id)->get();
      foreach ($pembelian_detail as $key) {
        Pembelian_detail::where('no_faktur',$key->no_faktur)->delete();
      }

      Pembelian_obat::where('no_faktur',$id)->delete();

      return redirect('pembelian-obat/data-hapus')->with('message', 'Delete Berhasil');
    }

    function tambah_pembelian_obat(Request $req){

      $pembelian_obat = new Pembelian_obat;
      $pembelian_obat->no_faktur = $req->no_fak;
      $pembelian_obat->no_po = $req->no_po;
      $pembelian_obat->kd_supplier = $req->kd_supplier;
      $pembelian_obat->jenis_pembayaran = $req->jenis_pembayaran;
      $pembelian_obat->kas = $req->kas;
      $pembelian_obat->kd_gudang = $req->kd_gudang;
      $pembelian_obat->diskon = $req->pembelianobat_dis;
      $pembelian_obat->pajak = $req->pembelianobat_pajak;
      $pembelian_obat->total = $req->total_bayar;
      $pembelian_obat->tgl_faktur = $req->tgl_fak;

      $pembelian_obat->save();
      $jmlh_detail =(integer) $req->jmlh_detail;
      for($i=1;$i < $jmlh_detail+1;$i++){
        $kd_obat="kd_obat".$i;
        $jmlh ="jmlh".$i;
        $kd_satuan ="kd_satuan".$i;
        $hna ="hna".$i;
        $diskon ="diskon".$i;
        $tgl_exp ="tgl_exp".$i;
        $no_batch ="no_batch".$i;
        $harga_jual_1 ="harga_jual_1".$i;
        $diskon_harga_jual_1 ="diskon_harga_jual_1".$i;
        $satuan = Satuan::where('kd_satuan',$req->$kd_satuan)->get();
        //Log::info(print_r($satuan[0]['stok'], true));
        $stok = $satuan[0]['stok'];
        $stok_satuan = (integer) $req->$jmlh;
        $stok_total = $stok_satuan + $stok;

        Satuan::where('kd_satuan',$req->$kd_satuan)->update([
          'stok' => $stok_total,
          'harga_jual_1' => (integer) $req->$harga_jual_1,
          'harga_jual_1' => (integer) $req->$diskon_harga_jual_1,
          'tgl_expired' => $req->$tgl_exp,
          'no_batch' => $req->$no_batch
        ]);
        
        $pembelian_detail = new Pembelian_detail;
        $no_pembelian_detail = Pembelian_detail::max('no_pembelian_detail');
       
       $pembelian_detail->no_pembelian_detail = (integer) $no_pembelian_detail + 1;
        $pembelian_detail->no_faktur = $req->no_fak;
        $pembelian_detail->kd_obat = $req->$kd_obat;
        $pembelian_detail->jmlh = $req->$jmlh;
        $pembelian_detail->kd_satuan = $req->$kd_satuan;
        $pembelian_detail->harga = $req->$hna;
        $pembelian_detail->diskon = $req->$diskon;
        $pembelian_detail->tgl_exp = $req->$tgl_exp;
        $pembelian_detail->retur = "false";

        $pembelian_detail->save();

        $this->destroy_obat_session($req->$kd_obat);

        if($req->jenis_pembayaran == 'HUTANG'){
          $total_hutang = ($req->$jmlh * $req->$hna) * (1 - ($req->$diskon/100));
          $hutang_pembelian = new HutangPembelian;
          $hutang_pembelian->no_faktur = $req->no_fak;
          $hutang_pembelian->total = $total_hutang;
          $hutang_pembelian->sisa = $total_hutang;
          $hutang_pembelian->tanggal_jatuh_tempo = $req->tanggal_jatuh_tempo;
          $hutang_pembelian->save();
        }

        if($req->jenis_pembayaran == 'KONSINYASI'){
          $konsinyasi_pembelian = new KonsinyasiPembelian;
          $konsinyasi_pembelian->no_faktur = $req->no_fak;
          $konsinyasi_pembelian->jumlah = $req->$jmlh;
          $konsinyasi_pembelian->sisa = $req->$jmlh;
          $konsinyasi_pembelian->save();
        }

      }

      return redirect('pembelian-obat');
    }

    //retur obat

    function tampil_retur_pembelian_obat(){
      $retur_pembelian = DB::table('retur_pembelian_obat')
                  ->join('pembelian_obat', 'retur_pembelian_obat.no_faktur', '=', 'pembelian_obat.no_faktur')
                  ->join('supplier', 'retur_pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->select('retur_pembelian_obat.*', 'pembelian_obat.no_po', 'supplier.nama_supplier')
                  ->orderBy('pembelian_obat.created_at', 'desc')
                  ->get();

      return view('transaksi_apotek.retur_pembelian_obat.retur_pembelian_obat',['retur_pembelian'=>$retur_pembelian]);
    }

    function tampil_tambah_retur(){
      $data = Obat::where('checkbox','true')->get();
      $supplier = Supplier::all();
      $id=false;
      return view('transaksi_apotek.retur_pembelian_obat.tambah_retur',['data'=>$data,'supplier'=>$supplier,'id'=>$id]);
    }

    function update_retur($id){
      $pembelian_detail = DB::table('pembelian_detail')->where('no_faktur',$id)->get();
      foreach($pembelian_detail as $key){
        Pembelian_detail::where('no_pembelian_detail',$key->no_pembelian_detail)->update([
          'retur' => 'true',
        ]);
      }

      return redirect('retur-pembelian/tambah/'.$id);
    }

    function hapus_obat_retur($id,$obat){
      $pembelian_detail = DB::table('pembelian_detail')->where('no_faktur',$id)->where('kd_obat','=',$obat)->get();
      foreach($pembelian_detail as $key){
        Pembelian_detail::where('no_pembelian_detail',$key->no_pembelian_detail)->update([
          'retur' => 'false',
        ]);
      }

      return redirect('retur-pembelian/tambah/'.$id);
    }

    function tampil_tambah_retur_id($id){
      $data = DB::table('pembelian_detail')->where('no_faktur','=',$id)->where('retur','=','true')->where('jmlh','<>',0)
                  ->join('satuan','pembelian_detail.kd_satuan','=','satuan.kd_satuan')
                  ->join('satuan_obat','satuan.kd_sat_obat','=','satuan_obat.kd_sat_obat')
                  ->join('obat','pembelian_detail.kd_obat','=','obat.kd_obat')->get();
      $pembelian_obat = DB::table('pembelian_obat')->where('no_faktur','=',$id)
                  ->join('supplier', 'pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->join('gudang', 'pembelian_obat.kd_gudang', '=', 'gudang.kd_gudang')
                  ->get();

      $supplier = Supplier::all();

      return view('transaksi_apotek.retur_pembelian_obat.tambah_retur',['data'=>$data,'supplier'=>$supplier,'id'=>$id, 'pembelian_obat' => $pembelian_obat]);
    }

    function proses_tambah_retur(Request $req){
      $kd_retur_max = Retur_pembelian::max('no_retur_pembelian');
      $first_kd_retur = "RET";
      $kd= (integer) substr($kd_retur_max,10);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) substr($date,2);
      $no_retur = "$first_kd_retur" .$date_value. sprintf("%04d",$kd);
      //dd($req);

      $retur_pembelian_obat = new Retur_pembelian;
      $retur_pembelian_obat->no_retur_pembelian = $no_retur;
      $retur_pembelian_obat->no_faktur = $req->no_faktur;
      $retur_pembelian_obat->jenis_pembayaran = $req->jenis_pembayaran;
      $retur_pembelian_obat->kd_supplier = $req->kd_supplier;
      $retur_pembelian_obat->total = $req->total_bayar;
      $retur_pembelian_obat->tanggal_retur = $req->tanggal_retur;
      $retur_pembelian_obat->save();

      $jmlh_obat =(integer) $req->jmlh_obat;
      for($i=1;$i < $jmlh_obat+1;$i++){
        Log::info(print_r($i, true));
        Log::info(print_r($jmlh_obat, true));
        $kd_obat="kd_obat".$i;
        $jmlh ="jmlh".$i;
        $kd_satuan ="kd_satuan".$i;
        $hna ="hna".$i;
        $subtotal ="subtotal".$i;
        $potongan ="potongan".$i;
        $total ="total".$i;

        $detail_pembelian = Pembelian_detail::where('no_faktur','=',$req->no_faktur)->where('kd_obat','=',$req->$kd_obat)->get();
        Log::info(print_r($detail_pembelian, true));
        $jmlh_detail = $detail_pembelian[0]['jmlh'];
        $jmlh_baru = $jmlh_detail - $req->$jmlh;

        Pembelian_detail::where('no_pembelian_detail',$detail_pembelian[0]['no_pembelian_detail'])->update([
          'jmlh' => $jmlh_baru
        ]);

        $detail_retur_pembelian = new Detail_retur_pembelian;

        $no_retur_pembelian_detail = Detail_retur_pembelian::max('no_retur_pembelian_detail');

        $detail_retur_pembelian->no_retur_pembelian_detail = $no_retur_pembelian_detail+1;
        $detail_retur_pembelian->no_retur_pembelian = $no_retur;
        $detail_retur_pembelian->kd_obat = $req->$kd_obat;
        $detail_retur_pembelian->jmlh = $req->$jmlh;
        $detail_retur_pembelian->kd_satuan = $req->$kd_satuan;
        $detail_retur_pembelian->harga_rata_rata = $req->$hna;
        $detail_retur_pembelian->potongan = $req->$potongan;
        $detail_retur_pembelian->total = $req->$total;

        $detail_retur_pembelian->save();


      }

      return redirect('retur-pembelian');
    }

    function tampil_retur_pembelian(){
      $pembelian_obat = DB::table('pembelian_obat')
                  ->join('supplier', 'pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->join('gudang', 'pembelian_obat.kd_gudang', '=', 'gudang.kd_gudang')
                  ->select('pembelian_obat.*', 'supplier.nama_supplier', 'gudang.nama_gudang')
                  ->orderBy('pembelian_obat.created_at', 'desc')
                  ->get();

      return view('transaksi_apotek.retur_pembelian_obat.data_pembelian_obat',[ 'pembelian_obat' => $pembelian_obat]);
    }

    function tampil_hapus_retur_pembelian(){
      
      $retur_pembelian = DB::table('retur_pembelian_obat')
                  ->join('pembelian_obat', 'retur_pembelian_obat.no_faktur', '=', 'pembelian_obat.no_faktur')
                  ->join('supplier', 'retur_pembelian_obat.kd_supplier', '=', 'supplier.kd_supplier')
                  ->select('retur_pembelian_obat.*', 'pembelian_obat.no_po', 'supplier.nama_supplier')
                  ->orderBy('pembelian_obat.created_at', 'desc')
                  ->get();

      return view('transaksi_apotek.retur_pembelian_obat.hapus_retur_pembelian', [ 'retur_pembelian' => $retur_pembelian]);
    }

    function proses_hapus_retur_pembelian($id){
      $pembelian_detail = Detail_retur_pembelian::where('no_retur_pembelian',$id)->delete();

      Retur_pembelian::where('no_retur_pembelian',$id)->delete();

      return redirect('retur-pembelian/data-hapus')->with('message', 'Delete Berhasil');
    }

    //penjualan Obat
    
    function hapuscheckbox_penjualan_obat($id){
      Obat::where('kd_obat',$id)->update([
        'checkbox' => 'false',
      ]);

      return redirect('penjualan-obat');
    }

    function tampil_penjualan_obat(){
      
      $faktur = Penjualan_obat::max('no_faktur');
      $first_no_faktur = "FAK";
      $kd= (integer) substr($faktur,10);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) substr($date,2);
      $no_faktur = "$first_no_faktur" .$date_value. sprintf("%04d",$kd);

      $data = Obat::where('checkbox','true')->get();
      $gudang = Gudang::all();
      $pasien = Pasien::all();
      $dokter = Dokter::all();

      return view('transaksi_apotek.penjualan_obat.penjualan_obat',['data'=>$data, 'gudang' => $gudang, 'pasien' => $pasien, 'dokter' => $dokter, 'no_faktur' => $no_faktur]);
    }

    function tampil_obat_penjualan_obat(){
      $obat = DB::table('obat')
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
                  ->join('lokasi_obat', 'satuan.kd_lok_obat', '=', 'lokasi_obat.kd_lok_obat')
                  ->get();


      return view('transaksi_apotek.penjualan_obat.data_obat',['obat' => $obat]);
    }

    function tampil_hapus_penjualan_obat(){
      $penjualan_obat = DB::table('penjualan_obat')
                  ->join('gudang', 'penjualan_obat.kd_gudang', '=', 'gudang.kd_gudang')
                  ->join('dokter', 'penjualan_obat.id_dokter', '=', 'dokter.id_dokter')
                  ->join('pasien', 'penjualan_obat.id_rm', '=', 'pasien.id_rm')
                  ->join('kategori_pasien', 'pasien.kd_kat_pasien', '=', 'kategori_pasien.kd_kat_pasien')
                  ->select('penjualan_obat.*', 'gudang.nama_gudang', 'dokter.nama_dokter', 'pasien.nama_pasien', 'kategori_pasien.disc_harga_jual')
                  ->orderBy('penjualan_obat.created_at', 'desc')
                  ->get();

      return view('transaksi_apotek.penjualan_obat.hapus_penjualan_obat',['penjualan_obat' => $penjualan_obat]);
    }

    function proses_hapus_penjualan_obat($id){
      $penjualan_detail = Penjualan_detail::where('no_faktur',$id)->get();
      foreach ($penjualan_detail as $key) {
        Penjualan_detail::where('no_faktur',$key->no_faktur)->delete();
      }

      Penjualan_obat::where('no_faktur',$id)->delete();

      return redirect('penjualan-obat/data-hapus')->with('message', 'Delete Berhasil');
    }

    function tampil_proses_pembayaran_penjualan(Request $req){

      $penjualan_obat = new Penjualan_obat;
      $penjualan_obat->no_faktur = $req->no_fak;
      $penjualan_obat->kasir = $req->kasir;
      $penjualan_obat->no_resep = $req->no_resep;
      $penjualan_obat->kd_gudang = $req->kd_gudang;
      $penjualan_obat->total = $req->total_bayar;
      $penjualan_obat->id_rm = $req->kd_pasien;
      $penjualan_obat->id_dokter = $req->kd_dokter; 
      $penjualan_obat->tgl_faktur = $req->tgl_penjualan;

      $penjualan_obat->save();
      $jmlh_detail =(integer) $req->jmlh_detail;
      for($i=1;$i < $jmlh_detail+1;$i++){
        $kd_obat="kd_obat".$i;
        $jmlh ="jmlh".$i;
        $kd_sat_obat ="kd_satuan".$i;
        $hna ="hna".$i;
        $diskon ="diskon".$i;
        $tgl_exp ="tgl_exp".$i;
        $no_batch ="no_batch".$i;
        $harga_jual_1 ="harga_jual_1".$i;
        $diskon_harga_jual_1 ="diskon_harga_jual_1".$i;
        $satuan = Satuan::where('kd_satuan',$req->$kd_sat_obat)->get();
        //Log::info(print_r($satuan[0]['stok'], true));
        $stok = $satuan[0]['stok'];
        $stok_satuan = (integer) $req->$jmlh;
        $stok_total = $stok - $stok_satuan;

        Satuan::where('kd_satuan',$req->$kd_sat_obat)->update([
          'stok' => $stok_total,
          'harga_jual_1' => (integer) $req->$harga_jual_1,
          'no_batch' => $req->$no_batch
        ]);
        
        $penjualan_detail = new Penjualan_detail;
        $no_penjualan_detail = Penjualan_detail::max('no_penjualan_detail');
       
       $penjualan_detail->no_penjualan_detail = (integer) $no_penjualan_detail + 1;
        $penjualan_detail->no_faktur = $req->no_fak;
        $penjualan_detail->kd_obat = $req->$kd_obat;
        $penjualan_detail->jmlh = $req->$jmlh;
        $penjualan_detail->kd_satuan = $req->$kd_sat_obat;
        $penjualan_detail->harga = $req->$hna;
        $penjualan_detail->diskon = $req->$diskon;
        $penjualan_detail->retur = "false";

        $penjualan_detail->save();

        $this->destroy_obat_session($req->$kd_obat);

      }

      return view('transaksi_apotek.penjualan_obat.proses_pembayaran',['data' => $req]);
    }

    function proses_pembayaran_penjualan(Request $req){

      $penjualan_pembayaran = new PenjualanPembayaran;
      $penjualan_pembayaran->no_faktur = $req->no_faktur;
      $penjualan_pembayaran->jenis_pembayaran = $req->pembayaran;
      $penjualan_pembayaran->total = $req->total;
      $penjualan_pembayaran->biaya_pengiriman = $req->biaya_pengiriman;
      $penjualan_pembayaran->pajak = $req->pajak;
      $penjualan_pembayaran->diskon = $req->diskon;
      if($req->pembayaran == 'kartu'){
        $penjualan_pembayaran->no_kartu = $req->no_kartu; 
        $penjualan_pembayaran->kartu = $req->kartu;
      }
      $penjualan_pembayaran->no_faktur_pajak = $req->faktur_pajak;
      $penjualan_pembayaran->save();
      Log::info(print_r($req->pembayaran, true));
      if($req->pembayaran == 'kredit'){
        $piutang_penjualan = new PiutangPenjualan;
        $piutang_penjualan->no_faktur = $req->no_faktur;
        $piutang_penjualan->total = $req->total;
        $piutang_penjualan->jumlah_bayar = $req->uang_muka;
        $piutang_penjualan->kekurangan = $req->total - $req->uang_muka;
        $piutang_penjualan->deadline = $req->tanggal_jatuh_tempo;
        $piutang_penjualan->save();
      }

      return redirect('penjualan-obat');
    }

    //retur penjualan obat

    function tampil_retur_penjualan_obat(){
      $retur_penjualan = DB::table('retur_penjualan_obat')
                  ->join('penjualan_obat', 'retur_penjualan_obat.no_faktur', '=', 'penjualan_obat.no_faktur')
                  ->join('penjualan_pembayaran', 'retur_penjualan_obat.no_faktur', '=', 'penjualan_pembayaran.no_faktur')
                  ->join('dokter', 'penjualan_obat.id_dokter', '=', 'dokter.id_dokter')
                  ->join('pasien', 'penjualan_obat.id_rm', '=', 'pasien.id_rm')
                  ->select('retur_penjualan_obat.*', 'dokter.nama_dokter', 'pasien.nama_pasien', 'penjualan_obat.*', 'penjualan_pembayaran.jenis_pembayaran')
                  ->orderBy('retur_penjualan_obat.created_at', 'desc')
                  ->get();

      return view('transaksi_apotek.retur_penjualan_obat.retur_penjualan_obat',['retur_penjualan'=>$retur_penjualan]);
    }

    function update_retur_penjualan($id){
      $penjualan_detail = DB::table('penjualan_detail')->where('no_faktur',$id)->get();
      foreach($penjualan_detail as $key){
        Penjualan_detail::where('no_penjualan_detail',$key->no_penjualan_detail)->update([
          'retur' => 'true',
        ]);
      }

      return redirect('retur-penjualan/tambah/'.$id);
    }

    function hapus_obat_retur_penjualan($id,$obat){
      $penjualan_detail = DB::table('penjualan_detail')->where('no_faktur',$id)->where('kd_obat','=',$obat)->get();
      foreach($penjualan_detail as $key){
        Penjualan_detail::where('no_penjualan_detail',$key->no_penjualan_detail)->update([
          'retur' => 'false',
        ]);
      }

      return redirect('retur-penjualan/tambah/'.$id);
    }

    function tampil_tambah_retur_penjualan_id($id){
      $data = DB::table('penjualan_detail')->where('no_faktur','=',$id)->where('retur','=','true')->where('jmlh','<>',0)
                  ->join('satuan','penjualan_detail.kd_satuan','=','satuan.kd_satuan')
                  ->join('satuan_obat','satuan.kd_sat_obat','=','satuan_obat.kd_sat_obat')
                  ->join('obat','penjualan_detail.kd_obat','=','obat.kd_obat')->get();

      $penjualan_obat = DB::table('penjualan_obat')
                  ->join('gudang', 'penjualan_obat.kd_gudang', '=', 'gudang.kd_gudang')
                  ->join('dokter', 'penjualan_obat.id_dokter', '=', 'dokter.id_dokter')
                  ->join('pasien', 'penjualan_obat.id_rm', '=', 'pasien.id_rm')
                  ->select('penjualan_obat.*', 'gudang.nama_gudang', 'dokter.nama_dokter', 'pasien.nama_pasien')
                  ->where('penjualan_obat.no_faktur', $id)
                  ->orderBy('penjualan_obat.created_at', 'desc')
                  ->get();

      return view('transaksi_apotek.retur_penjualan_obat.tambah_retur',['data'=>$data, 'id'=> $id, 'penjualan_obat' => $penjualan_obat]);
    }

    function tampil_tambah_retur_penjualan(){

      return view('transaksi_apotek.retur_penjualan_obat.tambah_retur_2');
    }

    function proses_tambah_retur_penjualan(Request $req){
      $kd_retur_max = Retur_penjualan::max('no_retur_penjualan');
      $first_kd_retur = "RET";
      $kd= (integer) substr($kd_retur_max,10);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) substr($date,2);
      $no_retur = "$first_kd_retur" .$date_value. sprintf("%04d",$kd);

      $retur_penjualan_obat = new Retur_penjualan;
      $retur_penjualan_obat->no_retur_penjualan = $no_retur;
      $retur_penjualan_obat->no_faktur = $req->no_faktur;
      $retur_penjualan_obat->total = $req->total_bayar;
      $retur_penjualan_obat->tanggal_retur = $req->tgl_retur;
      $retur_penjualan_obat->save();

      $jmlh_obat =(integer) $req->jmlh_detail;
      for($i=1;$i < $jmlh_obat+1;$i++){
        $kd_obat="kd_obat".$i;
        $jmlh ="jmlh".$i;
        $kd_satuan ="kd_satuan".$i;
        $hna ="hna".$i;
        $subtotal ="subtotal".$i;
        $potongan ="potongan".$i;
        $total ="total".$i;

        $detail_penjualan = Penjualan_detail::where('no_faktur','=',$req->no_faktur)->where('kd_obat','=',$req->$kd_obat)->get();
        //Log::info(print_r($detail_penjualan, true));
        $jmlh_detail = $detail_penjualan[0]['jmlh'];
        $jmlh_baru = $jmlh_detail - $req->$jmlh;

        Penjualan_detail::where('no_penjualan_detail',$detail_penjualan[0]['no_penjualan_detail'])->update([
          'jmlh' => $jmlh_baru
        ]);

        $detail_retur_penjualan = new Detail_retur_penjualan;

        $no_retur_penjualan_detail = Detail_retur_penjualan::max('no_retur_penjualan_detail');

        $detail_retur_penjualan->no_retur_penjualan_detail = $no_retur_penjualan_detail+1;
        $detail_retur_penjualan->no_retur_penjualan = $no_retur;
        $detail_retur_penjualan->kd_obat = $req->$kd_obat;
        $detail_retur_penjualan->jmlh = $req->$jmlh;
        $detail_retur_penjualan->kd_satuan = $req->$kd_satuan;
        $detail_retur_penjualan->harga_rata_rata = $req->$hna;
        $detail_retur_penjualan->potongan = $req->$potongan;
        $detail_retur_penjualan->total = $req->$total;

        $detail_retur_penjualan->save();


      }

      return redirect('retur-penjualan')->with("Tambah retur penjualan berhasil.");
    }

    function tampil_retur_penjualan(Request $req){
      $query = DB::table('penjualan_obat')
                  ->join('gudang', 'penjualan_obat.kd_gudang', '=', 'gudang.kd_gudang')
                  ->join('penjualan_pembayaran', 'penjualan_obat.no_faktur', '=', 'penjualan_pembayaran.no_faktur')
                  ->join('dokter', 'penjualan_obat.id_dokter', '=', 'dokter.id_dokter')
                  ->join('pasien', 'penjualan_obat.id_rm', '=', 'pasien.id_rm')
                  ->select('penjualan_obat.*', 'gudang.nama_gudang', 'dokter.nama_dokter', 'pasien.nama_pasien', 'penjualan_pembayaran.jenis_pembayaran', 'penjualan_pembayaran.diskon', 'penjualan_pembayaran.pajak')
                  ->orderBy('penjualan_obat.created_at', 'desc');
      
      $start_date = $req->start;
      $end_date = $req->end;

      if($start_date == null){
        $start_date = date('Y-m-d');
      }

      if($end_date == null){
        $end_date = date('Y-m-d');
      }
      
      $query->where('tgl_faktur', '>=', $start_date); 
      $query->where('tgl_faktur', '<=', $end_date); 
      $penjualan_obat = $query->get();

      return view('transaksi_apotek.retur_penjualan_obat.data_penjualan_obat',[ 'penjualan_obat' => $penjualan_obat, 'start_date' => $start_date, 'end_date' => $end_date]);
    }

    function tampil_hapus_retur_penjualan(Request $req){
      $start_date = $req->start;
      $end_date = $req->end;

      $query = DB::table('retur_penjualan_obat')
                  ->join('penjualan_obat', 'retur_penjualan_obat.no_faktur', '=', 'penjualan_obat.no_faktur')
                  ->join('dokter', 'penjualan_obat.id_dokter', '=', 'dokter.id_dokter')
                  ->join('pasien', 'penjualan_obat.id_rm', '=', 'pasien.id_rm')
                  ->select('retur_penjualan_obat.*', 'dokter.nama_dokter', 'pasien.nama_pasien', 'penjualan_obat.*')
                  ->orderBy('retur_penjualan_obat.created_at', 'desc');

      if($start_date != null){
        $query->where('tanggal_retur', '>=', $start_date);        
      }

      if($end_date != null){
        $query->where('tanggal_retur', '<=', $end_date);        
      }

      $retur_penjualan = $query->get();

      return view('transaksi_apotek.retur_penjualan_obat.hapus_retur_penjualan', [ 'retur_penjualan' => $retur_penjualan, 'start_date'=>$start_date, 'end_date'=>$end_date]);
    }

    function proses_hapus_retur_penjualan($id){
      $penjualan_detail = Detail_retur_penjualan::where('no_retur_penjualan',$id)->delete();

      Retur_penjualan::where('no_retur_penjualan',$id)->delete();

      return redirect('retur-penjualan/data-hapus')->with('message', 'Delete Berhasil');
    }

    //Mutasi Obat
    function hapuscheckbox_mutasi($id){
      Obat::where('kd_obat',$id)->update([
        'checkbox' => 'false',
      ]);

      return redirect('mutasi-obat/tambah');
    }

    function tampil_obat_mutasi(){
      $obat = DB::table('obat')
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('satuan','satuan.kd_obat','=','obat.kd_obat')
                  ->join('satuan_obat','satuan_obat.kd_sat_obat','=','satuan.kd_sat_obat')        
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->join('lokasi_obat', 'lokasi_obat.kd_lok_obat', '=', 'satuan.kd_lok_obat')
                  ->groupBy('obat.kd_obat')
                  ->get();


      return view('transaksi_apotek.mutasi_obat.data_obat',['obat' => $obat]);
    }

    function tampil_mutasi_obat(){
      $data = DB::table('mutasi_obats')
      ->join('obat', 'obat.kd_obat', '=', 'mutasi_obats.kd_obat')
      ->get();

      return view('transaksi_apotek.mutasi_obat.data_mutasi_obat', ['data'=>$data]);
    }

    function tambah_mutasi_obat(){
      $obat = Obat::where('checkbox','true')->get();

      return view('transaksi_apotek.mutasi_obat.tambah_mutasi', ['obat'=>$obat]);      
    }

    function tambah_mutasi(Request $req){

      $jmlh_obat =(integer) $req->jmlh_detail;
      for($i=1;$i < $jmlh_obat+1;$i++){
        
        $kd_obat="kd_obat".$i;
        $jmlh ="jmlh".$i;
        $kd_satuan ="kd_satuan".$i;

        $obat = DB::table('obat')
              ->join('satuan', 'obat.kd_obat', '=', 'satuan.kd_obat')
              ->whereRaw('obat.kd_obat = "'.$req->$kd_obat.'" and satuan.kd_satuan = "'.$req->$kd_satuan.'"')
              ->first();
      
        $stok_awal = 0;
        if(!empty($obat)){
          $stok_awal = $obat->stok;      
        }

        $mutasiObat = new MutasiObat();
        $mutasiObat->date = $req->date;
        $mutasiObat->no_bukti = $req->no_bukti;
        $mutasiObat->kd_obat = $req->$kd_obat;
        $mutasiObat->keterangan = $req->keterangan;
        $mutasiObat->petugas = $req->petugas;
        $mutasiObat->hpp = $obat->hna;
        $mutasiObat->stok_awal = $stok_awal;
        if($req->kd_mutasi == 0){
          $mutasiObat->keluar = $req->$jmlh;
          $mutasiObat->stok_akhir = $stok_awal - $req->$jmlh;
        }else{
          $mutasiObat->masuk = $req->$jmlh; 
          $mutasiObat->stok_akhir = $stok_awal + $req->$jmlh;       
        }

        $mutasiObat->save();

      }      

      return redirect('mutasi-obat')->with('message', 'Tambah mutasi obat berhasil.');
    }

    //Mutasi Konsinyasi Obat 
    function tampil_mutasi_konsinyasi(){
      $data = DB::table('mutasi_konsinyasi_obat')
      ->join('obat', 'obat.kd_obat', '=', 'mutasi_konsinyasi_obat.kd_obat')
      ->get();

      return view('transaksi_apotek.mutasi_konsinyasi_obat.data_mutasi_konsinyasi_obat', ['data'=>$data]);
    }

    function tambah_mutasi_konsinyasi(){
      $obat = Obat::all();

      return view('transaksi_apotek.mutasi_konsinyasi_obat.tambah_mutasi_konsinyasi_obat', ['obat'=>$obat]);      
    }

    function proses_tambah_mutasi_konsinyasi(Request $req){

      $data = MutasiKonsinyasiObat::where('kd_obat', "'".$req->kd_obat."'")->orderBy('date', 'DESC')->first();
      
      $stock = 0;
      if(!empty($data)){
        $stock = $data->stock;      
      }

      $mutasiKonsinyasi = new mutasiKonsinyasiObat();
      $mutasiKonsinyasi->date = $req->date;
      $mutasiKonsinyasi->no_bukti = $req->no_bukti;
      $mutasiKonsinyasi->kd_obat = $req->kd_obat;
      $mutasiKonsinyasi->keterangan = $req->keterangan;
      if($req->kd_mutasi == 0){
        $mutasiKonsinyasi->keluar = $req->jumlah;
        $stock = $stock - $req->jumlah;
      }else{
        $mutasiKonsinyasi->masuk = $req->jumlah; 
        $stock = $stock + $req->jumlah;        
      }

      $mutasiKonsinyasi->stock = $stock;
      $mutasiKonsinyasi->save();      

      return redirect('mutasi-konsinyasi-obat')->with('message', 'Tambah Berhasil.');
    }
}
