<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Identitas;
use App\AksesMenu;
use DB;
use App\User;
use App\DaftarShift;
use App\PengaturanAplikasi;
use Auth;
use App\JadwalShift;
class SistemController extends Controller
{
    public function indexIdentitas()
    {
      # code...
      $ident = Identitas::first();
      return view('sistem.identitas.index',['ident'=>$ident]);
    }
    
    public function postIdentitas(Request $request,$id)
    {
      # code...
      if ($request->file('img') == null) {
        # code...
        $ident = Identitas::find($id);
        $ident->nama = $request->nama;
        $ident->no_registrasi = $request->registrasi;
        $ident->pemilik = $request->pemilik;
        $ident->penanggung_jawab = $request->pj;
        $ident->penanggung_jawab_lab = $request->pj_lab;
        $ident->alamat = $request->alamat;
        $ident->kota = $request->kota;
        $ident->telepon = $request->telp;
        $ident->email = $request->email;
        $ident->website = $request->website;
        $ident->save();
        return redirect('identitas');
      }else{
        $image = $request->file('img');
        $input['imagename'] = 'logo-'.time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['imagename']);

        $ident = Identitas::find($id);
        $ident->nama = $request->nama;
        $ident->no_registrasi = $request->registrasi;
        $ident->pemilik = $request->pemilik;
        $ident->penanggung_jawab = $request->pj;
        $ident->penanggung_jawab_lab = $request->pj_lab;
        $ident->alamat = $request->alamat;
        $ident->kota = $request->kota;
        $ident->telepon = $request->telp;
        $ident->email = $request->email;
        $ident->website = $request->website;
        $ident->logo = $input['imagename'];
        $ident->save();
        return redirect('identitas');
      }
    }

    public function indexPengaturanApp()
    {
      $pengaturan = PengaturanAplikasi::first();

      return view('sistem.pengaturan-aplikasi.index',['pengaturan'=>$pengaturan]);
    }
    public function simpanCetakFaktur(Request $request)
    {
      
      $pengaturan = PengaturanAplikasi::find(1);
      $pengaturan->purchase_order = $request->purchase_order;
      $pengaturan->mki_po = $request->mki_po;
      $pengaturan->mka_po = $request->mka_po;
      $pengaturan->catatan_po = $request->catatan_po;
      $pengaturan->retur_pembelian_obat = $request->retur_pembelian_obat;
      $pengaturan->mki_retur_pembelian = $request->mki_retur_pembelian;
      $pengaturan->mka_retur_pembelian = $request->mka_retur_pembelian;
      $pengaturan->catatan_retur_pembelian = $request->catatan_retur_pembelian;
      $pengaturan->penjualan_obat = $request->penjualan_obat;
      $pengaturan->mki_penjualan = $request->mki_penjualan;
      $pengaturan->mka_penjualan = $request->mka_penjualan;
      $pengaturan->catatan_penjualan = $request->catatan_penjualan;
      $pengaturan->retur_penjualan_obat = $request->retur_penjualan_obat;
      $pengaturan->mki_retur_penjualan = $request->mki_retur_penjualan;
      $pengaturan->mka_retur_penjualan = $request->mka_retur_penjualan;
      $pengaturan->catatan_retur_penjualan = $request->catatan_retur_penjualan;
      $pengaturan->pembayaran_piutang = $request->pembayaran_piutang;
      $pengaturan->mki_pembayaran_piutang = $request->mki_pembayaran_piutang;
      $pengaturan->mka_pembayaran_piutang = $request->mka_pembayaran_piutang;
      $pengaturan->catatan_pembayaran_piutang = $request->catatan_pembayaran_piutang;
      $pengaturan->catatan_promo = $request->catatan_promo;
      $pengaturan->save();

      return redirect('pengaturan-aplikasi');
    }
    public function simpanNotifikasi(Request $request)
    {
      
      $pengaturan = PengaturanAplikasi::find(1);
      $pengaturan->obat_expired = $request->obat_expired;
      $pengaturan->piutang_jatuh_tempo = $request->piutang_jatuh_tempo;
      $pengaturan->hutang_jatuh_tempo = $request->hutang_jatuh_tempo;
      $pengaturan->save();

      return redirect('pengaturan-aplikasi');
    }
    public function simpanTuslahEmbalase(Request $request)
    {
      
      $pengaturan = PengaturanAplikasi::find(1);
      $pengaturan->tuslah = $request->tuslah;
      $pengaturan->embalase = $request->embalase;
      $pengaturan->save();

      return redirect('pengaturan-aplikasi');
    }
    public function simpanHargaPpn(Request $request)
    {
      
      $pengaturan = PengaturanAplikasi::find(1);
      $pengaturan->ppn = $request->ppn;
      $pengaturan->save();

      return redirect('pengaturan-aplikasi');
    }

    public function indexGrupUser()
    {
      # code...
      $grup = DB::table('grup_user')->get();
      return view('sistem.data-group-user.index',['grup'=>$grup]);
    }
    public function tambahGrupUser()
    {
      # code...
      $menu  = AksesMenu::all();
      return view('sistem.data-group-user.tambah',['menu'=>$menu]);
    }
    public function editGrupUser($id)
    {
      # code...
      $menu  = AksesMenu::all();
      $grup = DB::table('grup_user')->where('kode_grup', $id)->first();
      $menu_g = DB::table('grup_user_menu')->where('kode_grup', $id)->get();
      return view('sistem.data-group-user.edit',['menu'=>$menu,'grup'=>$grup,'menu_g'=>$menu_g]);
    }
    public function postGrupUser(Request $request)
    {
      # code...
      $nilai = DB::table('grup_user')->orderBy('kode_grup','desc')->first();
      if($nilai == null){
        $kode = 'G001';
      }else{
        $kode = substr($nilai->kode_group,3,4);
        $kode = $kode+1;
        $kode = str_pad($kode,3,"00",STR_PAD_LEFT);
      }
      $grup = DB::table('grup_user')->insert([
        'kode_grup'=>$kode,
        'nama' => $request->nama,
        'keterangan'=>$request->keterangan,
      ]);
      for ($i=0; $i <count($request->menu) ; $i++) {
        # code...
        $gm = DB::table('grup_user_menu')->insert([
          'kode_grup'=>$kode,
          'id_menu'=>$request->menu[$i],
        ]);
      }
      return redirect('data-group-user');
    }
    public function updateGrupUser(Request $request,$id)
    {
      # code...
      $grup = DB::table('grup_user')->where('kode_grup',$id)->update([
        'nama' => $request->nama,
        'keterangan'=>$request->keterangan,
      ]);
      $del = DB::table('grup_user_menu')->where('kode_grup',$id)->delete();
      for ($i=0; $i <count($request->menu) ; $i++) {
        # code...
        $gm = DB::table('grup_user_menu')->insert([
          'kode_grup'=>$id,
          'id_menu'=>$request->menu[$i],
        ]);
      }
      return redirect('data-group-user');
    }
    public function deleteGrupUser(Request $request,$id)
    {
      # code...
      $grup = DB::table('grup_user')->where('kode_grup',$id)->delete();
      $del = DB::table('grup_user_menu')->where('kode_grup',$id)->delete();

      return redirect('data-group-user');
    }

    public function indexUser()
    {
      # code...
      $user = DB::table('users')->join('grup_user','grup_user.kode_grup','=','users.kode_grup')->get();
      return view('sistem.data-user.index',['user'=>$user]);
    }
    public function tambahUser()
    {
      # code...
      $grup = DB::table('grup_user')->get();
      return view('sistem.data-user.tambah',['grup'=>$grup]);
    }
    public function editUser($id)
    {
      # code...
      $user = User::find($id);
      $grup = DB::table('grup_user')->get();
      return view('sistem.data-user.edit',['user'=>$user,'grup'=>$grup]);
    }
    public function updateUser(Request $request,$id)
    {
      # code...
      $user = User::find($id);
      if ($request->file('img') != null) {
        # code...
        $image = $request->file('img');
        $input['imagename'] = 'user-'.time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['imagename']);


        $user->name       = $request->nama;
        $user->email      = $request->email;
        $user->username   = $request->username;
        $user->alamat     = $request->alamat;
        $user->keterangan = $request->keterangan;
        $user->foto       = $input['imagename'];
        $user->kode_grup  = $request->grup;
        $user->save();
      }else {
        $user->name       = $request->nama;
        $user->email      = $request->email;
        $user->username   = $request->username;
        $user->alamat     = $request->alamat;
        $user->keterangan = $request->keterangan;
        $user->kode_grup  = $request->grup;
        $user->save();
      }

      return redirect('data-user');
    }
    public function postUser(Request $request)
    {
      # code...
      $image = $request->file('img');
      $input['imagename'] = 'user-'.time().'.'.$image->getClientOriginalExtension();
      $destinationPath = public_path('/images');
      $image->move($destinationPath, $input['imagename']);

      $user = new User;
      $user->name       = $request->nama;
      $user->email      = $request->email;
      $user->password   = $request->password;
      $user->username   = $request->username;
      $user->alamat     = $request->alamat;
      $user->keterangan = $request->keterangan;
      $user->foto       = $input['imagename'];
      $user->kode_grup  = $request->grup;
      $user->save();
      return redirect('data-user');
    }
    public function deleteUser(Request $request,$id)
    {
      # code...
      $grup = DB::table('users')->where('id',$id)->delete();

      return redirect('data-user');
    }

    public function indexJadwalShift()
    {
      # code...
      $shift = JadwalShift::all();
      return view('sistem.data-jadwal-shift.index',['shift'=>$shift]);
    }
    public function tambahJadwalShift()
    {
      # code...
      return view('sistem.data-jadwal-shift.tambah');
    }
    public function editJadwalShift($id)
    {
      # code...
      $shift = JadwalShift::find($id);
      return view('sistem.data-jadwal-shift.edit',['shift'=>$shift]);
    }
    public function postJadwalShift(Request $request)
    {
      # code...
      $shift = new JadwalShift;
      $shift->nama = $request->nama;
      $shift->mulai = $request->mulai;
      $shift->selesai = $request->selesai;
      $shift->save();
      return redirect('data-jadwal-shift');
    }
    public function updateJadwalShift(Request $request,$id)
    {
      # code...
      
      return redirect('data-jadwal-shift');
    }
    public function deleteJadwalShift(Request $request,$id)
    {
      # code...
      $shift = JadwalShift::where('id_js',$id)->delete();
      return redirect('data-jadwal-shift');
    }

    public function indexPergantianShift()
    {
      # code...
      $data = User::all();

      return view('sistem.pergantian-shift.index', ['data' => $data]);
    }

    public function tutupShift(Request $req){

      $shift = new DaftarShift();
      $shift->saldo_awal = $req->saldo_awal;
      $shift->hasil_penjualan = $req->hasil_penjualan;
      $shift->pembayaran_piutang = $req->pembayaran_piutang;
      $shift->jurnal_masuk = $req->jurnal_masuk;
      $shift->total_pendapatan = $req->total_pendapatan;
      $shift->retur_penjualan = $req->retur_penjualan;
      $shift->jurnal_keluar = $req->jurnal_keluar;
      $shift->total_pengeluaran = $req->total_pengeluaran;
      $shift->saldo_kasir = $req->saldo_kasir; 
      $shift->saldo_akhir = $req->saldo_akhir;      
      $shift->created_by = $req->user;      
      $shift->save();
      Auth::logout();
      return redirect ('/');
    }
}
