<?php

namespace App\Http\Controllers;

use App\Supplier;
use App\Gol_obat;
use App\Kat_obat;
use App\Obat;
use App\Apoteker;
use App\Pasien;
use App\Sat_obat;
use App\Racikan;
use App\Lok_obat;
use App\Gudang;
use App\Pabrik;
use App\Harga_jual;
use App\Diskon_harga;
use App\Satuan;
use App\KategoriPasien;
use App\JurnalKeuangan;
use App\Racikan_Obat;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class MasterapotekController extends Controller
{
    function add_obat_session($id){
      Obat::where('kd_obat',$id)->update([
        'checkbox' => 'true',
      ]);
    }

    function destroy_obat_session($id){

      Obat::where('kd_obat',$id)->update([
        'checkbox' => 'false',
      ]);

    }

    function cek_obat_session(){
      $obat = Obat::where('checkbox','true')->get();
      if(count($obat)>0){
      foreach ($obat as $key ) {
        $checkbox[] = $key->kd_obat;
      }
      return $checkbox;
      }else{
        $checkbox[]='';
        return $checkbox;
      }
    }

    //crud Pabrik
    function tampil_pabrik(){
      $pabrik = Pabrik::all();

      return view('master_apotek.data_pabrik.data_pabrik',['pabrik' => $pabrik]);
    }

    function tampil_detail_pabrik($id){

      $pabrik = Pabrik::where('kd_pabrik',$id)->first();

      return view('master_apotek.data_pabrik.detail_pabrik',['pabrik' => $pabrik]);
    }

    function tampil_tambah_pabrik(){

      $kd_pab_max = Pabrik::max('kd_pabrik');
      $first_kd_pab = "PAB";
      $kd= (integer) substr($kd_pab_max,11);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) $date;
      $kd_pabrik = "$first_kd_pab" .$date_value. sprintf("%04d",$kd);
      return view('master_apotek.data_pabrik.tambah_pabrik',['kd_pabrik' => $kd_pabrik]);
    }

    function proses_tambah_pabrik(Request $req){

      $status = "aktif";

      $pabrik = new Pabrik;
      $pabrik->kd_pabrik = $req->kd_pabrik;
      $pabrik->nama_pabrik = $req->nama_pabrik;
      $pabrik->alamat = $req->alamat;
      $pabrik->kota = $req->kota;
      $pabrik->no_tlpn = $req->no_tlpn;
      $pabrik->no_hp = $req->no_hp;
      $pabrik->npwp = $req->npwp;
      $pabrik->status = $status;

      $pabrik->save();

      return redirect('pabrik')->with('message', 'Tambah Data Berhasil');;
    }

    function tampil_update_pabrik($id){

      $pabrik = Pabrik::where('kd_pabrik',$id)->first();

      return view('master_apotek.data_pabrik.update_pabrik',['pabrik' => $pabrik]);
    }

    function proses_update_pabrik(Request $req){

      Pabrik::where('kd_pabrik',$req->kd_pabrik)->update([
        'nama_pabrik' => $req->nama_pabrik,
        'alamat' => $req->alamat,
        'kota' => $req->kota,
        'no_tlpn' => $req->no_tlpn,
        'no_hp' => $req->no_hp,
        'npwp' => $req->npwp,
        'status' => $req->status
      ]);

      return redirect('pabrik')->with('message', 'Update Berhasil');
    }

    function hapus_pabrik($id){

      Pabrik::where('kd_pabrik',$id)->delete();

      return redirect('pabrik')->with('message', 'Delete Berhasil');
    }
    //end crud pabrik

    //crud Supplier
    function tampil_supplier(){
      $supplier = Supplier::all();

      return view('master_apotek.data_supplier.data_supplier',['supplier' => $supplier]);
    }

    function tampil_detail_supplier($id){

      $supplier = Supplier::where('kd_supplier',$id)->first();

      return view('master_apotek.data_supplier.detail_supplier',['supplier' => $supplier]);
    }

    function tampil_tambah_supplier(){

      $kd_sup_max = Supplier::max('kd_supplier');

      $first_kd_sup = "SUP";
      $kd= (integer) substr($kd_sup_max,11);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) $date;
      $kd_supplier = "$first_kd_sup" .$date_value. sprintf("%04d",$kd);

      return view('master_apotek.data_supplier.tambah_supplier',['kd_supplier' => $kd_supplier]);
    }

    function proses_tambah_supplier(Request $req){

      $status = "aktif";

      $supplier = new Supplier;
      $supplier->kd_supplier = $req->kd_supplier;
      $supplier->nama_supplier = $req->nama_supplier;
      $supplier->alamat = $req->alamat;
      $supplier->kota = $req->kota;
      $supplier->no_tlpn = $req->no_tlpn;
      $supplier->no_hp = $req->no_hp;
      $supplier->no_rek = $req->no_rek;
      $supplier->npwp = $req->npwp;
      $supplier->status = $status;

      $supplier->save();

      return redirect('supplier')->with('message', 'Tambah Data Berhasil');;
    }

    function tampil_update_supplier($id){

      $supplier = Supplier::where('kd_supplier',$id)->first();

      return view('master_apotek.data_supplier.update_supplier',['supplier' => $supplier]);
    }

    function proses_update_supplier(Request $req){

      Supplier::where('kd_supplier',$req->kd_supplier)->update([
        'nama_supplier' => $req->nama_supplier,
        'alamat' => $req->alamat,
        'kota' => $req->kota,
        'no_tlpn' => $req->no_tlpn,
        'no_hp' => $req->no_hp,
        'no_rek' => $req->no_rek,
        'npwp' => $req->npwp,
        'status' => $req->status
      ]);

      return redirect('supplier')->with('message', 'Update Berhasil');
    }

    function hapus_supplier($id){

      Supplier::where('kd_supplier',$id)->delete();

      return redirect('supplier')->with('message', 'Delete Berhasil');
    }
    //end crud supplier

    //crud gudang
    function tampil_gudang(){
      $gudang = Gudang::all();

      return view('master_apotek.data_gudang.data_gudang',['gudang' => $gudang]);
    }

    function tampil_detail_gudang($id){

      $gudang = Gudang::where('kd_gudang',$id)->first();

      return view('master_apotek.data_gudang.detail_gudang',['gudang' => $gudang]);
    }

    function tampil_tambah_gudang(){

      $kd_gud_max = Gudang::max('kd_gudang');

      $first_kd_gud = "GUD";
      $kd= (integer) substr($kd_gud_max,11);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) $date;
      $kd_gudang = "$first_kd_gud" .$date_value. sprintf("%04d",$kd);

      return view('master_apotek.data_gudang.tambah_gudang',['kd_gudang' => $kd_gudang]);
    }

    function proses_tambah_gudang(Request $req){

      $status = "aktif";

      $gudang = new Gudang;
      $gudang->kd_gudang = $req->kd_gudang;
      $gudang->nama_gudang = $req->nama_gudang;
      $gudang->alamat = $req->alamat;
      $gudang->kota = $req->kota;
      $gudang->no_tlpn = $req->no_tlpn;
      $gudang->no_hp = $req->no_hp;
      $gudang->status = $status;

      $gudang->save();

      return redirect('gudang')->with('message', 'Tambah Data Berhasil');;
    }

    function tampil_update_gudang($id){

      $gudang = Gudang::where('kd_gudang',$id)->first();

      return view('master_apotek.data_gudang.update_gudang',['gudang' => $gudang]);
    }

    function proses_update_gudang(Request $req){

      Gudang::where('kd_gudang',$req->kd_gudang)->update([
        'nama_gudang' => $req->nama_gudang,
        'alamat' => $req->alamat,
        'kota' => $req->kota,
        'no_tlpn' => $req->no_tlpn,
        'no_hp' => $req->no_hp,
        'status' => $req->status
      ]);

      return redirect('gudang')->with('message', 'Update Berhasil');
    }

    function hapus_gudang($id){

      Gudang::where('kd_gudang',$id)->delete();

      return redirect('gudang')->with('message', 'Delete Berhasil');
    }
    //end crud gudang

    //crud gol_obat
    function tampil_gol_obat(){
      $gol_obat = Gol_obat::all();

      return view('master_apotek.data_gol_obat.data_gol_obat',['gol_obat' => $gol_obat]);
    }

    function tampil_tambah_gol_obat(){

      $kd_gol_max = Gol_obat::max('kd_gol_obat');

      $first_kd_gol = "GOL";
      $kd= (integer) substr($kd_gol_max,11);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) $date;
      $kd_gol_obat = "$first_kd_gol" .$date_value. sprintf("%04d",$kd);

      return view('master_apotek.data_gol_obat.tambah_gol_obat',['kd_gol_obat' => $kd_gol_obat]);
    }

    function proses_tambah_gol_obat(Request $req){

      $status = "aktif";

      $gol_obat = new Gol_obat;
      $gol_obat->kd_gol_obat = $req->kd_gol_obat;
      $gol_obat->nama_gol_obat = $req->nama_gol_obat;
      $gol_obat->keterangan = $req->keterangan;
      $gol_obat->status = $status;

      $gol_obat->save();

      return redirect('golongan-obat')->with('message', 'Tambah Data Berhasil');;
    }

    function tampil_update_gol_obat($id){

      $gol_obat = Gol_obat::where('kd_gol_obat',$id)->first();

      return view('master_apotek.data_gol_obat.update_gol_obat',['gol_obat' => $gol_obat]);
    }

    function proses_update_gol_obat(Request $req){

      Gol_obat::where('kd_gol_obat',$req->kd_gol_obat)->update([
        'nama_gol_obat' => $req->nama_gol_obat,
        'keterangan' => $req->keterangan,
        'status' => $req->status
      ]);

      return redirect('golongan-obat')->with('message', 'Update Berhasil');
    }

    function hapus_gol_obat($id){

      Gol_obat::where('kd_gol_obat',$id)->delete();

      return redirect('golongan-obat')->with('message', 'Delete Berhasil');
    }
    //end crud gol_obat


    //crud kat_obat
    function tampil_kat_obat(){
      $kat_obat = Kat_obat::all();

      return view('master_apotek.data_kat_obat.data_kat_obat',['kat_obat' => $kat_obat]);
    }

    function tampil_tambah_kat_obat(){

      $kd_kat_max = Kat_obat::max('kd_kat_obat');

      $first_kd_kat = "KAT-OB";
      $kd= (integer) substr($kd_kat_max,14);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) $date;
      $kd_kat_obat = "$first_kd_kat" .$date_value. sprintf("%04d",$kd);

      return view('master_apotek.data_kat_obat.tambah_kat_obat',['kd_kat_obat' => $kd_kat_obat]);
    }

    function proses_tambah_kat_obat(Request $req){

      $status = "aktif";

      $kat_obat = new Kat_obat;
      $kat_obat->kd_kat_obat = $req->kd_kat_obat;
      $kat_obat->nama_kat_obat = $req->nama_kat_obat;
      $kat_obat->keterangan = $req->keterangan;
      $kat_obat->status = $status;

      $kat_obat->save();

        return redirect('kategori-obat')->with('message', 'Tambah Data Berhasil');;
    }

      function tampil_update_kat_obat($id){

      $kat_obat = Kat_obat::where('kd_kat_obat',$id)->first();

      return view('master_apotek.data_kat_obat.update_kat_obat',['kat_obat' => $kat_obat]);
    }

    function proses_update_kat_obat(Request $req){

      Kat_obat::where('kd_kat_obat',$req->kd_kat_obat)->update([
        'nama_kat_obat' => $req->nama_kat_obat,
        'keterangan' => $req->keterangan,
        'status' => $req->status
      ]);

      return redirect('kategori-obat')->with('message', 'Update Berhasil');
    }

    function hapus_kat_obat($id){

      Kat_obat::where('kd_kat_obat',$id)->delete();

      return redirect('kategori-obat')->with('message', 'Delete Berhasil');
    }
    //end crud kat_obat

    //crud lok_obat
    function tampil_lok_obat(){
      $lok_obat = Lok_obat::all();

      return view('master_apotek.data_lok_obat.data_lok_obat',['lok_obat' => $lok_obat]);
    }

    function tampil_tambah_lok_obat(){

      $kd_lok_max = Lok_obat::max('kd_lok_obat');

      $first_kd_lok = "LOK-OB";
      $kd= (integer) substr($kd_lok_max,14);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) $date;
      $kd_lok_obat = "$first_kd_lok" .$date_value. sprintf("%04d",$kd);

      return view('master_apotek.data_lok_obat.tambah_lok_obat',['kd_lok_obat' => $kd_lok_obat]);
    }

    function proses_tambah_lok_obat(Request $req){

      $status = "aktif";

      $lok_obat = new Lok_obat;
      $lok_obat->kd_lok_obat = $req->kd_lok_obat;
      $lok_obat->nama_lok_obat = $req->nama_lok_obat;
      $lok_obat->status = $status;

      $lok_obat->save();

      return redirect('lokasi-obat')->with('message', 'Tambah Data Berhasil');;
    }

    function tampil_update_lok_obat($id){

      $lok_obat = Lok_obat::where('kd_lok_obat',$id)->first();

      return view('master_apotek.data_lok_obat.update_lok_obat',['lok_obat' => $lok_obat]);
    }

    function proses_update_lok_obat(Request $req){

      Lok_obat::where('kd_lok_obat',$req->kd_lok_obat)->update([
        'nama_lok_obat' => $req->nama_lok_obat,
        'status' => $req->status
      ]);

      return redirect('lokasi-obat')->with('message', 'Update Berhasil');
    }

    function hapus_lok_obat($id){

      Lok_obat::where('kd_lok_obat',$id)->delete();

      return redirect('lokasi-obat')->with('message', 'Delete Berhasil');
    }
    //end crud lok_obat

    //crud sat_obat
    function tampil_sat_obat(){
      $sat_obat = Sat_obat::all();

      return view('master_apotek.data_sat_obat.data_sat_obat',['sat_obat' => $sat_obat]);
    }

    function tampil_tambah_sat_obat(){

      $kd_sat_max = Sat_obat::max('kd_sat_obat');

      $first_kd_sat = "SAT-OB";
      $kd= (integer) substr($kd_sat_max,14);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) $date;
      $kd_sat_obat = "$first_kd_sat" .$date_value. sprintf("%04d",$kd);

      return view('master_apotek.data_sat_obat.tambah_sat_obat',['kd_sat_obat' => $kd_sat_obat]);
    }

    function proses_tambah_sat_obat(Request $req){

      $status = "aktif";

      $sat_obat = new Sat_obat;
      $sat_obat->kd_sat_obat = $req->kd_sat_obat;
      $sat_obat->nama_sat_obat = $req->nama_sat_obat;
      $sat_obat->status = $status;

      $sat_obat->save();

      return redirect('racikan')->with('message', 'Tambah Data Berhasil');;
    }

    function tampil_update_sat_obat($id){

      $sat_obat = Sat_obat::where('kd_sat_obat',$id)->first();

      return view('master_apotek.data_sat_obat.update_sat_obat',['sat_obat' => $sat_obat]);
    }

    function proses_update_sat_obat(Request $req){

      Sat_obat::where('kd_sat_obat',$req->kd_sat_obat)->update([
        'nama_sat_obat' => $req->nama_sat_obat,
        'status' => $req->status
      ]);

      return redirect('racikan')->with('message', 'Update Berhasil');
    }

    function hapus_sat_obat($id){

      Sat_obat::where('kd_sat_obat',$id)->delete();

      return redirect('racikan')->with('message', 'Delete Berhasil');
    }
    //end crud sat_obat

    //crud obat
    function tampil_obat(){
      $obat = DB::table('obat')
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->get();

      return view('master_apotek.data_obat.data_obat',['obat' => $obat]);
    }

    function tampil_detail_obat($id){
      $obat = DB::table('obat')->where('kd_obat','=',$id)
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->first();

      $satuan = DB::table('satuan')->where('kd_obat','=',$id)
                  ->join('satuan_obat', 'satuan.kd_sat_obat', '=', 'satuan_obat.kd_sat_obat')
                  ->join('lokasi_obat', 'satuan.kd_lok_obat', '=', 'lokasi_obat.kd_lok_obat')
                  ->get();


      return view('master_apotek.data_obat.detail_obat',['obat' => $obat, 'satuan' => $satuan]);
    }

    function tampil_tambah_obat(){

      $kd_obat_max = Obat::max('kd_obat');

      $first_kd_sat = "OBT";
      $kd= (integer) substr($kd_obat_max,11);
      $kd++;
      $date = date("Ymd");
      $date_value = (integer) $date;
      $kd_obat = "$first_kd_sat" .$date_value. sprintf("%04d",$kd);

      $pabrik = Pabrik::all();
      $gol_obat = Gol_obat::all();
      $kat_obat = Kat_obat::all();
      $sat_obat = Sat_obat::all();
      $lok_obat = Lok_obat::all();

      return view('master_apotek.data_obat.tambah_obat',[
        'kd_obat' => $kd_obat,
        'pabrik' => $pabrik,
        'gol_obat' => $gol_obat,
        'kat_obat' => $kat_obat,
        'sat_obat' => $sat_obat,
        'lok_obat' => $lok_obat
      ]);
    }

    function proses_tambah_obat(Request $req){


      $obat = new Obat;

      $status = "aktif";
      if($req->image != null){
        $imageName = time().'.'.$req->image->getClientOriginalExtension();
        $req->image->move(public_path('images'), $imageName);
        $obat->foto = $imageName;
      }

      $obat->kd_obat = $req->kd_obat;
      $obat->nama_obat = $req->nama_obat;
      $obat->kd_pabrik = $req->kd_pabrik;
      $obat->kd_gol_obat = $req->kd_gol_obat;
      $obat->kd_kat_obat = $req->kd_kat_obat;
      $obat->jenis_obat = $req->jenis_obat;
      $obat->minimal_stok = $req->minimal_stok;
      $obat->indikasi = $req->indikasi;
      $obat->kandungan = $req->kandungan;
      $obat->dosis = $req->dosis;
      $obat->checkbox = 'false';

      $obat->save();

      $satuan = new Satuan;
      $satuan->kd_obat = $req->kd_obat;
      $satuan->kd_sat_obat = $req->kd_sat_obat;
      $satuan->kd_lok_obat = $req->kd_lok_obat;
      $satuan->stok = $req->stok_obat;
      $satuan->hna = $req->hna;
      $satuan->barcode_obat = $req->barcode_obat;
      $satuan->no_batch = $req->no_batch;
      $satuan->tgl_expired = $req->tgl_expired;
      $satuan->harga_jual_1 = $req->hrg_jual_1;
      $satuan->harga_jual_2 = $req->hrg_jual_2;
      $satuan->harga_jual_3 = $req->hrg_jual_3;
      $satuan->diskon_harga_1 = $req->dis_hrg_1;
      $satuan->diskon_harga_2 = $req->dis_hrg_2;
      $satuan->diskon_harga_3 = $req->dis_hrg_3;

      $satuan->save();

      $jurnal = new JurnalKeuangan;
      $jurnal->tanggal = date('Y-m-d');
      $jurnal->kode_akun_kredit = "115001";
      if($req->jenis_obat = 'konsinyasi'){
        $jurnal->kode_akun_debit = "210002";        
      }else{
        $jurnal->kode_akun_debit = "210001";                
      }
      $jurnal->nobukti = "JL".$req->kd_sat_obat.''.$req->kd_obat;
      $jurnal->deskripsi = "Tambah Di Master Obat No Faktur. IMP".$req->kd_sat_obat.''.$req->kd_obat;
      $jurnal->nominal = $req->hna;
      $jurnal->created_by = Auth::id();
      $jurnal->save();


      return redirect('obat')->with('message', 'Tambah Data Berhasil');;
    }

    function tampil_update_obat($id){

      $obat = DB::table('obat')->where('kd_obat','=',$id)
                  ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
                  ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
                  ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
                  ->first();

      $satuan = DB::table('satuan')->where('kd_obat','=',$id)
                  ->join('satuan_obat', 'satuan.kd_sat_obat', '=', 'satuan_obat.kd_sat_obat')
                  ->join('lokasi_obat', 'satuan.kd_lok_obat', '=', 'lokasi_obat.kd_lok_obat')
                  ->get();


      $pabrik = Pabrik::all();
      $gol_obat = Gol_obat::all();
      $kat_obat = Kat_obat::all();
      $sat_obat = Sat_obat::all();
      $lok_obat = Lok_obat::all();

      return view('master_apotek.data_obat.update_obat',[
        'pabrik' => $pabrik,
        'gol_obat' => $gol_obat,
        'kat_obat' => $kat_obat,
        'sat_obat' => $sat_obat,
        'lok_obat' => $lok_obat,
        'obat' => $obat,
        'satuan' => $satuan
      ]);
    }

    function proses_update_obat(Request $req){
      if($req->imgae == null){
        Obat::where('kd_obat',$req->kd_obat)->update([
          'nama_obat' => $req->nama_obat,
          'kd_pabrik' => $req->kd_pabrik,
          'kd_gol_obat' => $req->kd_gol_obat,
          'kd_kat_obat' => $req->kd_kat_obat,
          'minimal_stok' => $req->minimal_stok,
          'indikasi' => $req->indikasi,
          'kandungan' => $req->kandungan,
          'jenis_obat' => $req->jenis_obat,          
          'dosis' => $req->dosis
        ]);
      }else{
        $imageName = time().'.'.$req->image->getClientOriginalExtension();
        $req->image->move(public_path('images'), $imageName);

        Obat::where('kd_obat',$req->kd_obat)->update([
          'nama_obat' => $req->nama_obat,
          'kd_pabrik' => $req->kd_pabrik,
          'kd_gol_obat' => $req->kd_gol_obat,
          'kd_kat_obat' => $req->kd_kat_obat,
          'minimal_stok' => $req->minimal_stok,
          'indikasi' => $req->indikasi,
          'kandungan' => $req->kandungan,
          'dosis' => $req->dosis,
          'foto' => $imageName
        ]);
      }

      Satuan::where('kd_satuan',$req->kd_satuan)->update([
        'kd_sat_obat' => $req->kd_sat_obat,
        'kd_lok_obat' => $req->kd_lok_obat,
        'barcode_obat' => $req->barcode_obat,
        'no_batch' => $req->no_batch,
        'tgl_expired' => $req->tgl_expired
      ]);


      return redirect('obat')->with('message', 'Update Berhasil');
    }

    function hapus_obat($id){


      $satuan = Satuan::where('kd_obat',$id)->get();
      foreach ($satuan as $key) {
        Satuan::where('kd_satuan',$key->kd_satuan)->delete();
      }
      Obat::where('kd_obat',$id)->delete();


      return redirect('obat')->with('message', 'Delete Berhasil');
    }
    //end crud obat

    //crud racikan
    function tampil_racikan(){
      $racikan = Racikan::all();

      return view('master_apotek.data_racikan.data_racikan',['racikan' => $racikan]);
    }

    function tampil_tambah_racikan(){

            $kd_obat = Racikan::max('kd_obat');

            $first_racikan = "RAC";
            $kd= (integer) substr($kd_obat,11);
            $kd++;
            $date = date("Ymd");
            $date_value = (integer) $date;
            $kd_racikan = "$first_racikan" .$date_value. sprintf("%04d",$kd);

            $kat_obat = Kat_obat::all();
            $obat= Obat::all();

            return view('master_apotek.data_racikan.tambah_racikan',[
              'kd_racikan' => $kd_racikan, 'kat_obat' => $kat_obat, 'obat' => $obat
            ]);
    }

    function proses_tambah_racikan(Request $req){
      $racikan = new Racikan;
      $racikan->kd_obat = $req->kd_racikan;
      $racikan->nama_obat = $req->nama_racikan;
      $racikan->kategori = $req->kd_kat_obat;
      $racikan->status = "active";

      $racikan->save();

      $obats = $req->kd_obat;

      for ($i=0; $i<count($obats); $i++){
          $obat = new Racikan_Obat;
          $obat->id_racikan = $racikan->kd_obat;
          $obat->id_obat = $obats[$i];
          $obat->save();
      }

      return redirect('racikan')->with('message', 'Tambah Data Berhasil');;
    }

    function tampil_update_racikan($id){

        $racikan = DB::table('racikan')->where('kd_obat','=',$id)->first();
        $kat_obat = Kat_obat::all();
        $obat= Obat::all();
        $rac_obat = Racikan_Obat::where('id_racikan', $id)->get();
        $r_obat = array();

        foreach ($rac_obat as $key => $value) {
          array_push($r_obat, $value->id_obat);          
        }

        return view('master_apotek.data_racikan.update_racikan',['racikan' => $racikan, 'kat_obat' => $kat_obat, 'obat' => $obat , 'r_obat' => $r_obat]);
    }

    function proses_update_racikan(Request $req){
      
      Racikan::where('kd_obat',$req->kd_racikan)->update([
        'nama_obat' => $req->nama_racikan,
        'kategori' => $req->kd_kat_obat
      ]);
      
      Racikan_Obat::where('id_racikan',$req->kd_racikan)->delete();
      $obats = $req->kd_obat;
      for ($i=0; $i<count($obats); $i++){
          $obat = new Racikan_Obat;
          $obat->id_racikan = $req->kd_racikan;
          $obat->id_obat = $obats[$i];
          $obat->save();
      }

      return redirect('racikan')->with('message', 'Update Berhasil');
    }

    function hapus_racikan($id){
      Racikan::where('kd_obat',$id)->delete();
      
      return redirect('racikan')->with('message', 'Delete Berhasil');
    }
    //end crud sat_obat

    //crud apoteker
    function tampil_apoteker(){
      $apoteker = Apoteker::all();

      return view('master_apotek.data_apoteker.data_apoteker',['apoteker' => $apoteker]);
    }

    function tampil_detail_apoteker($id){

      $apoteker = Apoteker::where('id_apoteker',$id)->first();

      return view('master_apotek.data_apoteker.detail_apoteker',['apoteker' => $apoteker]);
    }

    function tampil_tambah_apoteker(){

      return view('master_apotek.data_apoteker.tambah_apoteker');
    }

    function proses_tambah_apoteker(Request $req){

      $id_apo_max = Apoteker::max('id_apoteker');

      $first_id_apo = "APO-";
      $kd= (integer) substr($id_apo_max,4);
      $kd++;
      $id_apoteker = "$first_id_apo" . sprintf("%04d",$kd);

      $status = "aktif";

      $apoteker = new Apoteker;
      $apoteker->id_apoteker = $id_apoteker;
      $apoteker->nama_apoteker = $req->nama_apoteker;
      $apoteker->alamat = $req->alamat;
      $apoteker->kota = $req->kota;
      $apoteker->no_tlpn = $req->no_tlpn;
      $apoteker->no_stra = $req->no_stra;
      $apoteker->no_sik_sipa = $req->no_sik_sipa;
      $apoteker->email = $req->email;
      $apoteker->tanggal_mulai_tugas = $req->tgl_mulai;
      $apoteker->status = $status;

      $apoteker->save();

      return redirect('apoteker');
    }

    function tampil_update_apoteker($id){

      $apoteker = Apoteker::where('id_apoteker',$id)->first();

      return view('master_apotek.data_apoteker.update_apoteker',['apoteker' => $apoteker]);
    }

    function proses_update_apoteker(Request $req){

      Apoteker::where('id_apoteker',$req->id_apoteker)->update([
        'nama_apoteker' => $req->nama_apoteker,
        'alamat' => $req->alamat,
        'kota' => $req->kota,
        'no_tlpn' => $req->no_tlpn,
        'no_sik_sipa' => $req->no_sik_sipa,
        'no_stra' => $req->no_stra,
        'email' => $req->email,
        'tanggal_mulai_tugas' => $req->tgl_mulai,
        'status' => $req->status
      ]);

      return redirect('apoteker');
    }

    function hapus_apoteker($id){

      Apoteker::where('id_apoteker',$id)->delete();

      return redirect('apoteker')->with('message', 'Delete Success');
    }


    //crud pasien
    function tampil_pasien(){
      $pasien = Pasien::all();

      return view('master_apotek.data_pasien.data_pasien',['pasien' => $pasien]);
    }

    function tampil_detail_pasien($id){

      $pasien = Pasien::where('id_rm',$id)->first();

      return view('master_apotek.data_pasien.detail_pasien',['pasien' => $pasien]);
    }

    function tampil_tambah_pasien(){
      $id_pas_max = Pasien::max('id_rm');

      $first_id_pas = "PAS-";
      $kd= (integer) substr($id_pas_max,4);
      $kd++;
      $id_rm= "$first_id_pas" . sprintf("%04d",$kd);
      $kat_pasien = KategoriPasien::all();

      return view('master_apotek.data_pasien.tambah_pasien',['id_rm' => $id_rm, 'kat_pasien' => $kat_pasien]);
    }

    function proses_tambah_pasien(Request $req){

      $status = "aktif";

      $pasien = new Pasien;
      $pasien->id_rm = $req->id_rm;
      $pasien->nama_pasien = $req->nama_pasien;
      $pasien->gol_darah = $req->gol_darah;
      $pasien->jenis_kelamin = $req->jenis_kelamin;
      $pasien->alamat = $req->alamat;
      $pasien->kota = $req->kota;
      $pasien->umur = $req->umur;
      $pasien->no_tlpn = $req->no_tlpn;
      $pasien->status_perkawinan = $req->status_perkawinan;
      $pasien->pekerjaan = $req->pekerjaan;
      $pasien->nama_ibu = $req->nama_ibu;
      $pasien->nama_ayah = $req->nama_ayah;
      $pasien->no_kk = $req->no_kk;
      $pasien->email = $req->email;
      $pasien->pin_bbm = $req->pin_bbm;
      $pasien->alergi_obat = $req->alergi_obat;
      $pasien->tanggal_lahir = $req->tgl_lahir;
      $pasien->tanggal_registrasi = Carbon::now();
      $pasien->status = $status;

      $pasien->save();

      return redirect('pasien');
    }

    function tampil_update_pasien($id){

      $pasien = Pasien::where('id_rm',$id)->first();
      $kat_pasien = KategoriPasien::all();      

      return view('master_apotek.data_pasien.update_pasien',['pasien' => $pasien, 'kat_pasien' => $kat_pasien]);
    }

    function proses_update_pasien(Request $req){

      Pasien::where('id_rm',$req->id_pasien)->update([
        'nama_pasien' => $req->nama_pasien,
        'gol_darah' => $req->gol_darah,    
        'jenis_kelamin' => $req->jenis_kelamin,      
        'alamat' => $req->alamat,
        'kota' => $req->kota,
        'umur' => $req->umur,      
        'no_tlpn' => $req->no_tlpn,
        'status_perkawinan' => $req->status_perkawinan,      
        'pekerjaan' => $req->pekerjaan,
        'nama_ibu' => $req->nama_ibu,
        'nama_ayah' => $req->nama_ayah,
        'no_kk' => $req->no_kk,
        'email' => $req->email,
        'pin_bbm' => $req->pin_bbm,
        'alergi_obat' => $req->alergi_obat,
        'tanggal_lahir' => $req->tgl_lahir
      ]);

      return redirect('pasien');
    }

    function hapus_pasien($id){

      Pasien::where('id_rm',$id)->delete();

      return redirect('pasien')->with('message', 'Delete Success');
    }

    // Crud Kategori Pembeli
    function tampil_kat_pasien(){

      $data = KategoriPasien::join('users', 'users.id', '=', 'kategori_pasien.created_by')
                      ->get();

      return view('master_apotek.data_kat_pembeli.data_kat_pembeli',['data' => $data]);      
    }

    function tampil_tambah_kat_pasien(){
      return view('master_apotek.data_kat_pembeli.tambah_kat_pembeli');            
    }

    function proses_tambah_kat_pasien(Request $req){
      $kategori = new KategoriPasien();
      $kategori->created_by = Auth::id();
      $kategori->nama_kategori = $req->nama_kat_pembeli;
      $kategori->info_kategori = $req->keterangan;
      $kategori->harga_jual = $req->harga_jual;
      $kategori->disc_harga_jual = $req->harga_disc;
      $kategori->save();

      return redirect('kat-pasien');
    }

    function tampil_update_kat_pasien($id){
      $data = KategoriPasien::where('kd_kat_pasien', $id)->first();

      return view('master_apotek.data_kat_pembeli.update_kat_pembeli', ['data' => $data]);                  
    }

    function proses_update_kat_pasien(Request $req){

      KategoriPasien::where('kd_kat_pasien',$req->kd_kat_pasien)->update([
        'created_by' => Auth::id(),
        'nama_kategori' => $req->nama_kat_pembeli,
        'info_kategori' => $req->keterangan,
        'harga_jual' => $req->harga_jual,
        'disc_harga_jual' => $req->harga_disc
      ]);


      return redirect('kat-pasien');      
    }

    function hapus_kat_pasien($id){
      KategoriPasien::where('kd_kat_pasien',$id)->delete();

      return redirect('kat-pasien');            
    }
}
