<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\Input;
use App\Opname;
use DB;
use App\Obat;
use App\Apoteker;
use App\Dokter;
use App\Pasien;
use App\CetakBarcode;
use Response;
use App\PHPdate;
use App\Gol_obat;
use App\Pabrik;
use App\Kat_obat;
use App\Lok_obat;
use App\Sat_obat;
use App\Pembelian_obat;
use App\Penjualan_obat;
use App\HutangPembelian;
use App\HutangPembelianDetail;
use App\KonsinyasiPembelian;
use App\KonsinyasiPembelianDetail;
use App\PiutangPenjualan;
use App\PiutangPenjualanDetail;
use App\Satuan;
use App\HistoryHargaObat;
use App\Identitas;
use Illuminate\Support\Facades\Log;

class UtilitasController extends Controller
{
    public function indexImportExcel()
    {
      # code...

      return view('utilitas-apotek.import-excel.index');
    }
    public function indexImportExcelObat(Request $request)
    {
      # code...
      config(['excel.import.dates.columns' => [
        'tanggal_expired',
        'tanggal_expired_2',
        'tanggal_expired_3',
        'tanggal_expired_4',
    ]]);
      $path = Input::file('import_file')->getRealPath();
      $exc = Excel::filter('chunk')->formatDates(true)->load($path)->chunk(250, function($results)
      {
        foreach ($results as $res) {
          # code...
          $pabrik = Pabrik::where('nama_pabrik', $res->pabrik)->first();
          $gol = Gol_obat::where('nama_gol_obat', $res->golongan)->first();
          $kat = Kat_obat::where('nama_kat_obat', $res->kategori)->first();
          $lok = Lok_obat::where('nama_lok_obat', $res->lokasi)->first();
          if ($pabrik ==  null) {
            $kd_pab_max = Pabrik::max('kd_pabrik');
            $first_kd_pab = "PAB";
            $kd= (integer) substr($kd_pab_max,11);
            $kd++;
            $date = date("Ymd");
            $date_value = (integer) $date;
            $kd_pabrik = "$first_kd_pab" .$date_value. sprintf("%04d",$kd);
            $pabrik = new Pabrik;
            $pabrik->kd_pabrik = $kd_pabrik;
            $pabrik->nama_pabrik = $res->pabrik;
            $pabrik->save();
          }
          $pabrik = Pabrik::where('nama_pabrik', $res->pabrik)->first();
          $obat = new Obat;
          $obat->kd_obat = $res->kode_obat;
          $obat->nama_obat = $res->nama_obat;
          $obat->kd_pabrik = $pabrik->kd_pabrik;
          $obat->kd_gol_obat = $gol->kd_gol_obat;
          $obat->kd_kat_obat = $kat->kd_kat_obat;
          $obat->minimal_stok = $res->minimumstok;
          $obat->indikasi = $res->indikasi;
          $obat->kandungan = $res->kandungan;
          $obat->jenis_obat = $res->jenis_obat;
          $obat->save();

          if(isset($res->satuan)) {
            $sat = Sat_obat::where('nama_sat_obat', $res->satuan)->first();
            if ($sat !=null) {
              # code...
              $satuan = new Satuan;
              $satuan->kd_obat = $res->kode_obat;
              $satuan->kd_sat_obat = $sat->kd_sat_obat;
              $satuan->kd_lok_obat = $lok->kd_lok_obat;
              $satuan->stok = $res->stok_obat;
              $satuan->hna = $res->hpp;
              $satuan->barcode_obat = $res->barcode_obat;
              $satuan->no_batch = $res->no_batch;
              $satuan->tgl_expired = $res->tanggal_expired;
              $satuan->harga_jual_1 = $res->harga_jual_1;
              $satuan->harga_jual_2 = $res->harga_jual_2;
              $satuan->harga_jual_3 = $res->harga_jual_3;
              $satuan->diskon_harga_1 = $res->diskonharga_jual_1;
              $satuan->diskon_harga_2 = $res->diskonharga_jual_2;
              $satuan->diskon_harga_3 = $res->diskonharga_jual_3;
              $satuan->save();
            }
          }
          if(isset($res->satuan_2)) {

            $sat2 = Sat_obat::where('nama_sat_obat', $res->satuan_2)->first();
            if ($sat2 !=null) {
              # code...
              $satuan = new Satuan;
              $satuan->kd_obat = $res->kode_obat;
              $satuan->kd_sat_obat = $sat2->kd_sat_obat;
              $satuan->kd_lok_obat = $lok->kd_lok_obat;
              $satuan->stok = $res->stok_obatsatuan_2;
              $satuan->hna = $res->hpp_2;
              $satuan->barcode_obat = $res->barcode_obat_2;
              $satuan->no_batch = $res->no_batch_2;
              $satuan->tgl_expired = $res->tanggal_expired_2;
              $satuan->harga_jual_1 = $res->harga_jual_1_2;
              $satuan->harga_jual_2 = $res->harga_jual_2_2;
              $satuan->harga_jual_3 = $res->harga_jual_3_2;
              $satuan->diskon_harga_1 = $res->diskonharga_jual_1_2;
              $satuan->diskon_harga_2 = $res->diskonharga_jual_2_2;
              $satuan->diskon_harga_3 = $res->diskonharga_jual_3_2;
              $satuan->save();
            }
          }
          if(isset($res->satuan_3)) {
            $sat3 = Sat_obat::where('nama_sat_obat', $res->satuan_3)->first();
            if ($sat3 != null) {
              # code...
              $satuan = new Satuan;
              $satuan->kd_obat = $res->kode_obat;
              $satuan->kd_sat_obat = $sat3->kd_sat_obat;
              $satuan->kd_lok_obat = $lok->kd_lok_obat;
              $satuan->stok = $res->stok_obatsatuan_3;
              $satuan->hna = $res->hpp_3;
              $satuan->barcode_obat = $res->barcode_obat_3;
              $satuan->no_batch = $res->no_batch_3;
              $satuan->tgl_expired = $res->tanggal_expired_3;
              $satuan->harga_jual_1 = $res->harga_jual_1_3;
              $satuan->harga_jual_2 = $res->harga_jual_2_3;
              $satuan->harga_jual_3 = $res->harga_jual_3_3;
              $satuan->diskon_harga_1 = $res->diskonharga_jual_1_3;
              $satuan->diskon_harga_2 = $res->diskonharga_jual_2_3;
              $satuan->diskon_harga_3 = $res->diskonharga_jual_3_3;
              $satuan->save();
            }
          }
          if (isset($res->satuan_4)) {
            $sat4 = Sat_obat::where('nama_sat_obat', $res->satuan_4)->first();
            if ($sat4 != null) {
              # code...
              $satuan = new Satuan;
              $satuan->kd_obat = $res->kode_obat;
              $satuan->kd_sat_obat = $sat4->kd_sat_obat;
              $satuan->kd_lok_obat = $lok->kd_lok_obat;
              $satuan->stok = $res->stok_obatsatuan_4;
              $satuan->hna = $res->hpp_4;
              $satuan->barcode_obat = $res->barcode_obat_4;
              $satuan->no_batch = $res->no_batch_4;
              $satuan->tgl_expired = $res->tanggal_expired_4;
              $satuan->harga_jual_1 = $res->harga_jual_1_4;
              $satuan->harga_jual_2 = $res->harga_jual_2_4;
              $satuan->harga_jual_3 = $res->harga_jual_3_4;
              $satuan->diskon_harga_1 = $res->diskonharga_jual_1_4;
              $satuan->diskon_harga_2 = $res->diskonharga_jual_2_4;
              $satuan->diskon_harga_3 = $res->diskonharga_jual_3_4;
              $satuan->save();
            }

            $jurnal = new JurnalKeuangan;
            $jurnal->tanggal = date('Y-m-d');
            $jurnal->kode_akun_kredit = "115001";
            if($res->jenis_obat = 'konsinyasi'){
              $jurnal->kode_akun_debit = "210002";        
            }else{
              $jurnal->kode_akun_debit = "210001";                
            }
            $jurnal->nobukti = "JL".$res->kode_obat;
            $jurnal->deskripsi = "Tambah Di Master Obat No Faktur. IMP".$res->kode_obat;
            $jurnal->nominal = $res->hpp_4;
            $jurnal->created_by = Auth::id();
            $jurnal->save();
          }



        }

      });
      //return Response::make($exc);
      return redirect()->back();
    }

    public function indexImportExcelPabrik(Request $request)
    {
      # code...
      $path = Input::file('import_file')->getRealPath();
      Excel::filter('chunk')->load($path)->chunk(250, function($results)
      {
              foreach($results as $row)
              {
                if($row->kode){
                  DB::table('pabrik')->insert([
                    'kd_pabrik'=>$row->kode,
                    'nama_pabrik'=>$row->nama,
                    'alamat'=>$row->alamat,
                    'kota'=>$row->kota,
                    'no_tlpn'=>$row->telepon,
                    'no_hp'=>$row->no_hp,
                    'npwp'=>$row->npwp
                  ]);
                }else{
                  break;
                }
              }
      });
      return redirect()->back();
    }
    public function indexImportExcelSuplier(Request $request)
    {
      # code...
      $path = Input::file('import_file')->getRealPath();
      Excel::filter('chunk')->load($path)->chunk(250, function($results)
      {
              foreach($results as $row)
              {
                if($row->kode){
                  DB::table('supplier')->insert([
                    'kd_supplier'=>$row->kode,
                    'nama_supplier'=>$row->nama,
                    'alamat'=>$row->alamat,
                    'kota'=>$row->kota,
                    'no_tlpn'=>$row->telepon,
                    'no_hp'=>$row->no_hp,
                    'npwp'=>$row->npwp,
                    'no_rek'=>$row->no_rekening
                  ]);
                }else{
                  break;
                }
              }
      });
      return redirect()->back();
    }
    public function indexImportExcelGudang(Request $request)
    {
      # code...
      $path = Input::file('import_file')->getRealPath();
      Excel::filter('chunk')->load($path)->chunk(250, function($results)
      {
              foreach($results as $row)
              {
                if($row->kode){
                  DB::table('gudang')->insert([
                    'kd_gudang'=>$row->kode,
                    'nama_gudang'=>$row->nama,
                    'alamat'=>$row->alamat,
                    'kota'=>$row->kota,
                    'no_tlpn'=>$row->telepon,
                    'no_hp'=>$row->no_hp,
                  ]);
                }else{
                  break;
                }
              }
      });
      return redirect()->back();
    }
    public function indexImportExcelApoteker(Request $request)
    {
      # code...
      $path = Input::file('import_file')->getRealPath();
      Excel::filter('chunk')->load($path)->chunk(250, function($results)
      {
              foreach($results as $row){
                if($row->id_apoteker){
                  $date = date_format(date_create($row->tanggal_mulai_tugas),'Y-m-d');
                  $id_apo_max = Apoteker::max('id_apoteker');
                  $first_id_apo = "APO-";
                  $kd= (integer) substr($id_apo_max,4);
                  $kd++;
                  $id_apoteker = "$first_id_apo" . sprintf("%04d",$kd);
                  DB::table('apoteker')->insert([
                    'id_apoteker'=>$id_apoteker,
                    'nama_apoteker'=>$row->nama,
                    'no_sik_sipa'=>$row->no_sik_no_sipa,
                    'alamat'=>$row->alamat,
                    'kota'=>$row->kota,
                    'no_tlpn'=>$row->telepon,
                    'no_stra'=>$row->no_stra,
                    'email'=>$row->email,
                    'tanggal_mulai_tugas'=>$date,
                  ]);
                }else{
                  break;
                }
              }
      });
      return redirect()->back();
    }
    public function indexImportExcelPasien(Request $request)
    {
      # code...
      config(['excel.import.dates.columns' => [
        'tanggal_lahir',
        'tanggal_registrasi'
    ]]);
      $path = Input::file('import_file')->getRealPath();
      Excel::filter('chunk')->load($path)->formatDates(true)->chunk(250, function($results)
      {
              foreach($results as $row)
              {
                $id_pas_max = Pasien::max('id_rm');
                $first_id_pas = "PAS-";
                $kd= (integer) substr($id_pas_max,4);
                $kd++;
                $id_rm= "$first_id_pas" . sprintf("%04d",$kd);
                $pasien = new Pasien;
                $pasien->id_rm = $id_rm;
                $pasien->nama_pasien = $row->nama_;
                $pasien->gol_darah = $row->golongan_darah;
                $pasien->jenis_kelamin = $row->jenis_kelamin;
                $pasien->alamat = $row->alamat;
                $pasien->kota = $row->kota;
                $pasien->umur = $row->umur;
                $pasien->no_tlpn = $row->no_telepon;
                $pasien->status_perkawinan = $row->status_perkawinan;
                $pasien->pekerjaan = $row->pekerjaan;
                $pasien->nama_ibu = $row->nama_ibu;
                $pasien->nama_ayah = $row->nama_ayah;
                $pasien->no_kk = $row->no_kk;
                $pasien->email = $row->email;
                $pasien->pin_bbm = $row->pin_bbm;
                $pasien->alergi_obat = $row->alergi_obat;
                $pasien->tanggal_lahir = date_format(date_create($row->tanggal_lahir),'Y-m-d');
                $pasien->tanggal_registrasi= date_format(date_create($row->tanggal_registrasi),'Y-m-d');
                //$pasien->status = $status;

                $pasien->save();
              }
      });
      return redirect()->back();
    }
    public function indexHargaObat()
    {
      # code...
      $satuan = Satuan::Join('obat','obat.kd_obat','=','satuan.kd_obat')->Join('satuan_obat','satuan_obat.kd_sat_obat','=','satuan.kd_sat_obat')
      ->leftJoin('golongan_obat','golongan_obat.kd_gol_obat','=','obat.kd_gol_obat')
      ->leftJoin('kategori_obat','kategori_obat.kd_kat_obat','=','obat.kd_kat_obat')
      ->selectRaw('obat.nama_obat, satuan_obat.nama_sat_obat, golongan_obat.nama_gol_obat, kategori_obat.nama_kat_obat, obat.jenis_obat, satuan.*')
      ->get();
      return view('utilitas-apotek.harga-obat.index',['satuan'=>$satuan]);
    }
    public function editHargaObat(Request $request)
    {
      # code...
      $id = $request->check;
      $satuan = Satuan::join('obat', 'satuan.kd_obat', '=', 'obat.kd_obat')->whereIn('satuan.kd_satuan',$id)->get();
      return view('utilitas-apotek.harga-obat.edit',['satuan'=>$satuan,'sat1'=>$satuan]);
    }
    public function updateHargaObat(Request $request)
    {
      $kd = $request->kd_satuan;
      for ($i=0; $i <count($kd) ; $i++) {
        # code...
        $satuan = Satuan::find($kd[$i]);
        $satuan->hna = $request->harga_beli[$i];
        $satuan->harga_jual_1 = $request->harga_jual_1[$i];
        $satuan->harga_jual_2 = $request->harga_jual_2[$i];
        $satuan->harga_jual_3 = $request->harga_jual_3[$i];
        $satuan->diskon_harga_1 = $request->diskon_harga_1[$i];
        $satuan->diskon_harga_2 = $request->diskon_harga_2[$i];
        $satuan->diskon_harga_3 = $request->diskon_harga_3[$i];
        $satuan->save();

        $history = new HistoryHargaObat();
        $history->kd_obat = $satuan->kd_obat;
        $history->kd_sat_obat = $satuan->kd_sat_obat;
        $history->kd_lok_obat = $satuan->kd_lok_obat;
        $history->stok = $satuan->stok_obat;
        $history->hna = $satuan->hna;
        $history->barcode_obat = $satuan->barcode_obat;
        $history->no_batch = $satuan->no_batch;
        $history->tgl_expired = $satuan->tgl_expired;
        $history->harga_jual_1 = $satuan->harga_jual_1;
        $history->harga_jual_2 = $satuan->harga_jual_2;
        $history->harga_jual_3 = $satuan->harga_jual_3;
        $history->diskon_harga_1 = $satuan->diskon_harga_1;
        $history->diskon_harga_2 = $satuan->diskon_harga_2;
        $history->diskon_harga_3 = $satuan->diskon_harga_3;
        $history->save();
      }


      return redirect('ubah-harga-obat');
    }
    public function excelHargaObat()
    {
      # code...
      $excel = Excel::create('Data Harga Obat', function($excel) {
        
            // Set the title
          $excel->setTitle('DATA HARGA OBAT');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('DATA HARGA OBAT');

          $excel->sheet('Sheet1', function($sheet) {

              $data = Satuan::Join('obat','obat.kd_obat','=','satuan.kd_obat')->Join('satuan_obat','satuan_obat.kd_sat_obat','=','satuan.kd_sat_obat')
                ->leftJoin('golongan_obat','golongan_obat.kd_gol_obat','=','obat.kd_gol_obat')
                ->leftJoin('kategori_obat','kategori_obat.kd_kat_obat','=','obat.kd_kat_obat')
                ->selectRaw('obat.nama_obat, satuan_obat.nama_sat_obat, golongan_obat.nama_gol_obat, kategori_obat.nama_kat_obat, obat.jenis_obat, satuan.*')
                ->get();
              $ident = Identitas::first();
              
              $sheet->loadView('utilitas-apotek.harga-obat.export_harga_obat', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 7;
              $border = 'A6:O'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:O1');
              $sheet->mergeCells('A2:O2');
              $sheet->mergeCells('A3:O3');
              $sheet->mergeCells('A4:O4');
              $sheet->mergeCells('A5:O5');
              $sheet->getStyle('A1:O4')->applyFromArray(array(
                  'font' => array(
                      'name'      =>  'Calibri',
                      'size'      =>  12,
                      'bold'      =>  true
                  )          
              ));    
              $sheet->getStyle('A5')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }

    public function importExcelHargaObat(Request $request)
    {
      # code...
      config(['excel.import.dates.columns' => [
        'tanggal'
    ]]);
      $path = Input::file('import_file')->getRealPath();
      Excel::filter('chunk')->load($path)->formatDates(true)->chunk(250, function($results)
      {
              foreach($results as $row)
              {
                $satuan = new Satuan;
                $pasien->nama_pasien = $row->nama_;
                $pasien->gol_darah = $row->golongan_darah;
                $pasien->jenis_kelamin = $row->jenis_kelamin;
                $pasien->alamat = $row->alamat;
                $pasien->kota = $row->kota;
                $pasien->umur = $row->umur;
                $pasien->no_tlpn = $row->no_telepon;
                $pasien->status_perkawinan = $row->status_perkawinan;
                $pasien->pekerjaan = $row->pekerjaan;
                $pasien->nama_ibu = $row->nama_ibu;
                $pasien->nama_ayah = $row->nama_ayah;
                $pasien->no_kk = $row->no_kk;
                $pasien->email = $row->email;
                $pasien->pin_bbm = $row->pin_bbm;
                $pasien->alergi_obat = $row->alergi_obat;
                $pasien->tanggal_lahir = date_format(date_create($row->tanggal_lahir),'Y-m-d');
                $pasien->tanggal_registrasi= date_format(date_create($row->tanggal_registrasi),'Y-m-d');
                //$pasien->status = $status;

                $pasien->save();
              }
      });
      return redirect()->back();
    }

    public function indexStokOpname()
    {
      # code...
      $opname = Opname::Join('obat','obat.kd_obat','=','opname.kode_obat')
                  ->leftJoin('golongan_obat','golongan_obat.kd_gol_obat','=','obat.kd_gol_obat')
                ->leftJoin('kategori_obat','kategori_obat.kd_kat_obat','=','obat.kd_kat_obat')
                ->selectRaw('opname.*,obat.kd_obat, obat.nama_obat,golongan_obat.nama_gol_obat, kategori_obat.nama_kat_obat')
                  ->get();
      //return response($opname);
      return view('utilitas-apotek.stok-opname.index',['opname'=>$opname]);
    }
    public function deleteStokOpnameSatuan(Request $request,$id)
    {
      # code...
      $opname = Satuan::find($id);
      $opname->delete();
      //return response($opname);
      return redirect()->back();
    }
    public function updateStokOpnameSatuan(Request $request)
    {
      # code...
      $kd = $request->kd_satuan;

      for ($i=0; $i <count($kd) ; $i++) {
        # code...

        $opname = Satuan::find($kd[$i]);
        $opname->stok = $request->stok_baru[$i];
        $opname->save();
      }

      //return response($opname);
      return redirect('stok-opname');
    }
    public function editStokOpname(Request $request)
    {
      # code...
      //$opname = Obat::get();
      //return response($opname);
      $id = $request->check;   

      $sat = Satuan::whereIn('satuan.kd_obat', $id)->join('obat','obat.kd_obat','satuan.kd_obat')->join('satuan_obat','satuan_obat.kd_sat_obat','satuan.kd_sat_obat')->get();
      //return Response::make($sat);
      //echo $sat;
      return view('utilitas-apotek.stok-opname.edit',['sat'=>$sat,'sat1'=>$sat,'id']);
    }
    public function excelStokOpname()
    {
      # code...
      $excel = Excel::create('Data Stok Opname', function($excel) {
        
            // Set the title
          $excel->setTitle('DATA STOK OPNAME');
      
          // Chain the setters
          $excel->setCreator('Apotik')
                ->setCompany('Apotik');
      
          // Call them separately
          $excel->setDescription('DATA STOK OPNAME');

          $excel->sheet('Sheet1', function($sheet) {

              $data = Opname::Join('obat','obat.kd_obat','=','opname.kode_obat')
                  ->leftJoin('golongan_obat','golongan_obat.kd_gol_obat','=','obat.kd_gol_obat')
                ->leftJoin('kategori_obat','kategori_obat.kd_kat_obat','=','obat.kd_kat_obat')
                ->selectRaw('opname.*,obat.kd_obat, obat.nama_obat,golongan_obat.nama_gol_obat, kategori_obat.nama_kat_obat')
                  ->get();
              $ident = Identitas::first();
              
              $sheet->loadView('utilitas-apotek.stok-opname.export_stok_opname', ['data' => $data, 'identitas' => $ident]);
              $end = $data->count() + 2;
              $border = 'A2:G'.$end;
              $sheet->setBorder($border, 'thin');  
              $sheet->mergeCells('A1:G1');  
              $sheet->getStyle('A1')->applyFromArray(array(
                'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  16,
                    'bold'      =>  true
                )          
            ));              
          });
      
      });
      
      return $excel->export('xlsx');
    }
    public function importStokOpname(Request $request)
    {
      # code...
      $path = Input::file('import_file')->getRealPath();
      Excel::filter('chunk')->load($path)->chunk(250, function($results)
      {
          //print_r($results);
              foreach($results as $row)
              {
                DB::table('opname')->insert([
                  'kode_obat'=>$row->kode_obat,
                  'lokasi'=>$row->lokasi,
                  'gudang'=>$row->gudang,
                  'stok'=>$row->stok,
                  'stok_nyata'=>$row->stok,
                  'satuan'=>$row->satuan,
                  'keterangan'=>$row->keterangan,
                ]);
              }
      });
      return redirect()->back();


    }
    public function indexPiutangObat()
    {
      $data = Penjualan_obat::join('piutang_penjualan','penjualan_obat.no_faktur','piutang_penjualan.no_faktur')
        ->join('penjualan_pembayaran','penjualan_obat.no_faktur','penjualan_pembayaran.no_faktur')
        ->leftJoin('retur_penjualan_obat','penjualan_obat.no_faktur','retur_penjualan_obat.no_faktur')
        ->leftJoin('pasien', 'pasien.id_rm', 'penjualan_obat.id_rm')
        ->leftJoin('dokter', 'dokter.id_dokter', 'penjualan_obat.id_dokter')
        ->where('penjualan_pembayaran.jenis_pembayaran', 'kredit')
        ->selectRaw('penjualan_obat.tgl_faktur, penjualan_obat.no_faktur, penjualan_obat.no_resep,dokter.nama_dokter, pasien.nama_pasien, piutang_penjualan.*, retur_penjualan_obat.total as jumlah_retur ')
        ->get();

      return view('utilitas-apotek.pembayaran-piutang-obat.index', ['data' => $data]);
    }

    public function bayarPiutang($id)
    {
      $today = date('Y-m-d');
      
      $data = Penjualan_obat::join('piutang_penjualan','penjualan_obat.no_faktur','piutang_penjualan.no_faktur')
        ->leftJoin('retur_penjualan_obat','penjualan_obat.no_faktur','retur_penjualan_obat.no_faktur')
        ->leftJoin('pasien', 'pasien.id_rm', 'penjualan_obat.id_rm')
        ->leftJoin('dokter', 'dokter.id_dokter', 'penjualan_obat.id_dokter')
        ->selectRaw('penjualan_obat.tgl_faktur, penjualan_obat.no_faktur, penjualan_obat.no_resep,dokter.nama_dokter, pasien.nama_pasien, piutang_penjualan.*, retur_penjualan_obat.total as jumlah_retur ')
        ->where('piutang_penjualan.id_piutang', $id)
        ->get()->first();

      //Log::info(print_r($data, true));

      return view('utilitas-apotek.pembayaran-piutang-obat.bayar_piutang',['data'=>$data, 'today'=>$today]);
    }

    public function postBayarPiutang(Request $req)
    {
      
      $jumlah_bayar = 0;
      $sisa = 0;
      //get piutang with id
      $piutang = PiutangPenjualan::where('id_piutang', $req->id_piutang)->get()->first();
      
      if($req->jumlah_kartu == null){
        $req->jumlah_kartu = 0;
      }

      $jumlah_bayar = $piutang->jumlah_bayar + ($req->jumlah_tunai +$req->jumlah_kartu);

      $sisa = $piutang->total - $jumlah_bayar;

      PiutangPenjualan::where('id_piutang',$req->id_piutang)->update([
        'kekurangan' => $sisa,
        'jumlah_bayar' => $jumlah_bayar,
        'deadline' => $req->deadline,
      ]);

      $penjualan_piutang_detail = new PiutangPenjualanDetailn;
      $penjualan_piutang_detail->id_piutang = $req->id_piutang;
      $penjualan_piutang_detail->sisa_piutang = $sisa;
      $penjualan_piutang_detail->jenis_pembayaran = $req->jenis_pembayaran;
      $penjualan_piutang_detail->pembayaran_tunai = $req->jumlah_tunai;
      $penjualan_piutang_detail->pembayaran_kartu = $req->jumlah_kartu;
      $penjualan_piutang_detail->no_kartu = $req->no_kartu;
      $penjualan_piutang_detail->tunai = $req->tunai;
      $penjualan_piutang_detail->kartu = $req->kartu;
      $penjualan_piutang_detail->tanggal_pembayaran =  $req->tanggal_pembayaran;

      //Log::info(print_r($penjualan_piutang_detail, true));
      $penjualan_piutang_detail->save();

      return redirect('pembayaran-piutang-obat')->with('message', 'Tambah Data Pembayaran Piutang Berhasil');;
      
    }

    public function indexHutang()
    {
      $data = Pembelian_obat::where('pembelian_obat.jenis_pembayaran', 'HUTANG')
        ->join('hutang_pembelian','pembelian_obat.no_faktur','hutang_pembelian.no_faktur')
        ->leftJoin('retur_pembelian_obat','pembelian_obat.no_faktur','retur_pembelian_obat.no_faktur')
        ->leftJoin('supplier', 'supplier.kd_supplier', 'pembelian_obat.kd_supplier')
        ->selectRaw('pembelian_obat.tgl_faktur, pembelian_obat.no_faktur, pembelian_obat.no_po, supplier.nama_supplier, hutang_pembelian.*, retur_pembelian_obat.total as jumlah_retur ')
        ->get();

      return view('utilitas-apotek.pembayaran-hutang.index', ['data'=>$data]);
    }

    public function bayarHutang($id)
    {
      $today = date('Y-m-d');
      
      $data = Pembelian_obat::join('hutang_pembelian','pembelian_obat.no_faktur','hutang_pembelian.no_faktur')
        ->leftJoin('retur_pembelian_obat','pembelian_obat.no_faktur','retur_pembelian_obat.no_faktur')
        ->leftJoin('supplier', 'supplier.kd_supplier', 'pembelian_obat.kd_supplier')
        ->selectRaw('pembelian_obat.tgl_faktur, pembelian_obat.no_faktur, pembelian_obat.no_po, supplier.nama_supplier, hutang_pembelian.*, retur_pembelian_obat.total as jumlah_retur ')
        ->where('hutang_pembelian.id_hutang', $id)
        ->get()->first();

      //Log::info(print_r($data, true));

      return view('utilitas-apotek.pembayaran-hutang.bayar_hutang',['data'=>$data, 'today'=>$today]);
    }

    public function postBayarHutang(Request $req)
    {
      
      $jumlah_bayar = 0;
      $sisa = 0;
      //get hutang with id
      $hutang = HutangPembelian::where('id_hutang', $req->id_hutang)->get()->first();
      
      if($req->jumlah_kartu == null){
        $req->jumlah_kartu = 0;
      }

      $jumlah_bayar = $hutang->jumlah_bayar + ($req->jumlah_tunai +$req->jumlah_kartu);

      $sisa = $hutang->total - $jumlah_bayar;

      HutangPembelian::where('id_hutang',$req->id_hutang)->update([
        'sisa' => $sisa,
        'jumlah_bayar' => $jumlah_bayar,
        'tanggal_jatuh_tempo' => $req->tanggal_jatuh_tempo,
      ]);

      $pembelian_hutang_detail = new HutangPembelianDetail;
      $pembelian_hutang_detail->id_hutang = $req->id_hutang;
      $pembelian_hutang_detail->sisa_hutang = $sisa;
      $pembelian_hutang_detail->jenis_pembayaran = $req->jenis_pembayaran;
      $pembelian_hutang_detail->pembayaran_tunai = $req->jumlah_tunai;
      $pembelian_hutang_detail->pembayaran_kartu = $req->jumlah_kartu;
      $pembelian_hutang_detail->no_kartu = $req->no_kartu;
      $pembelian_hutang_detail->tunai = $req->tunai;
      $pembelian_hutang_detail->kartu = $req->kartu;
      $pembelian_hutang_detail->tanggal_pembayaran =  $req->tanggal_pembayaran;

      Log::info(print_r($pembelian_hutang_detail, true));
      $pembelian_hutang_detail->save();

      return redirect('pembayaran-hutang')->with('message', 'Tambah Data Pembayaran Hutang Berhasil');;

      
    }

    public function indexKonsinyasiObat()
    {
      # code...
      $data = Pembelian_obat::where('pembelian_obat.jenis_pembayaran', 'KONSINYASI')
        ->join('konsinyasi_pembelian','pembelian_obat.no_faktur','konsinyasi_pembelian.no_faktur')
        ->leftJoin('pembelian_detail','pembelian_detail.no_faktur','pembelian_obat.no_faktur')
        ->join('obat','pembelian_detail.kd_obat','obat.kd_obat')
        ->join('satuan','obat.kd_obat','satuan.kd_obat')
        ->join('satuan_obat', 'satuan_obat.kd_sat_obat', 'satuan.kd_sat_obat')
        ->leftJoin('retur_pembelian_obat','pembelian_obat.no_faktur','retur_pembelian_obat.no_faktur')
        ->leftJoin('supplier', 'supplier.kd_supplier', 'pembelian_obat.kd_supplier')
        ->selectRaw('pembelian_obat.tgl_faktur, pembelian_obat.no_faktur, supplier.nama_supplier, obat.nama_obat, konsinyasi_pembelian.*, satuan_obat.nama_sat_obat, retur_pembelian_obat.total as jumlah_retur, pembelian_detail.harga')
        ->get();


        Log::info(print_r($data, true));

      return view('utilitas-apotek.pembayaran-konsinyasi-obat.index', ['data'=> $data]);
    }

    public function returKonsinyasi($id)
    {
      $today = date('Y-m-d');
      
      $data = Pembelian_obat::join('konsinyasi_pembelian','pembelian_obat.no_faktur','konsinyasi_pembelian.no_faktur')
        ->leftJoin('retur_pembelian_obat','pembelian_obat.no_faktur','retur_pembelian_obat.no_faktur')
        ->leftJoin('supplier', 'supplier.kd_supplier', 'pembelian_obat.kd_supplier')
        ->leftJoin('pembelian_detail','pembelian_detail.no_faktur','konsinyasi_pembelian.no_faktur')
        ->join('obat','pembelian_detail.kd_obat','obat.kd_obat')
        ->join('satuan','obat.kd_obat','satuan.kd_obat')
        ->join('satuan_obat', 'satuan_obat.kd_sat_obat', 'satuan.kd_sat_obat')
        ->where('konsinyasi_pembelian.id_konsinyasi', $id)
        ->selectRaw('pembelian_obat.tgl_faktur, pembelian_obat.no_faktur, supplier.nama_supplier, konsinyasi_pembelian.*, satuan_obat.nama_sat_obat')
        ->get()->first();

      //Log::info(print_r($data, true));

      return view('utilitas-apotek.pembayaran-konsinyasi-obat.retur_konsinyasi',['data'=>$data, 'today'=>$today]);
    }

    public function postReturKonsinyasi(Request $req)
    {
      
      $jumlah_retur = 0;
      $sisa = 0;

      //get konsinyasi with id
      $konsinyasi = KonsinyasiPembelian::where('id_konsinyasi', $req->id_konsinyasi)->get()->first();

      $sisa = $konsinyasi->sisa - $req->jumlah_retur;
      $retur = $konsinyasi->retur + $req->jumlah_retur;

      KonsinyasiPembelian::where('id_konsinyasi',$req->id_konsinyasi)->update([
        'sisa' => $sisa,
        'retur' => $retur
      ]);

      $pembelian_konsinyasi_detail = new KonsinyasiPembelianDetail;
      $pembelian_konsinyasi_detail->id_konsinyasi = $req->id_konsinyasi;
      $pembelian_konsinyasi_detail->sisa_retur = $sisa;
      $pembelian_konsinyasi_detail->jumlah_retur = $req->jumlah_retur;

      Log::info(print_r($pembelian_konsinyasi_detail, true));
      $pembelian_konsinyasi_detail->save();

      return redirect('pembayaran-konsinyasi-obat')->with('message', 'Tambah Data Pembayaran Konsinyasi Berhasil');;
    }

    public function indexSalinanResep()
    {
      # code...\
      $obat = Obat::all();
      $penjualan_obat = Penjualan_obat::all();
      $racikan = DB::table('racikan')->get();
      $apoteker = Apoteker::all();
      $dokter = Dokter::all();
      $pasien = Pasien::all();
      $p_obat = Pembelian_obat::all();
      $date = date('d-m-Y');

      return view('utilitas-apotek.salinan-resep.index',['obat'=>$obat, 'penjualan_obat'=>$penjualan_obat, 'racikan'=>$racikan, 'apoteker' => $apoteker, 'dokter' => $dokter, 'pasien' => $pasien, 'date' => $date]);
    }
    public function racikanSalinanResep(Request $request)
    {
      # code...\
      //$obat = Obat::all();
      $racikan = DB::table('racikan')->whereIn('kd_obat',$request->racikan)->get();
      return Response::make($racikan);
      //return view('utilitas-apotek.salinan-resep.index',['obat'=>$obat,'racikan'=>$racikan]);
    }
    public function obatSalinanResep(Request $request)
    {
      # code...\
      //$obat = Obat::all();
      $racikan = Obat::whereIn('kd_obat',$request->obt)->get();
      return Response::make($racikan);
      //return view('utilitas-apotek.salinan-resep.index',['obat'=>$obat,'racikan'=>$racikan]);
    }
    public function indexBarcode($value='')
    {
      # code...
      $obat = CetakBarcode::join('obat', 'obat.kd_obat', '=', 'cetak_barcode.kd_obat')
              ->join('satuan','satuan.kd_obat','=','obat.kd_obat')
              ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')        
              ->join('satuan_obat','satuan_obat.kd_sat_obat','=','satuan.kd_sat_obat')
              ->get();

      return view('utilitas-apotek.barcode.index',['obat'=>$obat]);
    }

    public function barcodeObat(){
        $obat = DB::table('obat')
        ->join('pabrik', 'obat.kd_pabrik', '=', 'pabrik.kd_pabrik')
        ->join('satuan','satuan.kd_obat','=','obat.kd_obat')
        ->join('satuan_obat','satuan_obat.kd_sat_obat','=','satuan.kd_sat_obat')        
        ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')
        ->join('golongan_obat', 'obat.kd_gol_obat', '=', 'golongan_obat.kd_gol_obat')
        ->join('lokasi_obat', 'lokasi_obat.kd_lok_obat', '=', 'satuan.kd_lok_obat')
        ->get();


      return view('utilitas-apotek.barcode.data_obat',['obat' => $obat]);
    }

    public function addBarcode($id){
        $barcode = new CetakBarcode;
        $barcode->kd_obat = $id;
        $barcode->save();
    }

    public function removeBarcode($id){
        $barcode = CetakBarcode::where('kd_obat', $id);
        $barcode->forceDelete();
    }

    public function destroyBarcode($id){
        $barcode = CetakBarcode::where('kd_obat', $id);
        $barcode->forceDelete();

        $obat = CetakBarcode::join('obat', 'obat.kd_obat', '=', 'cetak_barcode.kd_obat')
        ->join('satuan','satuan.kd_obat','=','obat.kd_obat')
        ->join('kategori_obat', 'obat.kd_kat_obat', '=', 'kategori_obat.kd_kat_obat')        
        ->join('satuan_obat','satuan_obat.kd_sat_obat','=','satuan.kd_sat_obat')
        ->get();


        return view('utilitas-apotek.barcode.index',['obat'=>$obat]);      
    }

    public function generateBarcode(Request $request)
    {
      # code..
      $data = $request->barcode;
      $obats = array();
      for ($i=0; $i < count($data); $i++) { 
        $obat = new \stdClass();
        $obat->barcode = $data[$i];
        $obat->count = $request->count[$i];        
        array_push($obats, $obat);
      }

      // print_r($obats);
      return view('utilitas-apotek.barcode.barcode',['obats' => $obats]);
    }
}
