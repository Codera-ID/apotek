<?php

namespace App\Http\Controllers;

use App\Dokter;
use Illuminate\Http\Request;

class MasterklinikController extends Controller
{
    function tampil_dokter(){
      $dokter = Dokter::all();

      return view('master_klinik.data_dokter',['dokter' => $dokter]);
    }

    function tampil_detail_dokter($id){

      $dokter = Dokter::where('id_dokter',$id)->first();

      return view('master_klinik.detail_dokter',['dokter' => $dokter]);
    }

    function tampil_tambah_dokter(){

      return view('master_klinik.tambah_dokter');
    }

    function proses_tambah_dokter(Request $req){

      $id_dok_max = Dokter::max('id_dokter');

      $first_id_dok = "DOK-";
      $kd= (integer) substr($id_dok_max,4);
      $kd++;
      $id_dokter = "$first_id_dok" . sprintf("%04d",$kd);

      $status = "aktif";

      $dokter = new Dokter;
      $dokter->id_dokter = $id_dokter;
      $dokter->nama_dokter = $req->nama_dokter;
      $dokter->alamat = $req->alamat;
      $dokter->kota = $req->kota;
      $dokter->no_tlpn = $req->no_tlpn;
      $dokter->spesialis = $req->spesialis_dokter;
      $dokter->email = $req->email;
      $dokter->tanggal_mulai_tugas = $req->tgl_mulai;
      $dokter->status = $status;

      $dokter->save();

      return redirect('dokter');
    }

    function tampil_update_dokter($id){

      $dokter = Dokter::where('id_dokter',$id)->first();

      return view('master_klinik.update_dokter',['dokter' => $dokter]);
    }

    function proses_update_dokter(Request $req){

      Dokter::where('id_dokter',$req->id_dokter)->update([
        'nama_dokter' => $req->nama_dokter,
        'alamat' => $req->alamat,
        'kota' => $req->kota,
        'no_tlpn' => $req->no_tlpn,
        'spesialis' => $req->spesialis_dokter,
        'email' => $req->email,
        'tanggal_mulai_tugas' => $req->tgl_mulai,
        'status' => $req->status
      ]);

      return redirect('dokter');
    }

    function hapus_dokter($id){

      Dokter::where('id_dokter',$id)->delete();

      return redirect('dokter')->with('message', 'Delete Success');
    }
}
