<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Satuan extends Model
{
    protected $table = 'satuan';
    protected $primaryKey = 'kd_satuan';
    public function morp()
    {
        return $this->morphTo();
    }
    public function satobt($value='')
    {
      # code...
    $this->hasOne('App\Sat_obat','kd_sat_obat','kd_sat_obat');
    }
    
}
