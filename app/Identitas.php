<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Identitas extends Model
{
  protected $table = 'identitas_apotek';
  public $timestamps = false;
  protected $primaryKey = 'id';
}
