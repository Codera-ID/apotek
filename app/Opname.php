<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opname extends Model
{
    protected $table = 'opname';
    public $timestamps = false;
    protected $primaryKey = 'id_opname';

    public function obt()
    {
        return $this->hasManyThrough('App\Obat','App\Satuan','kd_obat','kd_obat','kode_obat','kd_obat');
    }
    public function gud()
    {
      # code...
      return $this->hasOne('App\Opname','kode_obat','kd_obat');
    }
}
