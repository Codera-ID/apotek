<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AkunKeuangan extends Model
{
  protected $table = 'akun_keuangan';
  public $timestamps = false;
  protected $primaryKey = 'kode_akun';
}
