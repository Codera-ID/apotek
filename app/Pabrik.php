<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pabrik extends Model
{
  protected $table = 'pabrik';

  public function obat()
    {
        return $this->hasMany('App\Obat');
    }
}
