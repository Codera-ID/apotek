<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriPasien extends Model
{
    protected $table = 'kategori_pasien';
}
