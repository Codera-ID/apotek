<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kat_obat extends Model
{
  protected $table = 'kategori_obat';

  public function obat()
    {
        return $this->hasMany('App\Obat');
    }
}
