<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MutasiKonsinyasiObat extends Model
{
    protected $table = 'mutasi_konsinyasi_obat';
    
    public function obat()
    {
        return $this->belongsTo('App\Obat','kd_obat', 'kd_obat');        
    }

}
