<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Route::get('home','HomeController@index');
Route::get('get-data-penjualan','HomeController@get_data_penjualan_obat');
Route::get('get-data-pembelian','HomeController@get_data_pembelian_obat');
Route::get('get-data-obat-terlaris','HomeController@get_data_obat_terlaris');
Route::get('data_user','HomeController@data');

/*----------------------------------Keuangan----------------------------------*/
//akun-keuangan
Route::get('akun','KeuanganController@indexAkun');
Route::get('laporan-keuangan','KeuanganController@indexLapKeuangan');
Route::get('akun/tambah-akun','KeuanganController@createAkun');
Route::post('akun/tambah-akun/post','KeuanganController@postAkun');
Route::get('akun/hapus-akun/{id}','KeuanganController@deleteAkun');
Route::get('akun/ubah-akun/{id}','KeuanganController@ubahAkun');
Route::post('akun/proses-akun','KeuanganController@prosesAkun');

Route::get('kelompok-akun','KeuanganController@getPosAkun');
Route::get('saldo','KeuanganController@getsaldo');
//jurnal-singkat
Route::get('jurnal-singkat','KeuanganController@indexJurnalSingkat');
Route::get('jurnal-singkat/tambah-jurnal-singkat','KeuanganController@tambahJurnalSingkat');
Route::get('jurnal-singkat/hapus/{id}','KeuanganController@hapusJurnalSingkat');
Route::post('jurnal-singkat/tambah-jurnal-singkat','KeuanganController@postJurnalSingkat');
//jurnal-Keuangan
Route::get('jurnal-keuangan','KeuanganController@indexJurnalKeuangan');
Route::get('jurnal-keuangan/tambah-jurnal-keuangan','KeuanganController@tambahJurnalKeuangan');
Route::post('jurnal-keuangan/tambah-jurnal-keuangan','KeuanganController@prosesJurnalKeuangan');
Route::get('jurnal-keuangan/get-short-jurnal','KeuanganController@getShortJurnal');
Route::get('jurnal-keuangan/hapus/{id}','KeuanganController@hapusJurnalKeuangan');
//laporan-jurnal-keuangan
Route::get('laporan-jurnal-keuangan','KeuanganController@indexLapJurnalKeuangan');
//laporan-buku-besar
Route::get('laporan-buku-besar','KeuanganController@indexLapBukuBesar');
//laporan-laba
Route::get('laporan-laba','KeuanganController@indexLapLaba');
//laporan-penambahan-modal
Route::get('laporan-penambahan-modal','KeuanganController@indexLapPenambahanModal');
//laporan-pajak
Route::get('laporan-pajak','KeuanganController@indexLapPajak');
//laporan-neraca
Route::get('laporan-neraca','KeuanganController@indexLapNeraca');
//laporan-tipe-akun
Route::get('laporan-tipe-akun','KeuanganController@indexLapTipeAkun');
//laporan-detail-akun
Route::get('laporan-detail-akun','KeuanganController@indexLapTipeAkun');

/*----------------------------------Sistem----------------------------------*/

//identitas
Route::get('identitas','SistemController@indexIdentitas');
Route::post('identitas/{id}','SistemController@postIdentitas');

//pengaturan-aplikasi
Route::get('pengaturan-aplikasi','SistemController@indexPengaturanApp');
Route::post('pengaturan-aplikasi/simpan-cetak-faktur','SistemController@simpanCetakFaktur');
Route::post('pengaturan-aplikasi/simpan-notifikasi','SistemController@simpanNotifikasi');
Route::post('pengaturan-aplikasi/simpan-tuslah-embalase','SistemController@simpanTuslahEmbalase');
Route::post('pengaturan-aplikasi/simpan-harga-ppn','SistemController@simpanHargaPpn');

//data-group-user
Route::get('data-group-user','SistemController@indexGrupUser');
Route::get('data-group-user/tambah','SistemController@tambahGrupUser');
Route::post('data-group-user/tambah','SistemController@postGrupUser');
Route::get('data-group-user/edit/{id}','SistemController@editGrupUser');
Route::post('data-group-user/edit/{id}','SistemController@updateGrupUser');
Route::post('data-group-user/delete/{id}','SistemController@deleteGrupUser');

//data-user
Route::get('data-user','SistemController@indexUser');
Route::get('data-user/tambah','SistemController@tambahUser');
Route::post('data-user/tambah','SistemController@postUser');
Route::get('data-user/edit/{id}','SistemController@editUser');
Route::post('data-user/edit/{id}','SistemController@updateUser');
Route::post('data-user/delete/{id}','SistemController@deleteUser');

//data-jadwal-shift
Route::get('data-jadwal-shift','SistemController@indexJadwalShift');
Route::get('data-jadwal-shift/tambah','SistemController@tambahJadwalShift');
Route::post('data-jadwal-shift/tambah','SistemController@postJadwalShift');
Route::get('data-jadwal-shift/edit/{id}','SistemController@editJadwalShift');
Route::post('data-jadwal-shift/edit/{id}','SistemController@updateJadwalShift');
Route::post('data-jadwal-shift/delete/{id}','SistemController@deleteJadwalShift');

//pergantian-shift
Route::get('pergantian-shift','SistemController@indexPergantianShift');
Route::post('tutup-shift','SistemController@tutupShift');

/*----------------------------------Utilitas Apootek----------------------------------*/
//Import excel
Route::get('import-excel','UtilitasController@indexImportExcel');
Route::post('import-excel/obat','UtilitasController@indexImportExcelObat');
Route::post('import-excel/pabrik','UtilitasController@indexImportExcelPabrik');
Route::post('import-excel/suplier','UtilitasController@indexImportExcelSuplier');
Route::post('import-excel/gudang','UtilitasController@indexImportExcelGudang');
Route::post('import-excel/apoteker','UtilitasController@indexImportExcelApoteker');
Route::post('import-excel/pasien','UtilitasController@indexImportExcelPasien');
//Ubah Harga Obat
Route::get('ubah-harga-obat','UtilitasController@indexHargaObat');
Route::get('ubah-harga-obat/edit','UtilitasController@editHargaObat');
Route::post('ubah-harga-obat/edit','UtilitasController@updateHargaObat');
Route::get('ubah-harga-obat/excel','UtilitasController@excelHargaObat');

//Stok Opname
Route::get('stok-opname','UtilitasController@indexStokOpname');
Route::get('stok-opname/edit','UtilitasController@editStokOpname');
Route::post('stok-opname/import','UtilitasController@importStokOpname');
Route::post('/stok-opname/delete/{id}','UtilitasController@deleteStokOpnameSatuan');
Route::post('/stok-opname/edit','UtilitasController@updateStokOpnameSatuan');
Route::get('stok-opname/excel','UtilitasController@excelStokOpname');

//pembayaran-piutang-obat
Route::get('pembayaran-piutang-obat','UtilitasController@indexPiutangObat');
Route::get('pembayaran-piutang-obat/{id}','UtilitasController@bayarPiutang');
Route::post('pembayaran-piutang-obat/bayar-piutang','UtilitasController@postBayarPiutang');

//pembayaran-hutang
Route::get('pembayaran-hutang','UtilitasController@indexHutang');
Route::get('pembayaran-hutang/{id}','UtilitasController@bayarHutang');
Route::post('pembayaran-hutang/bayar-hutang','UtilitasController@postBayarHutang');

//pembayaran-konsinyasi-obat
Route::get('pembayaran-konsinyasi-obat','UtilitasController@indexKonsinyasiObat');
Route::get('pembayaran-konsinyasi-obat/{id}','UtilitasController@returKonsinyasi');
Route::post('pembayaran-konsinyasi-obat/retur-konsinyasi','UtilitasController@postReturKonsinyasi');

//pembayaran-hutang
Route::get('salinan-resep','UtilitasController@indexSalinanResep');
Route::get('salinan-resep/data-racikan','UtilitasController@racikanSalinanResep');
Route::get('salinan-resep/data-obat','UtilitasController@obatSalinanResep');
//----------------------------Laporan Transaksi----------------------------------
//purchase order
Route::get('data-purchase-order','LaporanTransaksiController@indexPO');

Route::get('barcode','UtilitasController@indexBarcode');
Route::get('barcode/obat','UtilitasController@barcodeObat');
Route::get('barcode/addobat/{id}','UtilitasController@addBarcode');
Route::get('barcode/removeobat/{id}','UtilitasController@removeBarcode');
Route::get('barcode/destroybarcode/{id}','UtilitasController@destroyBarcode');
Route::get('barcode/generate','UtilitasController@generateBarcode');

//master_klinik
Route::get('dokter','MasterklinikController@tampil_dokter');
Route::get('dokter/detail/{id}','MasterklinikController@tampil_detail_dokter');
Route::get('dokter/tambah','MasterklinikController@tampil_tambah_dokter');
Route::post('dokter/tambah','MasterklinikController@proses_tambah_dokter');
Route::get('dokter/ubah/{id}','MasterklinikController@tampil_update_dokter');
Route::post('dokter/ubah','MasterklinikController@proses_update_dokter');
Route::get('dokter/hapus/{id}','MasterklinikController@hapus_dokter');

//master_apotek
    //apoteker
    Route::get('apoteker','MasterapotekController@tampil_apoteker');
    Route::get('apoteker/detail/{id}','MasterapotekController@tampil_detail_apoteker');
    Route::get('apoteker/tambah','MasterapotekController@tampil_tambah_apoteker');
    Route::post('apoteker/tambah','MasterapotekController@proses_tambah_apoteker');
    Route::get('apoteker/ubah/{id}','MasterapotekController@tampil_update_apoteker');
    Route::post('apoteker/ubah','MasterapotekController@proses_update_apoteker');
    Route::get('apoteker/hapus/{id}','MasterapotekController@hapus_apoteker');

    //pasien
    Route::get('pasien','MasterapotekController@tampil_pasien');
    Route::get('pasien/detail/{id}','MasterapotekController@tampil_detail_pasien');
    Route::get('pasien/tambah','MasterapotekController@tampil_tambah_pasien');
    Route::post('pasien/tambah','MasterapotekController@proses_tambah_pasien');
    Route::get('pasien/ubah/{id}','MasterapotekController@tampil_update_pasien');
    Route::post('pasien/ubah','MasterapotekController@proses_update_pasien');
    Route::get('pasien/hapus/{id}','MasterapotekController@hapus_pasien');

    //kategori pasien
    Route::get('kat-pasien','MasterapotekController@tampil_kat_pasien');
    Route::get('kat-pasien/tambah','MasterapotekController@tampil_tambah_kat_pasien');
    Route::post('kat-pasien/tambah','MasterapotekController@proses_tambah_kat_pasien');
    Route::get('kat-pasien/ubah/{id}','MasterapotekController@tampil_update_kat_pasien');
    Route::post('kat-pasien/ubah','MasterapotekController@proses_update_kat_pasien');
    Route::get('kat-pasien/hapus/{id}','MasterapotekController@hapus_kat_pasien');

    //pabrik
    Route::get('pabrik','MasterapotekController@tampil_pabrik');
    Route::get('pabrik/detail/{id}','MasterapotekController@tampil_detail_pabrik');
    Route::get('pabrik/tambah','MasterapotekController@tampil_tambah_pabrik');
    Route::post('pabrik/tambah','MasterapotekController@proses_tambah_pabrik');
    Route::get('pabrik/ubah/{id}','MasterapotekController@tampil_update_pabrik');
    Route::post('pabrik/ubah','MasterapotekController@proses_update_pabrik');
    Route::get('pabrik/hapus/{id}','MasterapotekController@hapus_pabrik');
    //supplier
    Route::get('supplier','MasterapotekController@tampil_supplier');
    Route::get('supplier/detail/{id}','MasterapotekController@tampil_detail_supplier');
    Route::get('supplier/tambah','MasterapotekController@tampil_tambah_supplier');
    Route::post('supplier/tambah','MasterapotekController@proses_tambah_supplier');
    Route::get('supplier/ubah/{id}','MasterapotekController@tampil_update_supplier');
    Route::post('supplier/ubah','MasterapotekController@proses_update_supplier');
    Route::get('supplier/hapus/{id}','MasterapotekController@hapus_supplier');
    //gudang
    Route::get('gudang','MasterapotekController@tampil_gudang');
    Route::get('gudang/detail/{id}','MasterapotekController@tampil_detail_gudang');
    Route::get('gudang/tambah','MasterapotekController@tampil_tambah_gudang');
    Route::post('gudang/tambah','MasterapotekController@proses_tambah_gudang');
    Route::get('gudang/ubah/{id}','MasterapotekController@tampil_update_gudang');
    Route::post('gudang/ubah','MasterapotekController@proses_update_gudang');
    Route::get('gudang/hapus/{id}','MasterapotekController@hapus_gudang');
    //golongan-obat
    Route::get('golongan-obat','MasterapotekController@tampil_gol_obat');
    Route::get('golongan-obat/tambah','MasterapotekController@tampil_tambah_gol_obat');
    Route::post('golongan-obat/tambah','MasterapotekController@proses_tambah_gol_obat');
    Route::get('golongan-obat/ubah/{id}','MasterapotekController@tampil_update_gol_obat');
    Route::post('golongan-obat/ubah','MasterapotekController@proses_update_gol_obat');
    Route::get('golongan-obat/hapus/{id}','MasterapotekController@hapus_gol_obat');
    //kategori-obat
    Route::get('kategori-obat','MasterapotekController@tampil_kat_obat');
    Route::get('kategori-obat/tambah','MasterapotekController@tampil_tambah_kat_obat');
    Route::post('kategori-obat/tambah','MasterapotekController@proses_tambah_kat_obat');
    Route::get('kategori-obat/ubah/{id}','MasterapotekController@tampil_update_kat_obat');
    Route::post('kategori-obat/ubah','MasterapotekController@proses_update_kat_obat');
    Route::get('kategori-obat/hapus/{id}','MasterapotekController@hapus_kat_obat');
    //lokasi-obat
    Route::get('lokasi-obat','MasterapotekController@tampil_lok_obat');
    Route::get('lokasi-obat/tambah','MasterapotekController@tampil_tambah_lok_obat');
    Route::post('lokasi-obat/tambah','MasterapotekController@proses_tambah_lok_obat');
    Route::get('lokasi-obat/ubah/{id}','MasterapotekController@tampil_update_lok_obat');
    Route::post('lokasi-obat/ubah','MasterapotekController@proses_update_lok_obat');
    Route::get('lokasi-obat/hapus/{id}','MasterapotekController@hapus_lok_obat');
    //satuan-obat
    Route::get('satuan-obat','MasterapotekController@tampil_sat_obat');
    Route::get('satuan-obat/tambah','MasterapotekController@tampil_tambah_sat_obat');
    Route::post('satuan-obat/tambah','MasterapotekController@proses_tambah_sat_obat');
    Route::get('satuan-obat/ubah/{id}','MasterapotekController@tampil_update_sat_obat');
    Route::post('satuan-obat/ubah','MasterapotekController@proses_update_sat_obat');
    Route::get('satuan-obat/hapus/{id}','MasterapotekController@hapus_sat_obat');
    //racikan
    Route::get('racikan','MasterapotekController@tampil_racikan');
    Route::get('racikan/obat','MasterapotekController@tampil_obat_racikan_obat');
    Route::get('racikan/tambah','MasterapotekController@tampil_tambah_racikan');
    Route::post('racikan/tambah','MasterapotekController@proses_tambah_racikan');
    Route::get('racikan/ubah/{id}','MasterapotekController@tampil_update_racikan');
    Route::post('racikan/ubah','MasterapotekController@proses_update_racikan');
    Route::get('racikan/hapus/{id}','MasterapotekController@hapus_racikan');
    //obat
    Route::get('obat','MasterapotekController@tampil_obat');
    Route::get('obat/detail/{id}','MasterapotekController@tampil_detail_obat');
    Route::get('obat/tambah','MasterapotekController@tampil_tambah_obat');
    Route::post('obat/tambah','MasterapotekController@proses_tambah_obat');
    Route::get('obat/ubah/{id}','MasterapotekController@tampil_update_obat');
    Route::post('obat/ubah','MasterapotekController@proses_update_obat');
    Route::get('obat/hapus/{id}','MasterapotekController@hapus_obat');

//Transaksi Apotek
  //purchaes_order
  Route::get('po','TransaksiapotekController@tampil_purchaes_order');
  Route::get('po/obat','TransaksiapotekController@tampil_obat_purchaes_order');
  Route::get('po/obat/addobat/{id}','TransaksiapotekController@add_obat_session');
  Route::get('po/obat/destroyobat/{id}','TransaksiapotekController@destroy_obat_session');
  Route::get('po/obat/cekobat','TransaksiapotekController@cek_obat_session');
  Route::get('po/obat/cekhargasatuan/{id}','TransaksiapotekController@cek_harga_satuan');
  Route::get('po/obat/hapuscheckbox/{id}','TransaksiapotekController@hapuscheckbox_po');
  Route::post('po/tambah','TransaksiapotekController@tambah_po');
  Route::get('po/data-hapus','TransaksiapotekController@tampil_hapus_po');
  Route::get('po/data-hapus/hapus/{id}','TransaksiapotekController@proses_hapus_po');
  
  //pembelian obat
  Route::get('pembelian-obat','TransaksiapotekController@tampil_pembelian_obat');
  Route::post('pembelian-obat/tambah','TransaksiapotekController@tambah_pembelian_obat');
  Route::get('pembelian-obat/obat/hapuspo/{id}','TransaksiapotekController@hapus_po_pembelian_obat');
  Route::get('pembelian-obat/obat/hapuscheckbox/{id}','TransaksiapotekController@hapuscheckbox_pembelian_obat');
  Route::get('pembelian-obat/po/{id}','TransaksiapotekController@tampil_pembelian_obat_po_id');
  Route::get('pembelian-obat/po','TransaksiapotekController@tampil_pembelian_obat_po');
  Route::get('pembelian-obat/obat','TransaksiapotekController@tampil_obat_pemebelian_obat');
  Route::get('pembelian-obat/obat/konsinyasi','TransaksiapotekController@tampil_pembelian_konsinyasi_obat');
  Route::get('pembelian-obat/obat/addobat/{id}','TransaksiapotekController@add_obat_session');
  Route::get('pembelian-obat/obat/destroyobat/{id}','TransaksiapotekController@destroy_obat_session');
  Route::get('pembelian-obat/obat/cekobat','TransaksiapotekController@cek_obat_session');
  Route::get('pembelian-obat/obat/cekhargasatuan/{id}','TransaksiapotekController@cek_harga_satuan');
  Route::get('pembelian-obat/data-hapus','TransaksiapotekController@tampil_hapus_pembelian_obat');
  Route::get('pembelian-obat/data-hapus/{id}','TransaksiapotekController@proses_hapus_pembelian_obat');
  
  //retur pembelian
  Route::get('retur-pembelian/update_retur/{id}','TransaksiapotekController@update_retur');
  Route::get('retur-pembelian/hapus_obat_retur/{id}/{obat}','TransaksiapotekController@hapus_obat_retur');
  Route::get('retur-pembelian','TransaksiapotekController@tampil_retur_pembelian_obat');
  Route::get('retur-pembelian/tambah/{id}','TransaksiapotekController@tampil_tambah_retur_id');
  Route::post('retur-pembelian/tambah/retur','TransaksiapotekController@proses_tambah_retur');
  Route::get('retur-pembelian/pembelian-obat','TransaksiapotekController@tampil_retur_pembelian');
  Route::get('retur-pembelian/data-hapus','TransaksiapotekController@tampil_hapus_retur_pembelian');
   Route::get('retur-pembelian/data-hapus/{id}','TransaksiapotekController@proses_hapus_retur_pembelian');
  
  //penjualan obat
  Route::get('penjualan-obat','TransaksiapotekController@tampil_penjualan_obat');
  Route::post('penjualan-obat/tambah','TransaksiapotekController@tambah_penjualan_obat');
  Route::post('penjualan-obat/tampil-proses-pembayaran','TransaksiapotekController@tampil_proses_pembayaran_penjualan');
  Route::post('penjualan-obat/proses-pembayaran','TransaksiapotekController@proses_pembayaran_penjualan');
  Route::get('penjualan-obat/tambah-obat','TransaksiapotekController@tampil_obat_penjualan_obat');
  Route::get('penjualan-obat/tambah-resep','TransaksiapotekController@tambah_resep_penjualan');
    Route::get('penjualan-obat/obat/hapus-obat/{id}','TransaksiapotekController@hapuscheckbox_penjualan_obat');
  Route::get('penjualan-obat/pasien/detail/{id}','TransaksiapotekController@cek_diskon_pasien');
  Route::get('penjualan-obat/obat/addobat/{id}','TransaksiapotekController@add_obat_session');
  Route::get('penjualan-obat/obat/destroyobat/{id}','TransaksiapotekController@destroy_obat_session');
  Route::get('penjualan-obat/obat/cekobat','TransaksiapotekController@cek_obat_session');
  Route::get('penjualan-obat/obat/cekhargasatuan/{id}','TransaksiapotekController@cek_harga_satuan');
  Route::get('penjualan-obat/data-hapus','TransaksiapotekController@tampil_hapus_penjualan_obat');
  Route::get('penjualan-obat/data-hapus/{id}','TransaksiapotekController@proses_hapus_penjualan_obat');
  
  //retur penjualan
  Route::get('retur-penjualan/update_retur/{id}','TransaksiapotekController@update_retur_penjualan');
  Route::get('retur-penjualan/hapus_obat_retur/{id}/{obat}','TransaksiapotekController@hapus_obat_retur_penjualan');
  Route::get('retur-penjualan','TransaksiapotekController@tampil_retur_penjualan_obat');
  Route::get('retur-penjualan/tambah/{id}','TransaksiapotekController@tampil_tambah_retur_penjualan_id');
  Route::get('retur-penjualan/tambah/','TransaksiapotekController@tampil_tambah_retur_penjualan');
  Route::post('retur-penjualan/tambah/retur','TransaksiapotekController@proses_tambah_retur_penjualan');
  Route::get('retur-penjualan/penjualan-obat','TransaksiapotekController@tampil_retur_penjualan');
  Route::get('retur-penjualan/data-hapus','TransaksiapotekController@tampil_hapus_retur_penjualan');
   Route::get('retur-penjualan/data-hapus/{id}','TransaksiapotekController@proses_hapus_retur_penjualan');
  

  //Mutasi Obat
  Route::get('mutasi-obat','TransaksiapotekController@tampil_mutasi_obat');
  Route::get('mutasi-obat/tambah','TransaksiapotekController@tambah_mutasi_obat');
  Route::post('mutasi-obat/tambah-mutasi','TransaksiapotekController@tambah_mutasi');
  Route::get('mutasi-obat/hapus-obat/{id}','TransaksiapotekController@hapuscheckbox_mutasi');
  Route::get('mutasi-obat/obat','TransaksiapotekController@tampil_obat_mutasi');
  Route::get('mutasi-obat/obat/addobat/{id}','TransaksiapotekController@add_obat_session');
  Route::get('mutasi-obat/obat/destroyobat/{id}','TransaksiapotekController@destroy_obat_session');
  Route::get('mutasi-obat/obat/cekobat','TransaksiapotekController@cek_obat_session');


  //Mutasi Konsinyasi
  Route::get('mutasi-konsinyasi-obat','TransaksiapotekController@tampil_mutasi_konsinyasi');
  Route::get('mutasi-konsinyasi-obat/tambah-mutasi','TransaksiapotekController@tambah_mutasi_konsinyasi');
  Route::post('mutasi-konsinyasi-obat/tambah','TransaksiapotekController@proses_tambah_mutasi_konsinyasi');

  //laporan master
  Route::get('laporan-data-pabrik','LaporanapotekController@laporan_data_pabrik');
  Route::get('cetak-pabrik','LaporanapotekController@cetak_data_pabrik');  
  Route::get('export-pabrik','LaporanapotekController@export_data_pabrik');    
  Route::get('laporan-data-supplier','LaporanapotekController@laporan_data_supplier');
  Route::get('cetak-supplier','LaporanapotekController@cetak_data_supplier');  
  Route::get('export-supplier','LaporanapotekController@export_data_supplier');
  Route::get('laporan-data-gudang','LaporanapotekController@laporan_data_gudang');
  Route::get('cetak-gudang','LaporanapotekController@cetak_data_gudang');  
  Route::get('export-gudang','LaporanapotekController@export_data_gudang');
  Route::get('laporan-data-apoteker','LaporanapotekController@laporan_data_apoteker');
  Route::get('cetak-apoteker','LaporanapotekController@cetak_data_apoteker');  
  Route::get('export-apoteker','LaporanapotekController@export_data_apoteker');
  Route::get('laporan-data-pasien','LaporanapotekController@laporan_data_pasien');
  Route::get('cetak-pasien','LaporanapotekController@cetak_data_pasien');  
  Route::get('export-pasien','LaporanapotekController@export_data_pasien');
  Route::get('laporan-data-stok-obat','LaporanapotekController@laporan_data_stok_obat');
  Route::get('cetak-stok-obat','LaporanapotekController@cetak_data_stok_obat');
   Route::get('export-stok-obat','LaporanapotekController@export_data_stok_obat'); 
  Route::get('laporan-data-obat-expired','LaporanapotekController@laporan_data_obat_expired');
  Route::get('cetak-obat-expired','LaporanapotekController@cetak_data_obat_expired');  
  Route::get('export-obat-expired','LaporanapotekController@export_data_obat_expired');
  Route::get('laporan-data-obat-habis','LaporanapotekController@laporan_data_obat_habis');
  Route::get('cetak-obat-habis','LaporanapotekController@cetak_data_obat_habis');  
  Route::get('export-obat-habis','LaporanapotekController@export_data_obat_habis');
  Route::get('laporan-data-perubahan-harga','LaporanapotekController@laporan_data_perubahan_harga_obat');
  Route::get('cetak-perubahan-harga','LaporanapotekController@cetak_data_perubahan_harga_obat');  
  Route::get('export-perubahan-harga','LaporanapotekController@export_data_perubahan_harga_obat');
  Route::get('laporan-data-stok-harga','LaporanapotekController@laporan_data_stok_harga_obat');
  Route::get('cetak-stok-harga-obat','LaporanapotekController@cetak_data_stok_harga_obat');  
  Route::get('export-stok-harga-obat','LaporanapotekController@export_data_stok_harga_obat');
  Route::get('laporan-data-mutasi-obat','LaporanapotekController@laporan_data_mutasi_obat');
  Route::get('cetak-stok-mutasi-obat/{$start_date}/{$end_date}','LaporanapotekController@cetak_data_mutasi_obat');  
  Route::get('export-stok-mutasi-obat/{$start_date}/{$end_date}','LaporanapotekController@export_data_mutasi_obat');
  Route::get('export-kartu-stok-obat/{id}','LaporanapotekController@export_data_mutasi_obat');
  Route::get('laporan-data-obat-konsinyasi','LaporanapotekController@laporan_data_obat_konsinyasi');
  Route::get('cetak-obat-konsinyasi','LaporanapotekController@cetak_data_obat_konsinyasi');  
  Route::get('export-obat-konsinyasi','LaporanapotekController@export_data_obat_konsinyasi');

  //Laporan transaksi
  Route::get('laporan-data-purchase-order','LaporanTransaksiController@laporan_data_purchase_order'); 
  Route::get('laporan-data-purchase-order/obat/{id}','LaporanTransaksiController@laporan_obat_purchase_order');
  Route::get('cetak-purchase-order','LaporanTransaksiController@cetak_purchase_order');
  Route::get('cetak-detail-purchase-order','LaporanTransaksiController@cetak_detail_purchase_order');
  Route::get('cetak-purchase-order/faktur/{id}','LaporanTransaksiController@cetak_faktur_purchase_order');
  Route::get('export-purchase-order','LaporanTransaksiController@export_purchase_order');
  Route::get('export-detail-purchase-order','LaporanTransaksiController@export_detail_purchase_order');
  
  Route::get('laporan-data-pembelian-obat','LaporanTransaksiController@laporan_data_pembelian_obat');
  Route::get('cetak-pembelian-obat/{start}/{end}','LaporanTransaksiController@cetak_pembelian_obat');
  Route::get('cetak-detail-pembelian-obat/{start}/{end}','LaporanTransaksiController@cetak_detail_pembelian_obat');
  Route::get('export-pembelian-obat/{start}/{end}','LaporanTransaksiController@export_pembelian_obat');
  Route::get('export-detail-pembelian-obat/{start]/{end}','LaporanTransaksiController@export_detail_pembelian_obat'); 
  Route::get('laporan-data-retur-pembelian','LaporanTransaksiController@laporan_data_retur_pembelian');
  Route::get('export-retur-pembelian-obat/{start}/{end}','LaporanTransaksiController@export_retur_pembelian_obat');
  Route::get('export-detail-retur-pembelian/{start]/{end}','LaporanTransaksiController@export_detail_retur_pembelian_obat'); 
  Route::get('cetak-retur-pembelian-obat/{start}/{end}','LaporanTransaksiController@cetak_retur_pembelian_obat');
  Route::get('cetak-detail-retur-pembelian-obat/{start}/{end}','LaporanTransaksiController@cetak_detail_retur_pembelian_obat');
  
  Route::get('laporan-data-penjualan-obat','LaporanTransaksiController@laporan_data_penjualan_obat');
  Route::get('laporan-data-penjualan-obat/obat/{id}','LaporanTransaksiController@laporan_obat_penjualan_obat');
  Route::get('laporan-data-retur-penjualan','LaporanTransaksiController@laporan_data_retur_penjualan');
  Route::get('laporan-data-retur-penjualan/obat/{retur}/{faktur}','LaporanTransaksiController@laporan_obat_retur_penjualan');
  Route::get('laporan-data-statistik-penjualan','LaporanTransaksiController@laporan_data_statistik_penjualan');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
