-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2018 at 06:59 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apotik`
--

-- --------------------------------------------------------

--
-- Table structure for table `akses_menu`
--

CREATE TABLE `akses_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `akses_menu`
--

INSERT INTO `akses_menu` (`id`, `menu`) VALUES
(1, 'Master Klinik'),
(2, 'Master Apotek'),
(3, 'Transaksi Apotek'),
(4, 'Utilitas Apotek'),
(5, 'Laporan Master Apotek'),
(6, 'Laporan Transaksi Apotek'),
(7, 'Keuangan'),
(8, 'Sistem');

-- --------------------------------------------------------

--
-- Table structure for table `akun_keuangan`
--

CREATE TABLE `akun_keuangan` (
  `kode_akun` int(11) NOT NULL,
  `nama_akun` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_pos_akun` int(11) NOT NULL,
  `saldo_normal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `akun_keuangan`
--

INSERT INTO `akun_keuangan` (`kode_akun`, `nama_akun`, `kode_pos_akun`, `saldo_normal`, `status`) VALUES
(110001, 'Kas Umum', 110, 'Debit', 'Aktif'),
(110002, 'Kas Pusat', 110, 'Debit', 'Aktif'),
(110003, 'Kas Kecil', 110, 'Debit', 'Aktif'),
(111001, 'Bank BRI', 111, 'Debit', 'Aktif'),
(111002, 'Bank Mandiri', 111, 'Debit', 'Aktif'),
(111003, 'Bank BNI', 111, 'Debit', 'Aktif'),
(111004, 'Bank Danamon', 111, 'Debit', 'Aktif'),
(111005, 'Bank BCA', 111, 'Debit', 'Aktif'),
(111006, 'Bank BII', 111, 'Debit', 'Aktif'),
(111007, 'Bank Panin', 111, 'Debit', 'Aktif'),
(115001, 'Nilai Obat', 115, 'Debit', 'Aktif'),
(210001, 'Hutang Pembelian Obat', 210, 'Kredit', 'Aktif'),
(210002, 'Hutang Konsinyasi Obat', 210, 'Kredit', 'Aktif'),
(310001, 'Modal Obat', 310, 'Kredit', 'Aktif'),
(310002, 'Ekuitas Pemilik', 310, 'Kredit', 'Aktif'),
(311002, 'Laba Ditahan', 311, 'Kredit', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `apoteker`
--

CREATE TABLE `apoteker` (
  `id_apoteker` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_apoteker` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_sik_sipa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_tlpn` bigint(20) NOT NULL,
  `no_stra` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_mulai_tugas` date NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `apoteker`
--

INSERT INTO `apoteker` (`id_apoteker`, `nama_apoteker`, `no_sik_sipa`, `alamat`, `kota`, `no_tlpn`, `no_stra`, `email`, `tanggal_mulai_tugas`, `status`, `created_at`, `updated_at`) VALUES
('APO-0001', 'septian', NULL, 'cantikan tengah gg.v no 47', 'surabaya', 85788810004, NULL, 'sbronky@gmail.com', '2016-04-01', 'active', NULL, NULL),
('APO-0002', 'septian zulkarnain', NULL, 'cantian pinggir GG.V no.47', 'Surabaya', 87851277445, NULL, 'sbronky1@gmail.com', '2016-04-01', 'active', NULL, NULL),
('APO-0003', 'septian zulkarnain latif', NULL, 'cantian pinggir GG.V no.47', 'Surabaya', 87851277445, NULL, 'sbronky1@gmail.com', '2016-04-01', 'active', NULL, NULL),
('APO-0004', 'anwar', '12345678', 'jl. laikang', 'makassar', 85242313222, '10', 'emmankchelsea@ymail.com', '2015-02-20', 'aktif', '2017-12-04 17:52:10', '2017-12-04 17:52:10');

-- --------------------------------------------------------

--
-- Table structure for table `cetak_barcode`
--

CREATE TABLE `cetak_barcode` (
  `id` int(11) NOT NULL,
  `kd_obat` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cetak_barcode`
--

INSERT INTO `cetak_barcode` (`id`, `kd_obat`, `created_at`, `updated_at`) VALUES
(19, 'OBT1614120001', '2017-12-10 16:26:55', '2017-12-10 16:26:55');

-- --------------------------------------------------------

--
-- Table structure for table `daftar_shift`
--

CREATE TABLE `daftar_shift` (
  `id` int(11) NOT NULL,
  `saldo_awal` int(11) NOT NULL DEFAULT '0',
  `hasil_penjualan` int(11) NOT NULL DEFAULT '0',
  `pembayaran_piutang` int(11) NOT NULL DEFAULT '0',
  `jurnal_masuk` int(11) NOT NULL DEFAULT '0',
  `total_pendapatan` int(11) NOT NULL DEFAULT '0',
  `retur_penjualan` int(11) NOT NULL DEFAULT '0',
  `jurnal_keluar` int(11) NOT NULL DEFAULT '0',
  `total_pengeluaran` int(11) NOT NULL DEFAULT '0',
  `saldo_akhir` int(11) NOT NULL DEFAULT '0',
  `saldo_kasir` int(11) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_retur_pembelian`
--

CREATE TABLE `detail_retur_pembelian` (
  `no_retur_pembelian_detail` int(10) UNSIGNED NOT NULL,
  `no_retur_pembelian` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jmlh` int(11) NOT NULL,
  `kd_satuan` int(11) NOT NULL,
  `harga_rata_rata` double(8,2) NOT NULL,
  `potongan` bigint(20) NOT NULL,
  `total` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_retur_pembelian`
--

INSERT INTO `detail_retur_pembelian` (`no_retur_pembelian_detail`, `no_retur_pembelian`, `kd_obat`, `jmlh`, `kd_satuan`, `harga_rata_rata`, `potongan`, `total`, `created_at`, `updated_at`) VALUES
(4, 'RET1711270001', 'OBT201711230001', 1, 1, 25000.00, 0, 25000, '2017-11-27 07:36:58', '2017-11-27 07:36:58'),
(5, 'RET1711270001', 'OBT201711240002', 1, 3, 5000.00, 0, 5000, '2017-11-27 07:36:58', '2017-11-27 07:36:58'),
(4, 'RET1711270001', 'OBT201711230001', 1, 1, 25000.00, 0, 25000, '2017-11-27 07:36:58', '2017-11-27 07:36:58'),
(5, 'RET1711270001', 'OBT201711240002', 1, 3, 5000.00, 0, 5000, '2017-11-27 07:36:58', '2017-11-27 07:36:58');

-- --------------------------------------------------------

--
-- Table structure for table `detail_retur_penjualan`
--

CREATE TABLE `detail_retur_penjualan` (
  `no_retur_penjualan_detail` int(10) UNSIGNED NOT NULL,
  `no_retur_penjualan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jmlh` int(11) NOT NULL,
  `kd_satuan` int(11) NOT NULL,
  `harga_rata_rata` double(8,2) NOT NULL,
  `potongan` bigint(20) NOT NULL,
  `total` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `detail_retur_penjualan`
--

INSERT INTO `detail_retur_penjualan` (`no_retur_penjualan_detail`, `no_retur_penjualan`, `kd_obat`, `jmlh`, `kd_satuan`, `harga_rata_rata`, `potongan`, `total`, `created_at`, `updated_at`) VALUES
(1, 'RET1712040001', 'OBT1614120001', 1, 27, 21560.00, 2, 21560, '2017-12-04 10:14:36', '2017-12-04 10:14:36'),
(1, 'RET1712040001', 'OBT1614120001', 1, 27, 21560.00, 2, 21560, '2017-12-04 10:14:36', '2017-12-04 10:14:36');

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

CREATE TABLE `dokter` (
  `id_dokter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_dokter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_tlpn` bigint(20) NOT NULL,
  `spesialis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_mulai_tugas` date NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dokter`
--

INSERT INTO `dokter` (`id_dokter`, `nama_dokter`, `alamat`, `kota`, `no_tlpn`, `spesialis`, `email`, `tanggal_mulai_tugas`, `status`, `created_at`, `updated_at`) VALUES
('DOK-0001', 'Jihad', 'Babakan Tarogong', 'Bandung', 81931382191, 'Jantung', 'balgohum@gmail.com', '2017-11-16', 'Aktif', '2017-11-21 17:33:02', '2017-12-17 17:04:56'),
('DOK-0002', 'Jihad', 'Jauh', 'Bandung', 9192102, 'Jantung', 'balgohum@gmail.com', '2017-01-01', 'Aktif', '2017-11-21 19:03:47', '2017-12-17 17:05:31'),
('DOK-0003', 'dr.caya', 'jl. skarda', 'makassar', 85216670649, 'anak', 'jukukalotoro2015@gmail.com', '2009-12-20', 'Non Aktif', '2017-12-03 17:54:02', '2017-12-19 19:45:33'),
('DOK-0004', 'Ririn', 'Tamalate 1', 'Makassar', 82213143555, 'Gigi', 'ririn@gmail.com', '2017-09-21', 'Aktif', '2017-12-04 08:49:48', '2017-12-04 08:50:15'),
('DOK-0005', 'enhol', 'jl kandea', 'makassar', 85216670649, 'anak', 'jukukalotoro2015@gmail.com', '2017-11-23', 'Aktif', '2017-12-10 16:24:22', '2017-12-10 19:58:06');

-- --------------------------------------------------------

--
-- Table structure for table `golongan_obat`
--

CREATE TABLE `golongan_obat` (
  `kd_gol_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_gol_obat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `golongan_obat`
--

INSERT INTO `golongan_obat` (`kd_gol_obat`, `nama_gol_obat`, `keterangan`, `status`, `created_at`, `updated_at`) VALUES
('GOL201711230001', 'Obat Bebas', 'Obat yang dijual bebas di pasaran dan dapat dibeli tanpa resep dokter.', 'Aktif', '2017-11-22 15:29:58', '2017-11-29 22:02:46'),
('GOL201711230002', 'Obat Narkotika', 'Obat yang berasal dari tanaman atau bukan tanaman baik sintetis maupun semi sintetis yang dapat menyebabkan penurunan atau perubahan kesadaran, hilangnya rasa, mengurangi sampai menghilangkan rasa nyeri dan menimbulkan ketergantungan.', 'Aktif', '2017-11-22 15:30:27', '2017-11-29 22:03:55'),
('GOL201711230003', 'Obat Keras', 'Obat yang hanya dapat dibeli di apotek dengan resep dokter.', 'Aktif', '2017-11-22 15:30:42', '2017-11-29 22:03:35'),
('GOL201711250001', 'Obat Psikotropika', 'Obat keras baik alamiah maupun sintetis bukan narkotik, yang berkhasiat psikoaktif melalui pengaruh selektif pada susunan saraf pusat yang menyebabkan perubahan khas pada aktivitas mental dan perilaku.', 'Aktif', '2017-11-25 08:57:38', '2017-11-29 22:04:21'),
('GOL201711250002', 'Obat Bebas Terbatas', 'Obat yang sebenarnya termasuk obat keras tetapi masih dapat dijual atau dibeli bebas tanpa resep dokter, dan disertai dengan tanda peringatan.', 'Aktif', '2017-11-25 09:08:40', '2017-11-29 22:03:16');

-- --------------------------------------------------------

--
-- Table structure for table `grup_user`
--

CREATE TABLE `grup_user` (
  `kode_grup` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grup_user`
--

INSERT INTO `grup_user` (`kode_grup`, `nama`, `keterangan`, `status`) VALUES
('G001', 'Administrator', 'Administrator', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `grup_user_menu`
--

CREATE TABLE `grup_user_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode_grup` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_menu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grup_user_menu`
--

INSERT INTO `grup_user_menu` (`id`, `kode_grup`, `id_menu`) VALUES
(24, 'G001', 1),
(25, 'G001', 2),
(26, 'G001', 3),
(27, 'G001', 4),
(28, 'G001', 5),
(29, 'G001', 6),
(30, 'G001', 7),
(31, 'G001', 8);

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

CREATE TABLE `gudang` (
  `kd_gudang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_gudang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_tlpn` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gudang`
--

INSERT INTO `gudang` (`kd_gudang`, `nama_gudang`, `alamat`, `kota`, `no_tlpn`, `no_hp`, `status`, `created_at`, `updated_at`) VALUES
('GUD1612140001', 'Gudang Utama obat', 'Jl. Kusuma Bangsa No. 88 - B', 'Surabaya', '0319922883', NULL, 'active', NULL, NULL),
('GUD1612140002', 'Gudang Cadangan', 'Jl. Tugiman 12 Kom. AL ', 'Surabaya', '031-5456633', NULL, 'active', NULL, NULL),
('GUD1612140003', 'Gudang Alat Kesehatan', 'Jln. Undaan Wetan 46 F', 'Surabaya', '031-5456633', NULL, 'active', NULL, NULL),
('GUD201711270001', 'Gudang Tambahan', 'jauh', 'bandung', '819287189', '912910112', 'aktif', '2017-11-26 15:30:42', '2017-11-26 15:30:42'),
('GUD201712110002', 'gudang maut', 'jl kotu', 'makassar', '085216670649', '0411234567', 'aktif', '2017-12-10 17:30:47', '2017-12-10 17:30:47'),
('GUD201712180003', 'gudang alay', 'jl joker', 'makassar', '0411234567', '085216670649', 'aktif', '2017-12-17 17:07:30', '2017-12-17 17:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `history_harga_obat`
--

CREATE TABLE `history_harga_obat` (
  `id` int(10) UNSIGNED NOT NULL,
  `kd_obat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kd_sat_obat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kd_lok_obat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `hna` bigint(20) DEFAULT NULL,
  `barcode_obat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_batch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga_jual_1` bigint(20) DEFAULT NULL,
  `harga_jual_2` bigint(20) DEFAULT NULL,
  `harga_jual_3` bigint(20) DEFAULT NULL,
  `diskon_harga_1` bigint(20) DEFAULT NULL,
  `diskon_harga_2` bigint(20) DEFAULT NULL,
  `diskon_harga_3` bigint(20) DEFAULT NULL,
  `tgl_expired` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_harga_obat`
--

INSERT INTO `history_harga_obat` (`id`, `kd_obat`, `kd_sat_obat`, `kd_lok_obat`, `stok`, `hna`, `barcode_obat`, `no_batch`, `harga_jual_1`, `harga_jual_2`, `harga_jual_3`, `diskon_harga_1`, `diskon_harga_2`, `diskon_harga_3`, `tgl_expired`, `created_at`, `updated_at`) VALUES
(34, 'OBT1614120001', 'SAT-OB201711270001', 'LOK-OB201711250001', NULL, 22000, '9780199532179', '22000', 15000, 0, 0, 0, 0, 0, '2017-11-25', '2017-12-04 08:52:42', '2017-12-04 08:52:42');

-- --------------------------------------------------------

--
-- Table structure for table `hutang_pembelian`
--

CREATE TABLE `hutang_pembelian` (
  `id_hutang` int(11) NOT NULL,
  `no_faktur` varchar(191) NOT NULL,
  `total` int(11) NOT NULL,
  `sisa` int(11) DEFAULT '0',
  `jumlah_bayar` int(11) DEFAULT '0',
  `tanggal_jatuh_tempo` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hutang_pembelian_detail`
--

CREATE TABLE `hutang_pembelian_detail` (
  `id_hutang_pembelian_detail` int(11) NOT NULL,
  `id_hutang` int(11) NOT NULL,
  `sisa_hutang` int(11) NOT NULL DEFAULT '0',
  `jenis_pembayaran` varchar(50) NOT NULL,
  `pembayaran_tunai` int(11) NOT NULL DEFAULT '0',
  `pembayaran_kartu` int(11) NOT NULL DEFAULT '0',
  `no_kartu` varchar(50) DEFAULT '0',
  `tunai` varchar(50) DEFAULT NULL,
  `kartu` varchar(50) DEFAULT NULL,
  `tanggal_pembayaran` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `identitas_apotek`
--

CREATE TABLE `identitas_apotek` (
  `id` int(11) NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_registrasi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pemilik` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penanggung_jawab` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penanggung_jawab_lab` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `identitas_apotek`
--

INSERT INTO `identitas_apotek` (`id`, `nama`, `no_registrasi`, `pemilik`, `penanggung_jawab`, `penanggung_jawab_lab`, `alamat`, `kota`, `telepon`, `email`, `website`, `logo`) VALUES
(1, 'Apotik', '101229', 'Bukan Saya', 'Joe Sandi', 'Saya Juga Bukan', 'Indonesia', 'Bandung', '085603032122', 'mail@mail.com', 'topibiru.com', 'logo-1512092152.png');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_shift`
--

CREATE TABLE `jadwal_shift` (
  `id_js` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mulai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `selesai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_keuangan`
--

CREATE TABLE `jurnal_keuangan` (
  `kd_jurnal` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `kode_akun_kredit` int(11) NOT NULL,
  `kode_akun_debit` int(11) NOT NULL,
  `kd_jurnal_singkat` int(11) DEFAULT NULL,
  `nobukti` varchar(100) NOT NULL DEFAULT '0',
  `deskripsi` text NOT NULL,
  `nominal` bigint(20) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal_keuangan`
--

INSERT INTO `jurnal_keuangan` (`kd_jurnal`, `tanggal`, `kode_akun_kredit`, `kode_akun_debit`, `kd_jurnal_singkat`, `nobukti`, `deskripsi`, `nominal`, `created_by`, `created_at`, `updated_at`) VALUES
(3, '2018-01-09', 0, 0, 1, '01119', 'lalalyeyey', 100, 2, '2018-01-07 22:42:46', '2018-01-07 22:42:46'),
(4, '2018-01-08', 115001, 210002, NULL, 'JLSAT-OB201711220001OBT201801080006', 'Tambah Di Master Obat No Faktur. IMPSAT-OB201711220001OBT201801080006', 20000, 2, '2018-01-07 22:45:32', '2018-01-07 22:45:32');

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_singkat`
--

CREATE TABLE `jurnal_singkat` (
  `id_jurnal` int(11) NOT NULL,
  `kode_akun_kredit` int(11) NOT NULL,
  `kode_akun_debit` int(11) NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jurnal_singkat`
--

INSERT INTO `jurnal_singkat` (`id_jurnal`, `kode_akun_kredit`, `kode_akun_debit`, `jenis`, `deskripsi`) VALUES
(1, 110001, 310001, 'Pengeluaran Kas', 'mencoba'),
(2, 110001, 310001, 'Pengeluaran Kas', 'mencoba');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_obat`
--

CREATE TABLE `kategori_obat` (
  `kd_kat_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_kat_obat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori_obat`
--

INSERT INTO `kategori_obat` (`kd_kat_obat`, `nama_kat_obat`, `keterangan`, `status`, `created_at`, `updated_at`) VALUES
('KAT-OB201711230001', 'Antibiotik', 'Antibiotik pokonya', 'aktif', '2017-11-22 15:31:33', '2017-11-22 15:31:33'),
('KAT-OB201711230002', 'INFUS', 'Habis', 'aktif', '2017-11-22 15:31:49', '2017-11-22 15:31:49'),
('KAT-OB201711250001', 'Suplemen', 'Suplemen', 'aktif', '2017-11-25 09:09:46', '2017-11-25 09:09:46'),
('KAT-OB201711270003', 'Salep', 'Salep untuk luka sakit hati', 'Aktif', '2017-11-26 12:20:15', '2017-12-04 09:07:55'),
('KAT-OB201712110004', 'daipet', 'mencret', 'aktif', '2017-12-10 17:31:54', '2017-12-10 17:31:54'),
('KAT-OB201712180005', 'anti mabuk', 'perjalanan', 'aktif', '2017-12-17 17:08:59', '2017-12-17 17:08:59'),
('KAT-OB201712200006', 'obat pcc', 'tidak untuk di jual bebas', 'aktif', '2017-12-19 17:35:36', '2017-12-19 17:35:36');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_pasien`
--

CREATE TABLE `kategori_pasien` (
  `kd_kat_pasien` int(11) NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `nama_kategori` varchar(100) DEFAULT NULL,
  `info_kategori` varchar(100) DEFAULT NULL,
  `harga_jual` bigint(20) NOT NULL DEFAULT '0',
  `disc_harga_jual` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_pasien`
--

INSERT INTO `kategori_pasien` (`kd_kat_pasien`, `created_by`, `nama_kategori`, `info_kategori`, `harga_jual`, `disc_harga_jual`, `created_at`, `updated_at`) VALUES
(3, 2, 'rawat jalan', 'sudah parah', 20000, 2, '2017-12-04 01:28:24', '2017-12-04 01:28:24'),
(4, 2, 'hhh', 'nnn', 3, 5, '2017-12-04 17:52:43', '2017-12-04 17:52:43');

-- --------------------------------------------------------

--
-- Table structure for table `kelompok_akun_keuangan`
--

CREATE TABLE `kelompok_akun_keuangan` (
  `kode_kelompok_akun` int(11) NOT NULL,
  `nama_kelompok_akun` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saldo_normal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kelompok_akun_keuangan`
--

INSERT INTO `kelompok_akun_keuangan` (`kode_kelompok_akun`, `nama_kelompok_akun`, `saldo_normal`) VALUES
(100, 'Aset / Harta', 'Debit'),
(200, 'Liabilitas / Kewajiban', 'Kredit'),
(300, 'Ekuitas / Modal', 'Kredit'),
(400, 'Pendapatan', 'Kredit'),
(500, 'Biaya Atas Pendapatan', 'Debit'),
(600, 'Beban Usaha', 'Debit');

-- --------------------------------------------------------

--
-- Table structure for table `konsinyasi_pembelian`
--

CREATE TABLE `konsinyasi_pembelian` (
  `id_konsinyasi` int(11) NOT NULL,
  `no_faktur` varchar(50) NOT NULL,
  `jumlah` int(11) NOT NULL DEFAULT '0',
  `retur` int(11) NOT NULL DEFAULT '0',
  `terjual` int(11) NOT NULL DEFAULT '0',
  `sisa` int(11) NOT NULL DEFAULT '0',
  `jumlah_bayar` int(11) NOT NULL DEFAULT '0',
  `kekurangan_bayar` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `konsinyasi_pembelian_detail`
--

CREATE TABLE `konsinyasi_pembelian_detail` (
  `id_konsinyasi_detail` int(11) NOT NULL,
  `id_konsinyasi` int(11) NOT NULL,
  `sisa_retur` int(11) NOT NULL DEFAULT '0',
  `jumlah_retur` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `lokasi_obat`
--

CREATE TABLE `lokasi_obat` (
  `kd_lok_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lok_obat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lokasi_obat`
--

INSERT INTO `lokasi_obat` (`kd_lok_obat`, `nama_lok_obat`, `status`, `created_at`, `updated_at`) VALUES
('LOK-OB201711220001', 'Rak Obat Utama', 'Aktif', '2017-11-21 23:15:32', '2017-11-29 22:09:32'),
('LOK-OB201711250001', 'Rak Utama', 'Aktif', '2017-11-25 09:11:48', '2017-11-29 22:09:38'),
('LOK-OB201711300002', 'Stok Gudang', 'Aktif', '2017-11-29 22:09:09', '2017-11-29 22:09:43'),
('LOK-OB201712040003', 'Obat Sakit hati', 'aktif', '2017-12-04 09:09:15', '2017-12-04 09:09:15'),
('LOK-OB201712110004', 'laci', 'aktif', '2017-12-10 17:32:40', '2017-12-10 17:32:40'),
('LOK-OB201712180005', 'SAKU', 'aktif', '2017-12-17 17:11:07', '2017-12-17 17:11:07');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_21_085302_create_table_akun_keuangan', 1),
(4, '2017_11_21_091007_create_table_pos_kun', 1),
(5, '2017_11_22_042201_create_table_kelompok_akun', 1),
(6, '2017_11_22_104737_create_table_jurnal_singkat', 2),
(7, '2017_11_23_162042_create_table_identitas_apotek', 3),
(8, '2017_11_23_183336_create_table_akses_menu', 4),
(9, '2017_11_23_184531_create_table_grup_user', 5),
(10, '2017_11_23_184820_create_table_grup_user_menu', 5),
(11, '2017_11_23_205244_create_table_jadwal_shift', 6),
(12, '2017_11_25_003343_create_purchaes_order_table', 7),
(13, '2017_11_25_003514_create_po_detail_table', 7),
(14, '2017_11_25_132135_create_table_opname', 8),
(15, '2017_11_21_105920_create_dokter_table', 9),
(16, '2017_11_22_063152_create_pabrik_table', 9),
(17, '2017_11_22_082042_create_supplier_table', 9),
(18, '2017_11_22_094000_create_gudang_table', 9),
(19, '2017_11_22_095455_create_golongan_obat_table', 9),
(20, '2017_11_22_103644_create_kategori_obat_table', 9),
(21, '2017_11_22_125221_create_lokasi_obat_table', 9),
(22, '2017_11_22_132321_create_satuan_obat_table', 9),
(23, '2017_11_23_014052_create_obat_table', 9),
(24, '2017_11_23_062657_create_satuan_table', 9),
(25, '2017_11_23_182219_create_racikan_table', 9),
(26, '2017_11_23_194809_create_apoteker_table', 9),
(27, '2017_11_23_203053_create_pasien_table', 9),
(29, '2017_11_29_114509_create_mutasi_obats_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `mutasi_konsinyasi_obat`
--

CREATE TABLE `mutasi_konsinyasi_obat` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `no_bukti` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `masuk` int(11) NOT NULL DEFAULT '0',
  `keluar` int(11) NOT NULL DEFAULT '0',
  `stock` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `mutasi_konsinyasi_obat`
--

INSERT INTO `mutasi_konsinyasi_obat` (`id`, `date`, `no_bukti`, `kd_obat`, `keterangan`, `masuk`, `keluar`, `stock`, `created_at`, `updated_at`) VALUES
(4, '2017-12-29', '9999999', 'OBT1614120001', 'tes aja', 0, 2, -2, '2017-11-30 10:07:55', '2017-11-30 10:07:55'),
(5, '2017-12-01', '23', 'OBT1614120001', 'belum', 0, 14, -14, '2017-12-04 18:19:04', '2017-12-04 18:19:04');

-- --------------------------------------------------------

--
-- Table structure for table `mutasi_obats`
--

CREATE TABLE `mutasi_obats` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `no_bukti` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_satuan` int(11) DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `petugas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hpp` int(11) NOT NULL DEFAULT '0',
  `masuk` int(11) NOT NULL DEFAULT '0',
  `keluar` int(11) NOT NULL DEFAULT '0',
  `stok_awal` int(11) NOT NULL DEFAULT '0',
  `stok_akhir` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `obat`
--

CREATE TABLE `obat` (
  `kd_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_obat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_pabrik` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_gol_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_kat_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_obat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Non Konsinyasi ',
  `minimal_stok` bigint(20) NOT NULL,
  `indikasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kandungan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dosis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `checkbox` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `obat`
--

INSERT INTO `obat` (`kd_obat`, `nama_obat`, `kd_pabrik`, `kd_gol_obat`, `kd_kat_obat`, `jenis_obat`, `minimal_stok`, `indikasi`, `kandungan`, `dosis`, `foto`, `checkbox`, `created_at`, `updated_at`) VALUES
('OBT201712220002', 'inzna', 'PAB1612150001', 'GOL201711230001', 'KAT-OB201711230001', 'non-konsinyasi', 12, 'flu', 'antibotik', '6mg', NULL, 'true', '2017-12-21 17:18:08', '2018-01-08 10:40:30'),
('OBT201801080004', 'Lem Biru', 'PAB1612150001', 'GOL201711230001', 'KAT-OB201711230001', 'nonkonsinyasi', 100, '100', 'tes', 'tes', '1515390007.jpg', 'false', '2018-01-07 22:40:07', '2018-01-08 09:38:44'),
('OBT201801080005', 'Lem Putih', 'PAB1612150001', 'GOL201711230001', 'KAT-OB201711230001', 'nonkonsinyasi', 200, '10', '39', '100', '1515390072.jpg', 'false', '2018-01-07 22:41:12', '2018-01-07 22:41:12'),
('OBT201801080006', 'Yeye Obat', 'PAB1612150001', 'GOL201711230001', 'KAT-OB201711230001', 'nonkonsinyasi', 200, '150', '102', '120', '1515390332.jpg', 'false', '2018-01-07 22:45:32', '2018-01-07 22:45:32');

-- --------------------------------------------------------

--
-- Table structure for table `opname`
--

CREATE TABLE `opname` (
  `id_opname` int(10) UNSIGNED NOT NULL,
  `kode_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lokasi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gudang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `stok_nyata` int(11) DEFAULT NULL,
  `satuan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `opname`
--

INSERT INTO `opname` (`id_opname`, `kode_obat`, `lokasi`, `gudang`, `stok`, `stok_nyata`, `satuan`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'OBT1614120001', 'Rak Utama', 'GUDANG UTAMA', 11, 11, 'Pcs', NULL, NULL, NULL),
(2, 'OBT1614120002', 'Rak Utama', 'GUDANG UTAMA', 1, 1, 'Pcs', NULL, NULL, NULL),
(3, 'OBT1614120003', 'Rak Utama', 'GUDANG UTAMA', 17, 17, 'Pcs', NULL, NULL, NULL),
(4, 'OBT1614120004', 'Rak Utama', 'GUDANG UTAMA', 83, 83, 'Strip', NULL, NULL, NULL),
(5, 'OBT1614120005', 'Rak Utama', 'GUDANG UTAMA', 50, 50, 'Strip', NULL, NULL, NULL),
(6, 'OBT1614120006', 'Rak Utama', 'GUDANG UTAMA', 57, 57, 'Strip', NULL, NULL, NULL),
(7, 'OBT1614120007', 'Rak Utama', 'GUDANG UTAMA', 65, 65, 'Pcs', NULL, NULL, NULL),
(8, 'OBT1614120008', 'Rak Utama', 'GUDANG UTAMA', 19, 19, 'Pcs', NULL, NULL, NULL),
(9, 'OBT1614120009', 'Rak Utama', 'GUDANG UTAMA', 27, 27, 'tab', NULL, NULL, NULL),
(10, 'OBT1614120010', 'Rak Utama', 'GUDANG UTAMA', 100, 100, 'Strip', NULL, NULL, NULL),
(11, 'OBT1614120011', 'Rak Utama', 'GUDANG UTAMA', 76, 76, 'Strip', NULL, NULL, NULL),
(12, 'OBT1614120012', 'Rak Utama', 'GUDANG UTAMA', 81, 81, 'Strip', NULL, NULL, NULL),
(13, 'OBT1614120013', 'Rak Utama', 'GUDANG UTAMA', 22, 22, 'Strip', NULL, NULL, NULL),
(14, 'OBT1614120014', 'Rak Utama', 'GUDANG UTAMA', 44, 44, 'Ampul', NULL, NULL, NULL),
(15, 'OBT1614120015', 'Rak Utama', 'GUDANG UTAMA', 82, 82, 'Pcs', NULL, NULL, NULL),
(16, 'OBT1614120016', 'Rak Utama', 'GUDANG UTAMA', 95, 95, 'Pcs', NULL, NULL, NULL),
(17, 'OBT1614120017', 'Rak Utama', 'GUDANG UTAMA', 91, 91, 'Pcs', NULL, NULL, NULL),
(18, 'OBT1614120018', 'Rak Utama', 'GUDANG UTAMA', 63, 63, 'Strip', NULL, NULL, NULL),
(19, 'OBT1614120019', 'Rak Utama', 'GUDANG UTAMA', 23, 23, 'Strip', NULL, NULL, NULL),
(20, 'OBT1614120020', 'Rak Utama', 'GUDANG UTAMA', 68, 68, 'Strip', NULL, NULL, NULL),
(21, 'OBT1614120021', 'STOK GUDANG', 'GUDANG UTAMA', 27, 27, 'Pcs', NULL, NULL, NULL),
(22, 'OBT1614120022', 'STOK GUDANG', 'GUDANG UTAMA', 8, 8, 'Tube', NULL, NULL, NULL),
(23, 'OBT1614120023', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'Pcs', NULL, NULL, NULL),
(24, 'OBT1614120024', 'STOK GUDANG', 'GUDANG UTAMA', 13, 13, 'Pcs', NULL, NULL, NULL),
(25, 'OBT1614120025', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'Pcs', NULL, NULL, NULL),
(26, 'OBT1614120026', 'STOK GUDANG', 'GUDANG UTAMA', 3, 3, 'Pcs', NULL, NULL, NULL),
(27, 'OBT1614120027', 'STOK GUDANG', 'GUDANG UTAMA', 3, 3, 'Pcs', NULL, NULL, NULL),
(28, 'OBT1614120028', 'STOK GUDANG', 'GUDANG UTAMA', 2, 2, 'Pcs', NULL, NULL, NULL),
(29, 'OBT1614120029', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'Pcs', NULL, NULL, NULL),
(30, 'OBT1614120030', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'Pcs', NULL, NULL, NULL),
(31, 'OBT1614120031', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'Pcs', NULL, NULL, NULL),
(32, 'OBT1614120032', 'STOK GUDANG', 'GUDANG UTAMA', 11, 11, 'BTL', NULL, NULL, NULL),
(33, 'OBT1614120033', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'BTL', NULL, NULL, NULL),
(34, 'OBT1614120034', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'Box', NULL, NULL, NULL),
(35, 'OBT1614120035', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'Box', NULL, NULL, NULL),
(36, 'OBT1614120036', 'STOK GUDANG', 'GUDANG UTAMA', 2, 2, 'KTK', NULL, NULL, NULL),
(37, 'OBT1614120037', 'STOK GUDANG', 'GUDANG UTAMA', 6, 6, 'BTL', NULL, NULL, NULL),
(38, 'OBT1614120038', 'STOK GUDANG', 'GUDANG UTAMA', 2, 2, 'Box', NULL, NULL, NULL),
(39, 'OBT1614120039', 'STOK GUDANG', 'GUDANG UTAMA', 2, 2, 'BTL', NULL, NULL, NULL),
(40, 'OBT1614120040', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'BTL', NULL, NULL, NULL),
(41, 'OBT1614120041', 'STOK GUDANG', 'GUDANG UTAMA', 3, 3, 'FLS', NULL, NULL, NULL),
(42, 'OBT1614120042', 'STOK GUDANG', 'GUDANG UTAMA', 4, 4, 'BTL', NULL, NULL, NULL),
(43, 'OBT1614120043', 'STOK GUDANG', 'GUDANG UTAMA', 6, 6, 'BTL', NULL, NULL, NULL),
(44, 'OBT1614120044', 'STOK GUDANG', 'GUDANG UTAMA', 50, 50, 'FLS', NULL, NULL, NULL),
(45, 'OBT1614120045', 'STOK GUDANG', 'GUDANG UTAMA', 2, 2, 'Pcs', NULL, NULL, NULL),
(46, 'OBT1614120046', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'BTL', NULL, NULL, NULL),
(47, 'OBT1614120047', 'STOK GUDANG', 'GUDANG UTAMA', 5, 5, 'FLS', NULL, NULL, NULL),
(48, 'OBT1614120048', 'STOK GUDANG', 'GUDANG UTAMA', 6, 6, 'BTL', NULL, NULL, NULL),
(49, 'OBT1614120049', 'STOK GUDANG', 'GUDANG UTAMA', 3, 3, 'BTL', NULL, NULL, NULL),
(50, 'OBT1614120050', 'STOK GUDANG', 'GUDANG UTAMA', 4, 4, 'FLS', NULL, NULL, NULL),
(51, 'OBT1614120051', 'STOK GUDANG', 'GUDANG UTAMA', 2, 2, 'Pcs', NULL, NULL, NULL),
(52, 'OBT1614120052', 'STOK GUDANG', 'GUDANG UTAMA', 2, 2, 'BTL', NULL, NULL, NULL),
(53, 'OBT1614120053', 'STOK GUDANG', 'GUDANG UTAMA', 2, 2, 'KTK', NULL, NULL, NULL),
(54, 'OBT1614120054', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'KTK', NULL, NULL, NULL),
(55, 'OBT1614120055', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'Pcs', NULL, NULL, NULL),
(56, 'OBT1614120056', 'STOK GUDANG', 'GUDANG UTAMA', 2, 2, 'BTL', NULL, NULL, NULL),
(57, 'OBT1614120057', 'STOK GUDANG', 'GUDANG UTAMA', 2, 2, 'BX', NULL, NULL, NULL),
(58, 'OBT1614120058', 'STOK GUDANG', 'GUDANG UTAMA', 3, 3, 'BTL', NULL, NULL, NULL),
(59, 'OBT1614120059', 'STOK GUDANG', 'GUDANG UTAMA', 6, 6, 'Strip', NULL, NULL, NULL),
(60, 'OBT1614120060', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'INF', NULL, NULL, NULL),
(61, 'OBT1614120061', 'STOK GUDANG', 'GUDANG UTAMA', 2, 2, 'BTL', NULL, NULL, NULL),
(62, 'OBT1614120062', 'STOK GUDANG', 'GUDANG UTAMA', 11, 11, 'Tube', NULL, NULL, NULL),
(63, 'OBT1614120063', 'STOK GUDANG', 'GUDANG UTAMA', 12, 12, 'Tube', NULL, NULL, NULL),
(64, 'OBT1614120064', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'Tube', NULL, NULL, NULL),
(65, 'OBT1614120065', 'STOK GUDANG', 'GUDANG UTAMA', 7, 7, 'BTL', NULL, NULL, NULL),
(66, 'OBT1614120066', 'STOK GUDANG', 'GUDANG UTAMA', 1, 1, 'BTL', NULL, NULL, NULL),
(67, 'OBT1614120067', 'STOK GUDANG', 'GUDANG UTAMA', 100, 100, 'tab', NULL, NULL, NULL),
(68, 'OBT1614120068', 'STOK GUDANG', 'GUDANG UTAMA', 16, 16, 'Tube', NULL, NULL, NULL),
(69, 'OBT1614120069', 'STOK GUDANG', 'GUDANG UTAMA', 35, 35, 'tab', NULL, NULL, NULL),
(70, 'OBT1614120070', 'STOK GUDANG', 'GUDANG UTAMA', 55, 55, 'tab', NULL, NULL, NULL),
(71, 'OBT1614120071', 'STOK GUDANG', 'GUDANG UTAMA', 30, 30, 'CAPS', NULL, NULL, NULL),
(72, 'OBT1614120072', 'STOK GUDANG', 'GUDANG UTAMA', 15, 15, 'tab', NULL, NULL, NULL),
(73, 'OBT1614120073', 'STOK GUDANG', 'GUDANG UTAMA', 90, 90, 'tab', NULL, NULL, NULL),
(74, 'OBT1614120074', 'STOK GUDANG', 'GUDANG UTAMA', 2, 2, 'Box', NULL, NULL, NULL),
(75, 'OBT1614120075', 'STOK GUDANG', 'GUDANG UTAMA', 30, 30, 'tab', NULL, NULL, NULL),
(76, 'OBT1614120076', 'STOK GUDANG', 'GUDANG UTAMA', 90, 90, 'tab', NULL, NULL, NULL),
(77, 'OBT1614120077', 'STOK GUDANG', 'GUDANG UTAMA', 100, 100, 'tab', NULL, NULL, NULL),
(78, 'OBT1614120078', 'STOK GUDANG', 'GUDANG UTAMA', 90, 90, 'tab', NULL, NULL, NULL),
(79, 'OBT1614120079', 'STOK GUDANG', 'GUDANG UTAMA', 100, 100, 'tab', NULL, NULL, NULL),
(80, 'OBT1614120080', 'STOK GUDANG', 'GUDANG UTAMA', 30, 30, 'tab', NULL, NULL, NULL),
(81, 'OBT1614120081', 'STOK GUDANG', 'GUDANG UTAMA', 379, 379, 'tab', NULL, NULL, NULL),
(82, 'OBT1614120082', 'STOK GUDANG', 'GUDANG UTAMA', 307, 307, 'tab', NULL, NULL, NULL),
(83, 'OBT1614120083', 'STOK GUDANG', 'GUDANG UTAMA', 595, 595, 'tab', NULL, NULL, NULL),
(84, 'OBT1614120084', 'STOK GUDANG', 'GUDANG UTAMA', 119, 119, 'Pcs', NULL, NULL, NULL),
(85, 'OBT1614120085', 'STOK GUDANG', 'GUDANG UTAMA', 247, 247, 'tab', NULL, NULL, NULL),
(86, 'OBT1614120086', 'STOK GUDANG', 'GUDANG UTAMA', 430, 430, 'tab', NULL, NULL, NULL),
(87, 'OBT1614120087', 'STOK GUDANG', 'GUDANG UTAMA', 190, 190, 'tab', NULL, NULL, NULL),
(88, 'OBT1614120088', 'STOK GUDANG', 'GUDANG UTAMA', 470, 470, 'tab', NULL, NULL, NULL),
(89, 'OBT1614120089', 'STOK GUDANG', 'GUDANG UTAMA', 549, 549, 'tab', NULL, NULL, NULL),
(90, 'OBT1614120090', 'STOK GUDANG', 'GUDANG UTAMA', 150, 150, 'tab', NULL, NULL, NULL),
(91, 'OBT1614120091', 'STOK GUDANG', 'GUDANG UTAMA', 185, 185, 'tab', NULL, NULL, NULL),
(92, 'OBT1614120092', 'STOK GUDANG', 'GUDANG UTAMA', 600, 600, 'tab', NULL, NULL, NULL),
(93, 'OBT1614120093', 'STOK GUDANG', 'GUDANG UTAMA', 150, 150, 'tab', NULL, NULL, NULL),
(94, 'OBT1614120094', 'STOK GUDANG', 'GUDANG UTAMA', 220, 220, 'tab', NULL, NULL, NULL),
(95, 'OBT1614120095', 'STOK GUDANG', 'GUDANG UTAMA', 80, 80, 'tab', NULL, NULL, NULL),
(96, 'OBT1614120096', 'STOK GUDANG', 'GUDANG UTAMA', 85, 85, 'tab', NULL, NULL, NULL),
(97, 'OBT1614120097', 'STOK GUDANG', 'GUDANG UTAMA', 150, 150, 'tab', NULL, NULL, NULL),
(98, 'OBT1614120098', 'STOK GUDANG', 'GUDANG UTAMA', 380, 380, 'tab', NULL, NULL, NULL),
(99, 'OBT1614120099', 'STOK GUDANG', 'GUDANG UTAMA', 413, 413, 'tab', NULL, NULL, NULL),
(100, 'OBT1614120100', 'STOK GUDANG', 'GUDANG UTAMA', 60, 60, 'tab', NULL, NULL, NULL),
(101, 'OBT1614120101', 'STOK GUDANG', 'GUDANG UTAMA', 270, 270, 'tab', NULL, NULL, NULL),
(102, 'OBT1614120102', 'STOK GUDANG', 'GUDANG UTAMA', 793, 793, 'tab', NULL, NULL, NULL),
(103, 'OBT1614120103', 'STOK GUDANG', 'GUDANG UTAMA', 130, 130, 'tab', NULL, NULL, NULL),
(104, 'OBT1614120104', 'STOK GUDANG', 'GUDANG UTAMA', 90, 90, 'tab', NULL, NULL, NULL),
(105, 'OBT1614120105', 'STOK GUDANG', 'GUDANG UTAMA', 40, 40, 'tab', NULL, NULL, NULL),
(106, 'OBT1614120106', 'STOK GUDANG', 'GUDANG UTAMA', 185, 185, 'tab', NULL, NULL, NULL),
(107, 'OBT1614120107', 'STOK GUDANG', 'GUDANG UTAMA', 4, 4, 'tab', NULL, NULL, NULL),
(108, 'OBT1614120108', 'STOK GUDANG', 'GUDANG UTAMA', 600, 600, 'tab', NULL, NULL, NULL),
(109, 'OBT1614120109', 'STOK GUDANG', 'GUDANG UTAMA', 264, 264, 'tab', NULL, NULL, NULL),
(110, 'OBT1614120110', 'STOK GUDANG', 'GUDANG UTAMA', 260, 260, 'tab', NULL, NULL, NULL),
(111, 'OBT1614120111', 'STOK GUDANG', 'GUDANG UTAMA', 215, 215, 'tab', NULL, NULL, NULL),
(112, 'OBT1614120112', 'STOK GUDANG', 'GUDANG UTAMA', 122, 122, 'tab', NULL, NULL, NULL),
(113, 'OBT1614120113', 'STOK GUDANG', 'GUDANG UTAMA', 294, 294, 'tab', NULL, NULL, NULL),
(114, 'OBT1614120114', 'STOK GUDANG', 'GUDANG UTAMA', 439, 439, 'tab', NULL, NULL, NULL),
(115, 'OBT1614120115', 'STOK GUDANG', 'GUDANG UTAMA', 150, 150, 'tab', NULL, NULL, NULL),
(116, 'OBT1614120116', 'STOK GUDANG', 'GUDANG UTAMA', 150, 150, 'tab', NULL, NULL, NULL),
(117, 'OBT1614120117', 'STOK GUDANG', 'GUDANG UTAMA', 82, 82, 'tab', NULL, NULL, NULL),
(118, 'OBT1614120118', 'STOK GUDANG', 'GUDANG UTAMA', 14170, 14170, 'tab', NULL, NULL, NULL),
(119, 'OBT1614120119', 'STOK GUDANG', 'GUDANG UTAMA', 200, 200, 'tab', NULL, NULL, NULL),
(120, 'OBT1614120120', 'STOK GUDANG', 'GUDANG UTAMA', 6533, 6533, 'tab', NULL, NULL, NULL),
(121, 'OBT1614120121', 'STOK GUDANG', 'GUDANG UTAMA', 90, 90, 'tab', NULL, NULL, NULL),
(122, 'OBT1614120122', 'STOK GUDANG', 'GUDANG UTAMA', 330, 330, 'tab', NULL, NULL, NULL),
(123, 'OBT1614120123', 'STOK GUDANG', 'GUDANG UTAMA', 466, 466, 'tab', NULL, NULL, NULL),
(124, 'OBT1614120124', 'STOK GUDANG', 'GUDANG UTAMA', 200, 200, 'tab', NULL, NULL, NULL),
(125, 'OBT1614120125', 'STOK GUDANG', 'GUDANG UTAMA', 110, 110, 'tab', NULL, NULL, NULL),
(126, 'OBT1614120126', 'STOK GUDANG', 'GUDANG UTAMA', 60, 60, 'tab', NULL, NULL, NULL),
(127, 'OBT1614120127', 'STOK GUDANG', 'GUDANG UTAMA', 730, 730, 'tab', NULL, NULL, NULL),
(128, 'OBT1614120128', 'STOK GUDANG', 'GUDANG UTAMA', 50, 50, 'tab', NULL, NULL, NULL),
(129, 'OBT1614120129', 'STOK GUDANG', 'GUDANG UTAMA', 20, 20, 'tab', NULL, NULL, NULL),
(130, 'OBT1614120130', 'STOK GUDANG', 'GUDANG UTAMA', 307, 307, 'tab', NULL, NULL, NULL),
(131, 'OBT1614120131', 'STOK GUDANG', 'GUDANG UTAMA', 60, 60, 'tab', NULL, NULL, NULL),
(132, 'OBT1614120132', 'STOK GUDANG', 'GUDANG UTAMA', 120, 120, 'tab', NULL, NULL, NULL),
(133, 'OBT1614120133', 'STOK GUDANG', 'GUDANG UTAMA', 279, 279, 'tab', NULL, NULL, NULL),
(134, 'OBT1614120134', 'STOK GUDANG', 'GUDANG UTAMA', 24, 24, 'tab', NULL, NULL, NULL),
(135, 'OBT1614120135', 'STOK GUDANG', 'GUDANG UTAMA', 213, 213, 'tab', NULL, NULL, NULL),
(136, 'OBT1614120136', 'STOK GUDANG', 'GUDANG UTAMA', 122, 122, 'tab', NULL, NULL, NULL),
(137, 'OBT1614120137', 'STOK GUDANG', 'GUDANG UTAMA', 20, 20, 'tab', NULL, NULL, NULL),
(138, 'OBT1614120138', 'STOK GUDANG', 'GUDANG UTAMA', 2609, 2609, 'tab', NULL, NULL, NULL),
(139, 'OBT1614120139', 'STOK GUDANG', 'GUDANG UTAMA', 250, 250, 'tab', NULL, NULL, NULL),
(140, 'OBT1614120140', 'STOK GUDANG', 'GUDANG UTAMA', 90, 90, 'tab', NULL, NULL, NULL),
(141, 'OBT1614120141', 'STOK GUDANG', 'GUDANG UTAMA', 175, 175, 'tab', NULL, NULL, NULL),
(142, 'OBT1614120142', 'STOK GUDANG', 'GUDANG UTAMA', 61, 61, 'tab', NULL, NULL, NULL),
(143, 'OBT1614120143', 'STOK GUDANG', 'GUDANG UTAMA', 190, 190, 'tab', NULL, NULL, NULL),
(144, 'OBT1614120144', 'STOK GUDANG', 'GUDANG UTAMA', 60, 60, 'tab', NULL, NULL, NULL),
(145, 'OBT1614120145', 'STOK GUDANG', 'GUDANG UTAMA', 138, 138, 'tab', NULL, NULL, NULL),
(146, 'OBT1614120146', 'STOK GUDANG', 'GUDANG UTAMA', 60, 60, 'tab', NULL, NULL, NULL),
(147, 'OBT1614120147', 'STOK GUDANG', 'GUDANG UTAMA', 130, 130, 'tab', NULL, NULL, NULL),
(148, 'OBT1614120148', 'STOK GUDANG', 'GUDANG UTAMA', 50, 50, 'tab', NULL, NULL, NULL),
(149, 'OBT1614120149', 'STOK GUDANG', 'GUDANG UTAMA', 60, 60, 'tab', NULL, NULL, NULL),
(150, 'OBT1614120150', 'STOK GUDANG', 'GUDANG UTAMA', 490, 490, 'tab', NULL, NULL, NULL),
(151, 'OBT1614120151', 'STOK GUDANG', 'GUDANG UTAMA', 420, 420, 'tab', NULL, NULL, NULL),
(152, 'OBT1614120152', 'STOK GUDANG', 'GUDANG UTAMA', 140, 140, 'tab', NULL, NULL, NULL),
(153, 'OBT1614120153', 'STOK GUDANG', 'GUDANG UTAMA', 80, 80, 'tab', NULL, NULL, NULL),
(154, 'OBT1614120154', 'STOK GUDANG', 'GUDANG UTAMA', 170, 170, 'tab', NULL, NULL, NULL),
(155, 'OBT1614120155', 'STOK GUDANG', 'GUDANG UTAMA', 250, 250, 'tab', NULL, NULL, NULL),
(156, 'OBT1614120156', 'STOK GUDANG', 'GUDANG UTAMA', 212, 212, 'tab', NULL, NULL, NULL),
(157, 'OBT1614120157', 'STOK GUDANG', 'GUDANG UTAMA', 100, 100, 'tab', NULL, NULL, NULL),
(158, 'OBT1614120158', 'STOK GUDANG', 'GUDANG UTAMA', 730, 730, 'tab', NULL, NULL, NULL),
(159, 'OBT1614120159', 'STOK GUDANG', 'GUDANG UTAMA', 33, 33, 'KPG', NULL, NULL, NULL),
(160, 'OBT1614120160', 'STOK GUDANG', 'GUDANG UTAMA', 100, 100, 'tab', NULL, NULL, NULL),
(161, 'OBT1614120161', 'STOK GUDANG', 'GUDANG UTAMA', 180, 180, 'tab', NULL, NULL, NULL),
(162, 'OBT1614120162', 'STOK GUDANG', 'GUDANG UTAMA', 20, 20, 'CAP', NULL, NULL, NULL),
(163, 'OBT1614120163', 'STOK GUDANG', 'GUDANG UTAMA', 140, 140, 'tab', NULL, NULL, NULL),
(164, 'OBT1614120164', 'STOK GUDANG', 'GUDANG UTAMA', 204, 204, 'tab', NULL, NULL, NULL),
(165, 'OBT1614120165', 'STOK GUDANG', 'GUDANG UTAMA', 70, 70, 'tab', NULL, NULL, NULL),
(166, 'OBT1614120166', 'STOK GUDANG', 'GUDANG UTAMA', 200, 200, 'tab', NULL, NULL, NULL),
(167, 'OBT1614120167', 'STOK GUDANG', 'GUDANG UTAMA', 525, 525, 'tab', NULL, NULL, NULL),
(168, 'OBT1614120168', 'STOK GUDANG', 'GUDANG UTAMA', 140, 140, 'tab', NULL, NULL, NULL),
(169, 'OBT1614120169', 'STOK GUDANG', 'GUDANG UTAMA', 220, 220, 'tab', NULL, NULL, NULL),
(170, 'OBT1614120170', 'STOK GUDANG', 'GUDANG UTAMA', 1000, 1000, 'tab', NULL, NULL, NULL),
(171, 'OBT1614120171', 'STOK GUDANG', 'GUDANG UTAMA', 60, 60, 'tab', NULL, NULL, NULL),
(172, 'OBT1614120172', 'STOK GUDANG', 'GUDANG UTAMA', 160, 160, 'tab', NULL, NULL, NULL),
(173, 'OBT1614120173', 'STOK GUDANG', 'GUDANG UTAMA', 579, 579, 'tab', NULL, NULL, NULL),
(174, 'OBT1614120174', 'STOK GUDANG', 'GUDANG UTAMA', 150, 150, 'tab', NULL, NULL, NULL),
(175, 'OBT1614120175', 'STOK GUDANG', 'GUDANG UTAMA', 8, 8, 'tab', NULL, NULL, NULL),
(176, 'OBT1614120176', 'STOK GUDANG', 'GUDANG UTAMA', 44, 44, 'tab', NULL, NULL, NULL),
(177, 'OBT1614120177', 'STOK GUDANG', 'GUDANG UTAMA', 430, 430, 'tab', NULL, NULL, NULL),
(178, 'OBT1614120178', 'STOK GUDANG', 'GUDANG UTAMA', 574, 574, 'tab', NULL, NULL, NULL),
(179, 'OBT1614120179', 'STOK GUDANG', 'GUDANG UTAMA', 65, 65, 'tab', NULL, NULL, NULL),
(180, 'OBT1614120180', 'STOK GUDANG', 'GUDANG UTAMA', 1000, 1000, 'tab', NULL, NULL, NULL),
(181, 'OBT1614120181', 'STOK GUDANG', 'GUDANG UTAMA', 120, 120, 'tab', NULL, NULL, NULL),
(182, 'OBT1614120182', 'STOK GUDANG', 'GUDANG UTAMA', 270, 270, 'tab', NULL, NULL, NULL),
(183, 'OBT1614120183', 'STOK GUDANG', 'GUDANG UTAMA', 130, 130, 'tab', NULL, NULL, NULL),
(184, 'OBT1614120184', 'STOK GUDANG', 'GUDANG UTAMA', 30, 30, 'tab', NULL, NULL, NULL),
(185, 'OBT1614120185', 'STOK GUDANG', 'GUDANG UTAMA', 130, 130, 'tab', NULL, NULL, NULL),
(186, 'OBT1614120186', 'STOK GUDANG', 'GUDANG UTAMA', 180, 180, 'tab', NULL, NULL, NULL),
(187, 'OBT1614120187', 'STOK GUDANG', 'GUDANG UTAMA', 500, 500, 'tab', NULL, NULL, NULL),
(188, 'OBT1614120188', 'STOK GUDANG', 'GUDANG UTAMA', 400, 400, 'tab', NULL, NULL, NULL),
(189, 'OBT1614120189', 'STOK GUDANG', 'GUDANG UTAMA', 172, 172, 'SACC', NULL, NULL, NULL),
(190, 'OBT1614120190', 'STOK GUDANG', 'GUDANG UTAMA', 196, 196, 'tab', NULL, NULL, NULL),
(191, 'OBT1614120191', 'STOK GUDANG', 'GUDANG UTAMA', 92, 92, 'tab', NULL, NULL, NULL),
(192, 'OBT1614120192', 'STOK GUDANG', 'GUDANG UTAMA', 157, 157, 'tab', NULL, NULL, NULL),
(193, 'OBT1614120193', 'STOK GUDANG', 'GUDANG UTAMA', 130, 130, 'tab', NULL, NULL, NULL),
(194, 'OBT1614120194', 'STOK GUDANG', 'GUDANG UTAMA', 130, 130, 'tab', NULL, NULL, NULL),
(195, 'OBT1614120195', 'STOK GUDANG', 'GUDANG UTAMA', 50, 50, 'tab', NULL, NULL, NULL),
(196, 'OBT1614120196', 'STOK GUDANG', 'GUDANG UTAMA', 180, 180, 'tab', NULL, NULL, NULL),
(197, 'OBT1614120197', 'STOK GUDANG', 'GUDANG UTAMA', 100, 100, 'tab', NULL, NULL, NULL),
(198, 'OBT1614120198', 'STOK GUDANG', 'GUDANG UTAMA', 60, 60, 'tab', NULL, NULL, NULL),
(199, 'OBT1614120199', 'STOK GUDANG', 'GUDANG UTAMA', 280, 280, 'tab', NULL, NULL, NULL),
(200, 'OBT1614120200', 'STOK GUDANG', 'GUDANG UTAMA', 20, 20, 'tab', NULL, NULL, NULL),
(201, 'OBT1614120201', 'STOK GUDANG', 'GUDANG UTAMA', 170, 170, 'tab', NULL, NULL, NULL),
(202, 'OBT1614120202', 'STOK GUDANG', 'GUDANG UTAMA', 70, 70, 'tab', NULL, NULL, NULL),
(203, 'OBT1614120203', 'STOK GUDANG', 'GUDANG UTAMA', 50, 50, 'KAP', NULL, NULL, NULL),
(204, 'OBT1614120204', 'STOK GUDANG', 'GUDANG UTAMA', 208, 208, 'tab', NULL, NULL, NULL),
(205, 'OBT1614120205', 'STOK GUDANG', 'GUDANG UTAMA', 130, 130, 'tab', NULL, NULL, NULL),
(206, 'OBT1614120206', 'STOK GUDANG', 'GUDANG UTAMA', 250, 250, 'tab', NULL, NULL, NULL),
(207, 'OBT1614120207', 'STOK GUDANG', 'GUDANG UTAMA', 20, 20, 'tab', NULL, NULL, NULL),
(208, 'OBT1614120208', 'STOK GUDANG', 'GUDANG UTAMA', 352, 352, 'tab', NULL, NULL, NULL),
(209, 'OBT1614120209', 'STOK GUDANG', 'GUDANG UTAMA', 190, 190, 'tab', NULL, NULL, NULL),
(210, 'OBT1614120210', 'STOK GUDANG', 'GUDANG UTAMA', 330, 330, 'tab', NULL, NULL, NULL),
(211, 'OBT1614120211', 'STOK GUDANG', 'GUDANG UTAMA', 130, 130, 'tab', NULL, NULL, NULL),
(212, 'OBT1614120212', 'STOK GUDANG', 'GUDANG UTAMA', 20, 20, 'tab', NULL, NULL, NULL),
(213, 'OBT1614120213', 'STOK GUDANG', 'GUDANG UTAMA', 12, 12, 'tab', NULL, NULL, NULL),
(214, 'OBT1614120214', 'STOK GUDANG', 'GUDANG UTAMA', 150, 150, 'tab', NULL, NULL, NULL),
(215, 'OBT1614120215', 'STOK GUDANG', 'GUDANG UTAMA', 240, 240, 'tab', NULL, NULL, NULL),
(216, 'OBT1614120216', 'STOK GUDANG', 'GUDANG UTAMA', 90, 90, 'BTL', NULL, NULL, NULL),
(217, 'OBT1614120217', 'STOK GUDANG', 'GUDANG UTAMA', 854, 854, 'tab', NULL, NULL, NULL),
(218, 'OBT1614120218', 'STOK GUDANG', 'GUDANG UTAMA', 192, 192, 'tab', NULL, NULL, NULL),
(219, 'OBT1614120219', 'STOK GUDANG', 'GUDANG UTAMA', 147, 147, 'tab', NULL, NULL, NULL),
(220, 'OBT1614120220', 'STOK GUDANG', 'GUDANG UTAMA', 200, 200, 'tab', NULL, NULL, NULL),
(221, 'OBT1614120221', 'STOK GUDANG', 'GUDANG UTAMA', 220, 220, 'tab', NULL, NULL, NULL),
(222, 'OBT1614120222', 'STOK GUDANG', 'GUDANG UTAMA', 183, 183, 'tab', NULL, NULL, NULL),
(223, 'OBT1614120223', 'STOK GUDANG', 'GUDANG UTAMA', 957, 957, 'tab', NULL, NULL, NULL),
(224, 'OBT1614120224', 'STOK GUDANG', 'GUDANG UTAMA', 257, 257, 'tab', NULL, NULL, NULL),
(225, 'OBT1614120225', 'STOK GUDANG', 'GUDANG UTAMA', 320, 320, 'tab', NULL, NULL, NULL),
(226, 'OBT1614120226', 'STOK GUDANG', 'GUDANG UTAMA', 140, 140, 'tab', NULL, NULL, NULL),
(227, 'OBT1614120227', 'STOK GUDANG', 'GUDANG UTAMA', 300, 300, 'CAP', NULL, NULL, NULL),
(228, 'OBT1614120228', 'STOK GUDANG', 'GUDANG UTAMA', 40, 40, 'tab', NULL, NULL, NULL),
(229, 'OBT1711140230', 'Rak Obat Utama', 'GUDANG UTAMA', 0, 0, 'Ampul', NULL, NULL, NULL),
(230, 'OBT1711140230', 'Rak Obat Utama', 'GUDANG UTAMA', 31, 31, 'Box', NULL, NULL, NULL),
(231, 'BAR001', 'Rak Obat Utama', 'GUDANG UTAMA', 15, 15, 'Strip', NULL, NULL, NULL),
(232, 'OBT1711230231', 'STOK GUDANG', 'GUDANG UTAMA', 0, 0, 'Box', NULL, NULL, NULL),
(233, 'OBT1711230231', 'STOK GUDANG', 'GUDANG UTAMA', 40, 40, 'BX', NULL, NULL, NULL),
(234, 'OBT1711240233', 'Rak Utama', 'GUDANG UTAMA', 0, 0, 'Botol', NULL, NULL, NULL),
(235, 'OBT1711240233', 'Rak Utama', 'GUDANG UTAMA', 70, 70, 'Box', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pabrik`
--

CREATE TABLE `pabrik` (
  `kd_pabrik` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_pabrik` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_tlpn` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp` bigint(20) DEFAULT NULL,
  `npwp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pabrik`
--

INSERT INTO `pabrik` (`kd_pabrik`, `nama_pabrik`, `alamat`, `kota`, `no_tlpn`, `no_hp`, `npwp`, `status`, `created_at`, `updated_at`) VALUES
('PAB1612150001', 'PT ADITAMARAYA FARMINDO', 'Jl. Rungkut Industri 2 / 45C', 'Surabaya', ' 031 8412522-23', NULL, NULL, 'active', NULL, NULL),
('PAB1612150002', 'PT APMO INDONESIA', 'Jl. Jendral A. Yani No. 17', 'MOJOKERTO', '0321 21034', 8881818, '91928188', 'Non Aktif', NULL, '2017-12-10 20:02:42'),
('PAB1612150003', 'PT DURAFARMA JAYA', 'Jl. Rungkut Industri VIII/22-24', 'Surabaya', ' 031 8439617', NULL, NULL, 'active', NULL, NULL),
('PAB1612150004', 'PT HENSON FARMA', 'Jl. Karangpilang Barat No. 200', 'Surabaya', '031 7662601 – 3', NULL, NULL, 'active', NULL, NULL),
('PAB1612150005', 'PT KALIROTO', 'Jl. Sidorame No. 19', 'Surabaya', ' 031 3719440', NULL, NULL, 'active', NULL, NULL),
('PAB1612150006', 'PT KASA HUSADA', 'jl. Kalimas Barat No. 17 – 19', 'Surabaya', NULL, NULL, NULL, 'active', NULL, NULL),
('PAB1612150007', 'PT RITA SINAR INDAH', 'Jl. Rungkut Industri IV/24 ', 'Surabaya', NULL, NULL, NULL, 'active', NULL, NULL),
('PAB1612150008', 'PT SANDAI FARMA', 'Jl. Kenjeran No. 401B', 'Surabaya', '031 3893921', NULL, NULL, 'active', NULL, NULL),
('PAB1612150009', 'PT USFI ', 'Jl. Kedungcowek No. 345', 'Surabaya', '3715451', NULL, NULL, 'active', NULL, NULL),
('PAB1612150010', 'PT PARI ANOM', 'Jl. Gembong Tebasan No. 18', 'Surabaya', '031 312578', NULL, NULL, 'active', NULL, NULL),
('PAB201711230003', 'Pt Sukacita', 'Jl Skaloa', 'Bandung', '81931382191', 918728788, '12.1212.121-121.212.12', 'aktif', '2017-11-22 15:28:34', '2017-11-22 15:28:34'),
('PAB201711270011', 'PT. ABBOT', NULL, NULL, NULL, NULL, NULL, 'active', '2017-11-26 13:10:15', '2017-11-26 13:10:15'),
('PAB201711270012', 'PT. CORONET CROWN', NULL, NULL, NULL, NULL, NULL, 'active', '2017-11-26 13:14:40', '2017-11-26 13:14:40'),
('PAB201711270013', 'PT. AMAN JAYA SENTOSA', NULL, NULL, NULL, NULL, NULL, 'active', '2017-11-26 13:14:40', '2017-11-26 13:14:40'),
('PAB201712040014', 'PT. RJs Makassar', 'Tamalate', 'Makassar', '0122113234', 82213143455, '5345324534', 'Aktif', '2017-12-04 08:52:19', '2017-12-04 09:01:07'),
('PAB201712040016', 'PT.Risalah', 'Tamalate', 'Makassar', '014334422', 82213143444, '35.769.172.4-502.000', 'Aktif', '2017-12-04 08:58:29', '2017-12-04 08:59:49'),
('PAB201712100017', 'PT maju mundur', 'jl ujung pandang', 'makassar', '085216670649', 411213454, '12345678', 'aktif', '2017-12-10 16:25:43', '2017-12-10 16:25:43');

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `id_rm` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_pasien` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kd_kat_pasien` int(11) NOT NULL,
  `gol_darah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `umur` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_tlpn` bigint(20) DEFAULT NULL,
  `status_perkawinan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pekerjaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_ibu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_ayah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_kk` bigint(20) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pin_bbm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alergi_obat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `tanggal_registrasi` date NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`id_rm`, `nama_pasien`, `kd_kat_pasien`, `gol_darah`, `jenis_kelamin`, `alamat`, `kota`, `umur`, `no_tlpn`, `status_perkawinan`, `pekerjaan`, `nama_ibu`, `nama_ayah`, `no_kk`, `email`, `pin_bbm`, `alergi_obat`, `tanggal_lahir`, `tanggal_registrasi`, `status`, `created_at`, `updated_at`) VALUES
('PAS-0001', NULL, 0, 'AB', 'Perempuan', 'C.3 NO.31', 'BEKASI', '25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Decolgen', '2017-11-26', '2016-12-16', 'active', '2017-11-25 12:26:03', '2017-11-25 12:26:03'),
('PAS-0002', NULL, 0, 'A', 'Perempuan', NULL, 'Surabaya', '25', NULL, 'Belum Kawin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1991-01-01', '2016-12-16', 'active', '2017-11-25 12:26:03', '2017-11-25 12:26:03'),
('PAS-0003', NULL, 0, 'A', 'Laki-laki', 'PURI GADING', 'BEKASI', '25', NULL, 'Sudah Kawin', 'Swasta', NULL, NULL, NULL, NULL, NULL, NULL, '1991-01-01', '2016-12-16', 'active', '2017-11-25 12:26:03', '2017-11-25 12:26:03'),
('PAS-0004', NULL, 0, 'AB', NULL, 'Jl . Tambak medokan ayu 3c No.23', 'Surabaya', '30', NULL, 'Sudah Kawin', 'Makelar', 'Supaiyem', 'Supaijo', NULL, NULL, NULL, NULL, '1986-10-14', '2016-12-16', 'active', '2017-11-25 12:26:03', '2017-11-25 12:26:03'),
('PAS-0005', NULL, 0, 'AB', 'Laki-laki', 'ALAM RAYA 1 L1 NO 6', 'BEKASI', '26', NULL, NULL, 'Swasta', NULL, NULL, NULL, NULL, NULL, NULL, '1990-09-10', '2016-12-16', 'active', '2017-11-25 12:26:04', '2017-11-25 12:26:04'),
('PAS-0006', NULL, 0, 'O', 'Laki-laki', 'Jl. Cantian Tengah', 'Surabaya', '46', NULL, NULL, NULL, NULL, 'rohman', NULL, NULL, '529sn30', NULL, '1970-01-01', '2016-12-16', 'active', '2017-11-25 12:26:04', '2017-11-25 12:26:04'),
('PAS-0007', NULL, 0, 'AB', 'Laki-laki', 'BLOK J 7 NO 1', 'BEKASI', '16', NULL, 'Duda', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2000-01-29', '2016-12-16', 'active', '2017-11-25 12:26:04', '2017-11-25 12:26:04'),
('PAS-0008', NULL, 0, 'O', 'Perempuan', 'Jakarta', 'Jakarta', '0', NULL, NULL, NULL, 'Pearce', NULL, NULL, NULL, NULL, NULL, '2016-01-30', '2016-12-16', 'active', '2017-11-25 12:26:04', '2017-11-25 12:26:04'),
('PAS-0009', NULL, 0, 'O', 'Laki-laki', 'H 814', 'BEKASI', '3', NULL, 'Belum Kawin', 'pengusaha', 'darmiasi', 'rohman', NULL, 'sbronky@gmail.com', NULL, 'Betadine', '2013-01-30', '2016-12-16', 'active', '2017-11-25 12:26:04', '2017-11-25 12:26:04'),
('PAS-0011', NULL, 0, 'B', 'Perempuan', 'Grand Phantasia', 'Surabaya', '31', NULL, 'Sudah Kawin', 'Pedagang', 'Ibu', 'Ayah', NULL, 'vayne@ai.com', NULL, 'Vitamin C CTM', '1985-01-01', '2016-12-16', 'active', '2017-11-25 12:26:04', '2017-11-25 12:26:04'),
('PAS-0012', NULL, 0, 'A', 'Perempuan', 'PONDOK MELATI INDAH', 'BEKASI', '46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', '2016-12-16', 'active', '2017-11-25 12:26:04', '2017-11-25 12:26:04'),
('PAS-0013', NULL, 0, 'AB', 'Perempuan', 'C2 NO;9', 'BEKASI', '46', NULL, 'Belum Kawin', 'guru', NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', '2016-12-16', 'active', '2017-11-25 12:26:04', '2017-11-25 12:26:04'),
('PAS-0014', NULL, 0, 'O', 'Perempuan', 'Jl. Kusuma Bangsa No.34', 'Palembang', '46', NULL, 'Janda', 'Artis', '-', '-', NULL, NULL, NULL, NULL, '1970-01-01', '2016-12-16', 'active', '2017-11-25 12:26:04', '2017-11-25 12:26:04'),
('PAS-0015', NULL, 0, 'O', 'Laki-laki', 'TN.FAUZI', 'BEKASI', '46', NULL, 'Belum Kawin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', '2016-12-16', 'active', '2017-11-25 12:26:04', '2017-11-25 12:26:04'),
('PAS-0016', NULL, 0, 'O', 'Laki-laki', 'TN .IRFAN', NULL, '46', NULL, 'Belum Kawin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', '2016-12-16', 'active', '2017-11-25 12:26:04', '2017-11-25 12:26:04'),
('PAS-0017', NULL, 0, 'O', 'Laki-laki', 'PERM.VILA CARITA', 'BEKASI', '46', NULL, 'Sudah Kawin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', '2016-12-16', 'active', '2017-11-25 12:26:04', '2017-11-25 12:26:04'),
('PAS-0018', 'Wawan', 3, 'A', 'Laki-laki', 'Pinggir Jalan', 'Makassar', '21', 822121213, 'Belum Kawin', 'Senior Animator', 'Ramla', 'Ramli', 245234523, 'wawan@gmail.com', '31wfw41', 'alergi obat nyamuk', '1996-02-23', '0000-00-00', 'aktif', '2017-12-04 09:29:40', '2017-12-04 09:30:21'),
('PAS-0019', 'anwar', 3, 'b', 'Laki-laki', 'jl. skarda', 'makassar', '17', 85242313222, 'Belum Kawin', 'swasta', 'maria', 'marten', 5346587698609809, 'emmankchelsea@ymail.com', '12w34', 'asemapenamat', '2000-02-12', '0000-00-00', 'aktif', '2017-12-04 17:55:03', '2017-12-04 17:55:03'),
('PAS-0020', 'ais', 0, 'b', 'Laki-laki', 'jl matahari', 'gowa', '28', 85216670649, 'Sudah Kawin', 'petani', 'hayati', 'burhan', 77889654323790, 'jukukalotoro2015@gmail.com', 'b5674d', 'vitamin c', '2017-12-06', '0000-00-00', 'aktif', '2017-12-10 18:38:54', '2017-12-10 18:38:54');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `no_pembelian_detail` int(10) UNSIGNED NOT NULL,
  `no_faktur` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jmlh` int(11) NOT NULL,
  `kd_satuan` int(11) NOT NULL,
  `harga` double(25,2) NOT NULL,
  `diskon` double(8,2) NOT NULL,
  `tgl_exp` date NOT NULL,
  `retur` set('true','false') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`no_pembelian_detail`, `no_faktur`, `kd_obat`, `jmlh`, `kd_satuan`, `harga`, `diskon`, `tgl_exp`, `retur`, `created_at`, `updated_at`) VALUES
(1, '12345678', 'OBT201711230001', 3, 1, 25000.00, 0.00, '2017-12-01', 'false', '2017-11-26 15:36:18', '2017-11-26 15:36:18'),
(2, '12345678', 'OBT201711240002', 1, 3, 5000.00, 0.00, '2017-01-01', 'false', '2017-11-26 15:36:18', '2017-11-26 15:36:18'),
(3, '12121212', 'OBT201711230001', 2, 5, 5000.00, 0.00, '2017-12-01', 'true', '2017-11-26 15:44:23', '2017-11-27 05:09:49'),
(4, '12121212', 'OBT201711240002', 1, 3, 5000.00, 0.00, '2017-01-01', 'false', '2017-11-26 15:44:23', '2017-11-27 05:11:07'),
(5, '12121212', 'OBT201711230001', 2, 5, 5000.00, 0.00, '2017-12-01', 'true', '2017-11-26 15:46:00', '2017-11-27 05:09:49'),
(6, '12121212', 'OBT201711240002', 1, 3, 5000.00, 0.00, '2017-01-01', 'false', '2017-11-26 15:46:00', '2017-11-27 05:11:07'),
(7, '12121212', 'OBT201711240003', 0, 4, 1231312.00, 0.00, '2017-11-09', 'false', '2017-11-26 15:46:00', '2017-11-27 05:11:03'),
(8, '2192019201', 'OBT201711230001', 3, 1, 25000.00, 0.00, '2017-12-01', 'false', '2017-11-27 04:03:30', '2017-11-27 04:03:30'),
(9, '2192019201', 'OBT201711240002', 2, 3, 5000.00, 0.00, '2017-01-01', 'false', '2017-11-27 04:03:31', '2017-11-27 04:03:31'),
(10, '2192019201', 'OBT201711240003', 0, 4, 1231312.00, 0.00, '2017-11-09', 'false', '2017-11-27 04:03:31', '2017-11-27 04:03:31'),
(11, '687676', 'OBT201711230001', 48, 1, 25000.00, 0.00, '2017-12-01', 'true', '2017-11-27 04:08:48', '2017-11-27 07:36:58'),
(12, '687676', 'OBT201711240002', 1, 3, 5000.00, 0.00, '2017-01-01', 'true', '2017-11-27 04:08:48', '2017-11-27 07:36:58'),
(13, '687676', 'OBT201711240003', 0, 4, 1231312.00, 0.00, '2017-11-09', 'true', '2017-11-27 04:08:48', '2017-11-27 07:32:10'),
(14, 'FAK1712040001', 'OBT1614120001', 1, 27, 22000.00, 0.00, '2017-11-25', 'false', '2017-12-04 01:51:05', '2017-12-04 01:51:05'),
(15, 'FAK1712040002', 'OBT1614120001', 1, 27, 22000.00, 0.00, '2017-11-25', 'true', '2017-12-04 09:46:08', '2017-12-04 18:06:29'),
(16, 'FAK1712040003', 'OBT1614120001', 1, 27, 22000.00, 0.00, '2017-11-25', 'false', '2017-12-04 10:43:55', '2017-12-04 10:43:55'),
(17, 'FAK1712040004', 'OBT1614120001', 1, 27, 22000.00, 0.00, '2017-11-25', 'false', '2017-12-04 10:44:45', '2017-12-04 10:44:45'),
(18, 'FAK1712050005', 'OBT1614120001', 1, 27, 22000.00, 0.00, '2017-11-25', 'true', '2017-12-04 18:01:21', '2017-12-04 18:02:20'),
(19, 'FAK1712050006', 'OBT1614120001', 1, 27, 22000.00, 0.00, '2017-11-25', 'false', '2017-12-04 18:04:42', '2017-12-04 18:04:42'),
(20, 'FAK1712100007', 'OBT1614120001', 1, 27, 22000.00, 0.00, '2017-11-25', 'false', '2017-12-10 16:27:59', '2017-12-10 16:27:59'),
(21, 'FAK1712110008', 'OBT1614120001', 1, 27, 22000.00, 0.00, '2017-11-25', 'true', '2017-12-10 17:40:46', '2017-12-10 17:41:38'),
(22, 'FAK1712110009', 'OBT1614120001', 1, 27, 22000.00, 0.00, '2017-11-25', 'false', '2017-12-10 18:07:55', '2017-12-10 18:07:55');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_obat`
--

CREATE TABLE `pembelian_obat` (
  `no_faktur` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_po` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kd_supplier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pembayaran` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_gudang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diskon` double(8,2) NOT NULL,
  `pajak` double(8,2) NOT NULL,
  `total` double(8,2) NOT NULL,
  `tgl_faktur` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pembelian_obat`
--

INSERT INTO `pembelian_obat` (`no_faktur`, `no_po`, `kd_supplier`, `jenis_pembayaran`, `kd_gudang`, `kas`, `diskon`, `pajak`, `total`, `tgl_faktur`, `created_at`, `updated_at`) VALUES
('12121212', 'PO1711250002', 'SUP201711250001', 'TUNAI', 'GUD201711270001', '', 10.00, 120.00, 131500.00, '0000-00-00', '2017-11-26 15:46:00', '2017-11-26 15:46:00'),
('12345678', NULL, 'SUP201711250001', 'TUNAI', 'GUD201711270001', '', 10.00, 20.00, 98000.00, '0000-00-00', '2017-11-26 15:36:18', '2017-11-26 15:36:18'),
('2192019201', 'PO1711250002', 'SUP201711250001', 'HUTANG', 'GUD201711270001', '', 10.00, 20.00, 93500.00, '0000-00-00', '2017-11-27 04:03:30', '2017-11-27 04:03:30'),
('687676', 'PO1711250002', 'SUP201711250001', 'TUNAI', 'GUD201711270001', '', 10.00, 10.00, 115000.00, '2017-11-27', '2017-11-27 04:08:48', '2017-11-27 04:08:48'),
('FAK1712040001', 'PO1711290001', 'SUP1612140001', 'TUNAI', 'GUD1612140001', 'Kas Umum', -1.00, 2.00, 62660.00, '2017-12-04', '2017-12-04 01:51:05', '2017-12-04 01:51:05'),
('FAK1712040002', NULL, 'SUP1612140002', 'TUNAI', 'GUD1612140001', 'Kas Umum', 0.00, 22.00, 26840.00, '2017-12-04', '2017-12-04 09:46:08', '2017-12-04 09:46:08'),
('FAK1712040003', NULL, 'SUP1612140001', 'HUTANG', 'GUD1612140001', 'Kas Umum', 0.00, 0.00, 22000.00, '2017-12-04', '2017-12-04 10:43:55', '2017-12-04 10:43:55'),
('FAK1712040004', NULL, 'SUP1612140001', 'HUTANG', 'GUD1612140001', 'Kas Umum', 0.00, 0.00, 22000.00, '2017-12-04', '2017-12-04 10:44:45', '2017-12-04 10:44:45'),
('FAK1712050005', 'PO1711290001', 'SUP1612140006', 'TUNAI', 'GUD1612140001', 'Kas Umum', 0.00, 0.00, 22000.00, '2017-12-05', '2017-12-04 18:01:21', '2017-12-04 18:01:21'),
('FAK1712050006', 'PO1711290001', 'SUP1612140001', 'TUNAI', 'GUD1612140001', 'Kas Umum', 0.00, 0.00, 22000.00, '2017-12-05', '2017-12-04 18:04:42', '2017-12-04 18:04:42'),
('FAK1712100007', 'PO1711290001', 'SUP1612140002', 'TUNAI', 'GUD1612140001', 'Kas Umum', 0.00, 0.00, 22000.00, '2017-12-10', '2017-12-10 16:27:59', '2017-12-10 16:27:59'),
('FAK1712110008', 'PO1711290001', 'SUP1612140003', 'TUNAI', 'GUD1612140001', 'Kas Umum', 0.00, 0.00, 22000.00, '2017-12-11', '2017-12-10 17:40:46', '2017-12-10 17:40:46'),
('FAK1712110009', 'PO1711290001', 'SUP1612140002', 'TUNAI', 'GUD1612140001', 'Kas Umum', 0.00, 0.00, 22000.00, '2017-12-11', '2017-12-10 18:07:54', '2017-12-10 18:07:54');

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan_aplikasi`
--

CREATE TABLE `pengaturan_aplikasi` (
  `id_pengaturan` tinyint(1) NOT NULL,
  `purchase_order` tinyint(1) DEFAULT '0',
  `mki_po` tinyint(5) DEFAULT '0',
  `mka_po` tinyint(5) DEFAULT '0',
  `catatan_po` tinyint(1) DEFAULT '0',
  `retur_pembelian_obat` tinyint(1) DEFAULT '0',
  `mki_retur_pembelian` tinyint(5) DEFAULT '0',
  `mka_retur_pembelian` tinyint(5) DEFAULT '0',
  `catatan_retur_pembelian` tinyint(1) DEFAULT '0',
  `retur_penjualan_obat` tinyint(1) DEFAULT '0',
  `mki_retur_penjualan` tinyint(5) DEFAULT '0',
  `mka_retur_penjualan` tinyint(5) DEFAULT '0',
  `catatan_retur_penjualan` tinyint(1) DEFAULT '0',
  `penjualan_obat` tinyint(1) DEFAULT '0',
  `mki_penjualan` tinyint(5) DEFAULT '0',
  `mka_penjualan` tinyint(5) DEFAULT '0',
  `catatan_penjualan` tinyint(1) DEFAULT '0',
  `pembayaran_piutang` tinyint(1) DEFAULT '0',
  `mki_pembayaran_piutang` tinyint(5) DEFAULT '0',
  `mka_pembayaran_piutang` tinyint(5) DEFAULT '0',
  `catatan_pembayaran_piutang` tinyint(1) DEFAULT '0',
  `catatan_promo` text,
  `obat_expired` tinyint(5) DEFAULT '0',
  `piutang_jatuh_tempo` tinyint(5) DEFAULT '0',
  `hutang_jatuh_tempo` tinyint(5) DEFAULT '0',
  `tuslah` tinyint(5) DEFAULT '0',
  `embalase` tinyint(5) DEFAULT '0',
  `ppn` tinyint(3) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengaturan_aplikasi`
--

INSERT INTO `pengaturan_aplikasi` (`id_pengaturan`, `purchase_order`, `mki_po`, `mka_po`, `catatan_po`, `retur_pembelian_obat`, `mki_retur_pembelian`, `mka_retur_pembelian`, `catatan_retur_pembelian`, `retur_penjualan_obat`, `mki_retur_penjualan`, `mka_retur_penjualan`, `catatan_retur_penjualan`, `penjualan_obat`, `mki_penjualan`, `mka_penjualan`, `catatan_penjualan`, `pembayaran_piutang`, `mki_pembayaran_piutang`, `mka_pembayaran_piutang`, `catatan_pembayaran_piutang`, `catatan_promo`, `obat_expired`, `piutang_jatuh_tempo`, `hutang_jatuh_tempo`, `tuslah`, `embalase`, `ppn`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `no_penjualan_detail` int(10) UNSIGNED NOT NULL,
  `no_faktur` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jmlh` int(11) NOT NULL,
  `kd_satuan` int(11) NOT NULL,
  `harga` double(25,2) NOT NULL,
  `diskon` double(8,2) NOT NULL,
  `retur` set('true','false') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`no_penjualan_detail`, `no_faktur`, `kd_obat`, `jmlh`, `kd_satuan`, `harga`, `diskon`, `retur`, `created_at`, `updated_at`) VALUES
(1, 'FAK1801080001', 'OBT201712220002', 1, 31, 1000.00, 0.00, 'false', '2018-01-08 09:38:43', '2018-01-08 09:38:43'),
(2, 'FAK1801080001', 'OBT201801080004', 1, 33, 20000.00, 0.00, 'false', '2018-01-08 09:38:44', '2018-01-08 09:38:44'),
(3, 'FAK1801080002', 'OBT201712220002', 1, 31, 1000.00, 0.00, 'false', '2018-01-08 09:40:37', '2018-01-08 09:40:37'),
(4, 'FAK1801080002', 'OBT201712220002', 1, 31, 1000.00, 0.00, 'false', '2018-01-08 10:29:07', '2018-01-08 10:29:07');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_obat`
--

CREATE TABLE `penjualan_obat` (
  `no_faktur` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kasir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_resep` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_gudang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` double(8,2) NOT NULL,
  `diskon` double(8,2) NOT NULL,
  `pajak` double(8,2) NOT NULL,
  `tgl_faktur` date NOT NULL,
  `id_rm` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_dokter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `penjualan_obat`
--

INSERT INTO `penjualan_obat` (`no_faktur`, `kasir`, `no_resep`, `kd_gudang`, `total`, `diskon`, `pajak`, `tgl_faktur`, `id_rm`, `id_dokter`, `created_at`, `updated_at`) VALUES
('FAK1801080001', 'Admin', '123456', 'GUD1612140001', 21000.00, 0.00, 0.00, '2018-01-08', 'PAS-0018', 'DOK-0005', '2018-01-08 09:38:43', '2018-01-08 09:38:43'),
('FAK1801080002', 'Admin', '313', 'GUD1612140001', 1000.00, 0.00, 0.00, '2018-01-09', 'PAS-0019', 'DOK-0005', '2018-01-08 09:40:37', '2018-01-08 09:40:37'),
('FAK1801080002', 'Admin', '313', 'GUD1612140001', 1000.00, 0.00, 0.00, '2018-01-09', 'PAS-0019', 'DOK-0005', '2018-01-08 10:29:07', '2018-01-08 10:29:07');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_pembayaran`
--

CREATE TABLE `penjualan_pembayaran` (
  `id_penjualan_pembayaran` int(11) NOT NULL,
  `no_faktur` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pembayaran` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_kartu` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kartu_bank` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_faktur_pajak` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` double(8,2) NOT NULL,
  `biaya_pengiriman` double(8,2) NOT NULL,
  `diskon` double(8,2) NOT NULL,
  `pajak` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `piutang_penjualan`
--

CREATE TABLE `piutang_penjualan` (
  `id_piutang` int(11) NOT NULL,
  `no_faktur` varchar(191) NOT NULL,
  `total` int(11) NOT NULL,
  `kekurangan` int(11) DEFAULT '0',
  `jumlah_bayar` int(11) DEFAULT '0',
  `deadline` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `piutang_penjualan_detail`
--

CREATE TABLE `piutang_penjualan_detail` (
  `id_piutang_pembelian_detail` int(11) NOT NULL,
  `id_piutang` int(11) NOT NULL,
  `sisa_piutang` int(11) NOT NULL DEFAULT '0',
  `jenis_pembayaran` varchar(50) NOT NULL,
  `pembayaran_tunai` int(11) NOT NULL DEFAULT '0',
  `pembayaran_kartu` int(11) NOT NULL DEFAULT '0',
  `no_kartu` varchar(50) DEFAULT '0',
  `tunai` varchar(50) DEFAULT NULL,
  `kartu` varchar(50) DEFAULT NULL,
  `tanggal_pembayaran` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `pos_akun_keuangan`
--

CREATE TABLE `pos_akun_keuangan` (
  `kode_pos_akun` int(11) NOT NULL,
  `nama_pos_akun` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_kelompok_akun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pos_akun_keuangan`
--

INSERT INTO `pos_akun_keuangan` (`kode_pos_akun`, `nama_pos_akun`, `kode_kelompok_akun`) VALUES
(110, 'Kas', 100),
(111, 'Bank', 100),
(112, 'Piutang Usaha', 100),
(113, 'Piutan Non Usaha', 100),
(114, 'Piutang Pajak', 100),
(115, 'Persediaan', 100),
(116, 'Biaya Bayar Dimuka', 100),
(117, 'Investasi', 100),
(118, 'Aktiva Tetap', 100),
(119, 'Normalisasi', 100),
(120, 'Jaminan', 100),
(121, 'Aset Tidak Lancar', 100),
(210, 'Hutang Lancar', 200),
(211, 'Hutang Jangka Panjang', 200),
(212, 'Hutang Lain lain', 200),
(310, 'Modal', 300),
(311, 'Prive', 300),
(312, 'Laba', 300),
(313, 'Modal Asing', 300),
(410, 'Pendapatan Usaha', 400),
(411, 'Pendapatan Luar Biasa', 400),
(412, 'Pendapatan Lain Lain', 400),
(413, 'Pendapatan Usaha Lainnya', 400),
(510, 'Pendapatan Usaha', 500),
(511, 'Pendapatan Luar Usaha', 500),
(512, 'Pendapatan Lain Lain', 500),
(513, 'Pendapatan Usaha Lainnya', 500),
(610, 'Biaya Operasional', 600),
(611, 'Biaya Non Operasional', 600);

-- --------------------------------------------------------

--
-- Table structure for table `po_detail`
--

CREATE TABLE `po_detail` (
  `no_po_detail` int(10) UNSIGNED NOT NULL,
  `no_po` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jmlh` int(11) NOT NULL,
  `kd_satuan` int(11) NOT NULL,
  `harga` bigint(20) NOT NULL,
  `diskon` bigint(20) NOT NULL,
  `subtotal` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `po_detail`
--

INSERT INTO `po_detail` (`no_po_detail`, `no_po`, `kd_obat`, `jmlh`, `kd_satuan`, `harga`, `diskon`, `subtotal`, `created_at`, `updated_at`) VALUES
(1, 'PO1711290001', 'OBT1614120001', 10, 27, 22000, 10, 220000, '2017-11-29 05:33:40', '2017-11-29 05:33:40'),
(2, 'PO1712110006', 'OBT1614120001', 1, 27, 22000, 0, 22000, '2017-12-10 17:39:25', '2017-12-10 17:39:25'),
(3, 'PO1712110007', 'OBT1614120001', 1, 27, 22000, 0, 22000, '2017-12-10 18:06:48', '2017-12-10 18:06:48'),
(4, 'PO1712110008', 'OBT1614120001', 3, 27, 22000, 0, 66000, '2017-12-10 18:09:55', '2017-12-10 18:09:55'),
(5, 'PO1712170009', 'OBT1614120001', 1, 27, 22000, 0, 22000, '2017-12-17 16:38:03', '2017-12-17 16:38:03'),
(6, 'PO1712220010', 'OBT1614120001', 2, 27, 22000, 0, 44000, '2017-12-21 17:44:41', '2017-12-21 17:44:41'),
(7, 'PO1712220010', 'OBT201712220002', 2, 31, 1000, 0, 2000, '2017-12-21 17:44:41', '2017-12-21 17:44:41');

-- --------------------------------------------------------

--
-- Table structure for table `purchaes_order`
--

CREATE TABLE `purchaes_order` (
  `no_po` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_supplier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_obat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` bigint(20) NOT NULL,
  `jenis_invoice` enum('cetak','invoice') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cetak',
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_po` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchaes_order`
--

INSERT INTO `purchaes_order` (`no_po`, `kd_supplier`, `jenis_obat`, `total`, `jenis_invoice`, `keterangan`, `tgl_po`, `created_at`, `updated_at`) VALUES
('PO1711290001', 'SUP1612140006', 'Konsinyasi', 198000, 'cetak', 'lalala', '2017-11-29', '2017-11-29 05:33:39', '2017-11-29 05:33:39'),
('PO1712020002', 'SUP1612140002', 'Non-Konsinyasi', 22000, 'invoice', 'Tes Purchase Order', '2017-12-02', '2017-12-02 03:34:23', '2017-12-02 03:34:23'),
('PO1712040003', 'SUP1612140005', 'Konsinyasi', 22000, 'cetak', 'Gg', '2017-12-06', '2017-12-03 18:25:01', '2017-12-03 18:25:01'),
('PO1712040004', 'SUP1612140001', 'Konsinyasi', 22000, 'cetak', 'tepat waktu', '2017-03-02', '2017-12-04 01:29:34', '2017-12-04 01:29:34'),
('PO1712040005', 'SUP1612140002', 'Konsinyasi', 22000, 'invoice', 'fgbfb', '2017-12-04', '2017-12-04 09:34:49', '2017-12-04 09:34:49'),
('PO1712110006', 'SUP1612140002', 'Non-Konsinyasi', 22000, 'cetak', 'tidak untuk di utang', '2017-12-05', '2017-12-10 17:39:25', '2017-12-10 17:39:25'),
('PO1712110007', 'SUP1612140002', 'Konsinyasi', 22000, 'cetak', 'hhhh', '2017-12-07', '2017-12-10 18:06:48', '2017-12-10 18:06:48'),
('PO1712110008', 'SUP1612140002', 'Non-Konsinyasi', 66000, 'invoice', 'jjj', '2017-12-08', '2017-12-10 18:09:55', '2017-12-10 18:09:55'),
('PO1712170009', 'SUP1612140001', 'Non-Konsinyasi', 22000, 'invoice', 'hdzz', '2017-12-04', '2017-12-17 16:38:03', '2017-12-17 16:38:03'),
('PO1712220010', 'SUP1612140001', 'Non-Konsinyasi', 46000, 'cetak', 'ikan hiu', '2017-12-07', '2017-12-21 17:44:41', '2017-12-21 17:44:41');

-- --------------------------------------------------------

--
-- Table structure for table `racikan`
--

CREATE TABLE `racikan` (
  `kd_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_obat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `racikan`
--

INSERT INTO `racikan` (`kd_obat`, `nama_obat`, `kategori`, `status`, `created_at`, `updated_at`) VALUES
('OBT1614120003', 'bwa bwa', 'KAT-OB201711230001', 'active', '2017-11-27 10:00:00', '2018-01-07 22:25:12'),
('RAC201711300004', 'Tes Racikan 1', 'KAT-OB201711230002', 'active', '2017-11-30 08:02:57', '2017-11-30 08:02:57'),
('RAC201712050005', 'sakit jiwa', 'KAT-OB201711230001', 'active', '2017-12-04 17:50:54', '2017-12-04 17:50:54'),
('RAC201712050006', 'jglk;kl', 'KAT-OB201711230001', 'active', '2017-12-04 20:41:07', '2017-12-04 20:41:07'),
('RAC201712110007', 'racik 2', 'KAT-OB201711270003', 'active', '2017-12-10 17:37:34', '2017-12-10 17:37:34'),
('RAC201712220008', 'racikan 2', 'KAT-OB201711250001', 'active', '2017-12-21 17:42:05', '2017-12-21 17:42:05');

-- --------------------------------------------------------

--
-- Table structure for table `racikan_obat`
--

CREATE TABLE `racikan_obat` (
  `id` int(11) NOT NULL,
  `id_racikan` varchar(191) NOT NULL,
  `id_obat` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `racikan_obat`
--

INSERT INTO `racikan_obat` (`id`, `id_racikan`, `id_obat`, `created_at`, `updated_at`) VALUES
(4, 'OBT1614120003', 'OBT1614120001', '2018-01-07 22:25:12', '2018-01-07 22:25:12');

-- --------------------------------------------------------

--
-- Table structure for table `retur_pembelian_obat`
--

CREATE TABLE `retur_pembelian_obat` (
  `no_retur_pembelian` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_faktur` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pembayaran` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_supplier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `retur_pembelian_obat`
--

INSERT INTO `retur_pembelian_obat` (`no_retur_pembelian`, `no_faktur`, `jenis_pembayaran`, `kd_supplier`, `total`, `created_at`, `updated_at`) VALUES
('RET1711270001', '687676', 'TUNAI', 'SUP201711250001', 30000, '2017-11-27 07:36:58', '2017-11-27 07:36:58');

-- --------------------------------------------------------

--
-- Table structure for table `retur_penjualan_obat`
--

CREATE TABLE `retur_penjualan_obat` (
  `no_retur_penjualan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_faktur` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` bigint(20) NOT NULL,
  `tanggal_retur` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `retur_penjualan_obat`
--

INSERT INTO `retur_penjualan_obat` (`no_retur_penjualan`, `no_faktur`, `total`, `tanggal_retur`, `created_at`, `updated_at`) VALUES
('RET1712040001', 'FAK1712040001', 21560, '2017-12-05', '2017-12-04 10:14:36', '2017-12-04 10:14:36');

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `kd_satuan` int(10) UNSIGNED NOT NULL,
  `kd_obat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kd_sat_obat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_sat` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_lok_obat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `hna` bigint(20) DEFAULT NULL,
  `barcode_obat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_batch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga_jual_1` bigint(20) DEFAULT NULL,
  `harga_jual_2` bigint(20) DEFAULT NULL,
  `harga_jual_3` bigint(20) DEFAULT NULL,
  `diskon_harga_1` bigint(20) DEFAULT NULL,
  `diskon_harga_2` bigint(20) DEFAULT NULL,
  `diskon_harga_3` bigint(20) DEFAULT NULL,
  `tgl_expired` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`kd_satuan`, `kd_obat`, `kd_sat_obat`, `nama_sat`, `kd_lok_obat`, `stok`, `hna`, `barcode_obat`, `no_batch`, `harga_jual_1`, `harga_jual_2`, `harga_jual_3`, `diskon_harga_1`, `diskon_harga_2`, `diskon_harga_3`, `tgl_expired`, `created_at`, `updated_at`) VALUES
(31, 'OBT201712220002', 'SAT-OB201712050005', '', 'LOK-OB201711220001', 9, 1000, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, '2017-12-21 17:18:08', '2018-01-08 10:29:07'),
(33, 'OBT201801080004', 'SAT-OB201711220001', '', 'LOK-OB201711220001', 99, 20000, '2000192', NULL, 0, 20200, 20200, 0, 0, 0, '2018-01-31', '2018-01-07 22:40:07', '2018-01-08 09:38:44'),
(34, 'OBT201801080005', 'SAT-OB201711220001', '', 'LOK-OB201711250001', 100, 20000, '1029101', '103', 20200, 24200, 24400, 0, 0, 0, '2018-01-31', '2018-01-07 22:41:12', '2018-01-07 22:41:12'),
(36, 'OBT201801080006', 'SAT-OB201711220001', '', 'LOK-OB201711220001', 100, 20000, '200012', '103', 22000, 22000, 22000, 0, 0, 0, '2018-01-31', '2018-01-07 22:45:32', '2018-01-07 22:45:32');

-- --------------------------------------------------------

--
-- Table structure for table `satuan_obat`
--

CREATE TABLE `satuan_obat` (
  `kd_sat_obat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_sat_obat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `satuan_obat`
--

INSERT INTO `satuan_obat` (`kd_sat_obat`, `nama_sat_obat`, `status`, `created_at`, `updated_at`) VALUES
('SAT-OB201711220001', 'Botol', 'Aktif', '2017-11-21 23:35:00', '2017-11-29 22:10:43'),
('SAT-OB201711230002', 'cair', 'aktif', '2017-11-22 15:35:20', '2017-11-22 15:35:20'),
('SAT-OB201711230003', 'strips', 'aktif', '2017-11-22 15:35:30', '2017-11-22 15:35:30'),
('SAT-OB201711270001', 'Pcs', 'aktif', '2017-11-26 13:19:26', '2017-11-26 13:19:26'),
('SAT-OB201711270002', 'BOX', 'aktif', '2017-11-26 13:20:07', '2017-11-26 13:20:07'),
('SAT-OB201711300003', 'Tablet', 'aktif', '2017-11-29 22:11:01', '2017-11-29 22:11:01'),
('SAT-OB201712040004', 'Obat Penahan Lapar', 'aktif', '2017-12-04 09:10:27', '2017-12-04 09:10:27'),
('SAT-OB201712050005', 'tablet', 'aktif', '2017-12-04 17:49:55', '2017-12-04 17:49:55'),
('SAT-OB201712110006', 'puyer', 'aktif', '2017-12-10 17:33:04', '2017-12-10 17:33:04'),
('SAT-OB201712180007', 'PUSING', 'aktif', '2017-12-17 17:11:28', '2017-12-17 17:11:28');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kd_supplier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_supplier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_tlpn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_rek` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `npwp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kd_supplier`, `nama_supplier`, `alamat`, `kota`, `no_tlpn`, `no_hp`, `no_rek`, `npwp`, `status`, `created_at`, `updated_at`) VALUES
('SUP1612140001', 'CV Cininta (SEMARANG)', 'Jl Lampersari 57 Semarang', 'Semarang', '( 024 ) 8313757', NULL, NULL, NULL, 'active', NULL, NULL),
('SUP1612140002', 'PT Daya Sembada', 'jl Simpang Lima 1 Mal Ciputra 66 Lt UG', 'Semarang', '( 024 ) 8449568', NULL, NULL, NULL, 'active', NULL, NULL),
('SUP1612140003', 'CV Eka Jaya Sakti', 'Jl Berdikari Raya 1 ', 'Semarang', '( 024 ) 7471786', NULL, NULL, NULL, 'active', NULL, NULL),
('SUP1612140004', 'CV Karsa Mandiri', 'Jl Lampersari 12', 'Semarang', '( 024 ) 8415540', NULL, NULL, NULL, 'active', NULL, NULL),
('SUP1612140005', 'PT Dexa Medica', 'Jl Kelapa Gading Slt Kompl Gading Serpong Sktr 1-B Bl BJ-8/2', 'Tangerang', '( 021 ) 54200134', NULL, NULL, NULL, 'active', NULL, NULL),
('SUP1612140006', 'PT Interbat', 'Jl Imam Bonjol Kompl Ligamas Bl A-2/21', 'Tangerang', '( 021 ) 55768884', NULL, NULL, NULL, 'active', NULL, NULL),
('SUP1612140007', 'Obat Sehat dot Com', 'Jl Sutera Niaga I 11 Tangerang', 'Tangerang', '( 021 ) 5399911', NULL, NULL, NULL, 'active', NULL, NULL),
('SUP1612140008', 'PT Metro Pillars Group', 'Jl Cempaka Km 37 Bl B/6 Kompl Niaga Kalimas', 'Bekasi', '( 021 ) 88357528', NULL, NULL, NULL, 'active', NULL, NULL),
('SUP1612140009', 'PT Panca Kebacita', 'Kompl Taman Narogong Indah Bl TA/27', 'Bekasi', '( 021 ) 8211008', NULL, NULL, NULL, 'active', NULL, NULL),
('SUP1612140010', 'PT Sri Dharma Tunggal', 'Kompl Central Niaga Bl C-1/17 Bekasi', 'Bekasi', '( 021 ) 88850678', NULL, NULL, NULL, 'active', NULL, NULL),
('SUP201711250001', 'PT. SukaJaya', 'Jl Skaloa', 'Bandung', '891121211', '918728788', '12918918198', '12.1212.121-121.212.12', 'aktif', '2017-11-24 10:58:09', '2017-11-24 10:58:09'),
('SUP201712040002', 'PT.RJs', 'Tamalate', 'Makassar', '0142299381', '082213143445', '0980989183383710', '231311413412', 'aktif', '2017-12-04 09:03:27', '2017-12-04 09:03:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_grup` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `alamat`, `keterangan`, `foto`, `kode_grup`, `remember_token`, `created_at`, `updated_at`, `username`, `status`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$zphti6XcdKQ4LAL.A1j4YuoJkIorEn5RIwrjxhIpRijQrZCAvx0n.', NULL, NULL, NULL, NULL, NULL, '2017-11-27 19:27:54', '2017-11-27 19:27:54', NULL, 'active'),
(2, 'Admin', 'admin@apotik.com', '$2y$10$3tJSlOG5qJIvdpYReMCpq.yK5qfQu.m5IjSfaJdOF27mfnYMQGmgm', NULL, NULL, NULL, NULL, 'vYLUWyuuO9Xy7H9DPhSGnWyy0zy2YABtAQ4ZBLNxbN3taANiXy0BSOhDHfNN', '2017-11-28 16:30:54', '2017-11-28 16:30:54', NULL, 'active'),
(3, 'Admin Shift 1', 'shift1@apotek.com', '$2y$10$X.3dgTSgv8Kg/kCOcaGvDeRfpOZRlZc3xRcLqKe.0Qj4eD7NfeXEW', NULL, NULL, NULL, NULL, 'zQK2eknQFuZ3LOkPV27JTWWn8Q9k25n2xfgbxlkjY3lmLGZSaHGv5bSN0wrM', '2017-11-30 18:19:51', '2017-11-30 18:19:51', NULL, 'active'),
(4, 'Anwar', 'rjs.2045@gmail.com', '$2y$10$rwhmBGu63yImSh7SXDvOjumBQqczGKZ7LGFJy5IJY5sdGUd54IzCq', NULL, NULL, NULL, NULL, NULL, '2017-12-03 18:22:22', '2017-12-03 18:22:22', NULL, 'active'),
(5, 'Example', 'admin1@example.com', '$2y$10$MuKnLJWWviIvPXyA6HmRXOAgzlSbvsHnB.Gvz59vjxKB5ZIcnfgqG', NULL, NULL, NULL, NULL, 'tb77jEFK1WLK6PXUNrtxRQKavdAIZOXcY79AH63uGCcbPQxC8xRjRXS63qqs', '2017-12-04 08:40:49', '2017-12-04 08:40:49', NULL, 'active'),
(6, 'resa', 'resa@gmail.com', '$2y$10$Dmhe3CQydDidbauvaAwUauiege9IKjtuvoU5manzh9/pMd/qW0lT.', NULL, NULL, NULL, NULL, NULL, '2018-01-05 00:11:04', '2018-01-05 00:11:04', NULL, 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akses_menu`
--
ALTER TABLE `akses_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `akun_keuangan`
--
ALTER TABLE `akun_keuangan`
  ADD PRIMARY KEY (`kode_akun`);

--
-- Indexes for table `apoteker`
--
ALTER TABLE `apoteker`
  ADD PRIMARY KEY (`id_apoteker`);

--
-- Indexes for table `cetak_barcode`
--
ALTER TABLE `cetak_barcode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daftar_shift`
--
ALTER TABLE `daftar_shift`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_daftar_shift_users` (`created_by`);

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id_dokter`);

--
-- Indexes for table `golongan_obat`
--
ALTER TABLE `golongan_obat`
  ADD PRIMARY KEY (`kd_gol_obat`);

--
-- Indexes for table `grup_user`
--
ALTER TABLE `grup_user`
  ADD PRIMARY KEY (`kode_grup`);

--
-- Indexes for table `grup_user_menu`
--
ALTER TABLE `grup_user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`kd_gudang`);

--
-- Indexes for table `history_harga_obat`
--
ALTER TABLE `history_harga_obat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hutang_pembelian`
--
ALTER TABLE `hutang_pembelian`
  ADD PRIMARY KEY (`id_hutang`);

--
-- Indexes for table `hutang_pembelian_detail`
--
ALTER TABLE `hutang_pembelian_detail`
  ADD PRIMARY KEY (`id_hutang_pembelian_detail`);

--
-- Indexes for table `identitas_apotek`
--
ALTER TABLE `identitas_apotek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_shift`
--
ALTER TABLE `jadwal_shift`
  ADD PRIMARY KEY (`id_js`);

--
-- Indexes for table `jurnal_keuangan`
--
ALTER TABLE `jurnal_keuangan`
  ADD PRIMARY KEY (`kd_jurnal`),
  ADD KEY `FK__jurnal_singkat` (`kd_jurnal_singkat`),
  ADD KEY `FK_jurnal_keuangan_users` (`created_by`);

--
-- Indexes for table `jurnal_singkat`
--
ALTER TABLE `jurnal_singkat`
  ADD PRIMARY KEY (`id_jurnal`);

--
-- Indexes for table `kategori_obat`
--
ALTER TABLE `kategori_obat`
  ADD PRIMARY KEY (`kd_kat_obat`);

--
-- Indexes for table `kategori_pasien`
--
ALTER TABLE `kategori_pasien`
  ADD PRIMARY KEY (`kd_kat_pasien`),
  ADD KEY `FK_kategori_pasien_users` (`created_by`);

--
-- Indexes for table `kelompok_akun_keuangan`
--
ALTER TABLE `kelompok_akun_keuangan`
  ADD PRIMARY KEY (`kode_kelompok_akun`);

--
-- Indexes for table `konsinyasi_pembelian`
--
ALTER TABLE `konsinyasi_pembelian`
  ADD PRIMARY KEY (`id_konsinyasi`);

--
-- Indexes for table `konsinyasi_pembelian_detail`
--
ALTER TABLE `konsinyasi_pembelian_detail`
  ADD PRIMARY KEY (`id_konsinyasi_detail`);

--
-- Indexes for table `lokasi_obat`
--
ALTER TABLE `lokasi_obat`
  ADD PRIMARY KEY (`kd_lok_obat`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mutasi_konsinyasi_obat`
--
ALTER TABLE `mutasi_konsinyasi_obat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mutasi_obats`
--
ALTER TABLE `mutasi_obats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mutasi_obats_kd_obat_foreign` (`kd_obat`);

--
-- Indexes for table `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`kd_obat`),
  ADD KEY `obat_kd_pabrik_foreign` (`kd_pabrik`),
  ADD KEY `obat_kd_gol_obat_foreign` (`kd_gol_obat`),
  ADD KEY `obat_kd_kat_obat_foreign` (`kd_kat_obat`);

--
-- Indexes for table `opname`
--
ALTER TABLE `opname`
  ADD PRIMARY KEY (`id_opname`);

--
-- Indexes for table `pabrik`
--
ALTER TABLE `pabrik`
  ADD PRIMARY KEY (`kd_pabrik`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id_rm`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pengaturan_aplikasi`
--
ALTER TABLE `pengaturan_aplikasi`
  ADD PRIMARY KEY (`id_pengaturan`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`no_penjualan_detail`);

--
-- Indexes for table `penjualan_pembayaran`
--
ALTER TABLE `penjualan_pembayaran`
  ADD PRIMARY KEY (`id_penjualan_pembayaran`);

--
-- Indexes for table `piutang_penjualan`
--
ALTER TABLE `piutang_penjualan`
  ADD PRIMARY KEY (`id_piutang`);

--
-- Indexes for table `piutang_penjualan_detail`
--
ALTER TABLE `piutang_penjualan_detail`
  ADD PRIMARY KEY (`id_piutang_pembelian_detail`);

--
-- Indexes for table `pos_akun_keuangan`
--
ALTER TABLE `pos_akun_keuangan`
  ADD PRIMARY KEY (`kode_pos_akun`);

--
-- Indexes for table `po_detail`
--
ALTER TABLE `po_detail`
  ADD PRIMARY KEY (`no_po_detail`);

--
-- Indexes for table `purchaes_order`
--
ALTER TABLE `purchaes_order`
  ADD PRIMARY KEY (`no_po`);

--
-- Indexes for table `racikan`
--
ALTER TABLE `racikan`
  ADD PRIMARY KEY (`kd_obat`);

--
-- Indexes for table `racikan_obat`
--
ALTER TABLE `racikan_obat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`kd_satuan`),
  ADD KEY `satuan_kd_lok_obat_foreign` (`kd_lok_obat`),
  ADD KEY `satuan_kd_obat_foreign` (`kd_obat`),
  ADD KEY `satuan_kd_sat_obat_foreign` (`kd_sat_obat`);

--
-- Indexes for table `satuan_obat`
--
ALTER TABLE `satuan_obat`
  ADD PRIMARY KEY (`kd_sat_obat`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kd_supplier`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akses_menu`
--
ALTER TABLE `akses_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cetak_barcode`
--
ALTER TABLE `cetak_barcode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `daftar_shift`
--
ALTER TABLE `daftar_shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grup_user_menu`
--
ALTER TABLE `grup_user_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `history_harga_obat`
--
ALTER TABLE `history_harga_obat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `hutang_pembelian`
--
ALTER TABLE `hutang_pembelian`
  MODIFY `id_hutang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hutang_pembelian_detail`
--
ALTER TABLE `hutang_pembelian_detail`
  MODIFY `id_hutang_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `identitas_apotek`
--
ALTER TABLE `identitas_apotek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jadwal_shift`
--
ALTER TABLE `jadwal_shift`
  MODIFY `id_js` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jurnal_keuangan`
--
ALTER TABLE `jurnal_keuangan`
  MODIFY `kd_jurnal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jurnal_singkat`
--
ALTER TABLE `jurnal_singkat`
  MODIFY `id_jurnal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kategori_pasien`
--
ALTER TABLE `kategori_pasien`
  MODIFY `kd_kat_pasien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `konsinyasi_pembelian`
--
ALTER TABLE `konsinyasi_pembelian`
  MODIFY `id_konsinyasi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `konsinyasi_pembelian_detail`
--
ALTER TABLE `konsinyasi_pembelian_detail`
  MODIFY `id_konsinyasi_detail` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `mutasi_konsinyasi_obat`
--
ALTER TABLE `mutasi_konsinyasi_obat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `mutasi_obats`
--
ALTER TABLE `mutasi_obats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `opname`
--
ALTER TABLE `opname`
  MODIFY `id_opname` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;

--
-- AUTO_INCREMENT for table `pengaturan_aplikasi`
--
ALTER TABLE `pengaturan_aplikasi`
  MODIFY `id_pengaturan` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `penjualan_pembayaran`
--
ALTER TABLE `penjualan_pembayaran`
  MODIFY `id_penjualan_pembayaran` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `piutang_penjualan`
--
ALTER TABLE `piutang_penjualan`
  MODIFY `id_piutang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `piutang_penjualan_detail`
--
ALTER TABLE `piutang_penjualan_detail`
  MODIFY `id_piutang_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `po_detail`
--
ALTER TABLE `po_detail`
  MODIFY `no_po_detail` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `racikan_obat`
--
ALTER TABLE `racikan_obat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `kd_satuan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `daftar_shift`
--
ALTER TABLE `daftar_shift`
  ADD CONSTRAINT `FK_daftar_shift_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jurnal_keuangan`
--
ALTER TABLE `jurnal_keuangan`
  ADD CONSTRAINT `FK__jurnal_singkat` FOREIGN KEY (`kd_jurnal_singkat`) REFERENCES `jurnal_singkat` (`id_jurnal`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_jurnal_keuangan_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kategori_pasien`
--
ALTER TABLE `kategori_pasien`
  ADD CONSTRAINT `FK_kategori_pasien_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mutasi_obats`
--
ALTER TABLE `mutasi_obats`
  ADD CONSTRAINT `mutasi_obats_kd_obat_foreign` FOREIGN KEY (`kd_obat`) REFERENCES `obat` (`kd_obat`);

--
-- Constraints for table `obat`
--
ALTER TABLE `obat`
  ADD CONSTRAINT `obat_kd_gol_obat_foreign` FOREIGN KEY (`kd_gol_obat`) REFERENCES `golongan_obat` (`kd_gol_obat`),
  ADD CONSTRAINT `obat_kd_kat_obat_foreign` FOREIGN KEY (`kd_kat_obat`) REFERENCES `kategori_obat` (`kd_kat_obat`),
  ADD CONSTRAINT `obat_kd_pabrik_foreign` FOREIGN KEY (`kd_pabrik`) REFERENCES `pabrik` (`kd_pabrik`);

--
-- Constraints for table `satuan`
--
ALTER TABLE `satuan`
  ADD CONSTRAINT `satuan_kd_lok_obat_foreign` FOREIGN KEY (`kd_lok_obat`) REFERENCES `lokasi_obat` (`kd_lok_obat`),
  ADD CONSTRAINT `satuan_kd_obat_foreign` FOREIGN KEY (`kd_obat`) REFERENCES `obat` (`kd_obat`),
  ADD CONSTRAINT `satuan_kd_sat_obat_foreign` FOREIGN KEY (`kd_sat_obat`) REFERENCES `satuan_obat` (`kd_sat_obat`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
