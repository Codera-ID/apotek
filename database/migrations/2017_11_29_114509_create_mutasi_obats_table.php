<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMutasiObatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mutasi_obats', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('no_bukti', 100);
            $table->string('kd_obat', 191);
            $table->text('keterangan');
            $table->integer('masuk');
            $table->integer('keluar');
            $table->integer('stock');

            $table->timestamps();
        });

        Schema::table('mutasi_obats', function (Blueprint $table) {
            $table->foreign('kd_obat')->references('kd_obat')->on('obat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mutasi_obats');
    }
}
